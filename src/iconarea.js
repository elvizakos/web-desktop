(function () {

	/**
	 * Constructor for creating iconarea objects.
	 * @constructor
	 * @param {HTMLElement} e - Element to attach icon area to.
	 */
	var IconareaObject = function ( e ) {
		IconareaObject.list.push( this );

		/**
		 * Type of object.
		 * @public
		 * @type {'iconarea'}
		 */
		this.type = 'iconarea';

		/**
		 * Associative array for storing icon area properties values.
		 * @public
		 * @type {Object}
		 */
		this.props = {};

		/**
		 * The id of container element.
		 * @public
		 * @type {false|string}
		 */
		this.props.elementId = false;

		/**
		 * CSS class name for theme in use.
		 * @type {null|string}
		 * @public
		 */
		this.props.themeClass = null;

		/**
		 * Defines if icon area can take focus.
		 * @public
		 * @type {boolean}
		 */
		this.props.canHaveFocus = true;

		/**
		 * Array with all selected icons.
		 * @public
		 * @type {Array}
		 */
		this.props.selectedIcons = [];

		/**
		 * Sets if clicking on the only selected icon should show label edit elements.
		 * @public
		 * @type {boolean}
		 */
		this.props.clickAgainForEditIconLabel = false;

		/**
		 * Contains class names strings of icon area object elements.
		 * @public
		 * @type {Object}
		 */
		this.props.classNames = {};

		/**
		 * Class name of icon area container element.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.container = 'web-desktop-iconarea';

		/**
		 * Class name for selection area.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.selectionArea = 'web-desktop-selectionarea';

		/**
		 * Class name for icon container element.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.icon = 'web-desktop-icon';

		/**
		 * Class name for selected icon element.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.selected = 'selected';

		/**
		 * The desktop object that this iconarea belongs to.
		 * @type {DekstopObject|null}
		 * @public
		 */
		this.desktop = null;

		/**
		 * Array for storing addicon event functions.
		 * @public
		 * @type {Array}
		 */
		this.onaddicon = [];

		/**
		 * Array for storing removeicon event functions.
		 * @public
		 * @type {Array}
		 */
		this.onremoveicon = [];

		/**
		 * Array for storing rightclick event fucntions.
		 * @public
		 * @type {Array}
		 */
		this.onrightclick = [];

		/**
		 * Array for storing iconrightclick event functions.
		 * @public
		 * @type {Array}
		 */
		this.oniconrightclick = [];

		/**
		 * Array for storing icondrag event functions.
		 * @public
		 * @type {Array}
		 */
		this.onicondrag = [];

		/**
		 * Array for storing icondrop event functions.
		 * @public
		 * @type {Array}
		 */
		this.onicondrop = [];

		/**
		 * Array for storing attachto event functions.
		 * @public
		 * @type {Array}
		 */
		this.onattachto = [];

		/**
		 * Array for storing "iconlongtap" event functions.
		 * @public
		 * @type {Array}
		 */
		this.oniconlongtap = [];

		/**
		 * Method for adding events to iconarea.
		 *
		 * @param {string} e - Event name.
		 * @param {function} f - Function to run on event.
		 * @return {mixed} False on error, current iconarea object otherwise.
		 */
		this.addEvent = window.lvzwebdesktop.fn.addEvent;

		/**
		 * Method for removing events from iconarea.
		 *
		 * @param {string} e - The name of the event to remove.
		 * @param {function} [f] - The function to remove. If this param is set then method will remove only the function that matches. Otherwise it will clear all functions from event.
		 * @return {mixed} False on error, current iconarea object otherwise.
		 */
		this.removeEvent = window.lvzwebdesktop.fn.removeEvent;

		/**
		 * Method for firing events.
		 *
		 * @param {string} e - The name of the event to fire.
		 * @param {object} obj - An object with properties to add to event object.
		 * @return {mixed} False on error, current iconarea object otherwise.
		 */
		this.fireEvent = window.lvzwebdesktop.fn.fireEvent;

		/**
		 * Take focus from currently focused object.
		 * @return {IconareaObject} Current iconarea object.
		 */
		this.focus = function () {
			if ( this.desktop !== null ) this.desktop.giveFocus(this);

			var o = this.elements.container.parentNode;
			while ( o && o.nodeName != '#document' ) {
				if ( typeof o.dataWindowObject == 'object' ) {
					o.dataWindowObject.focus ();
				}
				o = o.parentNode;
			}
			return this;
		};

		/**
		 * Method for setting iconarea's  theme.
		 * @param {string} theme - Theme name.
		 * @return {IconObject} Current iconarea object.
		 */
		this.setTheme = function ( theme ) {
			this.elements.container.classList.add ( theme );
			return this;
		};

		/**
		 * Method for adding icons to icon area.
		 * @public
		 * @param {IconObject} iconObj - The icon object.
		 * @return {false|IconareaObject} False on error, current icon area object otherwise.
		 */
		this.addIcon = function ( iconObj ) {

			if ( ! iconObj instanceof IconObject ) {
				console.error ( "Argument must be an IconObject." );
				return false;
			}

			iconObj.iconarea = this;

			this.elements.container.appendChild ( iconObj.elements.container );

			this.fireEvent ( 'addicon', {
				target: this,
				icon: iconObj,
				iconContainerElement: iconObj.elements.container
			});

			return this;
		};

		/**
		 * Method for removing icons from icon area.
		 * @public
		 * @param {IconObject} iconObj - The icon object.
		 * @return {false|IconareaObject} False on error. Current icon area object otherwise.
		 */
		this.removeIcon = function ( iconObj ) {
			if ( ! iconObj instanceof IconObject ) {
				console.error ( "Argument must be an IconObject." );
				return false;
			}

			if ( iconObj.elements.container.parentNode !== this.elements.container ) {
				console.error ( "Argument must be an icon belonging to this icon area." );
				return false;
			}

			iconObj.iconarea = null;
			iconObj.remove();

			iconObj.iconarea = false;

			this.fireEvent ( 'removeicon', {
				target: this,
				icon: iconObj,
				iconContainerElement: iconObj.elements.container
			});

			return this
		};

		/**
		 * Method for removing all iconobjects from iconarea.
		 * @return {IconareaObject} Current iconarea object.
		 */
		this.removeAllIcons = function () {
			for ( var i = this.elements.container.children.length-1; i >= 0; i-- )
				if ( this.elements.container.children[i].dataIconObject &&
					 this.elements.container.children[i].dataIconObject instanceof IconObject &&
					 this.elements.container.children[i].dataIconObject.iconarea == this
				   ) this.removeIcon ( this.elements.container.children[i].dataIconObject );
			return this;
		};

		/**
		 * Method for removing all iconobjects from iconarea and unsetting them.
		 * @return {IconareaObject} Current iconarea object.
		 */
		this.killAllIcons = function () {
			for ( var i = this.elements.container.children.length-1; i >= 0; i-- )
				if ( this.elements.container.children[i].dataIconObject &&
					 this.elements.container.children[i].dataIconObject instanceof IconObject &&
					 this.elements.container.children[i].dataIconObject.iconarea == this
				   ) {
					this.elements.container.children[i].dataIconObject
						.remove()
						.kill();
				}
			return this;
		};

		/**
		 * Returns all icon objects belogs to this iconarea.
		 * @public
		 * @return {Array} An array witl all icon objects of this iconarea.
		 */
		this.getAllIcons = function () {
			var ret = [];
			for ( var i = 0; i < this.elements.container.children.length; i ++ )
				if ( this.elements.container.children[i].dataIconObject instanceof IconObject )
					ret.push ( this.elements.container.children[i].dataIconObject );
			return ret;
		};

		/**
		 * Returns an array with the icons between two points in iconarea.
		 * @public
		 * @param {number} x1 - The x position of first point.
		 * @param {number} y1 - The y position of first point.
		 * @param {number} x2 - The x position of second point.
		 * @param {number} y2 - The y position of second point.
		 * @return {Array} An array with all icons of iconarea that are between the two given points.
		 */
		this.getIconsInArea = function ( x1, y1, x2, y2 ) {

			function isBetween(el,x1,x2,y1,y2){
				var o={top:el.offsetTop,left:el.offsetLeft},th=o.top+el.offsetHeight,lw=o.left+el.offsetWidth;
				return((o.top>=y1&&o.top<y2)||(o.top<y1&&th>=y1))&&((o.left>=x1&&o.left<x2)||(o.left<x1&&lw>=x1));
			}

			var ret = [];


			for ( var i = 0; i < this.elements.container.children.length; i ++ ) if ( this.elements.container.children[i].dataIconObject instanceof IconObject && isBetween(this.elements.container.children[i], x1,x2,y1,y2) )
				ret.push ( this.elements.container.children[i].dataIconObject );

			return ret;
		};

		/**
		 * Unselects all selected icons in iconarea.
		 * @public
		 * @return {IconareaObject} Current iconarea object.
		 */
		this.unSelectAll = function ( skipIcons ) {
			if ( typeof skipIcons == 'undefined' ) skipIcons = [];
			for ( var i = this.props.selectedIcons.length-1; i >= 0 ; i -- )
				if (
					skipIcons.indexOf ( this.props.selectedIcons[i].dataIconObject ) == -1 )
				this.props.selectedIcons[i].dataIconObject.unselect();
			return this;
		};

		/**
		 * Run all selected icons.
		 * @public
		 * @return {IconareaObject} Current iconarea object.
		 */
		this.openSelected = function () {
			for ( var i = 0; i < this.props.selectedIcons.length; i ++ )
				this.props.selectedIcons[i].dataIconObject.open();
			return this;
		};

		/**
		 * Puts the icons in order.
		 * @public
		 * @param {xmargin: number|false, ymargin: number|false, maxWidth: number|false, maxHeight: number|false } [props] - Object with properties for ordering the icons.
		 * @return {IconareaObject} Current iconarea object.
		 */
		this.arrangeIcons = function ( props ) {
			var icon = this.elements.container.firstElementChild;
			var x = 0;
			var y = 0;
			var xmargin = 0;
			var ymargin = 0;
			var maxWidth = false;
			var maxHeight = false;
			var iconsWidth = 0;
			var iconsHeight = 0;
			if ( props ) {
				if ( props.xmargin ) xmargin = props.xmargin;
				if ( props.ymargin ) ymargin = props.ymargin;
				if ( props.maxWidth ) maxWidth = props.maxWidth;
				if ( props.maxHeight ) maxHeight = props.maxHeight;
			}
			while ( icon !== null ) {
				if ( ! icon.dataIconObject ) {
					icon = icon.nextElementSibling;
					continue;
				}
				if ( maxWidth === false )
					iconsWidth = icon.offsetWidth;
				else if ( typeof maxWidth == 'number' )
					iconsWidth = maxWidth;

				if ( maxHeight === false )
					iconsHeight = icon.offsetHeight;
				else if ( typeof maxHeight == 'number' )
					iconsHeight = maxHeight;

				if ( x + iconsWidth + xmargin > this.elements.container.offsetWidth ) {
					x = 0;
					y += (iconsHeight + ymargin);
				}

				icon.dataIconObject.setPosition (y,x);

				x += (iconsWidth + xmargin);
				icon = icon.nextElementSibling;
			}

			return this;
		};

		/**
		 * Sort icons by an icon property.
		 * @public
		 * @param {string} [sortBy] - The name of the property to sort the icons by it.
		 * @param {boolean} [rev] - If this is true then the icons will be sorted in reverse order.
		 * @return {IconareaObject} Current iconarea object.
		 */
		this.sortIcons = function ( sortBy, rev ) {
			var sorted = [];
			var icon = this.elements.container.firstElementChild;
			while ( icon !== null ) {
				if ( icon.dataIconObject ) sorted.push( icon.dataIconObject );
				icon = icon.nextElementSibling;
			}

			sorted.sort ( function (a,b) {
				switch ( sortBy ) {
				case 'label':
				case 'name':
					return a.props.label.localeCompare(b.props.label);
					break;
				}
			});

			if ( rev === true ) sorted.reverse();

			for (var i = 0 ; i < sorted.length; i ++ )
				this.elements.container.appendChild( sorted[i].elements.container );

			return this.arrangeIcons();

		};

		/**
		 * Method for getting first icon at the left side of given icon.
		 * @param {IconObject} icon - The icon to begin with.
		 * @return {IconObject|null} The first found icon object at the left of the given icon.
		 */
		this.getFirstIconLeftOf = function ( icon ) {
			var i = this.elements.container.firstElementChild;
			var iconl = icon.props.left;
			var icont = icon.props.top;
			var tmpmx = 0;
			var tmpmy = 0;
			var minx = false;
			var miny = false;
			var mini = null;
			var ii = null;
			while ( i !== null ) {
				ii = i.dataIconObject;
				if ( i.dataIconObject ) {
					tmpmx = iconl - ii.props.left;
					tmpmy = icont - ii.props.top;
					if ( ii !== icon &&
						 ii.props.left < iconl &&
						 ( minx === false || (
							 tmpmx < minx
						 ) )
					   ) {

						minx = tmpmx;
						miny = tmpmy;
						mini = ii;

					}
				}
				i = i.nextElementSibling;
			}
			return mini;
		};

		/**
		 * Method for getting first icon at the right side of given icon.
		 * @param {IconObject} icon - The icon to begin with.
		 * @return {IconObject|null} The first found icon object at the right of the given icon.
		 */
		this.getFirstIconRightOf = function ( icon ) {
			var i = this.elements.container.firstElementChild;
			var iconl = icon.props.left;
			var icont = icon.props.top;
			var tmpmx = 0;
			var tmpmy = 0;
			var maxx = false;
			var maxy = false;
			var maxi = null;
			var ii = null;
			while ( i !== null ) {
				ii = i.dataIconObject;
				if ( i.dataIconObject ) {
					tmpmx = ii.props.left - iconl;
					tmpmy = ii.props.top - icont;
					if ( ii !== icon &&
						 ii.props.left > iconl &&
						 ( maxx === false || (
							 tmpmx < maxx
						 ) )
					   ) {

						maxx = tmpmx;
						maxy = tmpmy;
						maxi = ii;

					}
				}
				i = i.nextElementSibling;
			}
			return maxi;
		};

		/**
		 * Method for getting first icon at the upper side of given icon.
		 * @param {IconObject} icon - The icon to begin with.
		 * @return {IconObject|null} The first found icon object at the upper side of the given icon.
		 */
		this.getFirstIconUpOf = function ( icon ) {
			var i = this.elements.container.firstElementChild;
			var iconl = icon.props.left;
			var icont = icon.props.top;
			var tmpmx = 0;
			var tmpmy = 0;
			var minx = false;
			var miny = false;
			var mini = null;
			var ii = null;
			while ( i !== null ) {
				ii = i.dataIconObject;
				if ( i.dataIconObject ) {
					tmpmx = iconl - ii.props.left;
					tmpmy = icont - ii.props.top;
					if ( ii !== icon &&
						 ii.props.top < icont &&
						 ( miny === false || (
							 tmpmy < miny
						 ) )
					   ) {

						minx = tmpmx;
						minx = tmpmy;
						mini = ii;

					}
				}
				i = i.nextElementSibling;
			}
			return mini;
		};

		/**
		 * Method for getting first icon at the down side of given icon.
		 * @param {IconObject} icon - The icon to begin with.
		 * @return {IconObject|null} The first found icon object at the down side of the given icon.
		 */
		this.getFirstIconDownOf = function ( icon ) {
			var i = this.elements.container.firstElementChild;
			var iconl = icon.props.left;
			var icont = icon.props.top;
			var tmpmx = 0;
			var tmpmy = 0;
			var maxx = false;
			var maxy = false;
			var maxi = null;
			var ii = null;
			while ( i !== null ) {
				ii = i.dataIconObject;
				if ( i.dataIconObject ) {
					tmpmx = ii.props.left - iconl;
					tmpmy = ii.props.top - icont;
					if ( ii !== icon &&
						 ii.props.top > icont &&
						 ( maxy === false || (
							 tmpmy < maxy
						 ) )
					   ) {

						maxx = tmpmx;
						maxx = tmpmy;
						maxi = ii;

					}
				}
				i = i.nextElementSibling;
			}
			return maxi;
		};

		/**
		 * Method for attaching this object to another lvzwebdesktop object or an element. Fires "attachto" event.
		 * @param {HTMLElement|object} item - The lvzwebdekstop object or an element to attach this object to.
		 * @return {false|IconareaObject} false on error, otherwise current iconarea object.
		 */
		this.attachTo = function ( item ) {
			if ( item instanceof HTMLElement ) {
				if ( typeof item.dataLVZWDESKTOPOBJ == 'object' ) {
					if ( typeof item.dataLVZWDESKTOPOBJ.attachItem == 'function' ) {
						if ( item.dataLVZWDESKTOPOBJ.type == 'desktop' ) this.desktop = item.dataLVZWDESKTOPOBJ;
						item.dataLVZWDESKTOPOBJ.attachItem(this.elements.container);
					}
					else {
						console.error ( "Argument must be an element or an lvzwebdesktop object having \"attachItem\" method." );
						return false;
					}
				}
				else {
					var eobj = {
						"target" : this,
						"attachElement" : item,
						"item" : item
					};
					item.appendChild ( this.elements.container );
					this.fireEvent('attachto', eobj);
				}
			}
			else {
				var c = window.lvzwebdesktop.isPartOf( item );
				if ( c === false || typeof c.attachItem != 'function' ) {
					console.error ( "Argument must be an element or an lvzwebdesktop object having \"attachItem\" method." );
					return false;
				}
				if ( c.type == 'desktop' ) this.desktop = c;
				c.attachItem ( this );
			}
			return this;
		};

		/**
		 * Returns the first parent object of given type.
		 *
		 * @param {function|string} type - lvzwebobject constructor or constructor's name.
		 * @return {object|null|false} Return's the first parent lvzwebobject that matches "type" parameter. if not found returns null. on error returns false.
		 */
		this.getParentOfType = function ( type ) { return window.lvzwebdesktop(this.elements.container,'').getParentObject( type ); };

		/**
		 * Associative array for storing iconarea's elements.
		 * @public
		 * @type {Object}
		 */
		this.elements = {};

		/**
		 * Container element.
		 * @public
		 * @type {HTMLElement}
		 */
		this.elements.container = null;

		if ( e ) {
			this.elements.container = e;
			this.props.elementId = !e.id ? false : e.id;
		} else {
			this.elements.container = document.createElement('div');
		}

		this.elements.container.dataIconareaObject = this;
		this.elements.container.dataLVZWDESKTOPOBJ = this;

		this.elements.container.classList.add( this.props.classNames.container );

		/**
		 * Selection area element.
		 * @public
		 * @type {HTMLDivElement}
		 */
		this.elements.selectionArea = document.createElement('div');
		this.elements.selectionArea.classList.add ( this.props.classNames.selectionArea );
		this.elements.selectionArea.dataIconareaObject = this;
		this.elements.selectionArea.dataLVZWDESKTOPOBJ = this;
		this.elements.container.appendChild ( this.elements.selectionArea );

		this.elements.selectedIcons = document.createElement('div');

		var attr;

		if ( e ) {

			attr =  e.getAttribute('onaddicon');
			if ( attr !== null )this.addEvent ( 'addicon', eval ( "var a =  function ( Event ) { var event = Event;" + attr + "}; a" ) );

			attr =  e.getAttribute('onremoveicon');
			if ( attr !== null )this.addEvent ( 'removeicon', new Function ('event',attr) );

			attr =  e.getAttribute('onrightclick');
			if ( attr !== null ) this.addEvent ( 'rightclick', new Function ('event',attr) );

			attr =  e.getAttribute('oniconrightclick');
			if ( attr !== null ) this.addEvent ( 'iconrightclick', new Function ('event',attr) );

			attr =  e.getAttribute('onicondrag');
			if ( attr !== null ) this.addEvent ( 'icondrag', new Function ('event',attr) );

			attr =  e.getAttribute('onicondrop');
			if ( attr !== null )this.addEvent ( 'icondrop', new Function ('event',attr) );

			attr =  e.getAttribute('onattachto');
			if ( attr !== null )this.addEvent ( 'attachto', new Function ('event',attr) );

			attr = e.getAttribute('oniconlongtap');
			if ( attr !== null ) this.addEvent ( 'iconlongtap', new Function ( 'event', attr ) );

		}

		attr = getComputedStyle(document.body).getPropertyValue('--lvzwebdesktop-default-icon-theme').trim();
		if ( attr != '' ) {
			for ( var i = 0 ; i < this.elements.container.classList.length ; i ++ ) {
				if ( getComputedStyle(document.body).getPropertyValue('--lvzwebdesktop-theme-icon-'+this.elements.container.classList[i]).trim() == 'true' ) {
					this.props.themeClass = this.elements.container.classList[i];
					break;
				}
			}
			if ( this.props.themeClass === null ) {
				this.props.themeClass = attr;
				this.elements.container.classList.add(attr);
			}
		}
		else {
			console.error ("There is no theme loaded");
		}

		this.addEvent ('attachto', function ( e ) {
			var self = e.target;
			var ato = e.attachTo;

			if ( self.desktop && self.desktop !== null ) {
				self.desktop.removeEvent('mousedown', iconareaMDOWN);
				self.desktop.removeEvent('keypress', iconareaKeypress);
				self.desktop.removeEvent('keyup', iconareaKeypress);
				self.desktop.removeEvent('keydown', iconareaKeypress);
			}
			if ( self.window && self.window !== null ) {
				self.window.removeEvent('mousedown', iconareaMDOWN);
				self.window.removeEvent('keypress', iconareaKeypress);
				self.window.removeEvent('keyup', iconareaKeypress);
				self.window.removeEvent('keydown', iconareaKeypress);
			}
			else {
				self.elements.container.removeEventListener('keypress', iconareaKeypress);
				self.elements.container.removeEventListener('keyup', iconareaKeypress);
				self.elements.container.removeEventListener('keydown', iconareaKeypress);
				self.elements.container.removeEventListener('mousedown', iconareaMDOWN);
			}

			self[ato.type] = ato;
			ato.addEvent('keypress', iconareaKeypress)
				.addEvent('keyup', iconareaKeypress)
				.addEvent('keydown', iconareaKeypress)
				.addEvent('mousedown', iconareaMDOWN)
			;
		});

		// for ( var i = 0; i<this.elements.container.children.length; i++ ) {
		// 	this.addIcon ( null, null, null, this.elements.container.children[i] );
		// }

		this.elements.container.tabIndex = 1;
		this.elements.container.addEventListener('click', iconareaClick);
		this.elements.container.addEventListener('keypress', iconareaKeypress);
		this.elements.container.addEventListener('keyup', iconareaKeypress);
		this.elements.container.addEventListener('keydown', iconareaKeypress);
		this.elements.container.addEventListener('mousedown', iconareaMDOWN);
		this.elements.container.addEventListener('contextmenu', function(e){e.preventDefault();return false;});

		this.elements.container.addEventListener('touchstart', iconareaTouchStart , false);
		this.elements.container.addEventListener('touchend', iconareaTouchEnd , false);

		
		// If parent node is desktop, attach to it
		if ( this.elements.container.parentNode &&
			 this.elements.container.parentNode.dataLVZWDESKTOPOBJ &&
			 this.elements.container.parentNode.dataLVZWDESKTOPOBJ.type == 'desktop'
		   ) this.attachTo (this.elements.container.parentNode.dataLVZWDESKTOPOBJ );
	};

	document.addEventListener('drag', function (e) {}, false );
	document.addEventListener('dragstart', function (e) {}, false );
	document.addEventListener('dragover', function (e) {e.preventDefault();}, false );
	document.addEventListener('drop', iconDrop, false );

	/**
	 * List with all iconarea objects.
	 * @type {Array}
	 * @public
	 */
	IconareaObject.list = [];

	/**
	 * Constructor for creating icon objects.
	 * @constructor
	 * @param {HTMLElement} el - Element to attach icon to.
	 */
	var IconObject = function ( el ) {

		IconObject.list.push( this );

		/**
		 * Type of object.
		 * @public
		 * @type {'icon'}
		 */
		this.type = 'icon';

		/**
		 * IconareaObject that current IconObject belongs to.
		 * @public
		 * @type {false|IconareaObject}
		 */
		this.iconarea = false;

		/**
		 * Associative array for storing icon's properties values.
		 * @public
		 * @type {Object}
		 */
		this.props = {};

		/**
		 * The id of container element.
		 * @public
		 * @type {false|String}
		 */
		this.props.elementId = false;

		/**
		 * CSS classname for theme in use.
		 * @public
		 * @type {null|string}
		 */
		this.props.themeClass = null;

		/**
		 * Icons vertical position.
		 * @public
		 * @type {number}
		 */
		this.props.top = 0;

		/**
		 * Icons horizontal position.
		 * @public
		 * @type {number}
		 */
		this.props.left = 0;

		/**
		 * Icons max width. If false then there is no max width.
		 * @public
		 * @type {number|false}
		 */
		this.props.maxWidth = false;

		/**
		 * Icon's image source.
		 * @public
		 * @type {String}
		 */
		this.props.src = '';

		/**
		 * Icon's label text.
		 * @public
		 * @type {String}
		 */
		this.props.label = '';

		/**
		 * True if icon object is selected, false otherwise.
		 * @public
		 * @type {Boolean}
		 */
		this.props.selected = false;

		/**
		 * Contains class names strings for icon object elements.
		 * @public
		 * @type {Object}
		 */
		this.props.classNames = {};

		/**
		 * Class name for icon container element.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.container = 'web-desktop-icon';

		/**
		 * Class name for focused icon.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.active = 'active';

		/**
		 * Array for storing "open" event functions.
		 * @public
		 * @type {Array}
		 */
		this.onopen = [];

		/**
		 * Array for storing click event functions.
		 * @public
		 * @type {Array}
		 */
		this.onclick = [];

		/**
		 * Array for storing drag event functions.
		 * @public
		 * @type {Array}
		 */
		this.ondrag = [];

		/**
		 * Array for storing drop event functions.
		 * @public
		 * @type {Array}
		 */
		this.ondrop = [];

		/**
		 * Array for storing remove event functions.
		 * @public
		 * @type {Array}
		 */
		this.onremove = [];

		/**
		 * Array for storing kill event functions.
		 * @public
		 * @type {Array}
		 */
		this.onkill = [];

		/**
		 * Array for storing dropinto event functions.
		 * @public
		 * @type {Array}
		 */
		this.ondropinto = [];

		/**
		 * Array for storing "select" event functions.
		 * @public
		 * @type {Array}
		 */
		this.onselect = [];

		/**
		 * Array for storing "deselect" event functions.
		 * @public
		 * @type {Array}
		 */
		this.ondeselect = [];

		/**
		 * Array for storing "labelchange" event functions.
		 * @public
		 * @type {Array}
		 */
		this.onlabelchange = [];

		/**
		 * Array for storing "rename" event functions.
		 * @public
		 * @type {Array}
		 */
		this.rename = [];

		/**
		 * Array for storing "iconlongtap" event functions.
		 * @public
		 * @type {Array}
		 */
		this.oniconlongtap = [];

		/**
		 * Method for adding events to icon.
		 *
		 * @param {string} e - Event name.
		 * @param {function} f - Function to run on event.
		 * @return {false|IconObject} False on error, current object otherwise.
		 */
		this.addEvent = window.lvzwebdesktop.fn.addEvent;

		/**
		 * Method for removing events from icon object.
		 *
		 * @param {string} e - The name of the event.
		 * @param {function} [f] - The function to remove. If this is set then it will remove only this function. Otherwise it will remove all event functions.
		 * @return {false|IconObject} False on error, current object otherwise.
		 */
		this.removeEvent = window.lvzwebdesktop.fn.removeEvent;

		/**
		 * Method for firing events.
		 *
		 * @param {string} e - The name of the event to fire.
		 * @param {Object} obj - An object with properties and method to add to event object.
		 * @return {false|IconObject} False on error, current object otherwise.
		 */
		this.fireEvent = window.lvzwebdesktop.fn.fireEvent;

		/**
		 * Method for setting icons theme.
		 * @param {string} theme - Theme name.
		 * @return {IconObject} Current icon object.
		 */
		this.setTheme = function ( theme ) {
			this.elements.container.classList.add ( theme );
			return this;
		};

		/**
		 * Method for setting current icon object as selected. Fires "select" event.
		 *
		 * @return {IconObject} Current object.
		 */
		this.select = function () {
			if ( this.iconarea.props.selectedIcons.indexOf ( this.elements.container ) < 0 )  {
				this.iconarea.props.selectedIcons.push ( this.elements.container );
				this.elements.container.classList.add ( this.iconarea.props.classNames.selected );
				this.props.selected = true;
				this.fireEvent('select',{
					"target": this
				});
			}
			return this;
		};

		/**
		 * Method for setting current icon object as unselected. Fires "deselect" event.
		 *
		 * @return {IconObject} Current object.
		 */
		this.unselect = function () {
			if ( ! this.iconarea ) return this;
			var p = this.iconarea.props.selectedIcons.indexOf ( this.elements.container );
			if ( p > -1 )  {
				this.iconarea.props.selectedIcons.splice ( p, 1 );
				this.elements.container.classList.remove ( this.iconarea.props.classNames.selected );
				this.props.selected = false;
				this.fireEvent('deselect',{
					"target": this
				});
			}
			return this;
		};

		/**
		 * Run "open" events.
		 *
		 * @return {IconObject} Current object.
		 */
		this.open = function () {

			this.fireEvent('open',{
				"target": this
			});
			return this;
		};

		/**
		 * Set or change icon image for icon object.
		 * @param {string} src - Icon image URL.
		 * @return {IconObject} Current icon object.
		 */
		this.setIcon = function ( src ) {
			this.props.src = src;
			this.elements.icon.src = this.props.src;
			return this;
		};

		/**
		 * Set or change icon's label text.
		 * @param {string} label - Label text to set.
		 * @return {IconObject} Current icon object.
		 */
		this.setLabel = function ( label ) {
			var eobj = {
				"target" : this,
				"newLabel" : label,
				"oldLabel" : this.props.label
			};
			this.fireEvent('labelchange',eobj);
			this.fireEvent('rename',eobj);
			if ( eobj.defaultPrevented === true ) return this;
			
			this.props.label = label;
			this.elements.label.innerText = this.props.label;
			this.elements.inputbox.value = this.props.label;
			return this;
		};

		/**
		 * Method for setting top position of icon. Fires "onmove" event.
		 * @param {number} top - Icon's top border distance from iconarea's top border.
		 * @return {IconObject} Current icon object.
		 */
		this.setTop = function ( top ) {
			var eobj = {
				"target" : this
			};
			this.fireEvent('move',eobj);
			if ( eobj.preventDefault.a === true ) return this;

			this.props.top = top;
			this.elements.container.style.top = top + 'px';

			return this;
		};

		/**
		 * Method for setting left position of icon. Fires "onmove" event.
		 * @param {number} left - Icon's left border distance from iconarea's left border.
		 * @return {IconObject} Current icon object.
		 */
		this.setLeft = function ( left ) {
			var eobj = {
				"target" : this
			};
			this.fireEvent("move",eobj);
			if (eobj.preventDefault.a === true ) return this;

			this.props.left = left;
			this.elements.container.style.left = left + 'px';

			return this;
		};

		/**
		 * Method for setting/changing icons position. Firex "onmove" event.
		 * @param {number} top - Icon's top border distance from iconarea's top border.
		 * @param {number} left - Icon's left border disatance from iconarea's left border.
		 * @return {IconObject} Current icon object.
		 */
		this.setPosition = function ( top, left ) {
			var eobj = {
				"target" : this
			};
			this.fireEvent('move',eobj);
			if (eobj.preventDefault.a === true ) return this;

			this.props.top = top;
			this.props.left = left;

			this.elements.container.style.top = top + 'px';
			this.elements.container.style.left = left + 'px';

			return this;
		};

		/**
		 * Remove object from it's parent iconarea.
		 * @return {IconObject} Current icon object.
		 */
		this.remove = function () {
			var eobj = {
				"target" : this
			};
			this.fireEvent('remove', eobj);
			if ( eobj.preventDefault.a === true ) return this;

			this.elements.container.parentNode.removeChild(this.elements.container);

			return this;
		};

		/**
		 * Completely delete icon.
		 * @return {boolean} true if killed otherwise false.
		 */
		this.kill = function () {
			var eobj = {
				"target" : this
			};
			this.fireEvent('kill', eobj);
			if ( eobj.preventDefault.a === true ) return false;

			lvzwebdesktop.IconObject.removeItem ( this );
			return true;
		};

		/**
		 * Show edit box for changing icons label.
		 * @return {IconObject} This icon object.
		 */
		this.editLabelOn = function ( ) {
			this.elements.label.style.display = 'none';
			this.elements.inputbox.style.width = this.elements.inputbox.value.length + 'ch';
			this.elements.inputbox.style.display = '';
			this.elements.inputbox.focus();
			this.elements.container.removeEventListener('click', iconClick);
			this.elements.container.removeEventListener('dblclick', iconDBLClick);

			return this;
		};

		/**
		 * Hide edit box for changing icons label.
		 * @return {IconObject} This icon object.
		 */
		this.editLabelOff = function ( ) {
			this.elements.label.style.display = '';
			this.elements.inputbox.style.display = 'none';
			this.elements.container.addEventListener('click', iconClick);
			this.elements.container.addEventListener('dblclick', iconDBLClick);
			this.unselect().select();
			return this;
		};

		/**
		 * Returns the first parent object of given type.
		 *
		 * @param {function|string} type - lvzwebobject constructor or constructor's name.
		 * @return {object|null|false} Return's the first parent lvzwebobject that matches "type" parameter. if not found returns null. on error returns false.
		 */
		this.getParentOfType = function ( type ) { return window.lvzwebdesktop(this.elements.container,'').getParentObject( type ); };

		/**
		 * Associative array for storing icon's elements.
		 * @public
		 * @type {Object}
		 */
		this.elements = {};

		/**
		 * Container element.
		 * @public
		 * @type {HTMLDivElement}
		 */
		this.elements.container = null;

		/**
		 * Icon image element.
		 * @public
		 * @type {HTMLImageElement}
		 */
		this.elements.icon = null;

		/**
		 * Label element for icon.
		 * @public
		 * @type {HTMLDivElement}
		 */
		this.elements.label = null;

		if ( el ) {
			this.elements.container = el;
			this.props.elementId = !el.id ? false : el.id;

			this.props.src = el.getAttribute('icon');
			if ( this.props.src === null ) this.props.src = '';

		}
		else {
			this.elements.container = document.createElement('div');
		}

		this.elements.container.dataIconObject = this;
		this.elements.container.dataLVZWDESKTOPOBJ = this;
		this.elements.container.classList.add ( this.props.classNames.container );

		if ( this.elements.container.children.length > 0 ) {
			this.elements.icon = this.elements.container.querySelector('img');
			this.elements.label = this.elements.container.querySelector('div');
		}
		else {
			this.props.label = this.elements.container.innerText;
			this.elements.container.innerHTML = '';
		}

		if ( this.elements.icon === null ) this.elements.icon = document.createElement('img');
		else this.props.src = this.elements.icon.src;
		this.elements.container.appendChild ( this.elements.icon );

		if ( this.elements.label === null ) this.elements.label = document.createElement('div');
		else this.props.label = this.elements.label.innerText;
		this.elements.container.appendChild ( this.elements.label );

		this.elements.inputbox = document.createElement('input');
		this.elements.inputbox.type = 'text';
		this.elements.inputbox.style.display = 'none';
		this.elements.inputbox.dataLVZWDESKTOPOBJ = this;
		this.elements.inputbox.dataIconObject = this;
		this.elements.inputbox.value = this.props.label;
		this.elements.inputbox.addEventListener ( 'keyup', function ( e ) {
			var self = e.target.dataLVZWDESKTOPOBJ;

			e.target.style.width = e.target.value.length + 'ch';
			if ( e.charCode == 0 &&  e.keyCode == 13 ) {
				self.setLabel(self.elements.inputbox.value);
				self.editLabelOff();
			}
			else if ( e.charCode == 0 &&  e.keyCode == 27 ) {
				self.elements.inputbox.value = self.props.label;
				self.editLabelOff();
			}
		});
		this.elements.inputbox.addEventListener ( 'blur', function ( e ) {
			var self = e.target.dataLVZWDESKTOPOBJ;
			self.setLabel(self.elements.inputbox.value).editLabelOff();
		});
		this.elements.container.appendChild ( this.elements.inputbox );

		this.elements.icon.src = this.props.src;
		this.elements.label.innerHTML = this.props.label;

		this.elements.icon.dataIconObject = this;
		this.elements.icon.dataLVZWDESKTOPOBJ = this;
		this.elements.icon.draggable = false;

		this.elements.label.dataIconObject = this;
		this.elements.label.dataLVZWDESKTOPOBJ = this;
		this.elements.label.draggable = false;

		this.elements.container.draggable = true;

		var tmpattr;

		if ( el ) {
			tmpattr = el.getAttribute ( 'onrightclick' );
			if ( tmpattr !== null ) this.addEvent ( 'rightclick', eval ( "var a =  function ( Event ) {var event = Event;" + tmpattr + "}; a" ) );

			tmpattr = el.getAttribute ( 'oniconclick' );
			if ( tmpattr !== null ) this.addEvent ( 'click', new Function ('event',tmpattr) );

			tmpattr = el.getAttribute ( 'onselect' );
			if ( tmpattr !== null ) this.addEvent ( 'select', new Function('event',tmpattr) );

			tmpattr = el.getAttribute ( 'ondeselect' );
			if ( tmpattr !== null ) this.addEvent ( 'deselect', new Function ('event', tmpattr) );

			tmpattr = el.getAttribute ( 'onopen' );
			if ( tmpattr !== null ) this.addEvent ( 'open', new Function ('event', tmpattr) );

			tmpattr = el.getAttribute ( 'ondropinto' );
			if ( tmpattr !== null ) this.addEvent ( 'dropinto',  new Function('event',tmpattr) );

			tmpattr = el.getAttribute ( 'ondrag' );
			if ( tmpattr !== null ) this.addEvent ( 'drag', eval ( "var a =  function ( Event ) {var event = Event;" + tmpattr + "}; a" ) );

			tmpattr = el.getAttribute ( 'ondrop' );
			if ( tmpattr !== null ) this.addEvent ( 'drop', eval ( "var a =  function ( Event ) {var event = Event;" + tmpattr + "}; a" ) );

			tmpattr = el.getAttribute ( 'oniconlongtap' );
			if ( tmpattr !== null ) this.addEvent ( 'iconlongtap', new Function ( 'event', tmpattr ) );

			tmpattr = el.getAttribute ( 'top' );
			if ( tmpattr !== null ) this.props.top = parseFloat ( tmpattr );

			tmpattr = el.getAttribute ( 'left' );
			if ( tmpattr !== null ) this.props.left = parseFloat ( tmpattr );

			tmpattr = getComputedStyle(this.elements.container).getPropertyValue('max-width').trim();
			if ( tmpattr != '' && tmpattr != 'none' ) this.props.maxWidth = parseInt(tmpattr.match("^([0-9]+)(px)$")[1]);
		}

		tmpattr = getComputedStyle(document.body).getPropertyValue('--lvzwebdesktop-default-icon-theme').trim();
		if ( tmpattr != '' ) {
			for ( var i = 0 ; i < this.elements.container.classList.length ; i ++ ) {
				if ( getComputedStyle(document.body).getPropertyValue('--lvzwebdesktop-theme-icon-'+this.elements.container.classList[i]).trim() == 'true' ) {
					this.props.themeClass = this.elements.container.classList[i];
					break;
				}
			}
			if ( this.props.themeClass === null ) {
				this.props.themeClass = tmpattr;
				this.elements.container.classList.add(tmpattr);
			}
		}
		else {
			console.error ("There is no theme loaded");
		}

		this.elements.container.style.top = this.props.top + 'px';
		this.elements.container.style.left = this.props.left + 'px';

		this.elements.container.addEventListener('click', iconClick);
		this.elements.container.addEventListener('dblclick', iconDBLClick);
		this.elements.container.addEventListener('mousedown', iconMouseDown);
		this.elements.container.addEventListener('dragstart', function ( e ) {
			var iconarea = e.target.dataIconObject.iconarea;
			iconDragMove.icon = e.target.dataIconObject.elements.container;
			iconDragMove.iconobj = e.target.dataIconObject;

			if ( iconarea.props.selectedIcons.length == 0 ) {
				iconDragMove.iconobj.select();
			} else if ( ! iconDragMove.iconobj.props.selected ) {
				if ( e.ctrlKey ) iconDragMove.iconobj.select();
				else {
					iconarea.unSelectAll();
					iconDragMove.iconobj.select();
				}
			}
			// iconDragMove.x = ( e.layerX - (e.target.offsetWidth / 2) );
			// iconDragMove.y = ( e.layerY - (e.target.offsetHeight / 2) );
			iconDragMove.x = e.screenX;
			iconDragMove.y = e.screenY;
			iconDragMove.iconarea = iconarea;
			//document.addEventListener('mousemove', iconDragMove);
			iconarea.fireEvent ( 'icondrag', {
				target: iconarea,
				icon: e.target.dataIconObject,
				iconContainerElement: iconDragMove.icon
			});
		}, false );
		this.elements.container.addEventListener('dragenter', function ( e ) { e.preventDefault(); } , false );
		this.elements.container.addEventListener('dragover', function ( e ) { e.preventDefault(); } , false );

		this.elements.container.addEventListener('touchstart', iconTouchStart , false);
		this.elements.container.addEventListener('touchend', iconTouchEnd , false);

		if ( this.elements.container.parentNode !== null &&
			 this.elements.container.parentNode.dataIconareaObject ) {
			this.elements.container.parentNode.dataIconareaObject.addIcon ( this );
		}
	};

	/**
	 * List with all icon objects.
	 * @public
	 * @type {Array}
	 */
	IconObject.list = [];

	/**
	 * Method for removing icon objects from icon objects list.
	 * @param {IconObject|number|HTMLElement} item - IconObject or IconObject's order number in list or an element of IconObject.
	 * @return {boolean} False on error, true on success.
	 */
	IconObject.removeItem = function ( item ) {
		var itemnumber;
		if ( typeof item == 'number' || item instanceof Number ) {
			if ( item < 0 || item >= IconareaObject.list.length ) {
				console.error ( "Out of range" );
				return false;
			}
			itemnumber = item;
		}
		else if ( item instanceof IconObject ) {
			itemnumber = IconObject.list.indexOf ( item );
			if ( itemnumber < 0 ) {
				console.error ( "Not in list" );
				return false;
			}
		}
		else if ( item instanceof HTMLElement && item.dataLVZWDESKTOPOBJ instanceof IconObject ) {
			itemnumber = IconObject.list.indexOf ( item.dataLVZWDESKTOPOBJ );
			if ( itemnumber < 0 ) {
				console.error ( "Not in list" );
				return false;
			}
		}
		else {
			console.error ( "Must give object or nubmer of object or object's element" );
			return false;
		}

		IconObject.list.splice( itemnumber, 1);
		//for ( var i = itemnubmer; i < IconObject.list.length-1; i ++ )
		//	IconObject.list[i] = IconObject.list[i+1];

		return true;
	};

	/**
	 * Function to run when clicking an icon.
	 *
	 * @param {Event} e - Event object.
	 */
	function iconClick ( e ) {
		var self = e.target.dataIconObject.iconarea;
		var iconel = e.target;

		if ( ! iconel.classList.contains ( self.props.classNames.icon ) )
			iconel = iconel.parentNode;

		var iconobj = iconel.dataIconObject;

		if ( ! iconobj ) return;

		if ( e.ctrlKey ) {
			if ( ! iconobj.props.selected ) {
				iconobj.select();
			} else {
				iconobj.unselect();
			}
		}
		else {
			if ( self.props.selectedIcons.length == 1 &&
				 self.props.selectedIcons[0] === iconobj.elements.container &&
				 self.props.clickAgainForEditIconLabel === true 
			   ) iconobj.editLabelOn();
			else {
				self.unSelectAll( [ iconobj ] );
				iconobj.select();
			}
		}
	}

	/**
	 * Function to run when double click an icon.
	 *
	 * @param {Event} e - Event object.
	 */
	function iconDBLClick ( e ) {
		var self = e.target.dataIconObject.iconarea;
		var iconel = e.target;
		if ( ! iconel.classList.contains ( self.props.classNames.icon ) )
			iconel = iconel.parentNode;

		e.target.dataIconObject.open();
	}

	/**
	 * Function to run when mouse down on icon.
	 *
	 * @param {Event} e - Event object.
	 */
	function iconMouseDown ( e ) {
		var self = e.target.dataIconObject.iconarea;
		var iconel = e.target;
		var iconobj = iconel.dataIconObject;
		var evt = {
			clientX: e.clientX,
			clientY: e.clientY,
			layerX: e.layerX,
			layerY: e.layerY,
			pageX: e.pageX,
			pageY: e.pageY
		};

		evt.target = self;
		if ( ! iconel.classList.contains ( self.props.classNames.icon ) )
			iconel = iconel.parentNode;
		evt.iconContainerElement = iconel;

		if ( e.button == 2 ) {

			if ( ! iconobj.props.selected ) {
				if ( e.ctrlKey == false ) self.unSelectAll( [ iconobj ]);
				iconobj.select();
			}

			if ( self.props.selectedIcons.indexOf ( iconel ) < 0 )  {
				self.unSelectAll([e.target.dataIconObject]);
				//e.target.dataIconObject.select();
			}
			self.fireEvent ( 'iconrightclick', evt);
		}
	}

	/**
	 * Function to run when dragging an icon.
	 *
	 * @param {Event} e - Event object.
	 */
	function iconDragMove ( e ) {
		var self = iconDragMove.iconarea;
		var icon = iconDragMove.icon;
	}

	/**
	 * Function to control icon drop.
	 *
	 * @param {Event} e - Event object.
	 */
	function iconDrop ( e ) {
		var self = iconDragMove.iconarea;
		var icon = iconDragMove.iconobj;
		var eobj;

		// Drop icon to iconarea
		if ( e.target.dataIconareaObject ) {

			// Drop icon in the same iconarea
			if ( e.target.dataIconareaObject === self ) {
				eobj = {
					target: self,
					icon: icon,
					iconContainerElement: icon.elements.container
				};
				self.fireEvent ( 'icondrop', eobj);

				if ( eobj.preventDefault.a !== true ) {
					var dx = e.screenX - iconDragMove.x;
					var dy = e.screenY - iconDragMove.y;

					for ( var i = 0; i < self.props.selectedIcons.length; i ++ )
						self.props.selectedIcons[i].dataIconObject.setPosition (
							self.props.selectedIcons[i].offsetTop + dy,
							self.props.selectedIcons[i].offsetLeft + dx
						);
				}
			}
			// Drop icon in different iconarea
			else {
				var dx = e.layerX;
				var dy = e.layerY;
				var i1x = e.layerX - self.props.selectedIcons[0].dataIconObject.props.left;
				var i1y = e.layerY - self.props.selectedIcons[0].dataIconObject.props.top;
				var ticonarea = e.target.dataIconareaObject;
				ticonarea.unSelectAll();

				for ( var i = self.props.selectedIcons.length -1 ; i > -1; i -- ) {
					var cicon = self.props.selectedIcons[i].dataIconObject;
					eobj = {
						target: ticonarea,
						icon: cicon,
						iconContainerElement: cicon.elements.container
					};
					ticonarea.fireEvent ( 'icondrop', eobj);
					if ( eobj.preventDefault.a !== true ) {
						cicon.unselect()
						self.removeIcon ( cicon );
						ticonarea.addIcon ( cicon );
						cicon.setPosition ( i1y + cicon.props.top, i1x + cicon.props.left );

						cicon.select();
					}
				}
			}
		}
		// Drop icon to icon
		else if ( e.target.dataIconObject ) {
			e.target.dataIconObject.fireEvent ( 'dropinto', {
				"target" : e.target.dataIconObject,
				"droppedItems" : self.props.selectedIcons
			});
		}
	}

	function iconTouchStart ( e ) {
		e.preventDefault();
		if ( e.touches.length == 1 ) {
			iconTouchStart.icon = e.target.dataLVZWDESKTOPOBJ ;
			iconTouchStart.ev = e;
			iconTouchStart.tmr = setTimeout( function () {
				clearTimeout(iconTouchStart.tmr);
				iconTouchStart.tmr=false;
				if ( ! iconTouchStart.icon.props.selected )
					iconTouchStart.icon.select();
				else
					iconTouchStart.icon.unselect();

				var eobj = {
					"target" : iconTouchStart.icon,
					"clientX" : iconTouchStart.ev.changedTouches[0].clientX,
					"clientY" : iconTouchStart.ev.changedTouches[0].clientY
				};
				iconTouchStart.icon.fireEvent ( 'iconlongtap', eobj);
			}, 500 );
		}
	}

	iconTouchStart.tmr = false;
	iconTouchStart.icon = null;
	iconTouchStart.ev = null;

	function iconTouchEnd ( e ) {
		clearTimeout(iconTouchStart.tmr);
		iconTouchStart.tmr=false;
		iconTouchStart.icon = null;
		iconTouchStart.ev = null;
	}


	function iconareaTouchStart ( e ) {
		if ( e.touches.length == 1 ) {
			iconareaTouchStart.iconarea = e.target.dataLVZWDESKTOPOBJ;

			// If taped on an icon
			if ( iconareaTouchStart.iconarea.type != 'iconarea' ) {
				e.preventDefault();
				iconareaTouchStart.iconarea = iconareaTouchStart.iconarea.iconarea;
				iconareaTouchStart.ev = e;
				iconareaTouchStart.tmr = setTimeout( function () {
					clearTimeout(iconareaTouchStart.tmr);
					iconareaTouchStart.tmr=false;
					var eobj = {
						"target" : iconareaTouchStart.iconarea,
						"clientX" : iconareaTouchStart.ev.changedTouches[0].clientX,
						"clientY" : iconareaTouchStart.ev.changedTouches[0].clientY
					};
					iconareaTouchStart.iconarea.fireEvent ( 'iconlongtap', eobj);
				}, 500 );
			}
			// If taped on empty space
			else {
			}
		}

		else if ( e.touches.length == 2 ) {
			
		}

	}

	iconareaTouchStart.tmr = false;
	iconareaTouchStart.iconarea = null;
	iconareaTouchStart.ev = null;

	function iconareaTouchEnd ( e ) {
		clearTimeout(iconareaTouchStart.tmr);
		iconareaTouchStart.tmr=false;
		iconareaTouchStart.iconarea = null;
		iconareaTouchStart.ev = null;
	}

	/**
	 * Function to handle clicks on iconarea.
	 *
	 * @param {Event} e - Event object.
	 */
	function iconareaClick ( e ) {
		var self = e.target.dataIconareaObject;

		if ( ! self ) return ;

		self.focus();

		if ( iconareaMMOVE.self !== false ) {
			iconareaMMOVE.self = false;
			return ;
		}
		if ( self.elements.container === e.target ) self.unSelectAll();
	}

	/**
	 * Function to handle mousedown event on iconarea.
	 *
	 * @param {Event} e - Event object.
	 */
	function iconareaMDOWN ( e ) {
		var self = e.target.dataIconareaObject;
		if ( self === null || ! self ) return;
		if ( e.button == 0 ) {
			iconareaMMOVE.self = self;

			self.elements.container.appendChild (  self.elements.selectionArea );
			var coffset = self.elements.container.getBoundingClientRect();

			iconareaMMOVE.ts = e.timeStamp;
			iconareaMMOVE.mouse = {
				px : e.layerX,
				py : e.layerY,
				ppx : e.pageX,
				ppy : e.pageY,
				bot : coffset.top,
				bol : coffset.left
			};

			iconareaMMOVE.area = { x1: 0, x2: 0, y1: 0, y2: 0 };

			self.elements.selectionArea.style.display = 'block';
			self.elements.selectionArea.style.top = ( e.pageY - iconareaMMOVE.mouse.bot - scrollY ) + 'px';
			self.elements.selectionArea.style.left = ( e.pageX - iconareaMMOVE.mouse.bol - scrollX ) + 'px';
			self.elements.selectionArea.style.width = '0px';
			self.elements.selectionArea.style.height = '0px';

			if ( self.window ) {
				self.window.removeEvent('mousedown', iconareaMDOWN );
			} else {
				self.elements.container.removeEventListener('mousedown', iconareaMDOWN );
			}
			self.elements.container.removeEventListener('click', iconareaClick);
			self.elements.container.addEventListener('mousemove', iconareaMMOVE );
			self.elements.container.addEventListener('mouseup', iconareaMUP );
		}
		else if ( e.button == 2 ) {
			lvzwebdesktop.activeIconarea = self;
			var menu = lvzwebdesktop('#iconareamenu', 'MenuObject').show({left:e.clientX,top:e.clientY});
		}
	}

	/**
	 * Function to handle keypress, keydown and keyup events on iconarea.
	 * @param {Event} e - Event object.
	 */
	function iconareaKeypress ( e ) {
		var self = e.target;

		if ( ! self.dataIconareaObject ) return ;
		self = self.dataIconareaObject;
		if ( e.type == 'keyup' ) {

			if ( e.window && self.window == null ) return;
			if ( e.desktop && self.desktop == null ) return;

			switch ( e.code ) {

			case 'Enter':
				for ( var i = 0; i < self.props.selectedIcons.length; i ++ )
					self.props.selectedIcons[i].dataIconObject.open();
				break;

			case "ArrowLeft":
				if ( self.props.selectedIcons.length == 0 ) {
					var s = self.getAllIcons();
					s[s.length -1].select();
				}
				else {
					var ci = self.props.selectedIcons[0].dataIconObject;
					var li = self.getFirstIconLeftOf ( ci );
					if ( li !== null ) {
						if ( ! e.shiftKey ) ci.unselect();
						li.select();
					}
				}
				break;

			case "ArrowRight":
				if ( self.props.selectedIcons.length == 0 ) {
					var s = self.getAllIcons();
					s[0].select();
				}
				else {
					var ci = self.props.selectedIcons[0].dataIconObject;
					var li = self.getFirstIconRightOf ( ci );
					if ( li !== null ) {
						if ( ! e.shiftKey ) ci.unselect();
						li.select();
					}
				}
				break;

			case "ArrowUp":
				if ( self.props.selectedIcons.length == 0 ) {
					var s = self.getAllIcons();
					s[s.length -1].select();
				}
				else {
					var ci = self.props.selectedIcons[0].dataIconObject;
					var li = self.getFirstIconUpOf ( ci );
					if ( li !== null ) {
						if ( ! e.shiftKey ) ci.unselect();
						li.select();
					}
				}
				break;

			case "ArrowDown":
				if ( self.props.selectedIcons.length == 0 ) {
					var s = self.getAllIcons();
					s[0].select();
				}
				else {
					var ci = self.props.selectedIcons[0].dataIconObject;
					var li = self.getFirstIconDownOf ( ci );
					if ( li !== null ) {
						if ( ! e.shiftKey ) ci.unselect();
						li.select();
					}
				}
				break;

			case "F2":
				if ( self.props.selectedIcons.length == 1 )
					self.props.selectedIcons[0].dataLVZWDESKTOPOBJ.editLabelOn();
				break;
			}
		}
	}

	/**
	 * Function to handle mousemove event on iconarea.
	 *
	 * @param {Event} e - Event object.
	 */
	function iconareaMMOVE ( e ) {
		var self = iconareaMMOVE.self;
		if ( ! self ) return ;

		var w = ( e.pageX - iconareaMMOVE.mouse.bol - scrollX ) - iconareaMMOVE.mouse.px;
		var h = ( e.pageY - iconareaMMOVE.mouse.bot - scrollY ) - iconareaMMOVE.mouse.py;
		var t = iconareaMMOVE.mouse.py;
		var l = iconareaMMOVE.mouse.px;
		if ( w < 0 ) { l += w; w = Math.abs(w); }
		if ( h < 0 ) { t += h; h = Math.abs(h); }

		iconareaMMOVE.area.x1 = l;
		iconareaMMOVE.area.x2 = l+w;
		iconareaMMOVE.area.y1 = t;
		iconareaMMOVE.area.y2 = t+h;

		self.elements.selectionArea.style.top = t + 'px';
		self.elements.selectionArea.style.left = l + 'px';
		self.elements.selectionArea.style.width = w + 'px';
		self.elements.selectionArea.style.height = h + 'px';
	}

	iconareaMMOVE.self = false;

	/**
	 * Function to handle mouseup event on iconarea.
	 *
	 * @param {Event} e - Event object.
	 */
	function iconareaMUP ( e ) {
		var self = iconareaMMOVE.self;

		var selection = self.getIconsInArea ( iconareaMMOVE.area.x1, iconareaMMOVE.area.y1, iconareaMMOVE.area.x2, iconareaMMOVE.area.y2 ) ;

		if ( e.ctrlKey == false ) self.unSelectAll( );
		for ( var i = 0; i < selection.length; i ++ ) selection[i].select();

		if ( iconareaMMOVE.mouse.ppx == e.pageX &&
			 iconareaMMOVE.mouse.ppy == e.pageY &&
			 e.timeStamp - iconareaMMOVE.ts < 500 ) iconareaMMOVE.self = false;

		self.elements.selectionArea.style.display = 'none';

		if ( self.window ) {
			self.window.addEvent('mousedown', iconareaMDOWN );
		} else {
			self.elements.container.addEventListener('mousedown', iconareaMDOWN );
		}
		self.elements.container.removeEventListener('mousemove', iconareaMMOVE );
		self.elements.container.removeEventListener('mouseup', iconareaMUP );
		self.elements.container.addEventListener('click', iconareaClick);

	}

	window.lvzwebdesktop.IconareaObject = IconareaObject;
	window.lvzwebdesktop.constructors.push({
		"selector" : ".web-desktop-iconarea",
		"constructor" : IconareaObject
	});

	window.lvzwebdesktop.IconObject = IconObject;
	window.lvzwebdesktop.constructors.push({
		"selector" : ".web-desktop-icon",
		"constructor" : IconObject
	});
})();
