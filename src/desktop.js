(function () {

	/**
	 * Function for returning the desktop object of an html element.
	 *
	 * @param {HTMLElement} el - The element to check.
	 * @return {false|DesktopObject} The desktop object if found or false if not. 
	 */
	window.lvzwebdesktop.getParentDesktop = function ( el ) {
		if ( typeof el == 'undefined' ) return false;
		var o = el;
		do {
			o = o.parentNode;
			if (!o) return false;
			if ( typeof o.dataDesktopObject != 'undefined' ) return o.dataDesktopObject;
		} while ( o !== null && o.parentNode && o.nodeName != '#document' );
		return false;
	}


	/** 
	 * Constructor for desktop objects.
	 *
	 * @constructor
	 * @param {HTMLElement} e - Element to attach desktop to.
	 */
	var DesktopObject = function ( e ) {
		DesktopObject.list.push ( this );

		/**
		 * Type of object.
		 * @public
		 * @type {'desktop'}
		 */
		this.type = "desktop";

		/**
		 * Stores the current focused object of desktop. Null in none.
		 * @public
		 * @type {Object}
		 */
		this.focusedObject = null;

		/**
		 * Associative array for desktop's property values.
		 * @public
		 * @type {Object}
		 */
		this.props = {};

		/**
		 * Wallpaper url string or if none, false.
		 * @public
		 * @type {false|String}
		 */
		this.props.wallpaper = false;

		/**
		 * Set if the wallpaper image should be repeated until it fills desktop.
		 * @public
		 * @type {boolean}
		 */
		this.props.wallpaperRepeat = false;

		/**
		 * Set the wallpapers background image size.
		 * @public
		 * @type {string}
		 */
		this.props.wallpaperSize = 'contain';

		/**
		 * Array for storing objectadd event functions.
		 * @public
		 * @type {Array}
		 */
		this.onobjectadd = [];

		/**
		 * Array for storing objectremove event functions.
		 * @pulbic
		 * @type {Array}
		 */
		this.onobjectremove = [];

		/**
		 * Array for storing changewallpaper event functions.
		 * @public
		 * @type {Array}
		 */
		this.onchangewallpaper = [];

		/**
		 * Array for storing keypress event functions.
		 * @public
		 * @type {Array}
		 */
		this.onkeypress = [];

		/**
		 * Array for storing keyup event functions.
		 * @public
		 * @type {Array}
		 */
		this.onkeyup = [];

		/**
		 * Array for storing keydown event functions.
		 * @public
		 * @type {Array}
		 */
		this.onkeydown = [];

		/**
		 * Array for storing mosedown event functions.
		 * @public
		 * @type {Array}
		 */
		this.onmousedown = [];

		/**
		 * Array for storing resize event functions.
		 * @public
		 * @type {Array}
		 */
		this.onresize = [];

		/**
		 * Array for storing load event functions.
		 * @public
		 * @type {Array}
		 */
		this.onload = [];

		/**
		 * Array for storing event functions for desktop's itemattach event.
		 * @type {Array}
		 * @public
		 */
		this.onitemattached = [];

		/**
		 * Array for storing all objects attached to this desktop.
		 * @public
		 * @type {Array}
		 */
		this.objects = [];

		/**
		 * Taskbars attached to desktop.
		 * @public
		 * @type {array}
		 */
		this.taskbars = [];

		/**
		 * Method for changing desktop's wallpaper. Fires "changewallpaper" event.
		 *
		 * @param {false|String} src - false for deleting current wallpaper or wallpaper url to set it.
		 * @return {DesktopObject} This object.
		 */
		this.setWallpaper = function ( src ) {
			var eventobj = {};
			eventobj.target = this;
			eventobj.previousValue = this.props.wallpaper;
			this.props.wallpaper = src;
			eventobj.currentValue = this.props.wallpaper;

			if ( this.props.wallpaper !== false )
				this.elements.root.style.backgroundImage = "url('"+ this.props.wallpaper + "')";
			else
				this.elements.root.style.backgroundImage = "none";

			if ( this.props.wallpaperRepeat ) this.elements.root.style.backgroundRepeat = 'repeat';
			else this.elements.root.style.backgroundRepeat = 'no-repeat';

			if ( this.props.wallpaperSize !== false )
				this.elements.root.style.backgroundSize = this.props.wallpaperSize;

			return this.fireEvent('changewallpaper', eventobj );
		}

		/**
		 * Add an object to this desktop.
		 *
		 * @param {object} o - The object to add.
		 * @param {boolean} addElement - if false, dont append container to desktop container.
		 * @return {object} This desktop object.
		 */
		this.addObject = function ( o, addElement ) {
			if ( typeof o == 'object' &&
				 typeof o.elements == 'object' &&
				 o.elements.container instanceof HTMLElement ) {
				if ( this.objects.indexOf(o) < 0 ) {
					var eobj = {
						"desktop" : this,
						"target" : o
					};
					this.fireEvent('objectadd',eobj);
					if ( eobj.preventDefault.a === true ) return this;

					this.objects.push(o);
					o.desktop = this;
					if ( addElement !== false ) this.elements.root.appendChild(o.elements.container);

					if ( typeof o.focus == 'function' ) o.focus();

					this.taskbars.forEach ( function ( t, i ) { t.addObject ( o );} );

				} else {
					console.error ( 'Already inserted.' );
				}
			} else {
				console.error ( 'Not a valid object.' );
			}

			return this;
		};

		/**
		 * Method for attaching items to desktop. Fires "itemattached" event and "attachto" event on object to attach.
		 * @param {HTMLElement|object} item - An element or an lvzwebdesktop object to attach.
		 * @return {WindowObject|false} False on error. Otherwise, current desktop object.
		 */
		this.attachItem = function ( item ) {
			var eobj = { "target" : this };
			var eobj2 = { "attachTo" : this };

			if ( item instanceof HTMLElement ) {
				if ( typeof item.dataLVZWDESKTOPOBJ == 'object' ) {
					var c = window.lvzwebdesktop.isPartOf(item.dataLVZWDESKTOPOBJ);
					if ( c !== false && typeof item.dataLVZWDESKTOPOBJ.elements == 'object' && item.dataLVZWDESKTOPOBJ.elements.container instanceof HTMLElement ) {
						eobj2.target = item.dataLVZWDESKTOPOBJ;
						eobj2.attachElement = item;
						eobj.item = item.dataLVZWDESKTOPOBJ;
						this.elements.bodycontainer.appendChild ( item );

						if ( item.dataLVZWDESKTOPOBJ.desktop ) item.dataLVZWDESKTOPOBJ.desktop = this;

						item.dataLVZWDESKTOPOBJ.fireEvent('attachto', eobj2);
						item.dataLVZWDESKTOPOBJ.fireEvent('changedesktop', {
							"target" : item.dataLVZWDESKTOPOBJ,
							"desktop" : this,
							"inheritFrom" : this
						});
						
					}
					else {
						eobj.item = item;
						this.elements.bodycontainer.appendChild ( item );
					}
				}
				else {
					eobj.item = item;
					this.elements.bodycontainer.appendChild ( item );
				}
			}
			else {
				var c = window.lvzwebdesktop.isPartOf( item );
				if ( c === false ) {
					console.error ( "Argument must be an element or an lvzwebdesktop object with a container element.");
					return false;
				}
				if ( typeof item.elements == 'object' && item.elements.container instanceof HTMLElement ) {
					item.desktop = this;
					eobj2.target = item;
					eobj2.attachElement = item.elements.container;
					eobj.item = item;
					// this.elements.bodycontainer.appendChild ( item.elements.container );
					item.fireEvent('attachto', eobj2);
					item.fireEvent('changedesktop', {
						"target" : item,
						"desktop" : this,
						"inheritFrom" : this
					});
				}
			}
			this.fireEvent('itemattached', eobj);
			return this;
		};

		/**
		 * Remove object from desktop.
		 *
		 * @param {object} o - The object to remove.
		 * @return {object} This desktop object.
		 */
		this.removeObject = function ( o ) {

			if ( typeof o == 'object' && typeof o.elements == 'object' && typeof o.elements.container == 'object' ) {
				var eobj = {
					"desktop" : this,
					"target" : o
				};
				this.fireEvent('objectremove',eobj);
				if ( eobj.preventDefault.a === true ) return this;

				var i = this.objects.indexOf(o);
				if ( i > -1 ) this.objects.splice(i,1);

			}
			return this;
		}

		/**
		 * Give focus to an object member of this desktop object. Fires focus even on the object that is about to be focused and blur event to the one that currently has focus.
		 *
		 * @param {object} toObject - The object to give focus to.
		 * @return {false|DesktopObject} False on error, otherwise this desktop object.
		 */
		this.giveFocus = function ( toObject ) {
			if ( !toObject.desktop || toObject.desktop !== this ) {
				console.error ( "Object is not a member of this desktop" );
				return false;
			}
			if ( !toObject.props.canHaveFocus ) {
				console.error ( "Object can't have focus." );
				return false;
			}

			var focusEventData = {
				"target" : toObject,
				"previousFocusedObject" : null
			};

			var blurEventData = {
				"target" : null,
				"previousFocusedObject" : toObject
			};

			if ( this.focusedObject !== toObject ) {
				if ( this.focusedObject ) {
					this.focusedObject.props.focused = false;
					focusEventData.previousFocusedObject = this.focusedObject;
					blurEventData.target = this.focusedObject;

					// this.focusedObject.blur ();
					if ( blurEventData.target.props.classNames.hasfocus ) blurEventData.target.elements.container.classList.remove ( blurEventData.target.props.classNames.hasfocus );
					this.focusedObject.fireEvent('blur', blurEventData);
				}

				this.objects.push ( this.objects.splice ( this.objects.indexOf ( toObject ), 1 )[0] );
				this.elements.root.appendChild( toObject.elements.container );
				toObject.props.focused = true;
				this.focusedObject = toObject;
				toObject.fireEvent('focus', focusEventData);
				lvzwebdesktop.WindowObject.fireEvent('windowfocus',{
					"takeFocusFrom" : focusEventData.previousFocusedObject,
					"giveFocusTo" : focusEventData.target
				});
			}

			return this;
		}

		/**
		 * Method for positioning all windows of the desktop in a cascade formation.
		 *
		 * @return {DesktopObject} Current desktop object.
		 */
		this.cascadeWindows = function () {
			var windowList = [];
			for ( var i = 0 ; i < this.objects.length; i ++ )
				if ( this.objects[i].type == 'window' && this.objects[i].props.state == 'normal' )
					windowList.push( this.objects[i] );
			lvzwebdesktop.WindowObject.cascadeWindows(
				windowList,
				this.elements.root.offsetWidth,
				this.elements.root.offsetHeight,
				20,
				20
			);
			return this;
		};

		/**
		 * Method for positionning all windows of the desktop in a tile formation.
		 *
		 * @return {DesktopObject} Current desktop object.
		 */
		this.tileWindows = function () {
			var windowList = [];
			for ( var i = 0 ; i < this.objects.length; i ++ )
				if ( this.objects[i].type == 'window' && this.objects[i].props.state == 'normal' )
					windowList.push( this.objects[i] );
			lvzwebdesktop.WindowObject.tileWindows(
				windowList,
				this.elements.root.offsetWidth,
				this.elements.root.offsetHeight,
			);
			return this;
		};

		/**
		 * Returns the first parent object of given type.
		 *
		 * @param {function|string} type - lvzwebobject constructor or constructor's name.
		 * @return {object|null|false} Return's the first parent lvzwebobject that matches "type" parameter. if not found returns null. on error returns false.
		 */
		this.getParentOfType = function ( type ) { return window.lvzwebdesktop(this.elements.root,'').getParentObject( type ); };

		/**
		 * Method for adding events.
		 *
		 * @param {String} e - Event name.
		 * @param {Function} f - Function to run on event.
		 * @return {DesktopObject|false} Boolean false on error, desktop object otherwise.
		 */
		this.addEvent = window.lvzwebdesktop.fn.addEvent;

		/**
		 * Method for removing events.
		 *
		 * @param {String} e - Event name.
		 * @param {Function} [f] - Function name. If this param is set then method will remove only the function that matches. Otherwise it will clear all functions from event.
		 * @return {DesktopObject|false} Boolean false on error, desktop object otherwise.
		 */
		this.removeEvent = window.lvzwebdesktop.fn.removeEvent;

		/**
		 * Method for firing events.
		 *
		 * @param {String} e - Event name.
		 * @param {Object} obj - An object with parameters and info around the event.
		 * @return {DesktopObject|false} Boolean false on error, desktop object otherwise.
		 */
		this.fireEvent = window.lvzwebdesktop.fn.fireEvent;

		/**
		 * Associative array for storing desktop's elements.
		 * @public
		 * @type {Object}
		 */
		this.elements = {};

		/**
		 * Desktop's root element.
		 * @public
		 * @type {HTMLDivElement}
		 */
		this.elements.root = null;

		if (typeof e == 'undefined') this.elements.root = document.createElement('div'); else this.elements.root = e;
		this.elements.root.style.overflow = 'hidden';
		this.elements.root.style.zIndex = 'auto';
		this.elements.root.style.position = 'absolute';
		if (this.elements.root.style.width=='')this.elements.root.style.width = '100%';
		if (this.elements.root.style.height=='')this.elements.root.style.height = '100%';
		if (this.elements.root.style.left=='')this.elements.root.style.left = '0';
		this.elements.root.tabIndex = 1;
		this.elements.root.dataDesktopObject = this;
		this.elements.root.dataLVZWDESKTOPOBJ = this;

		this.elements.root.addEventListener('contextmenu', function (e) {e.preventDefault(); return false;});

		this.elements.root.addEventListener('keypress', desktopKeyPress);
		this.elements.root.addEventListener('keyup', desktopKeyUp);
		this.elements.root.addEventListener('keydown', desktopKeyDown);
		this.elements.root.addEventListener('mousedown', desktopMouseDown);

		// if ( ! DesktopObject.addedKeyEvents ) {
		// 	document.addEventListener('keypress', desktopKeyPress);
		// 	document.addEventListener('keyup', desktopKeyUp);
		// 	document.addEventListener('keydown', desktopKeyDown);
		// 	DesktopObject.addedKeyEvents = true
		// }

		var tmpattr;

		tmpattr =  e.getAttribute('onobjectadd');
		if ( tmpattr !== null )this.addEvent ( 'objectadd', new Function('event',tmpattr));

		tmpattr =  e.getAttribute('onobjectremove');
		if ( tmpattr !== null )this.addEvent ( 'objectremove', new Function('event',tmpattr));

		tmpattr =  e.getAttribute('onchangewallpaper');
		if ( tmpattr !== null )this.addEvent ( 'changewallpaper', new Function('event',tmpattr));

		tmpattr = e.getAttribute('onresize');
		if ( tmpattr !== null ) this.addEvent ('resize', new Function('event',tmpattr));

		tmpattr = e.getAttribute('onload');
		if ( tmpattr !== null ) this.addEvent ('load', new Function('event',tmpattr));

		tmpattr = e.getAttribute('onitemattach');
		if ( tmpattr !== null ) this.addEvent ('itemattach', new Function('event',tmpattr));

		tmpattr =  e.getAttribute('wallpaper-repeat');
		if ( tmpattr == 'true' ) this.props.wallpaperRepeat = true;

		tmpattr =  e.getAttribute('wallpaper-size');
		if ( tmpattr !== null ) this.props.wallpaperSize = tmpattr;

		tmpattr =  e.getAttribute('wallpaper');
		if ( tmpattr !== null ) this.setWallpaper ( tmpattr );

		for ( var i = 0; i < this.elements.root.children.length; i++ )
			if ( this.elements.root.children[i].dataLVZWDESKTOPOBJ &&
				 typeof this.elements.root.children[i].dataLVZWDESKTOPOBJ.desktop != 'undefined' &&
				 this.elements.root.children[i].dataLVZWDESKTOPOBJ.desktpo !== this
			   ) this.attachItem ( this.elements.root.children[i].dataLVZWDESKTOPOBJ );

		this.fireEvent('load', {
			"target" : this
		});
	}

	window.addEventListener('resize', function ( e ) {
		for ( var i = 0; i < DesktopObject.list.length; i ++ ) {
			DesktopObject.list[i].fireEvent('resize', {
				"target": DesktopObject.list[i],
				"innerWidth" : window.innerWidth,
				"innerHeight" : window.innerHeight,
				"outerWidth" : window.outerWidth,
				"outerHeight" : window.outerHeight
			});
		}
	});

	DesktopObject.list = [];

	/**
	 * Function for controling keypress event on desktop.
	 *
	 * @param {Event} e - Event data object.
	 */
	function desktopKeyPress ( e ) {
		var self = e.target;
		while ( self.nodeName != '#document' && ! self.dataDesktopObject ) { self = self.parentNode; }
		self = self.dataDesktopObject;
		if ( ! self ) return;
		var eobj = {
			"target" : e.target,
			"desktop" : self,
			"window" : null,
			"altKey" : e.altKey,
			"ctrlKey" : e.ctrlKey,
			"metaKey" : e.metaKey,
			"shiftKey" : e.shiftKey,
			"charCode" : e.charCode,
			"code" : e.code,
			"key" : e.key,
			"keyCode" : e.keyCode,
			"which" : e.which
		};
		self.fireEvent('keypress',eobj);
	}

	/**
	 * Function for controling keyup event on desktop.
	 *
	 * @param {Event} e - Event data object.
	 */
	function desktopKeyUp ( e ) {
		var self;
		if ( lvzwebdesktop.DesktopObject.list.length == 1 ) {
			self = lvzwebdesktop.DesktopObject.list[0];
		}
		else {
			self = e.target;
			while ( self.nodeName != '#document' && ! self.dataDesktopObject ) { self = self.parentNode; }
			self = self.dataDesktopObject;
			if ( ! self ) return;
		}

		var eobj = {
			"target" : e.target,
			"desktop" : self,
			"window" : null,
			"altKey" : e.altKey,
			"ctrlKey" : e.ctrlKey,
			"metaKey" : e.metaKey,
			"shiftKey" : e.shiftKey,
			"charCode" : e.charCode,
			"code" : e.code,
			"key" : e.key,
			"keyCode" : e.keyCode,
			"which" : e.which
		};
		self.fireEvent('keyup',eobj);
		// if ( eobj.defaultPrevented === true || e.repeat ) return;
	}

	/**
	 * Function for controling keydown event on desktop.
	 *
	 * @param {Event} e - Event data object.
	 */
	function desktopKeyDown ( e ) {
		var mkey = false;
		var self;
		if ( lvzwebdesktop.DesktopObject.list.length == 1 ) {
			self = lvzwebdesktop.DesktopObject.list[0];
		}
		else {
			self = e.target;
			while ( self.nodeName != '#document' && ! self.dataDesktopObject ) { self = self.parentNode; }
			self = self.dataDesktopObject;
			if ( ! self ) return;
		}

		var eobj = {
			"target" : e.target,
			"desktop" : self,
			"window" : null,
			"altKey" : e.altKey,
			"ctrlKey" : e.ctrlKey,
			"metaKey" : e.metaKey,
			"shiftKey" : e.shiftKey,
			"charCode" : e.charCode,
			"code" : e.code,
			"key" : e.key,
			"keyCode" : e.keyCode,
			"which" : e.which
		};
		self.fireEvent('keydown',eobj);
		//if ( eobj.defaultPrevented === true || e.repeat ) return;
	}

	/**
	 * Function for controling mousedown event on desktop.
	 * @param {Event} e - Event data object.
	 */
	function desktopMouseDown ( e ) {
		var self = e.target;
		while ( self.nodeName != '#document' && ! self.dataDesktopObject ) { self = self.parentNode; }
		self = self.dataDesktopObject;
		if ( ! self ) return;
		self.fireEvent('mousedown',{
			"target" : e.target,
			"desktop" : self,
			"window" : null,
			"button" : e.button,
			"buttons" : e.buttons,
			"which"  : e.which,
			"clientX" : e.clientX,
			"clientY" : e.clientY,
			"pageX" : e.pageX,
			"pageY" : e.pageY,
			"screenX" : e.screenX,
			"screenY" : e.screenY,
			"layerX" : e.layerX,
			"layerY" : e.layerY
		});
	}

	window.lvzwebdesktop.DesktopObject = DesktopObject;
	window.lvzwebdesktop.constructors.push({
		"selector" : ".desktop,web-desktop",
		"constructor" : DesktopObject
	});
	
}) ();
