(function () {

	window.addEventListener('load', function ( ) {
		var mboxes = document.getElementsByClassName ( 'rbx' );
		var ewindow = document.getElementById('windowOnEditor').dataWindowObject;
		for ( var i = 0; i < mboxes.length; i ++ ) {
			mboxes[i].addEventListener('mousedown', editormboxmdown );
		}

		ewindow.Layout = "menu:minimize,maximizeRestore,close";
		ewindow.addEvent('close', function ( e ) {
			e.preventDefault();
		} );

	});

	function editormboxmdown ( e ) {
		var self = document.getElementById('windowOnEditor').dataWindowObject;
		editormboxmdown.t = e.target;

		if ( e.target.classList.contains ( 'resmr' ) ) editormboxmdown.direction = 'right';
		else if ( e.target.classList.contains ( 'resbr' ) ) editormboxmdown.direction = 'down right';
		else if ( e.target.classList.contains ( 'resbc' ) ) editormboxmdown.direction = 'down';
		else editormboxmdown.direction = '';

		editormboxmdown.mx = e.clientX;
		editormboxmdown.my = e.clientY;
		editormboxmdown.ww = parseFloat(self.props.width);
		editormboxmdown.wh = parseFloat(self.props.height);

		document.addEventListener('mousemove', editormboxmmove );
		document.addEventListener('mouseup', editormboxmup );
	}

	editormboxmdown.direction = '';
	editormboxmdown.t = null;
	editormboxmdown.mx = false;
	editormboxmdown.ww = false;

	function editormboxmmove ( e ) {
		e.preventDefault();
		var self = document.getElementById('windowOnEditor').dataWindowObject;

		if ( editormboxmdown.direction == 'right' ) {
			var tmpwidth = editormboxmdown.ww + ( e.clientX - editormboxmdown.mx  );
			self.props.width = tmpwidth;
			self.resize();
		}
		else if ( editormboxmdown.direction == 'down' ) {
			var tmpheight = editormboxmdown.wh + ( e.clientY - editormboxmdown.my  );
			self.props.height = tmpheight;
			self.resize();
		}
		else if ( editormboxmdown.direction == 'down right' ) {
			var tmpwidth = editormboxmdown.ww + ( e.clientX - editormboxmdown.mx  );
			var tmpheight = editormboxmdown.wh + ( e.clientY - editormboxmdown.my  );
			self.props.width = tmpwidth;
			self.props.height = tmpheight;
			self.resize();
		}
	}

	function editormboxmup ( e ) {
		e.preventDefault();
		document.removeEventListener('mousemove', editormboxmmove );
		document.removeEventListener('mouseup', editormboxmup );
	}
})();
