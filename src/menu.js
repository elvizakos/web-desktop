(function () {

	/**
	 * Constructor for creating menubar objects.
	 * @constructor
	 * @param {HTMLElement} e - Element to attach menubar to.
	 */
	var MenubarObject = function ( e ) {
		MenubarObject.list.push ( this );

		/**
		 * Type of object.
		 * @public
		 * @type {'menubar'}
		 */
		this.type = 'menubar';

		/**
		 * Associative array for storing menubar properties and their values.
		 * @public
		 * @type {Object}
		 */
		this.props = {};

		/**
		 * Desktop object that menubar object belongs to. If it doesn't belog to any destkop then null.
		 * @public
		 * @type {DesktopObject|null}
		 */
		this.props.desktop = null;

		/**
		 * The id of the container element of false if none.
		 * @public
		 * @type {string|false}
		 */
		this.props.elementId = false;

		/**
		 * CSS class name for theme in use.
		 * @type {null|string}
		 * @public
		 */
		this.props.themeClass = null;

		/**
		 * Menubar opened item or null if none opened.
		 * @public
		 * @type {null|object}
		 */
		this.props.openedItem = null;

		/**
		 * Contains class names strings of menubar object elements.
		 * @public
		 * @type {object}
		 */
		this.props.classNames = {};

		/**
		 * Class name of menubar container element.
		 * @public
		 * @type {string}
		 */
		this.props.classNames.container = MenubarObject.classNames.container;

		/**
		 * Class name for menubar container element for active menu.
		 * @public
		 * @type {string}
		 */
		this.props.classNames.active = MenubarObject.classNames.active;

		/**
		 * Class name for menubar opened item.
		 * @public
		 * @type {string}
		 */
		this.props.classNames.opened = MenubarObject.classNames.opened;

		/**
		 * Class name for disabled items of menubar object.
		 * @public
		 * @type {string}
		 */
		this.props.classNames.disabledItem = MenubarObject.classNames.disabledItem;

		/**
		 * Dummy property so that menubar object works with menu object.
		 * @type {null}
		 * @public
		 */
		this.parentMenu = null;

		/**
		 * Array for storing menu items.
		 * @public
		 * @type {Array}
		 */
		this.items = [];

		/**
		 * Array for storing event functions on adding menu items on menubar.
		 * @type {Array}
		 * @public
		 */
		this.onadditem = [];

		/**
		 * Array for storing event functions on removing items from menubar.
		 * @type {Array}
		 * @public
		 */
		this.onremoveitem = [];

		/**
		 * Array for storing event functions on clicking menubar items.
		 * @type {Array}
		 * @public
		 */
		this.onclickitem = [];

		/**
		 * Array for storing event functions on changing menu items order.
		 * @type {Array}
		 * @public
		 */
		this.onorderchange = [];

		/**
		 * Method for adding events to menubar object.
		 *
		 * @param {string} e - Event name.
		 * @param {function} f - Function to run on event.
		 * @return {MenubarObject|false} False on error, this menubar object otherwise.
		 */
		this.addEvent = window.lvzwebdesktop.fn.addEvent;

		/**
		 * Method for removing events from menubar object.
		 *
		 * @param {string} e - The name of the event to remove.
		 * @param {function} [f] - The function to remove. If this param is set then method will remove only the function that matches. Otherwise it will clear all functions from event.
		 * @return {MenubarObject|false} False on error, this menubar object otherwise.
		 */
		this.removeEvent = window.lvzwebdesktop.fn.removeEvent;

		/**
		 * Method for firing events.
		 *
		 * @param {string} e - The name of the event to fire.
		 * @param {object} obj - An object with properties to add to event object.
		 * @return {MenubarObject|false} False on error, this menubar object otherwise.
		 */
		this.fireEvent = window.lvzwebdesktop.fn.fireEvent;

		/**
		 * Method for getting data of menuitem by giving item's position or item's element or item's object.
		 * @param {number|object|HTMLLIElement} item - Item's position or items object or items element.
		 * @return {object|false} Object with menu data { object, element, position} or false on error.
		 */
		this.getMenuItem = function ( item ) {

			if ( item instanceof HTMLLIElement ) {
				// Get item's data by element

				if ( item.dataMenubarObject !== this ) {
					console.error ( "Menu item element does not belong to this menubar" );
					return false;
				}

				return {
					"element": item,
					"object":  item.dataMenuItemObject,
					"position": this.items.indexOf ( item.dataMenuItemObject )
				};

			} else if ( typeof item == 'object' ) {
				// Get item's data by object

				var itmpos = this.items.indexOf ( item );
				if ( itmpos == -1 ) {
					console.error ( "Menu item object does not belong to this menubar" );
					return false;
				}

				return {
					"element": item.container,
					"object":  item,
					"position": itmpos
				};

			} else if ( typeof item == 'number' || item instanceof Number ) {
				// Get item's data by position number

				if ( item < 0 || item >= this.items.length ) {
					console.error ( "Out of range" );
					return false;
				}

				return {
					"element": this.items[item].container,
					"object": this.items[item],
					"position": item
				};

			}

			console.error ( "Argument isn't the position, the element nor the object of menu item." );
			return false;
		};

		/**
		 * Method for adding items on Menubar. Fires the "additem" event.
		 *
		 * @param {HTMLLIElement} elem - The item's element.
		 * @return {MenubarObject} Current menubar object.
		 */
		this.addMenuItem = function ( elem ) {

			var menuitem = {};
			var mismel;

			if ( elem instanceof HTMLLIElement ) {
				menuitem.container = elem;
				menuitem.container.dataLVZWEBDESKTOPOBJ = this;
				menuitem.container.dataMenubarObject = this;
				menuitem.container.dataMenuItemObject = menuitem;
				menuitem.labelElement = elem.firstElementChild;
				menuitem.labelElement.dataLVZWEBDESKTOPOBJ = this;
				menuitem.labelElement.dataMenubarObject = this;
				menuitem.labelElement.dataMenuItemObject = menuitem;
				menuitem.labelElement.addEventListener('click', menubaritemclick );
				menuitem.labelElement.addEventListener('mouseover', menubaritemmover );
				menuitem.label = menuitem.labelElement.innerText;
				this.items.push ( menuitem );

				if ( menuitem.container.parentNode !== this.elements.container ) this.elements.container.appendChild ( menuitem.container );

			}

			var tmpclick = menuitem.container.getAttribute('onclick');
			menuitem.container.setAttribute('onclick','');

			menuitem.container.onitemclick = menuitem.container.getAttribute('onitemclick');
			if ( menuitem.container.onitemclick === null ) menuitem.container.onitemclick = '';
			if ( tmpclick !== null ) menuitem.container.onitemclick += tmpclick;
			if ( menuitem.container.onitemclick === null ) menuitem.container.onitemclick = '';
			if ( menuitem.container.onclick !== null ) menuitem.container.onitemclick += " ( " + menuitem.container.onclick.toString() + ")(event);";
			menuitem.container.onclick = null;

			menuitem.labelElement.addEventListener ( 'click' , menubaritemclick );

			this.fireEvent ('additem', {
				"target" : menuitem
			});

			return this;
		};

		/**
		 * Method for removing items from Menubar. Fires the "removeitem" event.
		 *
		 * @param {HTMLLIElement|object|number} item - The item's element or object or number.
		 * @return {MenubarObject|false} Current menubar object or false on error.
		 */
		this.removeMenuItem = function ( item ) {
			var itemdata = this.getMenuItem ( item );

			if ( itemdata === false ) return false;

			itemdata.element.parentNode.removeChild(itemdata.element);
			this.items.splice(itemdata.position,1);

			this.fireEvent ('removeitem', {
				"target" : itemdata.object
			});

			return this;
		};

		/**
		 * Method for moving forward a menubar item by one position. Fires the "orderchage" event.
		 *
		 * @param {HTMLLIElement|object|number} item - The item to move.
		 * @return {MenubarObject|false} Current menubar object or false on error.
		 */
		this.moveMenuItemForward = function ( item ) {
			var itemdata = this.getMenuItem( item );

			if ( itemdata === false ) return false;

			if ( itemdata.element.nextElementSibling !== null ) {
				this.elements.container.insertBefore (
					itemdata.element.nextElementSibling,
					itemdata.element
				);
				this.fireEvent ( 'orderchange', {
					"target" : itemdata.object
				});
			}

			return this;
		};

		/**
		 * Method for moving backward a menubar item by one position. Fires the "orderchage" event.
		 *
		 * @param {HTMLLIElement|object|number} item - The item to move.
		 * @return {MenubarObject|false} Current menubar object or false on error.
		 */
		this.moveMenuItemBackward = function ( item ) {
			var itemdata = this.getMenuItem ( item );

			if ( itemdata === false ) return false;

			if ( itemdata.element.previousElementSibling !== null ) {
				this.elements.container.insertBefore (
					itemdata.element,
					itemdata.element.previousElementSibling
				);
				this.fireEvent ( 'orderchange', {
					"target" : itemdata.object
				});
			}

			return this;
		};

		/**
		 * Method for moving a menubar item to a specified position. Fires the "orderchange" event.
		 *
		 * @param {HTMLLIElemen|object|nubmer} item - The item to move.
		 * @param {number} pos - Items new position.
		 * @return {MenubarObject|false} Current menubar object or false on error.
		 */
		this.moveMenuItem = function ( item, pos ) {
			var itemdata = this.getMenuItem( item );

			if ( itemdata === false ) return false;

			if ( pos < 0 || pos > this.items.length -1 ) {
				console.error ( "Position out of range" );
				return false;
			}

			this.elements.container.insertBefore (
				itemdata.element,
				this.elements.container.children[pos]
			);

			this.fireEvent ( 'orderchange', {
				"target" : itemdata.object
			});

			return this;
		};

		/**
		 * Method for disabling menubar items.
		 *
		 * @param {number|HTMLLIElement} item - Item's container element or position number to disable.
		 * @return {false|MenubarObject} If no error, current menubar object or on error false.
		 */
		this.disableItem = function ( item ) {
			var idata = this.getMenuItem ( item );
			if ( idata === false ) return false;
			idata.element.classList.add ( this.props.classNames.disabledItem );
			return this;
		};

		/**
		 * Method for enabling menubar items.
		 *
		 * @param {number|HTMLLIElement} item - Item's container element or position nubmer to enable.
		 * @return {false|MenubarObject} If no error, current menubar object or on error false.
		 */
		this.enableItem = function ( item ) {
			var idata = this.getMenuItem ( item );
			if ( idata === false ) return false;
			idata.element.classList.remove ( this.props.classNames.disabledItem );
			return this;
		};

		/**
		 * Dummy method so that menubar object works with menu object.
		 * return {MenubarObject} Current menubar object.
		 */
		this.hide = function() {
			if ( this.props.openedItem !== null ) this.props.openedItem.container.classList.remove ( this.props.classNames.opened );
			this.elements.container.classList.remove(this.props.classNames.active);
			this.props.openedItem = null;
			MenubarObject.activeItem = null;
			MenubarObject.activeObject = null;
			return this;
		};

		/**
		 * Checks if all menu items fit and if they don't puts the non fitting ones on a dropdown menu.
		 *
		 * @return {MenubarObject} Current menubar object.
		 */
		this.checkResize = function ( ) {
			var w = this.elements.container.offsetWidth;
			var iw = 0;
			var tmpw = 0;
			var pw = false;
			var cc = this.elements.container.firstElementChild;
			while ( cc !== null ) {
				tmpw = iw + cc.offsetWidth;
				if ( tmpw > w ) break;
				iw = tmpw;
				cc = cc.nextElementSibling;
			}

			
			return this;
		};

		/**
		 * Method for attaching this object to another lvzwebdesktop object or an element. Fires "attachto" event.
		 * @param {HTMLElement|object} item - The lvzwebdekstop object or an element to attach this object to.
		 * @return {false|IconareaObject} false on error, otherwise current iconarea object.
		 */
		this.attachTo = function ( item ) {
			if ( item instanceof HTMLElement ) {

				if ( typeof item.dataLVZWEBDESKTOPOBJ == 'object' ) {

					if ( typeof item.dataLVZWDESKTOPOBJ.attachItem == 'function' ) {
						item.dataLVZWDESKTOPOBJ.attachItem(this.elements.container);
					}
					else {
						console.log ( "Argument must be an element or an lvzwebdesktop object having \"attachItem\" method." );
						return false;
					}

				}
				else {
					var eobj = {
						"target" : this,
						"attachElement" : item,
						"item" : item
					};
					this.fireEvent('attachto',eobj);
					if ( eobj.defaultPrevented ) return this;
					item.appendChild ( this.elements.container );
				}
			}
			else {
				var c = window.lvzwebdesktop.isPartOf( item );
				if ( c === false || typeof c.attachItem != 'function' ) {
					console.log ( "Argument must be an element or an lvzwebdesktop object having \"attachItem\" method." );
					return false;
				}
				c.attachItem ( this );
			}
			return this;
		};

		/**
		 * Returns the first parent object of given type.
		 *
		 * @param {function|string} type - lvzwebobject constructor or constructor's name.
		 * @return {object|null|false} Return's the first parent lvzwebobject that matches "type" parameter. if not found returns null. on error returns false.
		 */
		this.getParentOfType = function ( type ) { return window.lvzwebdesktop(this.elements.container,'').getParentObject( type ); };

		/**
		 * Associative array for storing menu elements.
		 * @public
		 * @type {object}
		 */
		this.elements = {};

		/**
		 * Container element.
		 * @public
		 * @type {HTMLElement}
		 */
		this.elements.container = null;
		if ( e ) {
			this.elements.container = e;
			this.props.elementId = !e.id ? false : e.id;

			var attr;
			attr =  e.getAttribute('onadditem');
			if ( attr !== null ) this.addEvent ( 'additem', new Function ( 'event', attr )  );

			attr =  e.getAttribute('onremoveitem');
			if ( attr !== null ) this.addEvent ( 'removeitem', new Function ( 'event', attr )  );

			attr =  e.getAttribute('onorderchange');
			if ( attr !== null ) this.addEvent ( 'orderchange', new Function ( 'event', attr )  );

			attr = getComputedStyle(document.body).getPropertyValue('--lvzwebdesktop-default-menubar-theme').trim();
			if ( attr != '' ) {
				for ( var i = 0 ; i < this.elements.container.classList.length ; i ++ ) {
					if ( getComputedStyle(document.body).getPropertyValue('--lvzwebdesktop-theme-menubar-'+this.elements.container.classList[i]).trim() == 'true' ) {
						this.props.themeClass = this.elements.container.classList[i];
						break;
					}
				}
				if ( this.props.themeClass === null ) {
					this.props.themeClass = attr;
					this.elements.container.classList.add(attr);
				}
			}
			else {
				console.error ("There is no theme loaded");
			}

			var mitm = this.elements.container.firstElementChild;
			while ( mitm && mitm !== null ) {
				this.addMenuItem ( mitm );
				mitm = mitm.nextElementSibling;
			}

			if ( typeof window.lvzwebdesktop.getParentDesktop == 'function' ) {
				this.props.desktop = window.lvzwebdesktop.getParentDesktop ( this.elements.container );
				if ( this.props.desktop === false ) this.props.desktop = null;
			}

		} else {
			this.elements.container = document.createElement('ul');
			this.elements.container.classList.add ( this.props.classNames.container );
		}

		this.elements.container.dataLVZWEBDESKTOPOBJ = this;

		lvzwebdesktop.fn.addEvent.call(lvzwebdesktop, "click", function ( e ) {

			if ( MenubarObject.activeObject === null ) return;
			if ( MenubarObject.activeItem !== null &&
				 MenubarObject.activeItem.element === e.target ) return;

			var mbo = lvzwebdesktop(e.target,"").getParentObject("MenubarObject");
			var menubar = MenubarObject.activeObject;
			var mitem = MenubarObject.activeItem;

			if ( mbo !== menubar ) {

				menubar.elements.container.classList.remove(menubar.props.classNames.active);
				mitem.container.classList.remove ( menubar.props.classNames.opened );

				if ( mitem.submenu && mitem.submenu !== null ) mitem.submenu.hide();
				menubar.props.openedItem = null;

				MenubarObject.activeItem = null;
				MenubarObject.activeObject = null;
			}
		});

	};


	/**
	 * List with all menu objects.
	 * @type {Array}
	 * @public
	 */
	MenubarObject.list = [];

	/**
	 * Property for keeping the active menubar object.
	 * @type {MenubarObject|null}
	 * @public
	 */
	MenubarObject.activeObject = null;

	/**
	 * Property for keeping the active menubar item.
	 * @type {object|null}
	 * @public
	 */
	MenubarObject.activeItem = null;

	/**
	 * Associative array for menubar object's default class names.
	 * @type {object}
	 * @public
	 */
	MenubarObject.classNames = {};

	/**
	 * Default class name for menubar objects container elements.
	 * @public
	 * @type {string}
	 */
	MenubarObject.classNames.container = 'web-desktop-menubar';

	/**
	 * Default class name for menubar objects container elements.
	 * @public
	 * @type {string}
	 */
	MenubarObject.classNames.active = 'active';

	/**
	 * Default class name for menubar objects container elements.
	 * @public
	 * @type {string}
	 */
	MenubarObject.classNames.opened = 'opened';

	/**
	 * Default class name for menubar objects container elements.
	 * @public
	 * @type {string}
	 */
	MenubarObject.classNames.disabledItem = 'disabled';


	window.lvzwebdesktop.MenubarObject = MenubarObject;
	window.lvzwebdesktop.constructors.push({
		"selector" : "."+MenubarObject.classNames.container+",web-menubar",
		"constructor" : MenubarObject
	});

	/**
	 * Function for controling mouse click on menubar item.
	 *
	 * @param {Event} e - Event object.
	 */
	function menubaritemclick ( e ) {
		var menubar = e.target.dataMenubarObject;
		var mitem = e.target.dataMenuItemObject;
		var elem = e.target;

		if ( elem.nodeName !== 'LI' ) elem = elem.parentNode;

		if ( MenubarObject.activeSubmenu ) MenubarObject.activeSubmenu.hide();

		if ( elem.classList.contains ( menubar.props.classNames.disabledItem ) ) {
			console.log ( 'disable click add event here ' );
			e.preventDefault();
			return false;
		}

		// If clicked menuitem is opened
		if ( menubar.props.openedItem === mitem ) {
			menubar.elements.container.classList.remove(menubar.props.classNames.active);
			mitem.container.classList.remove ( menubar.props.classNames.opened );
			menubar.props.openedItem = null;

			MenubarObject.activeItem = null;
			MenubarObject.activeObject = null;

		}
		// If clicked menuitem isn't opened
		else {

			// If there is a submenu container element
			if ( mitem.container.lastElementChild !== mitem.container.firstElementChild ) {
				mitem.submenu = mitem.container.lastElementChild.dataLVZWEBDESKTOPOBJ;
				mitem.submenu.menubar = menubar;
			}
			
			if ( ! menubar.elements.container.classList.contains (menubar.props.classNames.active) )
				menubar.elements.container.classList.add(menubar.props.classNames.active);

			if ( menubar.props.openedItem !== null ) menubar.props.openedItem.container.classList.remove ( menubar.props.classNames.opened );
			menubar.props.openedItem = mitem;
			mitem.container.classList.add ( menubar.props.classNames.opened );

			MenubarObject.activeObject = menubar;
			MenubarObject.activeItem = mitem;

			if ( mitem.container.lastElementChild.dataMenuObject ) {
				mitem.container.lastElementChild.dataMenuObject.parentMenu = menubar;
				mitem.container.lastElementChild.dataMenuObject.elements.container.style.display = '';
			}

			// If clicked item has a submenu
			if ( mitem.submenu ) {
				var d = MenubarObject.activeObject.props.desktop;
				if ( d === null) d = document.body;
				d.appendChild ( mitem.submenu.elements.container );
				mitem.submenu.show ( mitem.container );
				MenubarObject.activeSubmenu = mitem.submenu;
			}
			// If clicked item doesn't have a submenu
			else {
				menubar.elements.container.classList.remove(menubar.props.classNames.active);
				mitem.container.classList.remove ( menubar.props.classNames.opened );
				menubar.props.openedItem = null;
				MenubarObject.activeObject = null;
				MenubarObject.activeItem = null;
			}
		}

		(new Function ( 'event', elem.onitemclick )).call ( elem, {target: elem} );

		menubar.fireEvent('itemclick', {
			"target" : menubar,
			"menuItem" : elem
		});

		return null;
	}

	/**
	 * Function for controling mouse over event on menubar item.
	 *
	 * @param {Event} e - Event object.
	 */
	function menubaritemmover ( e ) {
		var menubar = e.target.dataMenubarObject;
		var mitem = e.target.dataMenuItemObject;

		if ( ! menubar.elements.container.classList.contains (menubar.props.classNames.active) ) return;
		if ( menubar.props.openedItem === mitem ) return;

		if ( menubar.props.openedItem !== null ) {
			menubar.props.openedItem.container.classList.remove ( menubar.props.classNames.opened );
		}

		menubar.props.openedItem = mitem;
		MenubarObject.activeItem = mitem;
		mitem.container.classList.add ( menubar.props.classNames.opened );


		if ( MenubarObject.activeSubmenu ) MenubarObject.activeSubmenu.hide();

		if ( mitem.container.lastElementChild !== mitem.container.firstElementChild ) mitem.submenu = mitem.container.lastElementChild.dataLVZWEBDESKTOPOBJ;

		if ( mitem.container.classList.contains ( menubar.props.classNames.disabledItem ) === false && mitem.submenu ) {
			document.body.appendChild ( mitem.submenu.elements.container );
			mitem.submenu.show ( mitem.container );
			MenubarObject.activeSubmenu = mitem.submenu;
		}

		if ( mitem.container.lastElementChild.dataMenuObject ) {
			mitem.container.lastElementChild.dataMenuObject.parentMenu = menubar;
			mitem.container.lastElementChild.dataMenuObject.elements.container.style.display = '';
		}
	}

	/**
	 * Constructor for creating menu objects.
	 * @constructor
	 * @param {HTMLElement} e - Element to attach menu to.
	 */
	var MenuObject = function ( e ) {
		MenuObject.list.push ( this );

		/**
		 * Type of object.
		 * @public
		 * @type {'menu'}
		 */
		this.type = 'menu';

		/**
		 * Parent menu object if exists. Null otherwise.
		 * @public
		 * @type {MenuObject|null}
		 */
		this.parentMenu = null;

		/**
		 * the menubar object if belongs to that object otherwise null.
		 * @public
		 * @type {MenubarObject|null}
		 */
		this.menubar = null;

		/**
		 * Associative array for storing menu properties values.
		 * @public
		 * @type {Object}
		 */
		this.props = {};

		/**
		 * The id of container element.
		 * @public
		 * @type {false|string}
		 */
		this.props.elementId = false;

		/**
		 * CSS class name for theme in use.
		 * @type {null|string}
		 * @public
		 */
		this.props.themeClass = null;

		/**
		 * The element of the menu item that is selected or null otherwise.
		 * @public
		 * @type {HTMLElement|null}
		 */
		this.props.selectedItem = null;

		/**
		 * Defines if menu can take focus.
		 * @public
		 * @type {boolean}
		 */
		this.props.canHaveFocus = false;

		/**
		 * True if menu has focus, false otherwise.
		 * @public
		 * @type {boolean}
		 */
		this.props.focused = false;

		/**
		 * Contains the top position of menu if has been set or false otherwise.
		 * @public
		 * @type {false|number}
		 */
		this.props.top = false;

		/**
		 * Container the left position of menu if has been set of false otherwise.
		 * @public
		 * @type {false|number}
		 */
		this.props.left = false;

		/**
		 * If true then hide menu when clicking away from menu.
		 * @public
		 * @type {boolean}
		 */
		this.props.hideOnClickAway = true;

		/**
		 * If true then hide menu on clicking menu item.
		 * @public
		 * @type {boolean}
		 */
		this.props.hideOnItemClick = true;

		/**
		 * Setting the maximum height of menu. If has the value "false", then there it has no maximum height.
		 * @public
		 * @type {false|number}
		 */
		this.props.maxHeight = false;

		/**
		 * Contains class names strings of menu object elements.
		 * @public
		 * @type {object}
		 */
		this.props.classNames = {};

		/**
		 * Class name of menu container element.
		 * @public
		 * @type {string}
		 */
		this.props.classNames.container = MenuObject.classNames.container;

		/**
		 * Class name for menu scroll-up button element.
		 * @public
		 * @type {string}
		 */
		this.props.classNames.scrollupbtn = MenuObject.classNames.scrollupbtn;

		/**
		 * Class name for menu scroll-down button element.
		 * @public
		 * @type {string}
		 */
		this.props.classNames.scrolldownbtn = MenuObject.classNames.scrolldownbtn;

		/**
		 * Class name for the selected item of menu.
		 * @public
		 * @type {string}
		 */
		this.props.classNames.selectedItem = MenuObject.classNames.selectedItem;

		/**
		 * Class name for disabled items of menu.
		 * @public
		 * @type {string}
		 */
		this.props.classNames.disabledItem = MenuObject.classNames.disabledItem;

		/**
		 * Class name for separator items of menu.
		 * @public
		 * @type {string}
		 */
		this.props.classNames.separator = MenuObject.classNames.separator;

		/**
		 * Array for storing event functions on adding items on menu.
		 * @type {Array}
		 * @public
		 */
		this.onadditem = [];

		/**
		 * Array for storing event functions on removing items  on menu.
		 * @type {Array}
		 * @public
		 */
		this.onremoveitem = [];

		/**
		 * Array for storing event functions on showing menu.
		 * @type {Array}
		 * @public
		 */
		this.onshow = [];

		/**
		 * Array for storing event functions on hidding menu.
		 * @type {Array}
		 * @public
		 */
		this.onhide = [];

		/**
		 * Array for storing event functions for when clicking item.
		 * @type {Array}
		 * @public
		 */
		this.onitemclick = [];

		/**
		 * Array for storing event functions for when moving items within the menu.
		 * @type {Array}
		 * @public
		 */
		this.onchangeorder = [];

		/**
		 * Array for storing event functions for when taking focus.
		 * @type {Array}
		 * @public
		 */
		this.onfocus = [];

		/**
		 * Array for storing event functions for when loosing focus.
		 * @type {Array}
		 * @public
		 */
		this.onblur = [];

		/**
		 * Array for storing event functions for when changing position of menu.
		 * @type {Array}
		 * @public
		 */
		this.onmove = [];

		/**
		 * Array for menu items.
		 * @public
		 * @type {Array}
		 */
		this.items = [];

		/**
		 * Method for adding events to menu object.
		 *
		 * @param {string} e - Event name.
		 * @param {function} f - Function to run on event.
		 * @return {MenuObject|false} False on error, this menu object otherwise.
		 */
		this.addEvent = window.lvzwebdesktop.fn.addEvent;

		/**
		 * Method for removing events from menu object.
		 *
		 * @param {string} e - The name of the event to remove.
		 * @param {function} [f] - The function to remove. If this param is set then method will remove only the function that matches. Otherwise it will clear all functions from event.
		 * @return {MenuObject|false} False on error, this menu object otherwise.
		 */
		this.removeEvent = window.lvzwebdesktop.fn.removeEvent;

		/**
		 * Method for firing events.
		 *
		 * @param {string} e - The name of the event to fire.
		 * @param {object} obj - An object with properties to add to event object.
		 * @return {menuObject|false} False on error, this menu object otherwise.
		 */
		this.fireEvent = window.lvzwebdesktop.fn.fireEvent;

		/**
		 * Method for getting data of menuitem by giving item's position or item's element or item's object.
		 * @param {number|object|HTMLLIElement} item - Item's position or items object or items element.
		 * @return {object|false} Object with menu data { object, element, position} or false on error.
		 */
		this.getMenuItem = function ( item ) {

			// Get item's data by element
			if ( item instanceof HTMLLIElement ) {

				if ( item.parentNode !== this.elements.container ) {
					console.error ( "Menu item element does not belong to this menu" );
					return false;
				}

				return {
					"element": item,
					"position": this.items.indexOf ( item )
				};

			}

			// Get item's data by position number
			else if ( typeof item == 'number' || item instanceof Number ) {

				if ( item < 0 || item >= this.items.length ) {
					console.error ( "Argument out of range" );
					return false;
				}

				return {
					"element": this.items[item],
					"position": item
				};

			}

			console.error ( "Given argument is not a number nor menu item container" );
			return false;
		};

		/**
		 * Method for displaying menu. Fires "show" event.
		 * 
		 * @param {HTMLElement} [underElement] - If is set with an HTMLElement, the menu will be displayed under that element.
		 * @return {MenuObject} This menu Object.
		 */
		this.show = function ( underElement ) {
			this.elements.container.style.display = '';

			CheckClicks ( { "target" : this.elements.container } );

			if ( underElement ) {
				if ( underElement instanceof HTMLElement ) {
					MenuObject.btn = underElement;
					this.elements.container.style.position = 'absolute';
					var eoffset = MenuObject.btn.getBoundingClientRect();
					var oth = ( eoffset.top + underElement.offsetHeight + scrollY);
					this.elements.container.style.top = oth + 'px';
					this.elements.container.style.left = eoffset.left + 'px';
				} else  if ( typeof underElement.top == 'number' &&
							 typeof underElement.left == 'number' ) {
					this.elements.container.style.position = 'absolute';
					this.elements.container.style.top = underElement.top + 'px';
					this.elements.container.style.left = underElement.left + 'px';
				}

			} else {
				this.elements.container.style.position = '';
				this.elements.container.style.top = '';
				this.elements.container.style.left = '';
			}

			if ( this.parentMenu === null ) MenuObject.activeMenu = this;

			// TEMP CODE FOR "onshow" menu item's attribute.
			for ( var i = 0; i < this.elements.container.children.length; i ++ ) {
				var tmpattr=this.elements.container.children[i].getAttribute('onshow');
				if ( tmpattr !== null )(new Function(tmpattr)).call(this.elements.container.children[i]);
			}
			// / TEMP CODE

			this.fireEvent('show',{ "target" : this });

			document.body.addEventListener('keydown', windowKeyboard );

			var innerH = this.getMenuInnerHeight();
			if ( this.props.maxHeight !== false &&
				 innerH > this.props.maxHeight ) {

				this.elements.scrollupbtn.style.top = this.elements.container.scrollTop + 'px';
				this.elements.scrolldownbtn.style.bottom = this.elements.container.scrollTop + 'px';
				this.elements.scrollupbtn.style.display = 'block';
				this.elements.scrolldownbtn.style.display = 'block';

				this.elements.scrolldownspace.style.height = this.elements.scrolldownbtn.offsetHeight + 'px';
				this.elements.scrolldownspace.style.display = '';
				this.elements.container.style.paddingTop = this.elements.scrollupbtn.offsetHeight + 'px';

				//this.elements.container.style.paddingTop = this.elements.scrollupbtn.offsetHeight + 'px';
				//this.elements.scrolldownbtn.style.marginBottom = this.elements.scrolldownbtn.offsetHeight + 'px';
			}

			this.fixPosition(underElement);

			return this;
		};

		/**
		 * Method for hidding menu. Fires "hide" event.
		 *
		 * @return {MenuObject} This menu object.
		 */
		this.hide = function () {
			this.elements.container.style.display = 'none';
			this.fireEvent('hide',{ "target" : this });
			if ( this.parentMenu === null ) MenuObject.activeMenu = null;
			document.body.removeEventListener('keydown', windowKeyboard );
			return this;
		};

		/**
		 * Toggles vidibility for menu object.
		 *
		 * @param {HTMLElement} [underElement] - If is set with an HTMLElement, the menu will be displayed under that element.
		 * @return {MenuObject} This menu object.
		 */
		this.toggle = function ( underElement ) {
			if ( this.elements.container.style.display == 'none' )
				return this.show ( underElement );
			return this.hide();
		};

		/**
		 * Set top position of menu. Fires "move" event.
		 * @param {number} t - Menu top position in number of pixels.
		 * @return {MenuObject} This menu object.
		 */
		this.setTop = function ( t ) {
			var eobj = {
				"target" : this,
				"newTop" : t,
				"oldTop" : this.props.top
			};
			this.fireEvent('move', eojb);
			if ( eobj.defaultPrevented === true ) return this;

			this.props.top = t;
			this.elements.container.style.top = t + 'px';
			return this;
		};

		/**
		 * Set left position of menu. Fires "move" event.
		 * @param {number} l - Menu left posotion in number of pixels.
		 * @return {MenuObject} This menu object.
		 */
		this.setLeft = function ( l ) {
			var eobj =  {
				"target" : this,
				"newLeft" : l,
				"olfLeft" : this.props.left
			};
			this.fireEvent('move', eobj);
			if ( eobj.defaultPrevented === true ) return this;

			this.props.left = l;
			this.elements.container.style.left = l + 'px';
			return this;
		};

		/**
		 * Reposition menu so that it fits into it's parent element.
		 *
		 * @return {MenuObject} This menu object.
		 */
		this.fixPosition = function ( underElement ) {
			var pnodeelem = this.elements.container.parentNode;

			var moffset = this.elements.container.getBoundingClientRect();
			var w = this.elements.container.offsetWidth;
			var h = this.elements.container.offsetHeight;
			var t = moffset.top;
			var l = moffset.left;

			var p = pnodeelem;
			while ( p.nodeName != '#document' ) {
				if ( p.dataMenuObject ) p = p.parentNode;
				else break;
			}
			var poffset = p.getBoundingClientRect();
			var pw = p.offsetWidth;
			var ph = p.offsetHeight;
			var pt = poffset.top;
			var pl = poffset.left;

			if ( pnodeelem === document.body ) {
				pt = 0;
				pl = 0;
				pw = window.innerWidth;
				ph = window.innerHeight;
			}

			// If menu appears after bottom border
			if ( t + h > ph ) {
				// t = t - ( (t+h) - ph );
				t = ph - h;
				t += ( scrollMaxY - (scrollMaxY - scrollY));
				this.elements.container.style.top = t+'px';
			}

			// If menu appears before top border
			if ( t < pt ) {
				this.elements.container.style.top = pt+'px';
			}

			// If menu appears after right border
			if ( l + w > pw ) {
				// l = l - ( (l+w) - pw );
				l = pw - w;
				l += ( scrollMaxX - (scrollMaxX - scrollX));
				this.elements.container.style.left = l+'px';
			}

			// If menu appears before left border
			if ( l < pl ) {
				this.elements.container.style.left = pl+'px';
			}

			return this;
		};

		/**
		 * Method for adding items on menu. Fires the "additem" event.
		 *
		 * @param {string|HTMLLIElement} captionOrElement - The items caption or the HTMLLIElement.
		 * @param {function} action - A function to run on item click.
		 * @return {MenuObject} This menu object.
		 */
		this.addItem = function ( captionOrElement, action ) {
			if ( captionOrElement instanceof HTMLLIElement ) {
				var tmpElement = captionOrElement;
				if ( captionOrElement.children.length > 0 && captionOrElement.children[0] instanceof HTMLSpanElement ) {
					var captionElement = captionOrElement.children[0];
				} else {
					var captionElement = document.createElement('span');
					tmpElement.appendChild ( captionElement );
				}
				var caption = captionElement.innerText;
			} else if ( typeof captionOrElement == 'string' ) {
				var tmpElement  = document.createElement('li');
				var captionElement = document.createElement('span');
				var caption = captionOrElement;
				captionElement.innerText = caption;
				tmpElement.appendChild ( captionElement );
				this.elements.container.appendChild ( tmpElement );
			} else if ( typeof captionOrElement == 'object' ) {
				var tmpElement  = document.createElement('li');
				var captionElement = document.createElement('span');
				var caption = captionOrElement.caption;
				var iconurl = captionOrElement.icon;

				tmpElement.style.backgroundImage = "url('" + iconurl + "')";
				tmpElement.style.backgroundSize = "16px 16px";
				tmpElement.style.backgroundRepeat = "no-repeat";
				tmpElement.style.paddingLeft = "18px";

				captionElement.innerText = caption;
				tmpElement.appendChild ( captionElement );
				this.elements.container.appendChild ( tmpElement );
			}

			// if ( tmpElement.onclick !== null )console.log ( tmpElement.onclick.toString() );
			var tmpclick = tmpElement.getAttribute('onclick');
			tmpElement.setAttribute('onclick','');
			tmpElement.onitemclick = tmpElement.getAttribute('onitemclick');
			if ( tmpElement.onitemclick === null ) tmpElement.onitemclick = '';
			if ( tmpclick !== null ) tmpElement.onitemclick += tmpclick;
			if ( tmpElement.onitemclick === null ) tmpElement.onitemclick = '';
			if ( tmpElement.onclick !== null ) tmpElement.onitemclick += " ( " + tmpElement.onclick.toString() + ")(event);";
			tmpElement.onclick = null;

			tmpElement.addEventListener ( 'mouseover' , MenuItemMouseOver );
			tmpElement.addEventListener ( 'mouseout' , MenuItemMouseOut );
			tmpElement.addEventListener ( 'click' , MenuItemClick );
			tmpElement.dataMenuObject = this;
			tmpElement.dataLVZWDESKTOPOBJ = this;

			if ( typeof action == 'function' ) captionElement.addEventListener ( 'click' , action );

			this.items.push ( tmpElement );

			this.fireEvent('additem',{
				"target" : this,
				"item" : tmpElement
			});
			return this;
		};

		/**
		 * Method for removing items on menu. Fires the "removeitem" event.
		 *
		 * @param {number|HTMLLIElement} itemPositionOrContainerElement - The menu item's container element or item's position to remove.
		 * @return {false|MenuObject} This menu object.
		 */
		this.removeItem = function ( itemPositionOrContainerElement ) {

			if ( typeof itemPositionOrContainerElement == 'number' ||
				 itemPositionOrContainerElement instanceof Number) {
				// Removing menu item by order number

				if ( itemPositionOrContainerElement < 0 ||
					 itemPositionOrContainerElement > this.items.length -1 ) {
					console.error ("Error on removing item. Out of range.");
					return false;
				}

				var tmp = this.items[itemPositionOrContainerElement];

				this.items[itemPositionOrContainerElement].remove();
				this.items.splice(itemPositionOrContainerElement, 1);

				this.fireEvent('removeitem',{
					"method" : "removeByOrderNumber",
					"target" : this,
					"removedItem" : tmp,
					"wasInPosition" : itemPositionOrContainerElement
				});

			} else if ( itemPositionOrContainerElement instanceof HTMLLIElement ) {
				// Removing menu item by item container element

				if ( itemPositionOrContainerElement.dataMenuObject !== this ) {
					console.error ( "Element is not menu item container" );
					return false;
				}

				var pos = this.items.indexOf ( itemPositionOrContainerElement );

				itemPositionOrContainerElement.remove();
				this.items.splice(pos,1);

				this.fireEvent('removeitem',{
					"method" : "removeByItemContainerElement",
					"target" : this,
					"removedItem" : itemPositionOrContainerElement,
					"wasInPosition" : pos
				});
				
			} else {

				console.error ( "Removing menu item: Given argument is not a number nor menu item container" );
				return false;

			}

			return this;

		};

		/**
		 * Method for disabling menu items.
		 *
		 * @param {number|HTMLLIElement} item - Item's container element or position number to disable.
		 * @return {false|MenuObject} The menu object or false on error.
		 */
		this.disableItem = function ( item ) {
			var idata = this.getMenuItem ( item );
			if ( idata === false ) return false;
			idata.element.classList.add  ( this.props.classNames.disabledItem );
			return this;
		};

		/**
		 * Method for enabling menu items.
		 *
		 * @param {number|HTMLLIElement} item - Item's container element or position number to disable.
		 * @return {false|MenuObject} The menu object of false on error.
		 */
		this.enableItem = function ( item ) {
			var idata = this.getMenuItem ( item );
			if ( idata === false ) return false;
			idata.element.classList.remove ( this.props.classNames.disabledItem );
			return this;
		};

		/**
		 * Method for moving menu items up. Fires the "changeorder" event.
		 *
		 * @param {number|HTMLLIElement} item - Item's container element or position number to disable.
		 * @return {false|MenuObject} This menu object or false on error.
		 */
		this.moveItemUp = function ( item ) {
			var idata = this.getMenuItem ( item );
			if ( idata === false ) return false;

			if ( idata.element.previousElementSibling === null ) return this;
			this.elements.container.insertBefore (idata.element, idata.element.previousElementSibling );
			this.items[idata.position] = this.items[position - 1];
			this.items[idata.position - 1] = idata.element;
			
			this.fireEvent('changeorder',{
				"target" : this,
				"movedItem" : idata.element,
				"oldPosition" : idata.position,
				"newPosition" : idata.position-1
			});

			return this;
		};

		/**
		 * Method for moving menu items down. Fires the "changeorder" event.
		 *
		 * @param {number|HTMLLIElement} item - Item's container element or position number to disable.
		 * @return {false|MenuObject} This menu object or false on error.
		 */
		this.moveItemDown = function ( item ) {
			var idata = this.getMenuItem ( item );
			if ( idata === false ) return false;

			if ( item.nextElementSibling === null ) return this;

			this.elements.container.insertBefore ( idata.element.nextElementSibling, idata.element  );
			this.items[idata.position] = this.items[idata.position + 1];
			this.items[idata.position + 1] = idata.element;
			
			this.fireEvent('changeorder',{
				"target" : this,
				"movedItem" : idata.element,
				"oldPosition" : idata.position,
				"newPosition" : idata.position+1
			});

			return this;
		};

		/**
		 * Method for getting all selectable items (all items but not the  separators and scrolling controls).
		 * @return {array} An array with all selectable items.
		 */
		this.getSelectableItems = function () {
			var e = this.elements.container.firstElementChild;
			var ret = [];
			while ( e !== null ) {
				if ( ! e.classList.contains ( this.props.classNames.separator ) &&
					 typeof e.dataDontHideOnClick == 'undefined'
				   ) ret.push( e );
				e = e.nextElementSibling;
			}
			return ret;
		};

		/**
		 * Method for selecting the next item.
		 * @return {MenuObject} Current menu object.
		 */
		this.selectNextItem = function () {
			var items = this.getSelectableItems();
			var selected = items.indexOf (this.props.selectedItem);

			if ( items.length == 0 ) return this;
			if ( items.length == 1 ) {
				this.props.selectedItem = items[0];
			}
			else if ( this.props.selectedItem !== null ) {
				this.props.selectedItem.classList.remove(this.props.classNames.selectedItem);

				if ( selected == items.length - 1 )
					this.props.selectedItem = items[0];
				else
					this.props.selectedItem = items[selected+1];
			}
			else
				this.props.selectedItem = items[0];

			this.props.selectedItem.focus();
			this.props.selectedItem.classList.add(this.props.classNames.selectedItem);
			scrollMenu ( this );

			return this;
		};

		/**
		 * Method for selecting the previous item.
		 * @return {MenuObject} Current menu object.
		 */
		this.selectPreviousItem = function () {
			var items = this.getSelectableItems();
			var selected = items.indexOf (this.props.selectedItem);

			if ( items.length == 0 ) return this;
			if ( items.length == 1 ) {
				this.props.selectedItem = items[0];
			}
			else if ( this.props.selectedItem !== null ) {
				this.props.selectedItem.classList.remove(this.props.classNames.selectedItem);

				if ( selected == 0 )
					this.props.selectedItem = items[items.length - 1];
				else
					this.props.selectedItem = items[selected-1];
			}
			else
				this.props.selectedItem = items[items.length - 1];

			this.props.selectedItem.focus();
			this.props.selectedItem.classList.add(this.props.classNames.selectedItem);
			scrollMenu ( this );

			return this;
		};

		/**
		 * Method for getting the total height of all menu items.
		 * @return {number} Total height of all menu items in number of pixels.
		 */
		this.getMenuInnerHeight = function () {
			var innerHeight = 0;
			if ( this.items.length == 0 ) return 0;
			var p = this.items[0].parentNode.parentNode;
			for ( var i = 0; i < this.items.length; i ++ ) {
				document.body.appendChild ( this.items[i].parentNode );
				innerHeight += this.items[i].offsetHeight;
				p.appendChild ( this.items[i].parentNode );
			}
			return innerHeight;
		};

		/**
		 * Method for moving menu items to specified position.s Fire the "changeorder" event.
		 *
		 * @param {number|HTMLLIElement} item - Item's container element or position number to disable.
		 * @param {number} moveItemToPosition - The position number to move menu item to.
		 * @return {false|MenuObject} This menu object of false on error.
		 */
		this.moveItem = function ( item, moveItemToPosition ) {
			var idata = this.getMenuItem ( item );
			if ( idata === false ) return false;


			var iteminpos = null;

			if ( moveItemToPosition < 0 || moveItemToPosition >= this.items.length) {
				console.error ( "Argument \"moveItemToPosition\" out of range" );
				return false;
			}

			if ( idata.position == moveItemToPosition ) return this;

			iteminpos = this.items[moveItemToPosition];

			this.elements.container.insertBefore ( idata.element, iteminpos  );

			this.items[idata.position] = iteminpos;
			this.items[moveItemToPosition] = idata.element;

			return this;
		};

		/**
		 * Gives focus to this menu object.
		 *
		 * @return {MenuObject} This menu object.
		 */
		this.focus = function () {
			if ( this.desktop !== null ) this.desktop.giveFocus(this);
			return this;
		};

		/**
		 * Returns the first parent object of given type.
		 *
		 * @param {function|string} type - lvzwebobject constructor or constructor's name.
		 * @return {object|null|false} Return's the first parent lvzwebobject that matches "type" parameter. if not found returns null. on error returns false.
		 */
		this.getParentOfType = function ( type ) { return window.lvzwebdesktop(this.elements.container,'').getParentObject( type ); };

		/**
		 * Associative array for storing menu elements.
		 * @public
		 * @type {object}
		 */
		this.elements = {};

		/**
		 * Container element.
		 * @public
		 * @type {HTMLElement}
		 */
		this.elements.container = null;
		if ( e ) {
			this.elements.container = e;
			this.props.elementId = !e.id ? false : e.id;

			if ( e.parentNode instanceof HTMLLIElement && typeof e.parentNode.parentNode.dataMenuObject == 'object' ) {
				this.parentMenu = e.parentNode.parentNode.dataMenuObject;

				var spn = document.createElement('span');
				spn.classList.add ('submenuicon');
				spn.appendChild ( document.createTextNode('\u25BA') );
				e.parentNode.appendChild ( spn );
				e.parentNode.dataSubMenu = this;
			}

			var tmpattr;

			tmpattr =  e.getAttribute('onitemclick');
			if ( tmpattr !== null ) this.addEvent ( 'itemclick', new Function ( 'event', tmpattr )  );

			tmpattr =  e.getAttribute('onadditem');
			if ( tmpattr !== null ) this.addEvent ( 'additem', new Function ( 'event', tmpattr )  );

			tmpattr =  e.getAttribute('onremoveitem');
			if ( tmpattr !== null ) this.addEvent ( 'removeitem', new Function ( 'event', tmpattr )  );

			tmpattr =  e.getAttribute('onchangeorder');
			if ( tmpattr !== null ) this.addEvent ( 'changeorder', new Function ( 'event', tmpattr )  );

			tmpattr =  e.getAttribute('onfocus');
			if ( tmpattr !== null ) this.addEvent ( 'focus', new Function ( 'event', tmpattr )  );

			tmpattr =  e.getAttribute('onblur');
			if ( tmpattr !== null ) this.addEvent ( 'blur', new Function ( 'event', tmpattr )  );

			tmpattr =  e.getAttribute('onhide');
			if ( tmpattr !== null ) this.addEvent ( 'hide', new Function ( 'event', tmpattr )  );

			tmpattr =  e.getAttribute('onshow');
			if ( tmpattr !== null ) this.addEvent ( 'show', new Function ( 'event', tmpattr )  );

			tmpattr =  e.getAttribute('hide-on-click-away');
			if ( tmpattr == 'true' ) this.props.hideOnClickAway = true;
			else if ( tmpattr == 'false' ) this.props.hideOnClickAway = false;

			tmpattr =  e.getAttribute('hide-on-item-click');
			if ( tmpattr == 'true' ) this.props.hideOnItemClick = true;
			else if ( tmpattr == 'false' ) this.props.hideOnItemClick = false;

			tmpattr = getComputedStyle(document.body).getPropertyValue('--lvzwebdesktop-default-menu-theme').trim();
			if ( tmpattr != '' ) {
				for ( var i = 0 ; i < this.elements.container.classList.length ; i ++ ) {
					if ( getComputedStyle(document.body).getPropertyValue('--lvzwebdesktop-theme-menu-'+this.elements.container.classList[i]).trim() == 'true' ) {
						this.props.themeClass = this.elements.container.classList[i];
						break;
					}
				}
				if ( this.props.themeClass === null ) {
					this.props.themeClass = tmpattr;
					this.elements.container.classList.add(tmpattr);
				}
			}
			else {
				console.error ("There is no theme loaded");
			}

		} else {
			this.elements.container = document.createElement('ul');
		}
		this.elements.container.classList.add ( this.props.classNames.container );
		this.elements.container.dataMenuObject = this;
		this.elements.container.dataLVZWEBDESKTOPOBJ = this;
		this.elements.container.addEventListener( 'contextmenu', function (e) { e.preventDefault(); return false; } );
		this.elements.container.addEventListener( 'wheel', wheelScrollMenu );

		// Read CSS rules and pass to object properties
		var tmpcssrule;
		tmpcssrule = getComputedStyle(this.elements.container).getPropertyValue('max-height').trim().match(/^([0-9]+([.][0-9]+)?)px$/);
		if ( tmpcssrule !== null ) {
			this.props.maxHeight = parseFloat (tmpcssrule[1]);
			this.elements.container.style.maxHeight = this.props.maxHeight + 'px';
		}

		/**
		 * Element for scrolling up menu items if they're hidden cause they didn't fit in menu.
		 * @public
		 * @type {HTMLLIElement}
		 */
		this.elements.scrollupbtn = document.createElement('li');
		this.elements.scrollupbtn.classList.add ( this.props.classNames.scrollupbtn );
		this.elements.scrollupbtn.dataDirection = 'up';
		this.elements.scrollupbtn.dataDontHideOnClick = true;
		this.elements.scrollupbtn.dataMenuObject = this;
		this.elements.scrollupbtn.dataLVZWEBDESKTOPOBJ = this;
		this.elements.container.insertBefore( this.elements.scrollupbtn, this.elements.container.firstElementChild );
		this.elements.scrollupbtn.addEventListener ( 'click', function ( e ) {
			var self = e.target.dataLVZWEBDESKTOPOBJ;
			if ( self.elements.container.scrollTop > 0 )
				self.elements.container.scrollTop -= 10;
			else
				self.elements.container.scrollTop = 0;
			self.elements.scrollupbtn.style.top = self.elements.container.scrollTop + 'px';
			self.elements.scrolldownbtn.style.bottom = -self.elements.container.scrollTop + 'px';
		});
		this.elements.scrollupbtn.addEventListener ( 'mouseover', menuMouseOverScroll );
		this.elements.scrollupbtn.addEventListener ( 'mouseout', menuMouseOutScroll );

		/**
		 * Element for scrolling down menu items if they're hidden cause the din't fit in menu.
		 * @public
		 * @type {HTMLLIElement}
		 */
		this.elements.scrolldownbtn = document.createElement('li');
		this.elements.scrolldownbtn.classList.add ( this.props.classNames.scrolldownbtn );
		this.elements.scrolldownbtn.dataDirection = 'down';
		this.elements.scrolldownbtn.dataDontHideOnClick = true;
		this.elements.scrolldownbtn.dataMenuObject = this;
		this.elements.scrolldownbtn.dataLVZWEBDESKTOPOBJ = this;
		this.elements.container.appendChild ( this.elements.scrolldownbtn );
		this.elements.scrolldownbtn.addEventListener ( 'click', function ( e ) {
			var self = e.target.dataLVZWEBDESKTOPOBJ;
			if ( self.elements.container.scrollTop < self.elements.container.scrollTopMax )
				self.elements.container.scrollTop += 10;
			else
				self.elements.container.scrollTop = self.elements.container.scrollTopMax;
			self.elements.scrollupbtn.style.top = self.elements.container.scrollTop + 'px';
			self.elements.scrolldownbtn.style.bottom = -self.elements.container.scrollTop + 'px';
		});
		this.elements.scrolldownbtn.addEventListener ( 'mouseover', menuMouseOverScroll );
		this.elements.scrolldownbtn.addEventListener ( 'mouseout', menuMouseOutScroll );

		this.elements.scrolldownspace = document.createElement('li');
		this.elements.scrolldownspace.style.display = 'none';
		this.elements.scrolldownspace.dataDontHideOnClick = true;
		this.elements.scrolldownspace.dataMenuObject = this;
		this.elements.scrolldownspace.dataLVZWEBDESKTOPOBJ = this;
		this.elements.container.appendChild ( this.elements.scrolldownspace );

		for ( var i = 0; i < this.elements.container.children.length; i ++ )
			this.addItem ( this.elements.container.children[i] );
	};

	/**
	 * Runs when clicking a menu item. Fires "itemclick" event.
	 *
	 * @param {Event} e - Event object.
	 */
	function MenuItemClick ( e ) {
		if (typeof e.preventDefault == 'function' ) e.preventDefault();
		e.stopPropagation();
		var elem = e.target, self = null;
		do {
			if ( typeof elem.dataMenuObject == 'undefined' ) elem = elem.parentNode;
			else {
				self = elem.dataMenuObject;
				break;
			}
		} while  ( typeof elem.nodeName != '#document' );
		if ( self === null ) return false;

		if ( elem.classList.contains ( self.props.classNames.disabledItem ) ) {
			console.log ( ' disable click add event here ' );
			return false;
		}

		// If there is a checkbox or radio button
		if ( elem.firstElementChild.nodeName == 'LABEL' && elem.firstElementChild.firstElementChild.nodeName == 'INPUT' ) {
			if ( elem.firstElementChild.firstElementChild.type == 'checkbox' ) {
				elem.firstElementChild.firstElementChild.checked = ! elem.firstElementChild.firstElementChild.checked;
			}
			else if ( elem.firstElementChild.firstElementChild.type == 'radio' && ! elem.firstElementChild.firstElementChild.checked )
				elem.firstElementChild.firstElementChild.checked = true;
		}

		(new Function ( 'event', elem.onitemclick )).call ( elem, {target: elem} );

		self.fireEvent('itemclick',{
			"target" : self,
			"menuItem" : elem
		});

		if (
			! e.target.dataDontHideOnClick &&
			self.props.hideOnItemClick ) self.hide();
		while ( self.parentMenu !== null ) self = self.parentMenu.hide();

		return null;
	}

	/**
	 * Run when mouse pointer is over a menu item.
	 *
	 * @param {Event} e - Event object.
	 */
	function MenuItemMouseOver ( e ) {
		e.stopPropagation();
		var elem = e.target, self = null;

		do {
			if ( typeof elem.dataMenuObject == 'undefined' ) elem = elem.parentNode;
			else {
				self = elem.dataMenuObject;
				break;
			}
		} while  ( typeof elem.nodeName != '#document' );
		if ( !elem.dataMenuObject ) return;
		elem.classList.add(self.props.classNames.selectedItem);
		if ( self.props.selectedItem !== null && self.props.selectedItem !== elem ) {
			self.props.selectedItem.classList.remove(self.props.classNames.selectedItem);
		}
		self.props.selectedItem = elem;
		elem.focus();

		if ( elem.dataSubMenu && elem.dataSubMenu !== null ) {
			elem.dataSubMenu.show()
				.setLeft( elem.offsetWidth )
				.fixPosition()
			;
		}
	}

	/**
	 * Run when mouse goes out of a menu item element.
	 *
	 * @param {Event} e - Event object.
	 */
	function MenuItemMouseOut ( e ) {
		var elem = e.target, self = null;

		do {
			if ( typeof elem.dataMenuObject == 'undefined' ) elem = elem.parentNode;
			else {
				self = elem.dataMenuObject;
				break;
			}
		} while  ( typeof elem.nodeName != '#document' );
		if ( !elem.dataMenuObject ) return;
		if ( self.props.selectedItem === elem ) self.props.selectedItem = null;
		elem.classList.remove(self.props.classNames.selectedItem);
	}
	

	/**
	 * Cheks for click away from an active menu to hide it.
	 *
	 * @param {Event} e - Event object.
	 */
	function CheckClicks ( e ) {
		if ( MenuObject.btn !== null && e.target === MenuObject.btn ) return;
		var o = e.target;
		if ( MenuObject.activeMenu ) {
			while ( o.nodeName != '#document' ) {
				if ( o === MenuObject.activeMenu.elements.container ) return;
				o = o.parentNode;
			}

			if ( MenuObject.activeMenu.props.hideOnClickAway ) MenuObject.activeMenu.hide();
		}
	}

	/**
	 * Cheks for mouseover menu scroll buttons for scrolling.
	 *
	 * @param {Event} e - Event object.
	 */
	function menuMouseOverScroll ( e ) {
		timedScroll.self  = e.target.dataLVZWEBDESKTOPOBJ;
		timedScroll.dir = e.target.dataDirection;
		timedScroll.t = setTimeout ( timedScroll, 100 );
	}

	/**
	 * Cheks for mouseout menu scroll buttons for scrolling.
	 *
	 * @param {Event} e - Event object.
	 */
	function menuMouseOutScroll ( e ) {
		var self = e.target.dataLVZWEBDESKTOPOBJ;
		clearTimeout( timedScroll.t );
		timedScroll.t = false;
	}

	/**
	 * Timed function to scroll menu items when mouse is over one of menu's scroll buttons.
	 */
	function timedScroll () {
		clearTimeout( timedScroll.t );
		var self = timedScroll.self;
		if ( timedScroll.dir == 'up' ) {
			if ( self.elements.container.scrollTop > 0 )
				self.elements.container.scrollTop -= 10;
			else
				self.elements.container.scrollTop = 0;
			self.elements.scrollupbtn.style.top = self.elements.container.scrollTop + 'px';
			self.elements.scrolldownbtn.style.bottom = -self.elements.container.scrollTop + 'px';
		}
		else if ( timedScroll.dir == 'down' ) {
			if ( self.elements.container.scrollTop < self.elements.container.scrollTopMax )
				self.elements.container.scrollTop += 10;
			else
				self.elements.container.scrollTop = self.elements.container.scrollTopMax;
			self.elements.scrollupbtn.style.top = self.elements.container.scrollTop + 'px';
			self.elements.scrolldownbtn.style.bottom = -self.elements.container.scrollTop + 'px';

		}
		timedScroll.t = setTimeout ( timedScroll, 100 );
	}

 	timedScroll.t = false;
	timedScroll.dir = false;
	timedScroll.self = false;

	/**
	 * Function for scrolling menu so that selected item stays visible.
	 * @param {MenuObject} self - Menu Object.
	 */
	function scrollMenu ( self ) {
		var o = self.props.selectedItem.previousElementSibling;
		var st = 0;
		while ( o !== null ) {
			if ( typeof o.dataDontHideOnClick == 'undefined' ) st += o.offsetHeight;
			o = o.previousElementSibling;
		}
		self.elements.container.scrollTop = st;
		self.elements.scrollupbtn.style.top = st + 'px';
		self.elements.scrolldownbtn.style.bottom = '-' + st + 'px';
	}

	/**
	 * Functions for scrolling menu items using mouse wheel.
	 *
	 * @param {Event} e - Event object.
	 */
	function wheelScrollMenu ( e ) {
		var self = e.target;
		while ( self.nodeName != '#document' && ! self.dataLVZWEBDESKTOPOBJ ) {
			self = self.parentNode;
		}
		if ( !self.dataLVZWEBDESKTOPOBJ ) return;
		e.preventDefault();
		e.stopPropagation();
		self = self.dataLVZWEBDESKTOPOBJ;
		if ( e.deltaY > 0 ) 
			self.elements.container.scrollTop += 10;
		else
			self.elements.container.scrollTop -= 10;
		
		self.elements.scrollupbtn.style.top = self.elements.container.scrollTop + 'px';
		self.elements.scrolldownbtn.style.bottom = -self.elements.container.scrollTop + 'px';
	}

	/**
	 * Function for selecting menu items with the keyboard.
	 *
	 * @param {Event} e - Event object.
	 */
	function windowKeyboard ( e ) {
		var self = MenuObject.activeMenu;
		if (!self) return;
		e.preventDefault();
		e.stopPropagation();

		if ( e.charCode == 0 ) switch ( e.keyCode ) {
			default:
			console.log ( e );

			case 13: // e.key == "Enter"
			if ( self.props.dataDontHideOnClick !== true ) {
				self.hide();
				MenuItemClick ( { target: self.props.selectedItem } );
			}
			self.props.selectedItem.click();
			return;

			case 27: // e.key == "Escape"
			self.hide();
			return;

			case 37: //e.key == "ArrowLeft"
			if ( self.props.selectedItem !== null ) {
				if ( self.parentMenu && self.parentMenu !== null ) {
					self.elements.container.style.display = "none";
					MenuObject.activeMenu = self.parentMenu;
				}

				else if ( self.menubar ) {
					console.log ( 'mbar');
				}
			}
			return;

			case 38: // e.key == "ArrowUp"
			self.selectPreviousItem();
			break;

			case 39: //e.key == "ArrowRight"
			if ( self.props.selectedItem !== null ) {
				if ( self.props.selectedItem.dataSubMenu && self.props.selectedItem.dataSubMenu !== null ) {
					self.props.selectedItem.dataSubMenu.elements.container.style.display = "block";
					self.props.selectedItem.dataSubMenu.elements.container.style.position = "";
					MenuObject.activeMenu = self.props.selectedItem.dataSubMenu;
				}
			}
			return;

			case 40: // e.key == "ArrowDown"
			self.selectNextItem();
			break;

		}
	}

	/**
	 * The active MenuObject or null object.
	 * @type {object}
	 * @public
	 */
	MenuObject.activeMenu = null;

	/**
	 * Active menu's button (element triggerd menu)
	 * @type {object}
	 * @public
	 */
	MenuObject.btn = null;

	/**
	 * Associative array for menu object's default class names.
	 * @type {Array}
	 * @public
	 */
	MenuObject.classNames = {};

	/**
	 * Default class name for menu container element.
	 * @public
	 * @type {string}
	 */
	MenuObject.classNames.container = 'web-desktop-menu';

	/**
	 * Default class name for menu scroll-up button.
	 * @public
	 * @type {string}
	 */
	MenuObject.classNames.scrollupbtn = 'web-desktop-menu-scroll-up-button';

	/**
	 * Default class name for menu scroll-down button.
	 * @public
	 * @type {string}
	 */
	MenuObject.classNames.scrolldownbtn = 'web-desktop-menu-scroll-down-button';

	/**
	 * Default class name for menu object selected item.
	 * @public
	 * @type {string}
	 */
	MenuObject.classNames.selectedItem = 'selected';

	/**
	 * Default class name for menu object disable items.
	 * @public
	 * @type {string}
	 */
	MenuObject.classNames.disabledItem = 'disabled';

	/**
	 * Default class name for menu object separators.
	 * @public
	 * @type {string}
	 */
	MenuObject.classNames.separator = 'web-desktop-separator';

	/**
	 * List with all menu objects.
	 * @type {Array}
	 * @public
	 */
	MenuObject.list = [];

	window.addEventListener('load', function () {
			document.body.addEventListener('mouseup',CheckClicks);
			document.addEventListener('mouseup',CheckClicks);
	});

	window.lvzwebdesktop.MenuObject = MenuObject;
	window.lvzwebdesktop.constructors.push({
		"selector" : "."+MenuObject.classNames.container+",web-menu",
		"constructor" : MenuObject
	});

})();
