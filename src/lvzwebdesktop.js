(function () {
	// tests on jsdoc: https://jsdoc.app/
	// jsdoc lvzwebdesktop.js
	/**
	 * @namespace lvzwebdesktopmainns
	 */

	/**
	 * Method for getting the first found object of type "ofType" of parent elements.
	 *
	 * @callback getParentObject
	 * @param {function|string} [ofType] - The object's constructor or constructor's name. If it's undefined (not set), the method will return the first parent lvzwebdesktop object.
	 * @return {object|null|false} The object of the given type or null if not found. Return false on error.
	 */

	/**
	 * The complete Triforce, or one or more components of the Triforce.
	 *
	 * @typedef {Object} WishGranter~Triforce
	 * @property {boolean} hasCourage - Indicates whether the Courage component is present.
	 * @property {boolean} hasPower - Indicates whether the Power component is present.
	 * @property {boolean} hasWisdom - Indicates whether the Wisdom component is present.
	 */

	var scriptTags=document.getElementsByTagName('script');
	var currentScriptURL = scriptTags[scriptTags.length-1].src;
	currentScriptURL = currentScriptURL.split(/[?]/);

	/**
	 * Function for getting a remote file or stream (asyncronus).
	 *
	 * @function getFile
	 * @param {string} file - URL of file or stream.
	 * @param {function} f - Function to run on getting data.
	 */
	function getFile ( file, f ) {
		var xht = new XMLHttpRequest();
		xht.onreadystatechange = function ( ) {
			if ( this.readyState == 4 && this.status == 200 ) {
				f( this.responseText );
			}
		};
		xht.open("GET", file, true);
		xht.send();
	}

	/**
	 * Function for getting a remote file or stream (syncronus).
	 *
	 * @function getFileS
	 * @param {string} file - URL of file or stream.
	 * @return {string} The contents of file or stream.
	 */
	function getFileS ( file ) {
		var xht = new XMLHttpRequest();

		xht.open("GET", file, false);
		xht.send(null);

		if ( xht.status == 200 ) {
			return xht.responseText;
		}
		return false;
	}

	/**
	 * The main function for controling lvzwebdesktop objects.
	 *
	 * @function lvzwebdesktop
	 * @memberof lvzwebdesktopmainns
	 * @param {...*} params - Parameters for searching objects.
	 * @return {object}
	 */
	window.lvzwebdesktop = function (params) {
		var i;

		// If one argumet is given
		if ( arguments.length == 1 ) {

			// Get lvzwebdesktop object from element
			if ( arguments[0] instanceof HTMLElement ) {

				var tmpc;
				for ( i = 0; i < window.lvzwebdesktop.constructors.length; i++ ) {
					//tmpc = arguments[0]['data'+lvzwebdesktop.constructors[i].constructor.name];
					tmpc = arguments[0].dataLVZWDESKTOPOBJ;
					if ( typeof tmpc == 'object' ) return tmpc;
				}
			}
			return false;
		}

		// If two arguments are given
		else if ( arguments.length == 2 ) {

			// Get lvzwebdesktop object from element defined with a css selector (arg1) of type (arg2)
			if ( typeof arguments[0] == 'string' &&
				 typeof arguments[1] == 'string') {

				var selector = arguments[0];
				var type = arguments[1];
				
				if ( typeof window.lvzwebdesktop[type] == 'undefined'
					 || typeof window.lvzwebdesktop[type].list  == 'undefined'
				   ) return false;

				if ( selector.substr(0,1) == '#' )
					for ( i = 0; i < window.lvzwebdesktop[type].list.length ; i ++ )
						if ( window.lvzwebdesktop[type].list[i].props.elementId == selector.substr(1) ) return window.lvzwebdesktop[type].list[i];
			}

			// if first argument is element
			else if ( arguments[0] instanceof HTMLElement ) {
				var elem = arguments[0];
				return {

					getParentObject : function ( ofType ) {
						var constr = false;
						var o = elem.parentNode;
						if ( typeof ofType == 'function' ) {
							constr = ofType;
						}
						else if ( typeof ofType == 'string' ) {
							for ( var i = 0; i < window.lvzwebdesktop.constructors.length; i ++ )
								if ( window.lvzwebdesktop.constructors[i].constructor.name == ofType ) {
									constr = window.lvzwebdesktop.constructors[i].constructor;
									break;
								}
							if ( constr === false ) {
								console.error ( " Can't find constructor \""+ofType+"\". Make sure that the constructors script is loaded." );
								return false;
							}
						}
						else if ( typeof ofType == 'undefined' ) {
							constr = false;
						}
						else {
							console.error ( "Must give constructor or string describing the object" );
							return false;
						}

						if ( o === null ) return null;
						while ( o.nodeName !== '#document' ) {
							if ( ( constr !== false && o.dataLVZWDESKTOPOBJ instanceof constr ) || constr === false ) return o.dataLVZWDESKTOPOBJ;
							o = o.parentNode;
						}

						return null;
					}

				};
			}
		}

		return false;
	};

	/**
	 * Method for getting a remote file or stream (asyncronus).
	 *
	 * @memberof lvzwebdesktopmainns
	 * @function lvzwebdesktop.getFile
	 * @param {string} file - URL of file or stream.
	 * @param {function} f - Function to run on getting data.
	 */
	window.lvzwebdesktop.getFile = getFile;

	/**
	 * Method for getting a remote file or stream (syncronus).
	 *
	 * @function lvzwebdesktop.getFileS
	 * @param {string} file - URL of file or stream.
	 * @return {string} The contents of file or stream.
	 */
	window.lvzwebdesktop.getFileS = getFileS;

	/**
	 * Checks if an element is between two points.
	 *
	 * @param {HTMLElement} el - the element to check.
	 * @param {number} x1 - the x position of the first point.
	 * @param {number} y1 - the y position of the first point.
	 * @param {number} x2 - the x position of the second point.
	 * @param {number} y2 = the y position of the second point.
	 * @return {boolean} True if the element, or part of it, is between the two points. False otherwise.
	 */
	window.lvzwebdesktop.isBetween = function(el,x1,x2,y1,y2) {
		var o=offset(el),th=o.top+el.offsetHeight,lw=o.left+el.offsetWidth;
		return((o.top>=y1&&o.top<y2)||(o.top<y1&&th>=y1))&&((o.left>=x1&&o.left<x2)||(o.left<x1&&lw>=x1));
	};

	/**
	 * Method for checking if an object has been constructed using an lvzwebdesktop constructor.
	 * @param {object} item - The item to check.
	 * @return {object|false} If "item" is member of an lvzwebdesktop constructor returns that constructor, if not it returns
	 */
	window.lvzwebdesktop.isPartOf = function ( item ) {
		if ( typeof item !== 'object' ) {
			return false;
		}

		for ( var i = 0; i < window.lvzwebdesktop.constructors.length; i ++ )
			if ( item instanceof window.lvzwebdesktop.constructors[i].constructor )
				return window.lvzwebdesktop.constructors[i].constructor;

		return false;
	};

	window.lvzwebdesktop.defaultTheme = 0;
	window.lvzwebdesktop.themes = [];

	/**
	 * Method for loading js files.
	 * @param {string} url - javascript file url, to load.
	 */
	window.lvzwebdesktop.loadjs = function ( url ) {
		var sc = document.createElement('script');
		sc.src = url;
		document.head.appendChild ( sc );
	}

	/**
	 * Method for loading css files.
	 * @param {string} url - css file url, to load.
	 */
	window.lvzwebdesktop.loadcss = function ( url ) {
		var ln = document.createElement('link');
		ln.setAttribute ('rel', 'stylesheet');
		ln.setAttribute ('type', 'text/css' );
		ln.setAttribute ('href', url );
		document.head.appendChild ( ln );
	}

	window.lvzwebdesktop.addThemeFile = function ( f ) {
		getFile( f , function ( contents ) {
			window.lvzwebdesktop.themes.push ( JSON.parse ( contents ) );
		});
	};

	window.lvzwebdesktop.addThemeString = function ( str ) {
		window.lvzwebdesktop.themes.push ( JSON.parse ( str ) );
	};

	window.lvzwebdesktop.constructors = [];

	// Actions after window has been loaded
	window.addEventListener ( 'load', function () {

		// document.addEventListener('keypress', desktopKeyPress);
		document.addEventListener('keyup', lvzwebdesktopKeyUp);
		document.addEventListener('keydown', lvzwebdesktopKeyDown);

		// Call each constructor.
		for ( var i = 0; i < window.lvzwebdesktop.constructors.length; i ++ ) {

			var elements = document.querySelectorAll(window.lvzwebdesktop.constructors[i].selector);
			for ( var j = 0 ; j < elements.length; j++ )
				new window.lvzwebdesktop.constructors[i].constructor( elements[j] );

			window.lvzwebdesktop.constructors[i].loaded = true;

		}

		// document.body.addEventListener('click',CheckClicks);
		document.addEventListener('mouseup',CheckClicks);

	} );

	window.lvzwebdesktop.onclick = [];

	window.lvzwebdesktop.fn = {};

	window.lvzwebdesktop.fn.addEvent = function ( e, f ) {
		if ( typeof this['on'+e] == 'undefined') return false;
		for ( var i = 0; i < this['on'+e].length; i++ ) if ( this['on'+e][i] === f) return this;
		this['on'+e].push(f);
		return this;
	};

	window.lvzwebdesktop.fn.removeEvent = function ( e, f ) {
		if ( typeof this['on'+e] == 'undefined' ) return false;
		if ( typeof f == 'undefined' ) { this['on'+e]=[]; return this; }
		for ( var i = 0; i < this['on'+e].length; i++ )
			if ( this['on'+e][i] === f ) {
				this['on'+e].splice(i,1);
				return this;
			}
		return this;
	};

	window.lvzwebdesktop.fn.fireEvent = function ( e, obj ) {
		obj.type = e;
		obj.timeStamp = Date.now();
		obj.preventDefault = function () { obj.defaultPrevented = true; obj.preventDefault.a = true; };
		obj.defaultPrevented = false;
		obj.preventDefault.a = false;
		obj.stopPropagation = function () { obj.stopPropagation.a = true; };
		obj.stopPropagation.a = false;

		if ( typeof this['on'+e] == 'undefined') return false;
		for ( var i = 0; i < this['on'+e].length; i++ )
			if ( obj.stopPropagation.a == false )
				this['on'+e][i].call(this, obj);
		return this;
	};

	/**
	 * Listens for clicks on document to report them on objects.
	 * @param {Event} e - Event object.
	 */
	function CheckClicks ( e ) {
		if ( CheckClicks.element !== null && CheckClicks.element === e.target ) return;

		window.lvzwebdesktop.fn.fireEvent.call(window.lvzwebdesktop, "click", {
			"target" : e.target
		});
	}

	CheckClicks.element = null;

	// Load scripts and css from this script's url
	if ( currentScriptURL.length == 2 ) {
		var currentPath = currentScriptURL[0];
		currentPath = currentPath.match(/^(.*[/])[^/]*$/);
		if ( currentPath === null )currentPath = './';
		else currentPath = currentPath[1];

		currentScriptURL = currentScriptURL[1].split(/[&]/);
		var currentURLArg;
		var items;
		for ( var i = 0; i < currentScriptURL.length; i ++ ) {
			currentURLArg = currentScriptURL[i].split(/=/);
			if ( ['loadjs','loadcss'].indexOf ( currentURLArg[0] ) > -1 ) {
				items = currentURLArg[1].split (/,/);
				for ( var j = 0; j < items.length; j ++ )
					window.lvzwebdesktop[currentURLArg[0]] ( currentPath + items[j] );
			}
		}
	}


	lvzwebdesktop.resizeObserver = new window.ResizeObserver ( function ( e ) {
		var self = e[0].target.dataLVZWDESKTOPOBJ;
		var eobj = {
			"target" : self,
			"oldWidth" : self.props.width,
			"oldHeight" : self.props.height,
			"newWidth" : self.elements.container.offsetWidth,
			"newHeight" : self.elements.container.offsetHeight
		};
		self.fireEvent('resize',eobj);
		if ( eobj.defaultPrevented === true ) {
			self.elements.container.style.height = self.props.height + 'px';
			self.elements.container.style.width = self.props.width + 'px';
		}
		else {
			self.props.height = self.elements.container.offsetHeight;
			self.props.width = self.elements.container.offsetWidth;
		}
	} );

	/**
	 * String of current emacs style key combination. (example: C-x )
	 * @public
	 * @type {string}
	 */
	var currentKeyComb = '';

	/**
	 * String of current emacs style multiple key combination (example: C-x 1 )
	 * @public
	 * @type {string}
	 */
	var currentMultipleKeyComb = '';

	/**
	 * Associative array with the currenty pressed keys.
	 * @public
	 * @type {object}
	 */
	var keyspressed = {
		"control" : false,
		"shift" : false,
		"alt" : false,
		"altg" : false,
		"escape" : false,
		"returnkey" : false,
		"super" : false,
		"character" : false,
		"spc" : false,
		"charkey" : false
	};

	/**
	 * Array for storing keycombinations that other objects setting.
	 * @public
	 * @type {array}
	 */
	var keycombs = [];

	/**
	 * Method for adding watch for a keycombination.
	 * @param {string} comb - Emacs like string for key combination.
	 * @param {function} runMethod - Method to run if key combination is captured.
	 */
	lvzwebdesktop.addKeyComb = function ( comb, runMethod, data ) {
		keycombs.push( {
			"combination" : comb,
			"method" : runMethod,
			"data" : data
		} );
	};

	/**
	 * Method for deleting all watches for the given key combination.
	 * @param {string} comb - Emacs like string for key combination.
	 * @return {DesktopObject} This desktop object.
	 */
	lvzwebdesktop.clearKeyComb = function ( comb ) {
		for ( var i = keycombs.length -1; i >= 0 ; i -- )
			if ( keycombs[i].combination == comb )
				keycombs.splice(i,1);
	};

	/**
	 * Array with methods that match exactly the current key combination.
	 * @type {array}
	 */
	var keycombmethodsf = [];

	/**
	 * Array with data for "keycombmethodsf" methods.
	 * @type {array}
	 */
	var keycombmethodss = [];

	/**
	 * Timer for key combinations.
	 * @type {number}
	 */
	var runkeycombtmr = false;

	/**
	 * Function for controling keyup events.
	 *
	 * @param {Event} e - Event data object.
	 */
	function lvzwebdesktopKeyUp ( e ) {
		var mkey = false;

		if ( e.key === "Control" ) {
			keyspressed.control = false;
			mkey = true;
		} else if ( e.key === "Shift" ) {
			keyspressed.shift = false;
			mkey = true;
		} else if ( e.key === "Alt" ) {
			keyspressed.alt = false;
			mkey = true;
		} else if ( e.key === "AltGraph" ) {
			keyspressed.altg = false;
			mkey = true;
		} else if ( e.key === " " )
			keyspressed.spc = false;
		else if ( e.key === "Enter" )
			keyspressed.returnkey = false;
		else if ( e.key === "Escape" )
			keyspressed.escape = false;
		else if ( /^[a-z0-9!@#$%\^&*()`~\[\]{};'\\:"|\-=_+,./<>?]$/.test(e.key) )
			keyspressed.charkey = false;

		if ( mkey ) return ;

		for ( var i = 0; i < keycombs.length; i ++ )
			if ( keycombs[i].combination != currentMultipleKeyComb &&
				 keycombs[i].combination.substr(0,currentMultipleKeyComb.length) == currentMultipleKeyComb
			   ) {
				if ( currentMultipleKeyComb == '' )
					currentMultipleKeyComb = currentKeyComb;
				else
					currentMultipleKeyComb += ' ' + currentKeyComb;
				return ;
			}

		currentKeyComb = buildKeyComb(keyspressed);
		currentMultipleKeyComb = '';
	}

	/**
	 * Function for controling keydown events.
	 *
	 * @param {Event} e - Event data object.
	 */
	function lvzwebdesktopKeyDown ( e ) {
		clearTimeout ( runkeycombtmr );
		var mkey = false;

		if ( e.key === "Control" ) {
			keyspressed.control = true;
			mkey = true;
		} else if ( e.key === "Shift" ) {
			keyspressed.shift = true;
			mkey = true;
		} else if ( e.key === "Alt" ) {
			keyspressed.alt = true;
			mkey = true;
		} else if ( e.key === "AltGraph" ) {
			keyspressed.altg = true;
			mkey = true;
		} else if ( e.key === " " )
			keyspressed.spc = true;
		else if ( e.key === "Enter" )
			keyspressed.returnkey = true;
		else if ( e.key === "Escape" )
			keyspressed.escape = true;
		else if ( /^[a-z0-9!@#$%\^&*()`~\[\]{};'\\:"|\-=_+,./<>?]$/.test(e.key) )
			keyspressed.charkey = e.key;

		if ( mkey ) return ;
		currentKeyComb = buildKeyComb(keyspressed);

		var ccombr = false;
		var possibleKComb = false;

		if ( ! currentMultipleKeyComb || currentMultipleKeyComb == '' ) currentMultipleKeyComb = currentKeyComb;
		else currentMultipleKeyComb += currentKeyComb;

		keycombmethodsf = [];
		keycombmethodss = [];

		for ( var i = 0; i < keycombs.length; i ++ )
			if ( keycombs[i].combination == currentMultipleKeyComb ) {
				keycombmethodsf.push( keycombs[i].method );
				keycombmethodss.push ( { "comb" : currentMultipleKeyComb, "data": keycombs[i].data  } );
				ccombr = true;
			} else if (  keycombs[i].combination.substr(0,currentMultipleKeyComb.length) == currentMultipleKeyComb ) {
				possibleKComb = true;
			}

		if ( ccombr ) {

			if ( ! possibleKComb ) {
				for ( i = 0; i < keycombmethodsf.length; i ++ )
					keycombmethodsf[i] ( currentMultipleKeyComb, keycombmethodss[i].data );
			}
			else {
				runkeycombtmr = setTimeout ( function () {
					clearTimeout ( runkeycombtmr );
					for ( var i = 0; i < keycombmethodsf.length; i ++ ) {
						keycombmethodsf[i] (  keycombmethodss[i].comb, keycombmethodss[i].data );

						currentKeyComb = '';
						currentMultipleKeyComb = '';
					}
					keycombmethodsf = [];
					keycombmethodss = [];

				}, 500 );
			}

		}

		currentKeyComb = '';
		if ( possibleKComb === false  )
			currentMultipleKeyComb = '';
	}

	/**
	 * Function for creating key combination strings from currently pressed keys.
	 * @param {string} keyspressed - The keys object.
	 * @return {string} Emacs style key combination string.
	 */
	function buildKeyComb ( keyspressed ) {
		 let keycomb = '';
		if ( keyspressed.control ) keycomb += '-C';
		if ( keyspressed.alt ||
			 keyspressed.altg
		   ) keycomb += '-M';
		if ( keyspressed.shift ) keycomb += '-S';
		if ( keyspressed.spc ) keycomb += '-SPC';
		else if ( keyspressed.returnkey ) keycomb += '-RET';
		else if ( keyspressed.escape ) keycomb += '-ESC';
		else if ( keyspressed.charkey !== false ) keycomb += '-' + keyspressed.charkey;
		return keycomb.substr(1);
	}

}) () ;
