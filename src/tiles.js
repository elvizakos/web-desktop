(function () {

	/**
	 * Constructor for the tile window object.
	 *
	 * @constructor
	 */
	let TileObject = function ( e, desktop ) {
		// tile-row="1"
		// tile-column="1"
		// moveLeft
		// moveRight
		// moveDown
		// moveUp

		/**
		 * TileObject id.
		 * @type {number}
		 * @public
		 */
		this.id = TileObject.list.push(this) - 1;

		/**
		 * Set the type of the object to "tile".
		 * @type {string}
		 * @public
		 */
		this.type = "tile";

		/**
		 * The tile manager that this tile belongs to.
		 * @type {TilesObject}
		 * @public
		 */
		this.tileManager = null;

		/**
		 * Associative array for the tile properties.
		 * @type {object}
		 * @public
		 */
		this.props = {};

		/**
		 * The tile's height in number of pixels. If false, then the height will be taken automaticaly.
		 * @type {false|number}
		 * @public
		 */
		this.props.height = false;

		/**
		 * The maximum height in number of pixels for the tile object. False for no maximum height.
		 * @type {number|false}
		 * @public
		 */
		this.props.maxHeight = false;

		/**
		 * The minimum height in number of pixels for the tile object.
		 * @type {number}
		 * @public
		 */
		this.props.minHeight = 20;

		/**
		 * The tile's width in number of pixels. If false, then the width will be taken automaticaly.
		 * @type {false|number}
		 * @public
		 */
		this.props.width = false;

		/**
		 * The tile's maximum width in number of pixels. False for no maximum width.
		 * @type {nubmer|false}
		 * @public
		 */
		this.props.maxWidth = false;

		/**
		 * The tile's minimum width in number of pixels.
		 * @type {number}
		 * @public
		 */
		this.props.minWidth = 100;

		/**
		 * True if tile has focus. False otherwise.
		 * @type {boolean}
		 * @public
		 */
		this.props.focused = false;

		/**
		 * Sets if the tile object is resizable.
		 * @type {boolean}
		 * @public
		 */
		this.resizable = true;

		/**
		 * Associative array for TileObject elements class names.
		 * @type {object}
		 * @public
		 */
		this.props.classNames = {};

		/**
		 * Class name for tile's container element.
		 * @type {string}
		 * @public
		 */
		this.props.classNames.container = TilesObject.classNames.tilecontainer;

		/**
		 * Returns the first parent object of given type.
		 *
		 * @param {function|string} type - lvzwebobject constructor or constructor's name.
		 * @return {object|null|false} Return's the first parent lvzwebobject that matches "type" parameter. if not found returns null. on error returns false.
		 */
		this.getParentOfType = function ( type ) { return window.lvzwebdesktop(this.elements.container,'').getParentObject( type ); };

		this.horizontalSplit = function () {};
		this.verticalSplit = function () {};

		/**
		 * Associative array for storing tile's html elements.
		 * @type {object}
		 * @public
		 */
		this.elements = {};

		/**
		 * Tile's container element.
		 * @type {HTMLElement}
		 * @public
		 */
		this.elements.container = null;
		if ( typeof e == 'undefined' || e === false ) this.elements.container = document.createElement('div'); else  this.elements.container = e;
		this.elements.container.position = '';
		this.elements.container.overflow = 'hidden';
		this.elements.container.zIndex = '8';
		this.elements.container.classList.add( this.props.classNames.container );
		this.elements.container.dataTileObject = this;
		this.elements.container.dataLVZWDESKTOPOBJ = this;

		// console.log ( e );
		// console.log ( desktop );
		// e.style.display = 'none';
		// var child = e.firstElementChild.cloneNode(true);

		if ( typeof e != 'undefined') {
			let tman = this.getParentOfType(TilesObject);
			if ( typeof tman == 'object' ) tman.addTile ( this );
		}

	};

	/**
	 * List of all tile objects.
	 * @type {Array}
	 * @public
	 */
	TileObject.list = [];

	/**
	 * Constructor for tiles window manager object.
	 *
	 * @constructor
	 * @param {HTMLElement} [e] - The element to attach tile to.
	 * @param {DesktopObject [desktop] - Desktop object that tile will appear into.
	 */
	let TilesObject = function ( e, desktop ) {
		let eobj = {
			"target" : this,
			"element" : e,
			"desktop" : desktop
		};
		TilesObject.fireEvent( 'load', eobj );

		/**
		 * TilesObject id.
		 * @type {number}
		 * @public
		 */
		this.id = TilesObject.list.push(this) - 1;

		/**
		 * Set the type of the object to "tiles".
		 * @type {string}
		 * @public
		 */
		this.type = 'tiles';

		/**
		 * Associative array for tiles properties.
		 * @type {object}
		 * @public
		 */
		this.props = {};

		/**
		 * The number of horizontal tiles for the desktop.
		 * @type {number}
		 * @public
		 */
		this.props.horizontalTiles = false;

		/**
		 * The number of vertical tiles for the desktop.
		 * @type {number}
		 * @public
		 */
		this.props.verticalTiles = false;

		/**
		 * The height in pixels of the tiles manager container element.
		 * @type {number}
		 * @public
		 */
		this.props.height = null;

		/**
		 * The width in pixels of the tiles manager container element.
		 * @type {number}
		 * @public
		 */
		this.props.width = null;

		/**
		 * Array for the rows elements.
		 * @type {array}
		 * @public
		 */
		this.props.rows = [];

		/**
		 * Array for the columns elements.
		 * @type {array}
		 * @public
		 */
		this.props.columns = [];

		this.props.tilepositions = [];

		/**
		 * Associative array for tileobject elements class names.
		 * @type {object}
		 * @public
		 */
		this.props.classNames = {};

		/**
		 * Class name for tiles manager container element.
		 * @type {string}
		 * @public
		 */
		this.props.classNames.container = TilesObject.classNames.container;

		/**
		 * Class name for each tile's container element.
		 * @type {string}
		 * @public
		 */
		this.props.classNames.tilecontainer = TilesObject.classNames.tilecontainer;

		/**
		 * Class name for the row elements.
		 * @type {string}
		 * @public
		 */
		this.props.classNames.tilerow = TilesObject.classNames.tilerow;

		/**
		 * Class name for the column elements.
		 * @type {string}
		 * @public
		 */
		this.props.classNames.tilecolumn = TilesObject.classNames.tilecolumn;

		this.tiles = [];

		/**
		 * The desktop object that tile manager is on.
		 * @type {DesktopObject|null}
		 * @public
		 */
		this.desktop = null;

		/**
		 * Method for adding events to tiles manager.
		 *
		 * @param {string} e - Event name.
		 * @param {function} f - Functions to run on event.
		 * @return {mixed} False on error, current tile manager object otherwise.
		 */
		this.addEvent = window.lvzwebdesktop.fn.addEvent;

		/**
		 * Method for removing events from tiles manager.
		 *
		 * @param {string} e - The name of the event to remove.
		 * @param {function} [f] - The function to remove. If this param is set then the method will remove only the function that matches. Otherwise it will clear all functions from the event.
		 * @return {mixed} False on error, current tile manager object otherwise.
		 */
		this.removeEvent = window.lvzwebdesktop.fn.removeEvent;

		/**
		 * Method for firing events.
		 *
		 * @param {string} e - The name of the event to fire.
		 * @param {object} obj - An object with properties to add to event object.
		 * @return {mixed} False on error, current tile manager object otherwise.
		 */
		this.fireEvent = window.lvzwebdesktop.fn.fireEvent;

		/**
		 * Returns the first parent object of the given type.
		 *
		 * @param {function|string} type - The name or the constructor function of an lvzwebdesktop object.
		 * @return {object|null|false} Return's the first parent lvzwebobject that matches "type" parameter. If not found returns null. On error it returns false.
		 */
		this.getParentOfType = function ( type ) { return window.lvzwebdesktop(this.elements.container,'').getParentObject( type ); };

		/**
		 * Method for adding rows in tile container.
		 */
		this.addRow = function ( ) {
			let tmprow = document.createElement('div');
			this.elements.container.appendChild(tmprow);
			tmprow.classList.add(this.props.classNames.tilerow);
			let lh = (100 / (this.props.rows.length+1)).toFixed(2);
			let lw = (100 / (this.props.columns.length+1)).toFixed(2);
			this.props.rows.push(tmprow);

			for(let i=0; i<this.props.rows.length; i++) {
				this.props.rows[i].style.height=lh+'%';
			}

			this.fireEvent('addrow',{
				"target":this,
				"row":tmprow
			});
		};

		this.addColumn = function () {
			let lw = (100 / (this.props.columns.length+1)).toFixed(2);
			let col = [];
			for(let i=0; i<this.props.rows.length; i++) {
				let tmpcol = document.createElement('div');
				col.push(tmpcol);
				this.props.tilepositions.push(tmpcol);
				this.props.rows[i].appendChild(tmpcol);

				for ( let j = 0; j < this.props.rows[i].children.length; j ++ )
					this.props.rows[i].children[j].style.width = lw + '%';

				tmpcol.classList.add(this.props.classNames.tilecolumn);
			}

			this.props.columns.push(col);

			this.fireEvent('addcolumn',{
				"target":this,
				"column":col
			});

		};

		/**
		 * Method for adding a tile.
		 * @return {false|TilesObject} False on error, otherwise the current TilesObject object.
		 */
		this.addTile = function ( t ) {
			if ( this.tiles.indexOf ( t ) > -1 ) return this;
			for ( let i = 0; i < this.props.tilepositions.length; i ++ )
				if ( this.props.tilepositions[i].children.length == 0 ) {
					this.props.tilepositions[i].appendChild ( t.elements.container );
					t.tileManager = this;
				}
			this.tiles.push ( t );
			this.fireEvent ( 'addtile', {
				"target" : this,
				"tile" : t
			});
			return this;
		};

		this.toggleTitle = function () {
			return this;
		};

		this.toggleBorder = function () {
			return this;
		};

		/**
		 * Associative array (object) for storing tiles manager object html elements
		 * @type {object}
		 * @public
		 */
		this.elements = {};

		/**
		 * Tiles manager container element.
		 * @type {HTMLElement}
		 * @public
		 */
		this.elements.container = null;
		if ( typeof e == "undefined" || e === false )this.elements.container = document.createElement('div');
		else this.elements.container = e;
		this.elements.container.dataLVZWDESKTOPOBJ = this;

		// Make properies from html element's attributes.
		var attr;

		attr = this.elements.container.getAttribute('data-rows');
		if ( attr !== null ) this.props.verticalTiles = parseInt(attr);

		attr = this.elements.container.getAttribute('rows');
		if ( attr !== null ) this.props.verticalTiles = parseInt(attr);

		attr = this.elements.container.getAttribute('data-columns');
		if ( attr !== null ) this.props.horizontalTiles = parseInt(attr);

		attr = this.elements.container.getAttribute('columns');
		if ( attr !== null ) this.props.horizontalTiles = parseInt(attr);

		if ( this.props.verticalTiles !== false )
			for ( let i=0; i < this.props.verticalTiles; i++ )
				this.addRow();

		if ( this.props.horizontalTiles !== false )
			for ( let i=0; i < this.props.horizontalTiles; i++ )
				this.addColumn();

		for ( let i = this.elements.container.children.length-1 ; i > -1; i-- ) {
			let tmptile;
			if ( this.elements.container.children[i].classList.contains(TilesObject.classNames.tilecontainer) ) {
				if ( typeof this.elements.container.children[i].dataLVZWDESKTOPOBJ == 'undefined' )
					tmptile = new TileObject( this.elements.container.children[i] );
				else
					tmptitle = this.elements.container.children[i].dataLVZWDESKTOPOBJ;

				this.addTile(tmptile);
			}
		}

		this.fireEvent('ready',{
			"target" : this
		});

		TilesObject.fireEvent('ready',{
			"target" : this
		});

	};

	/**
	 * List of all tiles window manager objects.
	 * @type {Array}
	 * @public
	 */
	TilesObject.list = [];

	/**
	 * Default CSS class-names for all tile managers.
	 * @type {object}
	 * @public
	 */
	TilesObject.classNames = {};

	/**
	 * Default CSS class-name for the tiles manager container element.
	 * @type {string}
	 * @public
	 */
	TilesObject.classNames.container = 'web-desktop-tiles';

	/**
	 * Default CSS class-name for the tiles container elements.
	 * @type {string}
	 * @public
	 */
	TilesObject.classNames.tilecontainer = 'web-desktop-tile';

	/**
	 * Default CSS class-name for the tile row elements.
	 * @type {string}
	 * @public
	 */
	TilesObject.classNames.tilerow = 'web-desktop-tile-row';

	/**
	 * Default CSS class-name for the tile column elements.
	 * @type {string}
	 * @public
	 */
	TilesObject.classNames.tilecolumn = 'web-desktop-tile-column';

	/**
	 * Array to store functions for the load event.
	 * @type {Array}
	 * @public
	 */
	TilesObject.onload = [];

	/**
	 * Array to store functions for the ready event.
	 * @type {Array}
	 * @public
	 */
	TilesObject.onready = [];

	/**
	 * Add events for all tile managers
	 *
	 * @param {string} e - Event name
	 * @param {function} f - Function to run on event.
	 * @return {boolean} false for error, true otherwise.
	 */
	TilesObject.addEvent = function ( e, f ) {
		if ( typeof TilesObject['on'+e] == 'undefined') return false;
		for ( var i = 0; i < TilesObject['on'+e].length; i++ ) if ( TilesObject['on'+e][i] === f) return true;
		TilesObject['on'+e].push(f);
		return true;
	};

	/**
	 * Fire events for all tile managers.
	 * @param {string} e - Event name to fire.
	 * @param {object} [obj] - Event data object.
	 * @return {boolean} false for error, true otherwise.
	 */
	TilesObject.fireEvent = function ( e, obj ) {
		var cobj;
		if ( obj.target ) cobj = obj.target; else cobj = this;
		obj.type = e;
		if ( typeof TilesObject['on'+e] == 'undefined') return false;
		for ( var i = 0; i < TilesObject['on'+e].length; i++ )TilesObject['on'+e][i].call(cobj, obj);
		return true;
	};

	window.lvzwebdesktop.TilesObject = TilesObject;
	window.lvzwebdesktop.constructors.push({
		"selector" : '.'+TilesObject.classNames.container+",tile-manager",
		"constructor" : TilesObject
	});

	window.lvzwebdesktop.TileObject = TileObject;
	window.lvzwebdesktop.constructors.push({
		"selector" : '.'+TilesObject.classNames.tilecontainer+",tile",
		"constructor" : TileObject
	});

})();
