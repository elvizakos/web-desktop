(function () {

	/**
	 * Constructor for creating panel objects.
	 * @constructor
	 * @param {HTMLElement} e - Element to attach panel to.
	 */
	var PanelObject = function ( e ) {
		PanelObject.list.push(this);

		/**
		 * Type of object.
		 * @public
		 * @param {'panel'}
		 */
		this.type = 'panel';

		/**
		 * Array for the items of the panel.
		 * @public
		 * @param {Array}
		 */
		this.items = [];

		/**
		 * The desktop object that this panel belongs to.
		 * @type {DesktopObject|null}
		 * @public
		 */
		this.desktop = null;

		/**
		 * Associative array for keeping panel properties values.
		 * @public
		 * @type {Object}
		 */
		this.props = {};

		/**
		 * The ID of the object. If there is no ID then it's false.
		 * @public
		 * @type {string|false}
		 */
		this.props.elementId = false;

		/**
		 * CSS class name for theme in use.
		 * @type {null|string}
		 * @public
		 */
		this.props.themeClass = null;

		/**
		 * Contains the panel's position relative to desktop.
		 * @public
		 * @type {'top'|'right'|'bottom'|'left'}
		 */
		this.props.position = 'top';

		/**
		 * The panel's top border distance from the top of the desktop object
		 * it belongs to, in number of pixels.
		 * @type {number}
		 * @public
		 */
		this.props.top = 0;

		/**
		 * The panel's left border distance from the left of the desktop object
		 * it bolongs to, in number of pixels.
		 * @type {number}
		 * @public
		 */
		this.props.left = 0;

		/**
		 * Contains class names strings of panel object elements.
		 * @public
		 * @type {Object}
		 */
		this.props.classNames = {};

		/**
		 * Class name of panel container element.
		 * @public
		 * @type {string}
		 */
		this.props.classNames.container = 'web-desktop-panel';

		/**
		 * Class name for setting panel orientation to top.
		 * @public
		 * @type {string}
		 */
		this.props.classNames.topOrientation = 'top-orientation';

		/**
		 * Class name for setting panel orientation to left.
		 * @public
		 * @type {string}
		 */
		this.props.classNames.leftOrientation = 'left-orientation';

		/**
		 * Class name for setting panel orientation to right.
		 * @public
		 * @type {string}
		 */
		this.props.classNames.rightOrientation = 'right-orientation';

		/**
		 * Class name for setting panel orientation to bottom.
		 * @public
		 * @type {string}
		 */ 
		this.props.classNames.bottomOrientation = 'bottom-orientation';

		/**
		 * Class name for setting panel or panel items as locked.
		 * @public
		 * @type {string}
		 */
		this.props.classNames.locked = 'locked';

		/**
		 * Array for storing event functions for when adding items to panel.
		 * @type {Array}
		 * @public
		 */
		this.onadditem = [];

		/**
		 * Array for storing event functions for when removing items from panel.
		 * @type {Array}
		 * @public
		 */
		this.onremoveitem = [];

		/**
		 * Array for storing event function for right before moving an item.
		 * @type {Array}
		 * @public
		 */
		this.onitemmovebegin = [];

		/**
		 * Array for storing event functions for when changing items position in panel.
		 * @type {Array}
		 * @public
		 */
		this.onitemmove = [];

		/**
		 * Array for storing event functions for when ending on moving an item.
		 * @type {Array}
		 * @public
		 */
		this.onitemmoveend = [];

		/**
		 * Array for storing event functions for when attaching this object to another object.
		 * @type {Array}
		 * @public
		 */
		this.onattachto = [];

		/**
		 * Array for storing event functions for when panel is changing desktop object.
		 * @type {Array}
		 * @public
		 */
		this.onchangedesktop = [];

		/**
		 * Method for adding events to panel object.
		 *
		 * @param {string} e - Event name.
		 * @param {function} f - Function to run on event.
		 * return {PanelObject|false} False on error, this panel object otherwise.
		 */ 
		this.addEvent = window.lvzwebdesktop.fn.addEvent;

		/**
		 * Method for removing events from panel object.
		 *
		 * @param {string} e - The name of the event to remove.
		 * @param {function} [f] - the function to remove. If this param is set, then the method will remove only the function that matches. Otherwise it will clear all functions from event.
		 * @return {PanelObject|false} False on error, this panel object otherwise.
		 */
		this.removeEvent = window.lvzwebdesktop.fn.removeEvent;

		/**
		 * Method for firing events.
		 *
		 * @param {string} e - The name of the event to fire.
		 * @param {object} obj - An object with properties to add to event object.
		 * @return {PanelObject|false} False on error, this panel object otherwise.
		 */
		this.fireEvent = window.lvzwebdesktop.fn.fireEvent;

		/**
		 * Puts panel on top, left, right or bottom side of desktop.
		 *
		 * @param {'top'|'right'|'bottom'|'left'} p - The side to put the panel to.
		 * @return {false|PanelObject} False on error, otherwise this panel object.
		 */
		this.position = function ( p ) {
			var pos = this.elements.container.getBoundingClientRect()
			if ( p == 'top' ) {
				this.elements.container.classList.remove (
					this.props.classNames.leftOrientation,
					this.props.classNames.bottomOrientation,
					this.props.classNames.rightOrientation
				);
				this.elements.container.classList.add ( this.props.classNames.topOrientation );
				var size = this.elements.container.offsetHeight;
				this.elements.container.parentElement.style.paddingTop = size + 'px';
				this.props.top = 0;
				this.props.left = 0;
			} else if ( p == 'bottom' ) {
				this.elements.container.classList.remove (
					this.props.classNames.topOrientation,
					this.props.classNames.leftOrientation,
					this.props.classNames.rightOrientation
				);
				this.elements.container.classList.add ( this.props.classNames.bottomOrientation );
				var size = this.elements.container.offsetHeight;
				this.elements.container.parentElement.style.paddingBottom = size + 'px';
				this.props.top = this.elements.container.parentElement.offsetHeight - this.elements.container.offsetHeight;
				this.props.left = 0;
			} else if ( p == 'left' ) {
				this.elements.container.classList.remove (
					this.props.classNames.topOrientation,
					this.props.classNames.bottomOrientation,
					this.props.classNames.rightOrientation
				);
				this.elements.container.classList.add ( this.props.classNames.leftOrientation );
				var sizev = this.elements.container.offsetHeight;
				var sizeh = this.elements.container.offsetWidth;
				var pheight = this.elements.container.parentNode.offsetHeight;
				this.elements.container.style.transform = "translate(-"+(sizeh / 2 - sizev / 2)+"px ) rotate(-90deg)";
				this.props.top = 0;
				this.props.left = 0;
			} else if ( p == 'right' ) {
				this.elements.container.classList.remove (
					this.props.classNames.topOrientation,
					this.props.classNames.bottomOrientation,
					this.props.classNames.leftOrientation
				);
				this.elements.container.classList.add ( this.props.classNames.rightOrientation );
				this.props.left = pos.left;
				var sizev = this.elements.container.offsetHeight;
				var sizeh = this.elements.container.offsetWidth;
				var pheight = this.elements.container.parentNode.offsetHeight;
				this.elements.container.style.transform = "translate("+(sizeh / 2 - sizev / 2)+"px ) rotate(-90deg)";
				this.props.top = 0;
				this.props.left = this.elements.container.parentElement.offsetWidth - this.elements.container.offsetHeight;
			} else {
				console.error ( 'Not a valid position for panel' );
				return false;
			}

			return this;
		};

		/**
		 * Method for locking panel.
		 * @return {PanelObject} This panel object.
		 */
		this.lock = function () {
			this.elements.container.classList.add ( this.props.classNames.locked );
			return this;
		};

		/**
		 * Method for unlocking panel.
		 * @return {PanelObject} This panel object.
		 */
		this.unlock = function () {
			this.elements.container.classList.remove ( this.props.classNames.locked );
			return this;
		};

		/**
		 * Method for checking if this panel object is locked.
		 * @return {boolean} true if is locked, false otherwise.
		 */
		this.isLocked = function () {
			return this.elements.container.classList.contains ( this.props.classNames.locked );
		};

		/**
		 * Method for adding items to panel. Fires the "additem" event.
		 *
		 * @param {HTMLElement|object} o - web-desktop object or web-desktop object container to add to panel.
		 * @return {PanelObject|false} False on error, current panel object otherwise.
		 */
		this.addItem = function ( o ) {
			var tmpelem = null, tmpobj = null;
			if ( o instanceof HTMLElement ) {
				tmpelem = o;

				if ( o.dataLVZWDESKTOPOBJ && typeof o.dataLVZWDESKTOPOBJ.onchangedesktop != 'undefined' ) {
					var pd = o.dataLVZWDESKTOPOBJ.desktop;
					o.dataLVZWDESKTOPOBJ.desktop = this.desktop;
					o.dataLVZWDESKTOPOBJ.fireEvent ( 'changedesktop', {
						"target" : o.dataLVZWDESKTOPOBJ,
						"desktop" : this.desktop,
						"previousDesktop" : pd,
						"inheritFrom" : this
					});
				}
			}
			else if ( typeof o.type == 'string' &&
					  typeof o.props == 'object' &&
					  typeof o.elements == 'object' &&
					  o.elements.container instanceof HTMLElement
					) {
				tmpobj = o;
				tmpelem = o.elements.container;
				if ( o.desktop != 'undefined' ) o.desktop = this.desktop;
			}
			else {
				console.error ( "Argument is not a valid web-desktop object or element" );
				return false;
			}

			// Check if element is allready in panel
			for ( var i = 0 ; i < this.items.length; i ++ ) if ( this.items[i].children[0] === tmpelem ) return this;

			var tmpitemcontainer = document.createElement('div');

			tmpitemcontainer.dataPanelObject = this;
			tmpitemcontainer.dataLVZWDESKTOPOBJ = this;
			tmpitemcontainer.addEventListener('mousedown', panelItemMD);

			this.items.push ( tmpitemcontainer );

			if ( tmpelem.parentNode === this.elements.container ) {
				this.elements.container.insertBefore ( tmpitemcontainer, tmpelem );
				tmpitemcontainer.appendChild ( tmpelem );
			} else {
				this.elements.container.appendChild ( tmpitemcontainer );
				tmpitemcontainer.appendChild ( tmpelem );
			}

			return this.fireEvent('additem',{
				"target" : this,
				"itemContainer" : tmpitemcontainer,
				"itemElement" : tmpelem
			});
		};

		/**
		 * Method for removing items from panel. Fires the "removeitem" event.
		 *
		 * @param {HTMLElement|number} item - the html element or item container or item order number to remove.
		 * @return {PanelObject|false} False on error, current panel object otherwise.
		 */
		this.removeItem = function ( item ) {
			var itmOrder;
			var itmContainer;
			if ( typeof item == 'number' ) {
				itmOrder = item;
				itmContainer = this.items[item];
				if ( itmOrder < 0 || itmOrder > this.items.length - 1 ) {
					console.error ( "Panel item with index " + item + " does not exist.");
					return false;
				}
			} else if ( item instanceof HTMLElement ) {
				if ( typeof item.dataPanelObject != 'undefined' ) {
					itmContainer = item;
					itmOrder = this.items.indexOf ( itmContainer );
				} else if ( typeof item.parentNodedataPanelObject != 'undefined' ) {
					itmContainer = item.parentNode;
					itmOrder = this.items.indexOf ( itmContainer );
				} else {
					console.error ( "Not a panel object item" );
					return false;
				}
			}

			if ( this.fireEvent('removeitem',{
				"target" : this,
				"itemContainer" : itmContainer,
				"itemElement" : itmContainer.children[0],
				"itemOrder" : itmOrder
			}) !== false ) {
				
				itmContainer.parentNode.removeChild ( itmContainer );
				this.items.splice ( itmOrder, 1 );

			}

			return this;
		};

		/**
		 * Returns the first parent object of given type.
		 *
		 * @param {function|string} type - lvzwebobject constructor or constructor's name.
		 * @return {object|null|false} Return's the first parent lvzwebobject that matches "type" parameter. if not found returns null. on error returns false.
		 */
		this.getParentOfType = function ( type ) { return window.lvzwebdesktop(this.elements.container,'').getParentObject( type ); };

		/**
		 * Associative array for keeping panel object  elements.
		 * @public
		 * @type {Object}
		 */
		this.elements = {};

		/**
		 * Container element.
		 * @public
		 * @type {HTMLElement}
		 */
		this.elements.container = null;
		if ( e ) {
			this.elements.container = e;
			this.elements.container.classList.add ( this.props.classNames.container );
			this.props.elementId = !e.id ? false : e.id;

			var desk = window.lvzwebdesktop.getParentDesktop ( this.elements.container);
			if ( desk ) desk.addObject(this, false);

			var attr;

			attr = e.getAttribute('onitemmovebegin');
			if ( attr !== null ) this.addEvent ( 'itemmovebegin', new Function ( 'event', attr ) );

			attr = e.getAttribute('onitemmove');
			if ( attr !== null ) this.addEvent ( 'itemmove', new Function ( 'event', attr ) );

			attr = e.getAttribute('onitemmoveend');
			if ( attr !== null ) this.addEvent ( 'itemmoveend', new Function ( 'event', attr ) );

			attr = e.getAttribute('locked');
			if ( attr == 'locked' || attr == 'true' ) this.lock();

			attr = getComputedStyle(document.body).getPropertyValue('--lvzwebdesktop-default-panel-theme').trim();
			if ( attr != '' ) {
				for ( var i = 0 ; i < this.elements.container.classList.length ; i ++ ) {
					if ( getComputedStyle(document.body).getPropertyValue('--lvzwebdesktop-theme-panel-'+this.elements.container.classList[i]).trim() == 'true' ) {
						this.props.themeClass = this.elements.container.classList[i];
						break;
					}
				}
				if ( this.props.themeClass === null ) {
					this.props.themeClass = attr;
					this.elements.container.classList.add(attr);
				}
			}
			else {
				console.error ("There is no theme loaded");
			}

			var position = e.getAttribute('position');
			if ( position !== null ) switch ( position ) {
				case 'top': case 'bottom': case 'left': case 'right':
				this.props.position = position;
				this.position(this.props.position);

				break;

				default:
				console.error ( 'Not a valid position for panel' );

			} else {
				if ( this.elements.container.classList.contains( this.props.classNames.topOrientation ) ) {
					position = 'top';
				} else if ( this.elements.container.classList.contains( this.props.classNames.bottomOrientation ) ) {
					position = 'bottom';
				} else if ( this.elements.container.classList.contains( this.props.classNames.leftOrientation ) ) {
					position = 'left';
				} else if ( this.elements.container.classList.contains( this.props.classNames.rightOrientation ) ) {
					position = 'right';
				} else {
					position = 'top';
				}

				this.props.position = position;
				this.position(this.props.position);
			}

			var tmpitms = [];
			for ( var i = 0 ; i < e.children.length; i ++ ) {
				tmpitms.push ( e.children [i] );
				this.addItem( tmpitms[i] );
			}
		} else {
			this.elements.container = document.createElement('div');
			this.elements.container.classList.add (this.props.classNames.container);
		}

		this.elements.container.dataPanelObject = this;
		this.elements.container.dataLVZWDESKTOPOBJ = this;

		//for ( var i = 0; i < this.elements.container.children.length; i++ ) {
			//this.addItem ( this.elements.container.children[i] );
		//}

		this.addEvent ( 'changedesktop', function ( e ) {
			var self = e.target;
			var cobj;
			for ( var i = 0; i < self.items.length; i ++ ) {
				cobj = self.items[i].firstElementChild.dataLVZWDESKTOPOBJ;
				if ( cobj && typeof cobj.onchangedesktop != 'undefined' ) {
					var pd = cobj.desktop;
					cobj.desktop = e.desktop;
					cobj.fireEvent ( 'changedesktop', {
						"target" : cobj,
						"desktop" : e.desktop,
						"previousDesktop" : pd,
						"inheritFrom" : self
					});
				}
			}
		});
	};

	/**
	 * List with all panel objects.
	 * @type {Array}
	 * @public
	 */
	PanelObject.list = [];

	window.addEventListener('resize', function ( e ) {
		for ( var i = 0; i < PanelObject.list.length; i ++ ) {
 			if ( PanelObject.list[i].props.position == 'top' ) {
				PanelObject.list[i].props.top = 0;
				PanelObject.list[i].props.left = 0;
			}
			else if ( PanelObject.list[i].props.position == 'bottom' ) {
				PanelObject.list[i].props.top = PanelObject.list[i].desktop.elements.root.offsetHeight - PanelObject.list[i].elements.container.offsetHeight;
				PanelObject.list[i].props.left = 0;
			}
			else if ( PanelObject.list[i].props.position == 'left' ) {
				PanelObject.list[i].props.top = 0;
				PanelObject.list[i].props.left = 0;
			} else if ( PanelObject.list[i].props.position == 'right' ) {
				PanelObject.list[i].props.top = 0;
				PanelObject.list[i].desktop.elements.root.offsetWidth - PanelObject.list[i].elements.container.offsetHeight;
			}
		}
	} );

	/**
	 * Function for handling mousedown event on panel items.
	 * @param {Event} e - Event object.
	 */
	function panelItemMD ( e ) {
		var self = e.target.dataPanelObject;
		if ( ! self ) return;
		var itemContainer = e.target;

		var eobj = {
			"target" : self,
			"item" : itemContainer
		};
		self.fireEvent('itemmovebegin', eobj);
		if ( eobj.defaultPrevented === true ) return;

		panelItemMD.itemContainer = itemContainer;
		panelItemMD.self = self;

		var o = itemContainer;
		panelItemMD.ol = 0;
		panelItemMD.prevLeftPos = itemContainer.style.left;
		panelItemMD.nextNode = o.nextSibling;
		panelItemMD.bol = o.offsetLeft;
		panelItemMD.peloff = 0;
		o = o.previousElementSibling;
		while ( o !== null ) {
			panelItemMD.peloff+=4;
			panelItemMD.ol += o.offsetWidth;
			o = o.previousElementSibling;
		}


		if ( itemContainer.nextElementSibling !== null )
			panelItemMD.neol = itemContainer.nextElementSibling.offsetLeft;

		panelItemMD.ox = e.clientX - panelItemMD.ol;
		panelItemMD.px = e.clientX;
		panelItemMD.py = e.clientY;

		itemContainer.removeEventListener('mousedown', panelItemMD );
		document.body.addEventListener('mousemove', panelItemMM );
		document.body.addEventListener('mouseup', panelItemMU );
	}

	/**
	 * Function for handling mousemove event for moving panel items.
	 * @param {Event} e - Event object.
	 */
	function panelItemMM ( e ) {
		var self = panelItemMD.self;
		var itmCont = panelItemMD.itemContainer;
		var lmv = e.clientX - panelItemMD.px;
		var l = lmv + panelItemMD.ox - panelItemMD.peloff;

		// Move item towards right side
		if ( lmv > 0 ) {
			var ne = itmCont.nextElementSibling;

			// If not hits on an item to the right
			if ( typeof ne == 'undefined'
				 || ne === null
				 || l + panelItemMD.ol + itmCont.offsetWidth + panelItemMD.peloff <= panelItemMD.neol
			   ) itmCont.style.left = l + 'px';

			else if ( typeof ne != 'undefined'
					  && ne !== null
					  && l + panelItemMD.ol + itmCont.offsetWidth + panelItemMD.peloff > panelItemMD.neol + ne.offsetWidth + itmCont.offsetWidth
					) {

				panelItemMD.peloff += 4;
				panelItemMD.px += ne.offsetWidth;
				itmCont.parentNode.insertBefore ( ne, itmCont  );
				if ( itmCont.nextElementSibling !== null )
					panelItemMD.neol = itmCont.nextElementSibling.offsetLeft;
			}

		}
		// Move item towards left side
		else if ( lmv < 0 ) {

			var pe = itmCont.previousElementSibling;
			if ( pe !== null ) var b = pe.getBoundingClientRect();

			// If not hits on an item to the left
			if ( typeof pe == 'undefined'
				 || pe === null
				 || e.clientX > b.left + b.width
			   ) itmCont.style.left = l + 'px';

			else if ( typeof pe != 'undefined'
					  && pe !== null
					  && e.clientX < b.left
					) {

				panelItemMD.peloff -= 4;
				panelItemMD.px -= pe.offsetWidth;

				itmCont.parentNode.insertBefore ( itmCont, pe  );
				panelItemMD.neol = pe.offsetLeft;
			}
		}

		var eobj = {
			"target" : self,
			"item" : itmCont
		};
		self.fireEvent('itemmove', eobj);
		// if ( eobj.defaultPrevented === true ) {
		// 	itemContainer.style.left = panelItemMD.prevLeftPos;
		// 	itemContainer.parentNode.insertBefore ( itemContainer, panelItemMD.nextNode );
		// }
	}

	/**
	 * Function for handling mousedown event on panel. Finalizes moving panel item.
	 * @param {Event} e - Event object.
	 */
	function panelItemMU ( e ) {
		var self = panelItemMD.self;
		var itemContainer = panelItemMD.itemContainer;
		var eobj = {
			"target" : self,
			"item" : itemContainer
		};
		self.fireEvent('itemmoveend', eobj);
		if ( eobj.defaultPrevented === true ) {
			itemContainer.style.left = panelItemMD.prevLeftPos;
			itemContainer.parentNode.insertBefore ( itemContainer, panelItemMD.nextNode );
		}
		panelItemMD.prevLeftPos = 0;
		document.body.removeEventListener('mousemove', panelItemMM );
		document.body.removeEventListener('mouseup', panelItemMU );
		itemContainer.addEventListener('mousedown', panelItemMD );
	}

	window.lvzwebdesktop.PanelObject = PanelObject;
	window.lvzwebdesktop.constructors.push({
		"selector" : ".web-desktop-panel,web-panel",
		"constructor" : PanelObject
	});

})();
