(function () {

	/**
	 * Constructor for creating button objects.
	 * @constructor
	 * @param {HTMLElement} [e] - Element to attach button object to.
	 */
	var ButtonObject = function ( e ) {
		ButtonObject.list.push(this);

		/**
		 * Defines the type of the object.
		 * @type {string}
		 * @public
		 */
		this.type = 'button';

		/**
		 * Associative array for button properties.
		 * @type {object}
		 * @public
		 */
		this.props = {};

		/**
		 * The ID of the object. If there is no ID then it's false.
		 * @public
		 * @type {string|false}
		 */
		this.props.elementId = false;

		/**
		 * CSS class name for theme in use.
		 * @type {null|string}
		 * @public
		 */
		this.props.themeClass = null;

		/**
		 * Button's top position in number of pixels.
		 * @type {number}
		 * @public
		 */
		this.props.top = 0;

		/**
		 * Button's left position in number of pixles.
		 * @type {number}
		 * @public
		 */
		this.props.left = 0;

		/**
		 * Button's width in number of pixels.
		 * @type {number}
		 * @public
		 */
		this.props.width = 0;

		/**
		 * Button's height in number of pixels.
		 * @type {number}
		 * @public
		 */
		this.props.height = 0;

		/**
		 * Associative array for button element class names.
		 * @type {object}
		 * @public
		 */
		this.props.classNames = {};

		/**
		 * CSS class-name for button element.
		 * @type {string}
		 * @public
		 */
		this.props.classNames.container = ButtonObject.classNames.container;

		/**
		 * Array for storing event functions for clicking button.
		 * @type {Array}
		 * @public
		 */
		this.onclick = [];

		/**
		 * Array for storing event functions for moving button.
		 * @type {Array}
		 * @public
		 */
		this.onmove = [];

		/**
		 * Array for storing event functions for resizing button.
		 * @type {Array}
		 * @public
		 */
		this.onresize = [];

		/**
		 * Method for adding events to button.
		 *
		 * @param {string} e - Event name.
		 * @param {function} f - Function to run on event.
		 * @return {mixed} False on error, current button object otherwise.
		 */
		this.addEvent = window.lvzwebdesktop.fn.addEvent;

		/**
		 * Method for removing events from button.
		 *
		 * @param {string} e - The name of the event to remove.
		 * @param {function} [f] - The function to remove. If this param is set then method will remove only the function that matches. Otherwise it will clear all functions from event.
		 * @return {mixed} False on error, current button object otherwise.
		 */
		this.removeEvent = window.lvzwebdesktop.fn.removeEvent;

		/**
		 * Method for firing events.
		 *
		 * @param {string} e - The name of the event to fire.
		 * @param {object} obj - An object with properties to add to event object.
		 * @return {mixed} False on error, current button object otherwise.
		 */
		this.fireEvent = window.lvzwebdesktop.fn.fireEvent;

		/**
		 * Method for setting vertical position of button. Fires "move" event.
		 * @param {number} t - Distance in pixels, from parent element's top border.
		 * @return {ButtonObject} This button object.
		 */
		this.setTop = function ( t ) {
			var eobj = {
				"target" : this,
				"oldTop" : this.props.top,
				"oldLeft" : this.props.left,
				"newTop" : t,
				"newLeft" : this.props.left
			};
			this.fireEvent('move',eobj);
			if ( eobj.defaultPrevented === true ) return this;
			this.props.top = t;
			this.elements.container.style.top = t + 'px';
			return this;
		};

		/**
		 * Method for setting horizontal position of button. Fires "move" event.
		 * @param {nubmer} l - Distance in pixels, from parent element's left border.
		 * @return {ButtonObject} This button object.
		 */
		this.setLeft = function ( l ) {
			var eobj = {
				"target" : this,
				"oldTop" : this.props.top,
				"oldLeft" : this.props.left,
				"newTop" : this.props.top,
				"newLeft" : l
			};
			this.fireEvent('move',eobj);
			if ( eobj.defaultPrevented === true ) return this;
			this.props.left = l;
			this.elements.container.style.left = l + 'px';
			return this;
		};

		/**
		 * Method for changing buttons width.
		 * @param {number} w - The desired width in number of pixels.
		 * @return {ButtonObject} This button object.
		 */
		this.setWidth = function ( w ) {
			this.elements.container.style.width = w + 'px';
			return this;
		};

		/**
		 * Method for changing button's height.
		 * @param {number} h - The desired height in number of pixels.
		 * @return {ButtonObject} This button object.
		 */
		this.setHeight = function ( h ) {
			this.elements.container.style.height = h + 'px';
			return this;
		};

		/**
		 * Method for disabling button.
		 * @return {ButtonObject} This button object.
		 */
		this.disable = function ( ) {
			this.elements.container.disabled = true;
			return this;
		};

		/**
		 * Method for enabling button.
		 * @return {ButtonObject} This button object.
		 */
		this.enable = function ( ) {
			this.elements.container.disabled = false;
			return this;
		};

		/**
		 * Returns the first parent object of given type.
		 *
		 * @param {function|string} type - lvzwebobject constructor or constructor's name.
		 * @return {object|null|false} Return's the first parent lvzwebobject that matches "type" parameter. if not found returns null. on error returns false.
		 */
		this.getParentOfType = function ( type ) { return window.lvzwebdesktop(this.elements.container,'').getParentObject( type ); };

		/**
		 * Associative array for storing button elements.
		 * @type {object}
		 * @public
		 */
		this.elements = {};

		/**
		 * button element.
		 * @type {HTMLButtonElement|HTMLInputElement}
		 * @public
		 */
		this.elements.container = null;

		if ( typeof e == 'object' ) {
			if ( e instanceof HTMLButtonElement || ( e instanceof HTMLInputElement && e.type == 'button' ) ) {

				this.elements.container = e;
				this.props.elementId = !e.id ? false : e.id;

				var tmpattr;

				tmpattr = e.getAttribute('onclick');
				if ( tmpattr !== null ) {
					this.addEvent ( 'click', new Function ( 'event', tmpattr ) );
					e.setAttribute ( 'onbuttonclick', tmpattr );
					e.removeAttribute ( 'onclick' );
				}

				tmpattr = e.getAttribute('onmove');
				if ( tmpattr !== null ) this.addEvent ( 'move' , new Function ( 'event' , tmpattr ) );

				tmpattr = e.getAttribute('onresize');
				if ( tmpattr !== null ) this.addEvent ( 'resize' , new Function ( 'event' , tmpattr ) );

				tmpattr = getComputedStyle(document.body).getPropertyValue('--lvzwebdesktop-default-button-theme').trim();
				if ( tmpattr != '' ) {
					for ( var i = 0 ; i < this.elements.container.classList.length ; i ++ ) {
						if ( getComputedStyle(document.body).getPropertyValue('--lvzwebdesktop-theme-button-'+this.elements.container.classList[i]).trim() == 'true' ) {
							this.props.themeClass = this.elements.container.classList[i];
							break;
						}
					}
					if ( this.props.themeClass === null ) {
						this.props.themeClass = tmpattr;
						this.elements.container.classList.add(tmpattr);
					}
				}
				else {
					console.error ("There is no theme loaded for this button.");
				}

			}
			else {
				console.error ( "Invalid object type" );
				return false;
			}
		}
		else {
			this.elements.container = document.createElement('button');
		}

		this.elements.container.classList.add ( this.props.classNames.container );
		this.elements.container.dataLVZWDESKTOPOBJ = this;
		this.elements.container.dataButtonObject = this;
		this.props.height = this.elements.container.offsetHeight;
		this.props.width = this.elements.container.offsetWidth;

		lvzwebdesktop.resizeObserver.observe ( this.elements.container );

		this.elements.container.addEventListener('click', function ( e ) {
			var self = e.target.dataLVZWDESKTOPOBJ;
			var eobj={
				"buttons" : e.buttons,
				"button" : e.button,
				"clientX" : e.clientX,
				"clientY" : e.clientY,
				"layerX" : e.layerX,
				"layerY" : e.layerY,
				"offsetX" : e.offsetX,
				"offsetY" : e.offsetY,
				"pageX" : e.pageX,
				"pageY" : e.pageY,
				"screenX" : e.screenX,
				"screenY" : e.screenY,
				"x" : e.x,
				"y" : e.y,
				"altKey" : e.altKey,
				"ctrlKey" : e.ctrlKey,
				"metaKey" : e.metaKey,
				"shiftKey" : e.shiftKey,
				"target" : self,
				"element" : e.target
			};
			self.fireEvent('click',eobj);
		});
	};

	/**
	 * List with all button objects.
	 * @type {Array}
	 * @public
	 */
	ButtonObject.list = [];

	/**
	 * Associative array for button object's default class names.
	 * @type {object}
	 * @public
	 */
	ButtonObject.classNames = {};

	/**
	 * Defautl class name for button object's elements.
	 * @type {string}
	 * @public
	 */
	ButtonObject.classNames.container = 'web-desktop-button';

	/**
	 * Constructor for creating textbox objects.
	 * @constructor
	 * @param {HTMLInputElement} [e] - Element to attach text object to.
	 */
	var TextboxObject = function ( e ) {
		TextboxObject.list.push(this);

		/**
		 * Defines the type of the object.
		 * @type {'textbox'}
		 * @public
		 */
		this.type = 'textbox';

		/**
		 * Associative array for textbox properties.
		 * @public
		 * @type {object}
		 */
		this.props = {};

		/**
		 * The ID of the object. If there is no ID then it must be boolean false.
		 * @public
		 * @type {string|false}
		 */
		this.props.elementId = false;

		/**
		 * CSS class name for theme in use.
		 * @type {null|string}
		 * @public
		 */
		this.props.themeClass = null;

		/**
		 * Textbox value.
		 * @public
		 * @type {string}
		 */
		this.props.value = '';

		/**
		 * Textbox top position in number of pixels.
		 * @type {number}
		 * @public
		 */
		this.props.top = 0;

		/**
		 * Textbox left position in number of pixels.
		 * @type {number}
		 * @public
		 */
		this.props.left = 0;

		/**
		 * Textboxe width in number of pixels.
		 * @type {number}
		 * @public
		 */
		this.props.width = 0;

		/**
		 * Textbox height in number of pixels.
		 * @type {number}
		 * @public
		 */
		this.props.height = 0;

		/**
		 * Associative array for textbox element class names.
		 * @type {object}
		 * @public
		 */
		this.props.classNames = {};

		/**
		 * CSS class-name for textbox element.
		 * @type {string}
		 * @public
		 */
		this.props.classNames.container = TextboxObject.classNames.container;

		/**
		 * Array for storing event functions for moving textbox.
		 * @type {Array}
		 * @public
		 */
		this.onmove = [];

		/**
		 * Array for storing event functions for resizing textbox.
		 * @type {Array}
		 * @public
		 */
		this.onresize = [];

		/**
		 * Array for storing event functions for when changing textbox value.
		 * @type {Array}
		 * @public
		 */
		this.onchange = [];

		/**
		 * Array for storing event functions for when textbox gets focus.
		 * @type {Array}
		 * @public
		 */
		this.onfocus = [];

		/**
		 * Array for storing event functions for whet textbox looses focus.
		 * @type {Array}
		 * @public
		 */
		this.onblur = [];

		/**
		 * Array for storing event functions for keyup on textbox.
		 * @public
		 * @type {Array}
		 */
		this.onkeyup = [];

		/**
		 * Array for storing event functions for keypress on textbox.
		 * @public
		 * @type {Array}
		 */
		this.onkeypress = [];

		/**
		 * Array for storing event functions for keydown on textbox.
		 * @public
		 * @type {Array}
		 */
		this.onkeydown = [];

		/**
		 * Method for adding events to textbox.
		 *
		 * @param {string} e - Event name.
		 * @param {function} f - Functions to run on event.
		 * @return {TextboxObject|false} False on error, this textbox object otherwise.
		 */
		this.addEvent = window.lvzwebdesktop.fn.addEvent;

		/**
		 * Method for removing events from textbox.
		 *
		 * @param {string} e - The name of the event to remove.
		 * @param {function} [f] - The function to remove. If this parameter is set then method will remove only the function that matches. Otherwise it will clear all functions from event.
		 * @return {TextboxObject|false} False on error, this textbox object otherwise.
		 */
		this.removeEvent = window.lvzwebdesktop.fn.removeEvent;

		/**
		 * Method for firing events.
		 *
		 * @param {string} e - The name of the event to fire.
		 * @param {object} obj - An object with properties to add to event object.
		 * @return {TextboxObject|false} False on error, this textbox object otherwise.
		 */
		this.fireEvent = window.lvzwebdesktop.fn.fireEvent;

		/**
		 * Method for setting vertical position of textbox. Fires "move" event.
		 * @param {number} t - Distance in pixels, from parent element's top border.
		 * @return {TextboxObject} This textbox object.
		 */
		this.setTop = function ( t ) {
			var eobj = {
				"target" : this,
				"oldTop" : this.props.top,
				"oldLeft" : this.props.left,
				"newTop" : t,
				"newLeft" : this.props.left
			};
			this.fireEvent('move',eobj);
			if ( eobj.defaultPrevented === true ) return this;
			this.props.top = t;
			this.elements.container.style.top = t + 'px';
			return this;
		};

		/**
		 * Method for setting horizontal position of textbox. Fires 'move' event.
		 * @param {number} l - Distance in pixels, from parent element's left border.
		 * @return {TextboxObject} This textbox object.
		 */
		this.setLeft = function ( l ) {
			var eobj = {
				"target" : this,
				"oldTop" : this.props.top,
				"oldLeft" : this.props.left,
				"newTop" : this.props.top,
				"newLeft" : l
			};
			this.fireEvent('move',eobj);
			if (eobj.defaultPrevented === true ) return this;
			this.props.left = l;
			this.elements.container.style.left = l + 'px';
			return this;
		};

		/**
		 * Method for changing textbox's width.
		 * @param {number} w - The desired width in number of pixels.
		 * @return {TextboxObject} This textbox object.
		 */
		this.setWidth = function ( w ) {
			this.elements.container.style.width = w + 'px';
			return this;
		};

		/**
		 * Method for changing textbox's height.
		 * @param {number} h - The desired height in number of pixels.
		 * @return {TextboxObject} This textbox object.
		 */
		this.setHeight = function ( h ) {
			this.elements.container.style.height = h + 'px';
			return this;
		};

		/**
		 * Method for disabling textbox.
		 * @return {TextboxObject} This textbox object.
		 */
		this.disable = function () {
			this.elements.container.disabled = true;
			return this;
		};

		/**
		 * Method for enabling textbox.
		 * @return {TextboxObject} This textbox object.
		 */
		this.enable = function () {
			this.elements.container.disabled = false;
			return this;
		};

		/**
		 * Method for changing textbox's text.
		 * @return {TextboxObject} This textbox object.
		 */
		this.setText = function ( t ) {
			this.elements.container.value = t;
			return this;
		};

		/**
		 * Returns the first parent object of given type.
		 *
		 * @param {function|stirng} type - lvzwebobject constructor of constructor's name.
		 * @reutrn {object|null|false} Returns the first parent lvzwebobject that matches "type" parameter. if not found returns null. on error returns false.
		 */
		this.getParentOfType = function ( type ) { return window.lvzwebdesktop(this.elements.container,'').getParentObject( type ); };

		/**
		 * Associative array for storing textbox elements.
		 * @type {object}
		 * @public
		 */
		this.elements = {};

		/**
		 * Textbox element.
		 * @type {HTMLInputElement}
		 * @public
		 */
		this.elements.container = null;

		if ( typeof e != 'undefined' ) {
			if ( e instanceof HTMLInputElement && e.type == 'text' ) {

				this.elements.container = e;
				this.props.elementId = !e.id ? false : e.id;

				var tmpattr;

				tmpattr = e.getAttribute('onclick');
				if ( tmpattr !== null ) {
					this.addEvent ( 'click', new Function ( 'event', tmpattr ) );
					e.setAttribute ('ontextboxclick', tmpattr );
					e.removeAttribute('onclick');
				}

				tmpattr = e.getAttribute ('onmove');
				if ( tmpattr !== null ) this.addEvent('move', new Function ( 'event', tmpattr ) );

				tmpattr = getComputedStyle(document.body).getPropertyValue('--lvzwebdesktop-default-textbox-theme').trim();
				if ( tmpattr != '' ) {
					for ( var i = 0 ; i < this.elements.container.classList.length ; i ++ ) {
						if ( getComputedStyle(document.body).getPropertyValue('--lvzwebdesktop-theme-textbox-'+this.elements.container.classList[i]).trim() == 'true' ) {
							this.props.themeClass = this.elements.container.classList[i];
							break;
						}
					}
					if ( this.props.themeClass === null ) {
						this.props.themeClass = tmpattr;
						this.elements.container.classList.add(tmpattr);
					}
				}
				else {
					console.error ("There is no theme loaded for this button.");
				}

			}
			else {
				console.error ( "Invalid element type" );
				return false;
			}
		}
		else {

			this.elements.container = document.createElement('input');
			this.elements.container.type = 'text';

		}

		this.elements.container.classList.add ( this.props.classNames.container );
		this.elements.container.dataLVZWDESKTOPOBJ = this;
		this.elements.container.dataTextboxObject = this;
		this.elements.container.addEventListener('change', function ( e ) {
			var self = e.target.dataLVZWDESKTOPOBJ;
			self.fireEvent('change',{
				"target" : self,
				"oldValue" : self.props.value,
				"newValue" : e.target.value
			});
			self.props.value = e.target.value;
		});
		this.elements.container.addEventListener('focus', function ( e ) {
			var self = e.target.dataLVZWDESKTOPOBJ;
			self.fireEvent('focus',{
				"target" : self,
			});
		});
		this.elements.container.addEventListener('blur', function ( e ) {
			var self = e.target.dataLVZWDESKTOPOBJ;
			self.fireEvent('blur',{
				"target" : self,
			});
		});

		this.elements.container.addEventListener('keyup', textboxkeypress);
		this.elements.container.addEventListener('keypress', textboxkeypress);
		this.elements.container.addEventListener('keydown', textboxkeypress);

		lvzwebdesktop.resizeObserver.observe ( this.elements.container );
	};

	/**
	 * List with all textbox objects.
	 * @type {Array}
	 * @public
	 */
	TextboxObject.list = [];

	/**
	 * Associative array for textbox object's default class names.
	 * @type {object}
	 * @public
	 */
	TextboxObject.classNames = {};

	/**
	 * Default class name for textbox object's element.
	 * @type {string}
	 * @public
	 */
	TextboxObject.classNames.container = 'web-desktop-textbox';

	window.lvzwebdesktop.ButtonObject = ButtonObject;
	window.lvzwebdesktop.constructors.push({
		"selector" : "." + ButtonObject.classNames.container,
		"constructor" : ButtonObject
	});

	window.lvzwebdesktop.TextboxObject = TextboxObject;
	window.lvzwebdesktop.constructors.push({
		"selector" : "." + TextboxObject.classNames.container,
		"constructor" : TextboxObject
	});

	/**
	 * Handle keypress events on textboxes.
	 * @param {Event} e - event object.
	 */
	function textboxkeypress ( e ) {
		var self = e.target.dataLVZWDESKTOPOBJ;
		var eobj = {
			"target" : self,
			"key" : e.key,
			"charCode" : e.charCode,
			"keyCode" : e.keyCode
		};
		self.fireEvent(e.type,eobj);
		if ( eobj.defaultPrevented === true ) e.preventDefault();
	}

})();
