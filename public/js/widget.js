{
	document.currentScript.dataLVZWLIB = true;

	/**
	 * Constructor for creating widget objects.
	 * @constructor
	 * @param {HTMLElement} [e] - Element to attach widget to.
	 */
	var WidgetObject = function ( e ) {

		lvzwebdesktop.basicObject.call(this,'widget');

		/**
		 * Method for attaching elements or lvzwebdesktop objects to the widget.
		 * Fires the events "itemattach" and "itemattached".
		 * @param {HTMLElement|object} o - The html element or lvzwebdesktop object to attach to the widget.
		 * @return {WidgetObject} Current widget object.
		 */
		this.attachItem = function ( o ) {

			var eventData = {
				"target" : this,
				"item" : o
			};

			if ( this.props.attachedItem !== null ) {
				console.warn("Must first dettach attached item before attach a new.");
				return this;
			}

			this.fireEvent('itemattach',eventData);
			if ( eventData.defaultPrevented === true ) return this;

			if ( typeof o == 'object' ) {

				if ( o instanceof HTMLElement ) {

					if ( typeof o.dataLVZWDESKTOPOBJ == 'object' ) {
						if ( typeof o.dataLVZWDESKTOPOBJ.elements == 'object' ) {
							if ( o.dataLVZWDESKTOPOBJ.elements.container instanceof HTMLElement ) {
								this.elements.body.appendChild( o.dataLVZWDESKTOPOBJ.elements.container );
								this.props.attachedItem = o;
							}
						}
					}
					else {
						this.elements.body.appendChild ( o );
						this.props.attachedItem = o;
					}
				}
				else if ( lvzwebdesktop.isPartOf(o) !== false ) {

					if ( ['widget'].indexOf === -1 ) {
						this.elements.body.appendChild( o.elements.container );
						this.props.attachedItem = o;
					}

				}
				else {
					console.warn ( "Unknown item type to be attached." );
					return this;
				}

			}

			eventData = {
				"target" : this,
				"item" : o
			};

			return this.fireEvent( 'itemattached', eventData );

		};

		/**
		 * Method for detaching elements of lvzwebdesktop objects from the widget.
		 * Fires the events "itemdetach" and "itemdetached".
		 * @return {WidgetObject} Current widget object.
		 */
		this.detachItem = function () {
			var eventData = {"target" : this,
							 "item" : this.props.attachedItem
							};

			var o;

			if ( this.props.attachedItem !== null ) {
				if ( this.props.attachedItem instanceof HTMLElement ) {
					o = this.props.attachedItem;
				}
				else if ( window.lvzwebdestkop.isPartOf(this.props.attachedItem) !== false ) {
					o = this.props.attachedItem.elements.container;
				}

				this.fireEvent('itemdetach',eventData);
				if (eventData.defaultPrevented === true ) return this;
				this.props.attachedItem = null;
				this.elements.body.removeChild(this.props.attachedItem);
				eventData = {"target" : this,
							 "item" : this.props.attachedItem
							};
				this.fireEvent('itemdetached',eventData);
			}

			return this;
		};

		/**
		 * Method for attaching this object to another lvzwebdesktop object or an html element.
		 * Fires the "attachto" event.
		 * @param {HTMLElement|object} item - The lvzwebdesktop object or the html element to attach this object to.
		 * @return {WidgetObject} This widget object.	
		 */
		this.attachTo = function ( item ) {
			if ( item instanceof HTMLElement ) {
				if ( typeof item.dataLVZWDESKTOPOBJ == 'object' ) {
					if ( typeof item.dataLVZWDESKTOPOBJ.attachItem == 'function' ) {
						if ( item.dataLVZWDESKTOPOBJ.type == 'desktop' ) this.desktop = item.dataLVZWDESKTOPOBJ;
						item.dataLVZWDESKTOPOBJ.attachItem(this.elements.container);
						this.props.attachedTo = item;
					}
					else {
						console.warn("Argument must be an html element or an lvzwebdesktop object having \"attachItem\" method." );
						return this;
					}
				}
				else {
					var eventData = {
						"target" : this,
						"attachElement" : item,
						"item" : item
					};
					item.appendChild( this.elements.container );
					this.props.attachedTo = item;
					this.fireEvent('attachto', eventData);
				}
			}
			else {
				var c = window.lvzwebdesktop.isPartOf( item );
				if ( c === false || typeof item.attachItem != 'function' ) {
					console.warn ( "Argument must be an html element or on lvzwebdesktop object having \"attachItem\" method." );
					return this;
				}
				if ( item.type == 'desktop' ) this.desktop = item;
				item.attachItem ( this ) ;
				this.props.attachedTo = item;
			}

			return this;
		};

		/**
		 * Method for resize the widget based on it's contents size.
		 * @param {boolean} t - true for resizing widget by it's content size, false for keeping a standard size.
		 * @return {WidgetObject} Current widget object.
		 */
		this.resizeOnContent = function ( t ) {
			if ( typeof t !== 'undefined' ) this.props.resizeOnContent = t;

			if ( this.elements.body.children.length > 0  ) {
				if (this.props.resizeOnContent === true) {
					this.resizeObserver.observe(this.elements.body.children[0]);
				}
				else {
					this.resizeObserver.unobserve(this.elements.body.children[0]);
				}
			}

			return this.matchContentSize();
		};

		/**
		 * Method for resizing widget matching it's content's size.
		 * @return {WidgetObject} This widget object.
		 */
		this.matchContentSize = function () {
			var prevHeight = this.elements.body.offsetHeight;
			var prevWidth = this.elements.body.offsetWidth;

			var pw = this.elements.body.style.height;
			var ph = this.elements.body.style.height;

			this.elements.body.style.display = 'inline-block';
			this.elements.body.style.height = '';
			this.elements.body.style.width = '';

			var currentHeight = this.elements.body.offsetHeight;
			var currentWidth = this.elements.body.offsetWidth;

			this.elements.body.style.display = 'block';

			var eventData = {
				"target" : this,
				"previousWidth" : prevWidth,
				"previousHeight" : prevHeight,
				"newWidth" : currentWidth,
				"newHeight" : currentHeight
			};
			this.fireEvent('resize', eventData );
			if ( eventData.defaultPrevented === true ) {
				this.elements.body.style.height = ph;
				this.elements.body.style.width = pw;
				return this;
			}

			if ( currentHeight < this.props.minHeight ) currentHeight = this.props.minHeight;
			if ( currentWidth  < this.props.minWidth  ) currentWidth  = this.props.minWidth;

			this.props.height = currentHeight;
			this.props.width = currentWidth;

			this.elements.body.style.height = currentHeight + 'px';
			this.elements.body.style.width = currentWidth + 'px';

			return this;
		};

		/**
		 * Dummy method for not returning error if called by applications thinking they are in a window.
		 * @return {WidgetObject} This widget object.
		 */
		this.setLayout = function (s){
			return this;
		};

		/**
		 * Method for setting movable state of the widget to true/false.
		 * @param {boolean} v - The value to set the movable state of the widget to.
		 * @return {WidgetObject} This widget object.
		 */
		this.setMovable = function ( v ) {
			this.props.movable = v === true;
			return this;
		};

		/**
		 * Method for setting resizable state of the widget to true/false.
		 * @param {boolean} v - The value to set the resizable state of the widget to.
		 * @return {WidgetObject} This widget object.
		 */
		this.setResizable = function ( v ) {
			this.props.resizable = v === true;
			return this;
		};

		/**
		 * Method for setting editable state of the widget to true/false.
		 * Fires the "editstatuschange" event.
		 * @param {boolean} v - The value to set to edit state.
		 * @return {WidgetObject} This widget object.
		 */
		this.setEditable = function ( v ) {
			var tmpv = v === true;
			var oldv = this.props.editable;
			if ( tmpv != oldv ) {
				var eventData = {
					"target" : this,
					"oldValue" : oldv,
					"newValue" : tmpv
				};
				this.props.editable = tmpv;
				this.fireEvent('editstatuschange',eventData);
				if ( eventData.defaultPrevented === true ) {
					this.props.editable = oldv;
					return this;
				}

				if ( this.props.editable === true )
					this.elements.container.classList.add(this.props.classNames.editable );
				else
					this.elements.container.classList.remove(this.props.classNames.editable );
			}

			return this;
		};

		/**
		 * Method for setting locked state of the widget to true/false.
		 * Fires the "lockstatuschange" event.
		 * @param {boolean} v - the value to set to lock state.
		 * @return {WidgetObject} This widget object.
		 */
		this.setLocked = function ( v ) {
			var tmpv = v === true;
			var oldv = this.props.lock;
			if ( tmpv != oldv ) {
				var eventData={
					"target" : this,
					"oldValue" : oldv,
					"newValue" : tmpv
				};
				this.props.lock = tmpv;
				this.fireEvent('lockstatuschange', eventData);
				if ( eventData.defaultPrevented === true ) {
					this.props.lock = oldv;
					return this;
				}
				if ( this.props.lock === true ) {
					this.elements.container.classList.add(this.props.classNames.locked);
					this.elements.controls.classList.add(this.props.classNames.locked);
					this.elements.lockControlBtn.classList.remove(this.props.classNames.lockControlBtn);
					this.elements.lockControlBtn.classList.add(this.props.classNames.unlockControlBtn);
				}
				else {
					this.elements.container.classList.remove(this.props.classNames.locked);
					this.elements.controls.classList.remove(this.props.classNames.locked);
					this.elements.lockControlBtn.classList.add(this.props.classNames.lockControlBtn);
					this.elements.lockControlBtn.classList.remove(this.props.classNames.unlockControlBtn);
				}
			}
			return this;
		};

		/**
		 * Method for setting the top position of the widget.
		 * Fires the "onmove" event.
		 * @param {Number} v - The desired distance, in number of pixels, of the top border of the widget from the top border of the parent element.
		 * @return {WidgetObject} Current widget object.
		 */
		this.setTop = function ( v ) {
			if ( this.props.movable !== true ) return this;
			if ( this.props.minTop !== false && v < this.props.minTop ) v = this.props.minTop;
			if ( this.props.maxTop !== false && v > this.props.maxTop ) v = this.props.maxTop;

			if ( this.props.minBottom !== false && this.elements.container.parentNode.offsetHeight - (v + this.elements.container.offsetHeight) < this.props.minBottom ) {
				v = this.elements.container.parentNode.offsetHeight - this.props.minBottom - this.elements.container.offsetHeight;
			}

			if ( this.props.maxBottom !== false && this.elements.container.parentNode.offsetHeight - (v + this.elements.container.offsetHeight) > this.props.maxBottom ) {
				v = this.elements.container.parentNode.offsetHeight - this.props.maxBottom - this.elements.container.offsetHeight;
			}
				
			var eventData = {
				"target" : this,
				"previousTopValue" : this.props.top,
				"currentTopValue" : v
			};
			this.fireEvent('move',eventData);
			if ( eventData.preventDefault.a === true ) return this;
			this.props.top = v;
			this.elements.container.style.top = v + 'px';

			if ( this.props.controlsBarPosition == 'bottom' ) {
				if ( this.elements.controls.dataPosSwitched == false
					 && this.props.top + this.elements.container.offsetHeight + this.elements.controls.offsetHeight > this.elements.container.parentNode.offsetHeight
				   ) {
					this.switchControlsBarPosition();
				}
				else if ( this.elements.controls.dataPosSwitched == true
						  && this.props.top + this.elements.container.offsetHeight + this.elements.controls.offsetHeight <= this.elements.container.parentNode.offsetHeight
						) {
					this.switchControlsBarPosition();
				}
			}
			else if ( this.props.controlsBarPosition == 'top' ) {
				if ( this.elements.controls.dataPosSwitched == false
					 && this.props.top - this.elements.controls.offsetHeight < 0
				   ) {
					this.switchControlsBarPosition();
				}
				else if ( this.elements.controls.dataPosSwitched == true
						  && this.props.top - this.elements.controls.offsetHeight >= 0
						) {
					this.switchControlsBarPosition();
				}
			}

			return this.controlsBarPosition();

		};

		/**
		 * Method for setting the left position of the widget.
		 * Fires the "onmove" event.
		 * @param {Number} v - The desired distance, in number of pixels, of the left border of the widget from the left border of the parent element.
		 * @return {WidgetObject} Current widget object.
		 */
		this.setLeft = function ( v ) {
			if ( this.props.movable !== true ) return this;
			if ( this.props.minLeft !== false && v < this.props.minLeft ) v = this.props.minLeft;
			if ( this.props.maxLeft !== false && v > this.props.maxLeft ) v = this.props.maxLeft;

			if ( this.props.minRight !== false && this.elements.container.parentNode.offsetWidth - ( v + this.elements.container.offsetWidth ) < this.props.minRight ) {
				v = this.elements.container.parentNode.offsetWidth - this.props.minRight - this.elements.container.offsetWidth;
			}

			if ( this.props.maxRight !== false && this.elements.container.parentNode.offsetWidth - (v + this.elements.container.offsetWidth) > this.props.maxRight ) {
				v = this.elements.container.parentNode.offsetWidth - this.props.maxRight - this.elements.container.offsetWidth;
			}

			var eventData = {
				"target" : this,
				"previousLeftValue" : this.props.left,
				"currentLeftValue" : v
			};
			this.fireEvent('move',eventData);
			if ( eventData.preventDefault.a === true ) return this;
			this.props.left = v;
			this.elements.container.style.left = v + 'px';

			if ( this.props.controlsBarPosition == 'left' ) {
				if ( this.elements.controls.dataPosSwitched == false
					 && this.props.left - this.elements.controls.offsetWidth  < 0
				   ) {
					this.switchControlsBarPosition();
				}
				else if ( this.elements.controls.dataPosSwitched == true
						  && this.props.left - this.elements.controls.offsetWidth >= 0
						) {
					this.switchControlsBarPosition();
				}
			}
			else if ( this.props.controlsBarPosition == 'right' ) {
				if ( this.elements.controls.dataPosSwitched == false
					 && this.props.left + this.elements.container.offsetWidth + this.elements.controls.offsetWidth > this.elements.container.parentNode.offsetWidth
				   ) {
					this.switchControlsBarPosition();
				}
				else if ( this.elements.controls.dataPosSwitched == true
						  && this.props.left + this.elements.container.offsetWidth + this.elements.controls.offsetWidth <= this.elements.container.parentNode.offsetWidth
						) {
					this.switchControlsBarPosition();
				}
			}

			return this.controlsBarPosition();
		};

		/**
		 * Method for setting the right position of the widget.
		 * Fires the "onmove" event.
		 * @param {Number} v - The desired distance, in number of pixels, of the right border of the widget from the right border of the parent element.
		 * @return {WidgetObject} Current widget object.
		 */
		this.setRight = function ( v ) {
			return this.setLeft(this.elements.container.parentNode.offsetWidth-(v+this.elements.container.offsetWidth));
		};

		/**
		 * Method for setting the bottom position of the widget.
		 * Fires the "onmove" event.
		 * @param {Number} v - The desired distance, in number of pixels, of the bottom border of the widget from the bottom border of the parent element.
		 * @return {WidgetObject} Current widget object.
		 */
		this.setBottom = function ( v ) {
			return this.setTop(this.elements.container.parentNode.offsetHeight-(v+this.elements.container.offsetHeight));
		};

		/**
		 * Method for setting the width of the widget.
		 * Fires the "onresize" event.
		 * @param {Number} v - The desired width of the widget.
		 * @return {WidgetObject} This widget object.
		 */
		this.setWidth = function ( v ) {
			var eventData = {
				"target" : this,
				"previousWidthValue" : this.props.width,
				"currentWidthValue" : v
			};
			if ( this.props.minWidth !== false && v < this.props.minWidth ) v = this.props.minWidth;
			if ( this.props.maxWidth !== false && v > this.props.maxWidth ) v = this.props.maxWidth;
			this.fireEvent('resize',eventData);
			if ( eventData.defaultPrevented === true ) return this;
			this.props.width = v;
			this.refresh();
			return this;
		};

		/**
		 * Method for setting the width of the widget but keeping width/height ratio.
		 * Fires the "onresize" event.
		 * @param {Number} v - The desired width of the widget.
		 * @return {WidgetObject} This widget object.
		 */
		this.setWidthKeepRatio = function ( v ) {
			var oldw = this.props.width;
			var oldh = this.props.height;
			var ratio = oldh / oldw;
			var newh = v * ratio;

			var eventData = {
				"target" : this,
				"previousHeightValue" : oldh,
				"currentHeightValue" : newh,
				"previousWidthValue" : oldw,
				"currentWidthValue" : v
			};

			if ( this.props.minHeight !== false && v < this.props.minHeight ) v = this.props.minHeight;
			if ( this.props.maxHeight !== false && v > this.props.maxHeight ) v = this.props.maxHeight;
			if ( this.props.minWidth !== false && v < this.props.minWidth ) v = this.props.minWidth;
			if ( this.props.maxWidth !== false && v > this.props.maxWidth ) v = this.props.maxWidth;
			this.fireEvent('resize',eventData);
			if ( eventData.defaultPrevented === true ) return this;

			this.props.height = newh;
			this.props.width = v;

			this.refresh();

			return this;
		};

		/**
		 * Method for setting the height of the widget.
		 * Fires the "onresize" event.
		 * @param {Number} v - The desired height of the widget.
		 * @return {WidgetObject} This widget object.
		 */
		this.setHeight = function ( v ) {
			var eventData = {
				"target" : this,
				"previousHeightValue" : this.props.height,
				"currentHeightValue" : v
			};
			if ( this.props.minHeight !== false && v < this.props.minHeight ) v = this.props.minHeight;
			if ( this.props.maxHeight !== false && v > this.props.maxHeight ) v = this.props.maxHeight;
			this.fireEvent('resize',eventData);
			if ( eventData.defaultPrevented === true ) return this;
			this.props.height = v;
			this.refresh();
			return this;
		};

		/**
		 * Method for setting the height of the widget but keeping width/height ratio.
		 * Fires the "onresize" event.
		 * @param {Number} v - The desired height of the widget.
		 * @return {WidgetObject} This widget object.
		 */
		this.setHeightKeepRatio = function ( v ) {
			var oldw = this.props.width;
			var oldh = this.props.height;
			var ratio = oldw / oldh;
			var neww = v * ratio;

			var eventData = {
				"target" : this,
				"previousHeightValue" : oldh,
				"currentHeightValue" : v,
				"previousWidthValue" : oldw,
				"currentWidthValue" : neww
			};

			if ( this.props.minHeight !== false && v < this.props.minHeight ) v = this.props.minHeight;
			if ( this.props.maxHeight !== false && v > this.props.maxHeight ) v = this.props.maxHeight;
			if ( this.props.minWidth !== false && v < this.props.minWidth ) v = this.props.minWidth;
			if ( this.props.maxWidth !== false && v > this.props.maxWidth ) v = this.props.maxWidth;
			this.fireEvent('resize',eventData);
			if ( eventData.defaultPrevented === true ) return this;

			this.props.height = v;
			this.props.width = neww;

			this.refresh();

			return this;
		};

		/**
		 * Method for placing the resize elements.
		 * * @return {WidgetObject} This widget object.
		 */
		this.positionResizeElements = function () {
			var contvm = this.elements.container.offsetHeight / 2;
			var conthm = this.elements.container.offsetWidth / 2;

			this.elements.resizeTopLeft.style.top = '-' + this.elements.resizeTopLeft.offsetHeight + 'px';
			this.elements.resizeTopLeft.style.left = '-' + this.elements.resizeTopLeft.offsetWidth + 'px';
			this.elements.resizeTopCenter.style.top = '-' + this.elements.resizeTopCenter.offsetHeight + 'px';
			this.elements.resizeTopCenter.style.left = (conthm - this.elements.resizeTopCenter.offsetWidth / 2 ) + "px";
			this.elements.resizeTopRight.style.top = '-' + this.elements.resizeTopRight.offsetHeight + 'px';
			this.elements.resizeTopRight.style.right = '-' + this.elements.resizeTopRight.offsetWidth + 'px';
			this.elements.resizeMiddleLeft.style.top = ( contvm - this.elements.resizeMiddleLeft.offsetHeight / 2 ) + "px";
			this.elements.resizeMiddleLeft.style.left = '-' + this.elements.resizeMiddleLeft.offsetWidth + 'px';
			this.elements.resizeMiddleRight.style.top = (contvm - this.elements.resizeMiddleRight.offsetHeight / 2 ) +"px";
			this.elements.resizeMiddleRight.style.right = '-' + this.elements.resizeMiddleRight.offsetWidth + 'px';
			this.elements.resizeBottomLeft.style.bottom = '-' + this.elements.resizeBottomLeft.offsetHeight + 'px';
			this.elements.resizeBottomLeft.style.left = '-' + this.elements.resizeBottomLeft.offsetWidth + 'px';
			this.elements.resizeBottomCenter.style.bottom = '-' + this.elements.resizeBottomCenter.offsetHeight + 'px';
			this.elements.resizeBottomCenter.style.left = (conthm - this.elements.resizeBottomCenter.offsetWidth / 2) + "px";
			this.elements.resizeBottomRight.style.bottom = '-' + this.elements.resizeBottomRight.offsetHeight + 'px';
			this.elements.resizeBottomRight.style.right = '-' + this.elements.resizeBottomRight.offsetWidth + 'px';
			return this;
		};

		/**
		 * Method for start the process of resizing the widget.
		 * @return {WidgetObject} This widget object.
		 */
		this.startResize = function () {
			this.elements.container.classList.add(this.props.classNames.resize);
			this.elements.controls.classList.add(this.props.classNames.resize);
			this.positionResizeElements();
			return this;
		};

		/**
		 * Method for end the process of resizing the widget.
		 * @return {WidgetObject} This widget object.
		 */
		this.endResize = function () {
			this.elements.container.classList.remove(this.props.classNames.resize);
			this.elements.controls.classList.remove(this.props.classNames.resize);
			return this;
		};

		/**
		 * Method for starting the resize process or if it has already started, end it.
		 * @return {WidgetObject} This widget object.
		 */
		this.toggleResize = function () {
			if ( this.elements.container.classList.contains(this.props.classNames.resize) ) {
				return this.endResize();
			}
			return this.startResize();
		};

		/**
		 * Method to set lock state to locked.
		 * Fires the "lockstatuschange" event.
		 * @return {WidgetObject} This widget object.
		 */
		this.lock = function () {
			return this.setLocked(true);
		};

		/**
		 * Method to set lcok state to unlocked.
		 * Fires the "lockstatuschange" event.
		 * @return {WidgetObject} This widget object.
		 */
		this.unlock = function () {
			return this.setLocked(false);
		};

		/**
		 * Method to toggle lock state.
		 * Fires the "lockstatuschange" event.
		 * @return {WidgetObject} This widget object.
		 */
		this.toggleLock = function () {
			return this.setLocked(!this.props.lock);
		};

		/**
		 * Method for disabling the widget.
		 * Fires the "disable" and "enablestatechange" events.
		 * @return {WidgetObject} This widget object.
		 */
		this.disable = function () {
			var eventData = {"target":this,
							 "currentState":this.elements.vail.style.display=='block'?'disabled':'enabled',
							 "newState":'disabled'
							};
			this.fireEvent('disable',eventData);
			if ( eventData.defaultPrevented === true ) return this;
			this.fireEvent('enablestatechange',eventData);
			if ( eventData.defaultPrevented === true ) return this;
			this.elements.vail.style.display = 'block';
			this.elements.vail.style.position = 'relative';
			this.refresh();
			return this;
		};

		/**
		 * Method for enabling the widget.
		 * Fires the "enable" and "enablestatechange" events.
		 * @return {WidgetObject} This widget object.
		 */
		this.enable = function () {
			var eventData = {"target":this,
							 "currentState":this.elements.vail.style.display=='block'?'disabled':'enabled',
							 "newState":'enabled'
							};
			this.fireEvent('enable',eventData);
			if ( eventData.defaultPrevented === true ) return this;
			this.fireEvent('enablestatechange',eventData);
			if ( eventData.defaultPrevented === true ) return this;
			this.elements.vail.style.display = 'none';
			return this;
		};

		/**
		 * Method for setting the width and height of body and vail elements.
		 * @return {WidgetObject} This widget object.
		 */
		this.refresh = function () {
			if ( this.props.minWidth !== false && this.props.width < this.props.minWidth ) this.props.width = this.props.minWidth;
			if ( this.props.maxWidth !== false && this.props.width > this.props.maxWidth ) this.props.width = this.props.maxWidth;
			if ( this.props.minHeight !== false && this.props.height < this.props.minHeight ) this.props.height = this.props.minHeight;
			if ( this.props.maxHeight !== false && this.props.height > this.props.maxHeight ) this.props.height = this.props.maxHeight;
			this.elements.body.style.width = this.props.width + 'px';
			this.elements.vail.style.width = this.props.width + 'px';
			this.elements.body.style.height = this.props.height + 'px';
			this.elements.vail.style.height = this.props.height + 'px';
			this.elements.vail.style.top = '-' + this.elements.body.offsetHeight + 'px';
			this.elements.vail.style.overflow = 'hidden';
			return this;
		};

		/**
		 * Method for loading the attributes of the container element.
		 * @return {WidgetObject} This widget object.
		 */
		this.loadPositionAttributes = function () {
			var attr;

			attr = this.elements.container.getAttribute('top');
			if ( attr !== null ) this.setTop(parseFloat(attr));

			attr = this.elements.container.getAttribute('min-top');
			if ( attr !== null ) this.props.minTop = (attr === "false") ? false : parseFloat(attr);

			attr = this.elements.container.getAttribute('max-top');
			if ( attr !== null ) this.props.maxTop = (attr === "false") ? false : parseFloat(attr);

			attr = this.elements.container.getAttribute('left');
			if ( attr !== null ) this.setLeft(parseFloat(attr));

			attr = this.elements.container.getAttribute('min-left');
			if ( attr !== null ) this.props.minLeft = (attr === "false") ? false : parseFloat(attr);

			attr = this.elements.container.getAttribute('max-left');
			if ( attr !== null ) this.props.maxLeft = (attr === "false") ? false : parseFloat(attr);

			attr = this.elements.container.getAttribute('right');
			if ( attr !== null ) this.setRight(parseFloat(attr));

			attr = this.elements.container.getAttribute('min-right');
			if ( attr !== null ) this.props.minRight = (attr === "false") ? false : parseFloat(attr);

			attr = this.elements.container.getAttribute('max-right');
			if ( attr !== null ) this.props.maxRight = (attr === "false") ? false : parseFloat(attr);

			attr = this.elements.container.getAttribute('bottom');
			if ( attr !== null ) this.setBottom(parseFloat(attr));

			attr = this.elements.container.getAttribute('min-bottom');
			if ( attr !== null ) this.props.minBottom = (attr === "false") ? false : parseFloat(attr);

			attr = this.elements.container.getAttribute('max-bottom');
			if ( attr !== null ) this.props.maxBottom = (attr === "false") ? false : parseFloat(attr);

			attr = this.elements.container.getAttribute('width');
			if ( attr !== null ) this.setWidth(parseFloat(attr));

			attr = this.elements.container.getAttribute('min-width');
			if ( attr !== null ) this.props.minWidth = ( attr === "false" ) ? false : parseFloat(attr);

			attr = this.elements.container.getAttribute('max-width');
			if ( attr !== null ) this.props.maxWidth = ( attr === "false" ) ? false : parseFloat(attr);

			attr = this.elements.container.getAttribute('height');
			if ( attr !== null ) this.setHeight(parseFloat(attr));

			attr = this.elements.container.getAttribute('min-height');
			if ( attr !== null ) this.props.minHeight = ( attr === "false" ) ? false : parseFloat(attr);

			attr = this.elements.container.getAttribute('max-height');
			if ( attr !== null ) this.props.maxHeight = ( attr === "false" ) ? false : parseFloat(attr);

			return this;
		};

		/**
		 * Method for activating controls of the widget.
		 * @return {WidgetObject} This widget object.
		 */
		this.activateControls = function ( c ) {
			var l = c.split(/,/);
			for ( var i = 0; i < l.length; i++ ) if ( typeof this.elements[l[i]+'ControlBtn'] == 'object' ) {
				this.elements[l[i]+'ControlBtn'].style.display = '';
				this.elements.controls.appendChild(this.elements[l[i]+'ControlBtn']);
			}

			return this;
		};

		/**
		 * Method for deactivating controls of the widget.
		 * @return {WidgetObject} This widget object.
		 */
		this.deactivateControls = function ( c ) {
			var l = c.split(/,/);
			for ( var i = 0; i < l.length; i++ ) if ( typeof this.elements[l[i]+'ControlBtn'] == 'object' ) {
				this.elements[l[i]+'ControlBtn'].style.display = 'none';
				if ( this.elements[l[i]+'ControlBtn'].parentNode === this.elements.controls )
					this.elements.controls.removeChild(this.elements[l[i]+'ControlBtn']);
			}

			return this;
		};

		/**
		 * Method for getting the controls of the widget.
		 * @return {Array} A string array of the control names of the widget.
		 */
		this.getControls = function() {
			var ret = [];
			for ( var i in this.elements )
				if ( i.length > 10 && i.substr(0,i.length-10) )
					ret.push(i);
			return ret;
		};

		/**
		 * Method for setting the controls bar position.
		 * @param {'top','bottom','left','right','default'} [p] - The position of the bar.
		 * @return {WidgetObject} This widget object.
		 */
		this.controlsBarPosition = function ( p ) {
			this.elements.controls.style.display = 'block';
			this.elements.controls.style.position = 'absolute';
			var ow = this.elements.controls.offsetWidth;
			var oh = this.elements.controls.offsetHeight;
			var t = this.elements.container.offsetTop;
			var l = this.elements.container.offsetLeft;
			var w = this.elements.container.offsetWidth;
			var h = this.elements.container.offsetHeight;

			if ( typeof p == 'string' ) {
				if ( p == 'default' ) this.props.controlsBarPosition = WidgetObject.controlsBarPosition;
				else this.props.controlsBarPosition = p;
			}

			if ( (this.props.controlsBarPosition == 'top' && this.elements.controls.dataPosSwitched == false )
				 || (this.props.controlsBarPosition == 'bottom' && this.elements.controls.dataPosSwitched == true ) ) {
				this.elements.controls.classList.remove(this.props.classNames.verticalControlsBar);
				this.elements.controls.style.top = (t - oh) + 'px';
				this.elements.controls.style.left = l + 'px';
			}
			else if ( (this.props.controlsBarPosition == 'bottom' && this.elements.controls.dataPosSwitched == false )
					  || (this.props.controlsBarPosition == 'top' && this.elements.controls.dataPosSwitched == true ) ) {
				this.elements.controls.classList.remove(this.props.classNames.verticalControlsBar);
				this.elements.controls.style.top = (t + h) + 'px';
				this.elements.controls.style.left = l + 'px';
			}
			else if ( (this.props.controlsBarPosition == 'left' && this.elements.controls.dataPosSwitched == false )
					  || (this.props.controlsBarPosition == 'right' && this.elements.controls.dataPosSwitched == true ) ) {
				this.elements.controls.classList.add(this.props.classNames.verticalControlsBar);
				this.elements.controls.style.top = t + 'px';
				this.elements.controls.style.left = (l - ow) + 'px';
			}
			else if ( (this.props.controlsBarPosition == 'right' && this.elements.controls.dataPosSwitched == false )
					  || (this.props.controlsBarPosition == 'left' && this.elements.controls.dataPosSwitched == true ) ) {
				this.elements.controls.classList.add(this.props.classNames.verticalControlsBar);
				this.elements.controls.style.top = t + 'px';
				this.elements.controls.style.left = (l + w) + 'px';
			}

			return this;
		}

		/**
		 * Method for temporary switching the position of the controls bar to the opposite side.
		 * @return {WidgetObject} This widget object.
		 */
		this.switchControlsBarPosition = function () {
			if ( ( this.props.controlsBarPosition == 'top' && this.elements.controls.dataPosSwitched ) || ( this.props.controlsBarPosition == 'bottom' && this.elements.controls.dataPosSwitched == false ) ) {
				if ( this.props.controlsBarPosition == 'bottom' ) this.elements.controls.dataPosSwitched = true;
				else this.elements.controls.dataPosSwitched = false;
				this.elements.controls.classList.remove(this.props.classNames.verticalControlsBar);
			}
			else if ( ( this.props.controlsBarPosition == 'bottom' && this.elements.controls.dataPosSwitched ) || ( this.props.controlsBarPosition == 'top' && this.elements.controls.dataPosSwitched == false ) ) {
				if ( this.props.controlsBarPosition == 'top' ) this.elements.controls.dataPosSwitched = true;
				else this.elements.controls.dataPosSwitched = false;
				this.elements.controls.classList.remove(this.props.classNames.verticalControlsBar);
			}
			else if ( ( this.props.controlsBarPosition == 'left' && this.elements.controls.dataPosSwitched ) || ( this.props.controlsBarPosition == 'right' && this.elements.controls.dataPosSwitched == false ) ) {
				if ( this.props.controlsBarPosition == 'right' ) this.elements.controls.dataPosSwitched = true;
				else this.elements.controls.dataPosSwitched = false;
				this.elements.controls.classList.add(this.props.classNames.verticalControlsBar);
			}
			else if ( ( this.props.controlsBarPosition == 'right' && this.elements.controls.dataPosSwitched ) || ( this.props.controlsBarPosition == 'left' && this.elements.controls.dataPosSwitched == false ) ) {
				if ( this.props.controlsBarPosition == 'left' ) this.elements.controls.dataPosSwitched = true;
				else this.elements.controls.dataPosSwitched = false;
				this.elements.controls.classList.add(this.props.classNames.verticalControlsBar);
			}
			return this;
		};

		/**
		 * Method for removing widget.
		 * Fires 'remove' event.
		 * @return {null|WidgetObject} Returns null on remove, otherwise this widget object.
		 */
		this.remove = function () {
			var eventData = {"target" : this};
			this.fireEvent('remove',eventData);
			if ( eventData.defaultPrevented === true ) return this;

			var c = lvzwebdesktop.isPartOf(this.props.attachedTo);
			if ( c !== false && typeof c.detachItem == 'function' ) c.detachItem(this);

			if ( typeof this.PropertiesWindow !== 'undefined' )
				this.PropertiesWindow.destruct();

			return this.destruct();
		};

		/**
		 * Method for showing the controls bar of the widget.
		 * @param {boolean} [force] - true if force to show the control bar.
		 * @return {WidgetObject} This widget object.
		 */
		this.showControlsBar = function ( force ) {
			if ( this.props.lock === false || ( typeof force != 'undefined' && force === true ) ) {
				this.controlsBarPosition().elements.container.parentNode.appendChild(this.elements.controls);
			}
			return this;
		};

		/**
		 * Method for hiding the controls bar of the widget.
		 * @return {WidgetObject} This widget object.
		 */
		this.hideControlsBar = function () {
			this.elements.controls.style.display = 'none';
			return this;
		};

		/** Set dynamic properties **/
		try {

			/**
			 * Set or the the active controls of the widget.
			 * @public
			 * @type {Array}
			 */
			Object.defineProperty ( this, "ActiveControls", {
				get: function () {
					var c=this.getControls();
					var ret=[];
					for(var i=0;i<c.length;i++)
						if ( c[i].parentNode === this.elements.controls )
							ret.push(c[i]);
					return ret;
				},
				set: function (v) {this.deactivateControls(this.getControls()).activateControls(v);},
				enumerable: false,
				configurable: false
			});

			/**
			 * Set or get the controls bar position of the widget.
			 * @public
			 * @type {String}
			 */
			Object.defineProperty ( this, "BarPosition", {
				get: function () { return this.props.controlsBarPosition; },
				set: function (v) { this.controlsBarPosition(v); },
				enumerable: false,
				configurable: false
			});

			/**
			 * Set of get bottom position of the widget.
			 * @public
			 * @type {Number}
			 */
			Object.defineProperty ( this, "Bottom", {
				get: function () { return this.elements.container.parentNode.offsetHeight-(this.props.top+this.elements.container.offsetHeight); },
				set: function ( v ) { this.setBottom(v); },
				enumerable: false,
				configurable: false
			});

			/**
			 * Set or get the disabled state of the widget.
			 * @public
			 * @type {boolean}
			 */
			Object.defineProperty ( this, "Disabled", {
				get: function () { return this.elements.vail.style.display == 'block'; },
				set: function ( v ) {
					if ( v === true ) this.disable();
					if ( v === false ) this.enable();
				},
				enumerable: false,
				configurable: false
			});

			/**
			 * Set or get the editable state of the widget.
			 * @public
			 * @type {boolean}
			 */
			Object.defineProperty ( this, "Editable", {
				get: function () { return this.props.editable; },
				set: function ( v ) { this.setEditable(v); },
				enumerable: false,
				configurable: false
			});

			/**
			 * Set or get the enabled state of the widget.
			 * @public
			 * @type {boolean}
			 */
			Object.defineProperty ( this, "Enabled", {
				get: function () { return this.elements.vail.style.display != 'block'; },
				set: function ( v ) {
					if ( v === true ) this.enable();
					if ( v === false ) this.disable();
				},
				enumerable: false,
				configurable: false
			});

			/**
			 * Set or get the height of the widget.
			 * @public
			 * @type {Number}
			 */
			Object.defineProperty ( this, "Height", {
				get: function () { return this.props.height; },
				set: function ( v ) { this.setHeight (v); },
				enumerable: false,
				configurable: false
			});

			/**
			 * Set or get left position of the widget.
			 * @public
			 * @type {Number}
			 */
			Object.defineProperty ( this, "Left", {
				get: function () { return this.props.left; },
				set: function ( v ) { this.setLeft(v); },
				enumerable: false,
				configurable: false
			});

			/**
			 * Set or get the locked state of the widget.
			 * @public
			 * @type {boolean}
			 */
			Object.defineProperty ( this, "Locked", {
				get: function () { return this.props.lock; },
				set: function ( v ) { this.setLocked( v ); },
				enumerable: false,
				configurable: false
			});

			/**
			 * Set or get the movable state of the widget.
			 * @public
			 * @type {boolean}
			 */
			Object.defineProperty ( this, "Movable", {
				get: function () { return this.props.movable; },
				set: function ( v ) { this.setMovable ( v ); },
				enumerable: false,
				configurable: false
			});

			/**
			 * Set or get the resizable state of the widget.
			 * @public
			 * @type {boolean}
			 */
			Object.defineProperty ( this, "Resizable", {
				get: function () { return this.props.resizable; },
				set: function ( v ) { this.setResizable(v); },
				enumerable: false,
				configurable: false
			});

			/**
			 * Set of get right position of the widget.
			 * @public
			 * @type {Number}
			 */
			Object.defineProperty ( this, "Right", {
				get: function () { return this.elements.container.parentNode.offsetWidth-(this.props.left+this.elements.container.offsetWidth); },
				set: function ( v ) { this.setRight(v); },
				enumerable: false,
				configurable: false
			});

			/**
			 * Set or get top position of the widget.
			 * @public
			 * @type {Number}
			 */
			Object.defineProperty ( this, "Top", {
				get: function () { return this.props.top; },
				set: function( v ) { this.setTop(v); },
				enumerable: false,
				configurable: false
			});

			/**
			 * Set or get the width of the widget.
			 * @public
			 * @type {Number}
			 */
			Object.defineProperty ( this, "Width", {
				get: function () { return this.props.width; },
				set: function ( v ) { this.setWidth(v); },
				enumerable: false,
				configurable: false
			});

		} catch ( e ) {}

		/**
		 * Array for storing event functions for when this widget is attached to another object or HTMLElement.
		 * @public
		 * @type {Array}
		 */
		this.onattachto = [];

		/**
		 * Array for storing event functions for when attaching an item to the widget.
		 * @public
		 * @type {Array}
		 */
		this.onitemattach = [];

		/**
		 * Array for storing event functions for when an item has been attached to the widget.
		 * @public
		 * @type {Array}
		 */
		this.onitemattached = [];

		/**
		 * Array for storing event functions for when detaching an item from the widget.
		 * @public
		 * @type {Array}
		 */
		this.onitemdetach = [];

		/**
		 * Array for storing event functions for when an item has been detached from the widget.
		 * @public
		 * @type {Array}
		 */
		this.onitemdetached = [];

		/**
		 * Array for storing event functions for when moving the widget.
		 * @public
		 * @type {Array}
		 */
		this.onmove = [];

		/**
		 * Array for storing event functions for when resizing the widget.
		 * @public
		 * @type {Array}
		 */
		this.onresize = [];

		/**
		 * Array for storing event functions for when changing the edit status.
		 * @public
		 * @type {Array}
		 */
		this.oneditstatuschange = [];

		/**
		 * Array for storing event functions for when changing the lock status.
		 * @public
		 * @type {Array}
		 */
		this.onlockstatuschange = [];

		/**
		 * Array for storing event function for when an app has been
		 * loaded on the widget.
		 * @public
		 * @type {Array}
		 */
		this.onapploaded = [];

		/**
		 * Array for storing event functions for when removing the widget.
		 * @public
		 * @type {Array}
		 */
		this.onremove = [];

		/**
		 * Array for storing event functions for when the properties controls is clicked.
		 * @public
		 * @type {Array}
		 */
		this.onproperties = [];

		/**
		 * Array for storing event functions for when disabling the widget.
		 * @public
		 * @type {Array}
		 */
		this.ondisable = [];

		/**
		 * Array for storing event functions for when disabling or enabling the widget.
		 * @public
		 * @type {Array}
		 */
		this.onenablestatechange = [];

		/**
		 * Array for storing event functions for when enabling the widget.
		 * @public
		 * @type {Array}
		 */
		this.onenable = [];

		/**
		 * Resize observer for resizing widget based on it's contents size.
		 * @public
		 * @type {ResizeObserver}
		 */
		this.resizeObserver = new ResizeObserver ( resizeWidgetOnContent );

		/**
		 * The desktop object that the widget belongs to. In it doesn't belong to any desktop the this property should be null.
		 * @public
		 * @type {DesktopObject|null}
		 */
		this.desktop = null;

		/**
		 * Property for the top position of the widget.
		 * @public
		 * @type {Number}
		 */
		this.props.top = 0;

		/**
		 * Property for setting the minimum distance of the top border
		 * of the widget from the top of its parent. If there is no
		 * minimum then this must be set to 'false'.
		 * @public
		 * @type {Number|false}
		 */
		this.props.minTop = false;

		/**
		 * Property for setting the maximum distance of the top border
		 * of the widget from the top border of its parent. If there
		 * is no maximum then this must be set to 'false'.
		 * @public
		 * @type {Number|false}
		 */
		this.props.maxTop = false;

		/**
		 * Property for setting the minimum distance of the bottom
		 * border of the widget from the bottom border of its
		 * parent. If there is no minimum then this must be set to
		 * 'false'.
		 * @public
		 * @type {Number|false}
		 */
		this.props.minBottom = false;

		/**
		 * Property for setting the maximum distance of the bottom
		 * border of the widget from the bottom border of its
		 * parent. If there is no maximum then this must be set to
		 * 'false'.
		 * @public
		 * @type {Number|false}
		 */
		this.props.maxBottom = false;

		/**
		 * Property for the left position of the widget.
		 * @public
		 * @type {Number}
		 */
		this.props.left = 0;

		/**
		 * Property for setting the minimum distance of the left
		 * border of the widget from the left border of its parent. If
		 * there is no minimum then this must be set to 'false'.
		 * @public
		 * @type {Number|false}
		 */
		this.props.minLeft = false;

		/**
		 * Property for setting the maximum distance of the left
		 * border of the widget from the left border of its parent. If
		 * there is no maximum then this must be set to boolean
		 * 'false'.
		 * @public
		 * @type {Number|false}
		 */
		this.props.maxLeft = false;

		/**
		 * Property to set the minimum distance of the right border of
		 * the widget from the right border of its parent. If there is
		 * no minimum then this must be set to 'false'.
		 * @public
		 * @type {Number|false}
		 */
		this.props.minRight = false;

		/**
		 * Property to set the maximum destance of the right border of
		 * the widget from the right border of its parent. If there is
		 * no maximum then this must be set to 'false'.
		 * @public
		 * @type {Number|false}
		 */
		this.props.maxRight = false;

		/**
		 * Property for the width of the widget. If value is false, then the width is auto.
		 * @public
		 * @type {boolean|Number}
		 */
		this.props.width = false;

		/**
		 * Property to indicate the minimum width size in pixels that the widget can take.
		 * @public
		 * @type {Number}
		 */
		this.props.minWidth = false;

		/**
		 * Property to set the maximum width that the widget can take. if there no maximum width then this property must be set to boolean 'false'.
		 * @public
		 * @type {Number|false}
		 */
		this.props.maxWidth = false;

		/**
		 * Property for the height of the widget. If value is false, then the height is auto.
		 * @public
		 * @type {boolean|Number}
		 */
		this.props.height = false;

		/**
		 * Property to indicate the minimum height size in pixels that the widget can take.
		 * @public
		 * @type {Number}
		 */
		this.props.minHeight = false;

		/**
		 * Property to set the maximum height that the widget can take. If there is no maximum height then this property must be set to boolean 'false'.
		 * @public
		 * @type {Number|false}
		 */
		this.props.maxHeight = false;

		/**
		 * Property to set if the widget is movable.
		 * @public
		 * @type {boolean}
		 */
		this.props.movable = true;

		/**
		 * Property to set if the widget is resizable.
		 * @public
		 * @type {boolean}
		 */
		this.props.resizable = true;

		/**
		 * Property to set if the widget parameters are editable.
		 * @public
		 * @type {boolean}
		 */
		this.props.editable = false;

		/**
		 * Property to set if widget is locked and it's parameters cannot be changed neither it can be move or resized.
		 * @public
		 * @type {boolean}
		 */
		this.props.lock = false;

		/**
		 * Property to set true if the widget is supposed to resize by it's contents size or not.
		 * @public
		 * @type {boolean}
		 */
		this.props.resizeOnContent = false;

		/**
		 * Property to set the object or element that this widget is attached to.
		 * @public
		 * @type {HTMLElement|object}
		 */
		this.props.attachedTo = null;

		/**
		 * Property to set the attached item.
		 * @public
		 * @type {Object}
		 */
		this.props.attachedItem = null;

		/**
		 * Property to set the position of the controls bar.
		 * @public
		 * @type {'top'|'left'|'right'|'bottom'}
		 */
		this.props.controlsBarPosition = WidgetObject.controlsBarPosition;

		/**
		 * CSS class-name for the widget's container element.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.container = WidgetObject.classNames.container;

		/**
		 * CSS class-name for the widget's container element for when widget is editable.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.editable = WidgetObject.classNames.editable;

		/**
		 * CSS class-name for the widget's container element for when the controls bar is vertical.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.verticalControlsBar = WidgetObject.classNames.verticalControlsBar;

		/**
		 * CSS class-name for the widget's container element for when the widget is in the process of resizing.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.resize = WidgetObject.classNames.resize;

		/**
		 * CSS class-name for the widget's container element for when widget is locked.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.locked = WidgetObject.classNames.locked;

		/**
		 * CSS class-name for the widget's vail element.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.vail = WidgetObject.classNames.vail;

		/**
		 * CSS class-name for the widget's body element.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.body = WidgetObject.classNames.body;

		/**
		 * CSS class-name for the widget's controls container element.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.controls = WidgetObject.classNames.controls;

		/**
		 * CSS class-name for the widget's move control element.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.moveControlBtn = WidgetObject.classNames.moveControlBtn;

		/**
		 * CSS class-name for the widget's resize control element.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.resizeControlBtn = WidgetObject.classNames.resizeControlBtn;

		/**
		 * CSS class-name for the widget's properties control element.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.propertiesControlBtn = WidgetObject.classNames.propertiesControlBtn;

		/**
		 * CSS class-name for the widget's remove control element.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.removeControlBtn = WidgetObject.classNames.removeControlBtn;

		/**
		 * CSS class-name for the widget's lock control element.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.lockControlBtn = WidgetObject.classNames.lockControlBtn;

		/**
		 * CSS class-name for the widget's lock control element for when widget is locked.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.unlockControlBtn = WidgetObject.classNames.unlockControlBtn;

		/**
		 * CSS class-name for the widget resize box elements.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.resizeBox = WidgetObject.classNames.resizeBox;

		/**
		 * Widget container element.
		 * @public
		 * @type {HTMLElement}
		 */
		this.elements.container = lvzwebdesktop.createBasicElement.call(this,'div',e,[
			this.props.classNames.container
		]);
		this.elements.container.addEventListener('contextmenu', function(e){e.preventDefault();return false;});
		this.elements.container.addEventListener('mousedown', widgetMDOWN);
		this.elements.container.addEventListener('mouseover', widgetMOVER);
		this.elements.container.addEventListener('mouseout',widgetMOUT,true)

		/**
		 * Widget vail element.
		 * @public
		 * @type {HTMLElement}
		 */
		this.elements.vail = lvzwebdesktop.createBasicElement.call(this,'div',undefined,[
			this.props.classNames.vail
		]);

		/**
		 * Widget body element.
		 * @public
		 * @type {HTMLElement}
		 */
		this.elements.body = lvzwebdesktop.createBasicElement.call(this,'div',undefined,[
			this.props.classNames.body
		]);

		if ( typeof e != 'undefined' && e.firstElementChild !== null ) this.attachItem(e.firstElementChild);

		this.elements.container.appendChild(this.elements.body);
		this.elements.container.appendChild(this.elements.vail);

		/**
		 * Widget controls container element.
		 * @public
		 * @type {HTMLElement}
		 */
		this.elements.controls = lvzwebdesktop.createBasicElement.call(this,'div',undefined,[
			this.props.classNames.controls
		]);
		this.elements.controls.addEventListener('mouseout',widgetMOUT,true)
		this.elements.controls.dataPosSwitched = false;
		this.elements.container.appendChild(this.elements.controls);

		/**
		 * Widget top left resize box element.
		 * @public
		 * @type {HTMLElement}
		 */
		this.elements.resizeTopLeft = lvzwebdesktop.createBasicElement.call(this,'div',undefined,[
			this.props.classNames.resizeBox
		]);
		this.elements.resizeTopLeft.dataAction = 'resize-top-left';
		this.elements.resizeTopLeft.addEventListener('mousedown', WidgetMDRS);
		this.elements.container.appendChild(this.elements.resizeTopLeft);

		/**
		 * Widget top center resize box element.
		 * @public
		 * @type {HTMLElement}
		 */
		this.elements.resizeTopCenter = lvzwebdesktop.createBasicElement.call(this,'div',undefined,[
			this.props.classNames.resizeBox
		]);
		this.elements.resizeTopCenter.dataAction = 'resize-top';
		this.elements.resizeTopCenter.addEventListener('mousedown', WidgetMDRS);
		this.elements.container.appendChild(this.elements.resizeTopCenter);

		/**
		 * Widget top right resize box element.
		 * @public
		 * @type {HTMLElement}
		 */
		this.elements.resizeTopRight = lvzwebdesktop.createBasicElement.call(this,'div',undefined,[
			this.props.classNames.resizeBox
		]);
		this.elements.resizeTopRight.dataAction = 'resize-top-right';
		this.elements.resizeTopRight.addEventListener('mousedown', WidgetMDRS);
		this.elements.container.appendChild(this.elements.resizeTopRight);

		/**
		 * Widget middle left resize box element.
		 * @public
		 * @type {HTMLElement}
		 */
		this.elements.resizeMiddleLeft = lvzwebdesktop.createBasicElement.call(this,'div',undefined,[
			this.props.classNames.resizeBox
		]);
		this.elements.resizeMiddleLeft.dataAction = 'resize-left';
		this.elements.resizeMiddleLeft.addEventListener('mousedown', WidgetMDRS);
		this.elements.container.appendChild(this.elements.resizeMiddleLeft);

		/**
		 * Widget middle right resize box element.
		 * @public
		 * @type {HTMLElement}
		 */
		this.elements.resizeMiddleRight = lvzwebdesktop.createBasicElement.call(this,'div',undefined,[
			this.props.classNames.resizeBox
		]);
		this.elements.resizeMiddleRight.dataAction = 'resize-right';
		this.elements.resizeMiddleRight.addEventListener('mousedown', WidgetMDRS);
		this.elements.container.appendChild(this.elements.resizeMiddleRight);

		/**
		 * Widget bottom left resize box element.
		 * @public
		 * @type {HTMLElement}
		 */
		this.elements.resizeBottomLeft = lvzwebdesktop.createBasicElement.call(this,'div',undefined,[
			this.props.classNames.resizeBox
		]);
		this.elements.resizeBottomLeft.dataAction = 'resize-bottom-left';
		this.elements.resizeBottomLeft.addEventListener('mousedown', WidgetMDRS);
		this.elements.container.appendChild(this.elements.resizeBottomLeft);

		/**
		 * Widget bottom center resize box element.
		 * @public
		 * @type {HTMLElement}
		 */
		this.elements.resizeBottomCenter = lvzwebdesktop.createBasicElement.call(this,'div',undefined,[
			this.props.classNames.resizeBox
		]);
		this.elements.resizeBottomCenter.dataAction = 'resize-bottom';
		this.elements.resizeBottomCenter.addEventListener('mousedown', WidgetMDRS);
		this.elements.container.appendChild(this.elements.resizeBottomCenter);

		/**
		 * Widget bottom right resize box element.
		 * @public
		 * @type {HTMLElement}
		 */
		this.elements.resizeBottomRight = lvzwebdesktop.createBasicElement.call(this,'div',undefined,[
			this.props.classNames.resizeBox
		]);
		this.elements.resizeBottomRight.dataAction = 'resize-bottom-right';
		this.elements.resizeBottomRight.addEventListener('mousedown', WidgetMDRS);
		this.elements.container.appendChild(this.elements.resizeBottomRight);

		/**
		 * Widget's move control element.
		 * @public
		 * @type {HTMLElement}
		 */
		this.elements.moveControlBtn = lvzwebdesktop.createBasicElement.call(this,'div',undefined,[
			this.props.classNames.moveControlBtn
		]);
		this.elements.moveControlBtn.title = 'move';
		this.elements.controls.appendChild(this.elements.moveControlBtn);
		this.elements.moveControlBtn.addEventListener('mousedown', WidgetMDMV);

		/**
		 * Widget's resize control element.
		 * @public
		 * @type {HTMLElement}
		 */
		this.elements.resizeControlBtn = lvzwebdesktop.createBasicElement.call(this,'div',undefined,[
			this.props.classNames.resizeControlBtn
		]);
		this.elements.resizeControlBtn.title = 'resize';
		this.elements.controls.appendChild(this.elements.resizeControlBtn);
		this.elements.resizeControlBtn.addEventListener('click', function ( e ) {
			var self = lvzwebdesktop(e.target);
			// self.startResize();
			self.toggleResize();
		});

		/**
		 * Widget's properties control element.
		 * @public
		 * @type {HTMLElement}
		 */
		this.elements.propertiesControlBtn = lvzwebdesktop.createBasicElement.call(this,'div',undefined,[
			this.props.classNames.propertiesControlBtn
		]);
		this.elements.propertiesControlBtn.title = 'properties';
		this.elements.propertiesControlBtn.style.display = 'none';
		this.elements.controls.appendChild( this.elements.propertiesControlBtn );
		this.elements.propertiesControlBtn.addEventListener('click',function(e){
			var self = lvzwebdesktop(e.target);

			if ( typeof self.PropertiesWindow == 'undefined' ) {
				var wwind = (new lvzwebdesktop.WindowObject(false,self.getParentOfType('DesktopObject')))
					.setTheme ( self.props.themeClass )
					.setTitle("Properties")
					.setWidth(300)
					.setHeight(200)
					.setLayout('menu:close')
					.buildTheme()
					.decorate()
				;
				wwind.props.showInTaskbar = false;
				wwind.widget = self;
			}
			else {
				var wwind = self.PropertiesWindow;
			}

			var p = lvzwebdesktop(self.elements.container.parentNode);
			if ( p !== false && typeof p.attachItem == 'function' ) {
				p.attachItem(wwind);
			}
			else
				self.elements.container.parentNode.appendChild(wwind.elements.container);

			self.PropertiesWindow = wwind;

			wwind.setWidth(wwind.props.width)
				.positionHMiddle()
				.positionVMiddle()
			;

			self.fireEvent('properties',{"target":self,
										 "propertiesWindow":wwind});
		});

		/**
		 * Widget's lock control element.
		 * @public
		 * @type {HTMLElement}
		 */
		this.elements.lockControlBtn = lvzwebdesktop.createBasicElement.call(this,'div',undefined,[
			this.props.classNames.lockControlBtn
		]);
		this.elements.lockControlBtn.title = 'lock';
		this.elements.lockControlBtn.addEventListener('click', function ( e ) {
			lvzwebdesktop(e.target).toggleLock();
		});
		this.elements.controls.appendChild(this.elements.lockControlBtn);

		/**
		 * Widget's remove control element. 
		 * @public
		 * @type {HTMLElement}
		 */
		this.elements.removeControlBtn = lvzwebdesktop.createBasicElement.call(this,'div',undefined,[
			this.props.classNames.removeControlBtn
		]);
		this.elements.removeControlBtn.title = 'remove';
		this.elements.removeControlBtn.addEventListener('click', widgetDelete );
		this.elements.controls.appendChild(this.elements.removeControlBtn);

		this.props.setThemeToElements.push(this.elements.container);
		this.props.setThemeToElements.push(this.elements.controls);

		var attr;

		this.loadPositionAttributes();

		attr = this.elements.container.getAttribute('activate-controls');
		if ( attr !== null ) this.activateControls ( attr );

		attr = this.elements.container.getAttribute('deactivate-controls');
		if ( attr !== null ) this.deactivateControls ( attr );

		attr = this.elements.container.getAttribute('locked');
		if ( attr !== null ) this.setLocked ( attr.trim() === 'true' );

		attr = this.elements.container.getAttribute('movable');
		if ( attr !== null ) this.setMovable ( attr.trim() === 'true' );

		attr = this.elements.container.getAttribute('resizable');
		if ( attr !== null ) this.setResizable ( attr.trim() === 'true' );

		attr = this.elements.container.getAttribute('enable');
		if ( attr == 'true' ) this.enable();

		attr = this.elements.container.getAttribute('disable');
		if ( attr == 'true' ) this.disable();

		attr = this.elements.container.getAttribute('editable');
		if ( attr !== null ) this.setEditable ( attr.trim() === 'true' );

		attr = this.elements.container.getAttribute('controls-bar-position');
		if ( attr !== null ) this.controlsBarPosition( attr );

		attr = this.elements.container.getAttribute('resize-on-content');
		if ( attr !== null ) {
			this.props.resizeOnContent = attr === 'true';
			this.resizeOnContent(this.props.resizeOnContent);
		}

		attr = this.elements.container.getAttribute('onapploaded');
		if ( attr !== null ) this.addEvent('apploaded', new Function ( 'event', attr ) );

		attr = this.elements.container.getAttribute('load-app');
		if ( attr !== null ) {
			// pass arguments to app
			var p = this.elements.container.getAttribute('data-attributes');
			if ( p !== null ) p = JSON.parse(p);
			if ( p === null ) p = {};

			for ( var i = 0; i < this.elements.container.attributes.length; i ++ )
				if ( this.elements.container.attributes[i].name.substr(0,10) == 'app-param-' )
					p[this.elements.container.attributes[i].name.substr(10)] = this.elements.container.attributes[i].value;
			lvzwebdesktop(attr).loadApp(this.elements.body, p);
		}

		attr = this.elements.container.getAttribute('onmove');
		if ( attr !== null ) this.addEvent('move', new Function ( 'event', attr ) );

		attr = this.elements.container.getAttribute('ondisable');
		if ( attr !== null ) this.addEvent('disable', new Function ( 'event', attr ) );

		attr = this.elements.container.getAttribute('onenable');
		if ( attr !== null ) this.addEvent('enable', new Function ( 'event', attr ) );

		attr = this.elements.container.getAttribute('onenablestatechange');
		if ( attr !== null ) this.addEvent('enablestatechange', new Function ( 'event', attr ) );

		attr = this.elements.container.getAttribute('onresize');
		if ( attr !== null ) this.addEvent('resize', new Function ( 'event', attr ) );

		attr = this.elements.container.getAttribute('oneditstatuschange');
		if ( attr !== null ) this.addEvent('editstatuschange', new Function ( 'event', attr ) );

		attr = this.elements.container.getAttribute('onlockstatuschange');
		if ( attr !== null ) this.addEvent('lockstatuschange', new Function ( 'event', attr ) );

		attr = this.elements.container.getAttribute('onattachto');
		if ( attr !== null ) this.addEvent('attachto', new Function ( 'event', attr ) );

		attr = this.elements.container.getAttribute('onitemattach');
		if ( attr !== null ) this.addEvent('itemattch', new Function ( 'event' , attr ) );

		attr = this.elements.container.getAttribute('onitemattached');
		if ( attr !== null ) this.addEvent('itemattached', new Function ( 'event', attr ) );

		attr = this.elements.container.getAttribute('onitemdetach');
		if ( attr !== null ) this.addEvent('itemdetach', new Function ( 'event', attr ) );

		attr = this.elements.container.getAttribute('onitemdetached');
		if ( attr !== null ) this.addEvent('itemdetached', new Function ( 'event', attr ) );

		this.refresh();

		lvzwebdesktop.endBasicObject.call(this);
	};

	// Properly attach widget objects to its parents after loading the constructors.
	if ( lvzwebdesktop.constructorsReady === false ) {
		lvzwebdesktop.fn.addEvent.call(lvzwebdesktop,'constructorsready', function ( e ) {
			var self = null;
			var p;

			for ( var i=0; i < e.target.WidgetObject.list.length; i ++ ) {
				self = e.target.WidgetObject.list[i];
				p = lvzwebdesktop(self.elements.container,false).getParentToAttach();
				if ( p !== null ) self.attachTo( p );
				else self.attachTo(self.elements.container.parentNode);
				self.loadPositionAttributes();
			}

		});
	}

	/**
	 * List of all widget objects.
	 * @public
	 * @type {Array}
	 */
	WidgetObject.list = [];

	/**
	 * Array for storing event functions for when a widget object is loaded and ready.
	 * @public
	 * @type {Array}
	 */
	WidgetObject.onready = [];

	/**
	 * Default controls bar position of the widgets.
	 * @public
	 * @type {'top','left','right','bottom'}
	 */
	WidgetObject.controlsBarPosition = 'left';

	/**
	 * Array for storing event functions for when a widget is loading.
	 * @public
	 * @type {Array}
	 */
	WidgetObject.onload = [];

	/**
	 * Associative array for widget object's default class-names.
	 * @public
	 * @type {Object}
	 */
	WidgetObject.classNames = {};

	/**
	 * Default class name for the container elements of widgets.
	 * @public
	 * @type {String}
	 */
	WidgetObject.classNames.container = 'web-widget';

	/**
	 * Default class name for the container elements of widgets for when the controls bar is vertical.
	 * @public
	 * @type {String}
	 */
	WidgetObject.classNames.verticalControlsBar = 'vertical-controls-bar';

	/**
	 * Default class name for the container elements of widget for when they are editable.
	 * @public
	 * @type {String}
	 */
	WidgetObject.classNames.editable = 'editable';

	/**
	 * Default class name for the container elements of widgets for when they are in the process of resizing.
	 * @public
	 * @type {String}
	 */
	WidgetObject.classNames.resize = 'resize';

	/**
	 * Default class name for the container elements of widgets for when they are locked.
	 * @public
	 * @type {String}
	 */
	WidgetObject.classNames.locked = 'locked';

	/**
	 * Default class name for the vail elements of widgets.
	 * @public
	 * @type {String}
	 */
	WidgetObject.classNames.vail = 'vail';

	/**
	 * Default class name for the body elements of widgets.
	 * @public
	 * @type {String}
	 */
	WidgetObject.classNames.body = 'body';

	/**
	 * Default class name for the controls container elements of widgets.
	 * @public
	 * @type {String}
	 */
	WidgetObject.classNames.controls = 'wd-widget-controls';

	/**
	 * Default class name for the move control elements of widget objects.
	 * @public
	 * @type {String}
	 */
	WidgetObject.classNames.moveControlBtn = 'move-control-btn';

	/**
	 * Default class name for the resize control elements of widget objects.
	 * @public
	 * @type {String}
	 */
	WidgetObject.classNames.resizeControlBtn = 'resize-control-btn';

	/**
	 * Default class name for the properties control elements of widget objects.
	 * @public
	 * @type {String}
	 */
	WidgetObject.classNames.propertiesControlBtn = 'properties-control-btn';

	/**
	 * Default class name for the remove control elements of widget objects.
	 * @public
	 * @type {String}
	 */
	WidgetObject.classNames.removeControlBtn = 'remove-control-btn';

	/**
	 * Default class name for the lock control elements of widget objects.
	 * @public
	 * @type {String}
	 */
	WidgetObject.classNames.lockControlBtn = 'lock-control-btn';

	/**
	 * Default class name for the lock control elements of widget objects for when they are locked.
	 * @public
	 * @type {String}
	 */
	WidgetObject.classNames.unlockControlBtn = 'unlock-control-btn';

	/**
	 * Default class name for the resize box elements of widget objects.
	 * @public
	 * @type {String}
	 */
	WidgetObject.classNames.resizeBox = 'resize-box';

	/**
	 * Called by WidgetObject resizeObserver for resizing widget on it's contents.
	 *
	 * @param {Event} e - the resize observer event object.
	 */
	function resizeWidgetOnContent ( e ) {
		var o = e[0].target;
		var w = e[0].target.offsetWidth;
		var h = e[0].target.offsetHeight;
		var self = lvzwebdesktop(e[0].target,false).getParentObject('WidgetObject');
		if ( self === null ) return false;
		self.matchContentSize();
		return true;
	}

	/**
	 * Called by the mousedown event on the move control element of the widget.
	 *
	 * @param {Event} e - The mousedown event object.
	 */
	function WidgetMDMV ( e ) {
		e.preventDefault();
		var self = lvzwebdesktop(e.target);

		WidgetMDMV.data = {};

		WidgetMDMV.data.o = self;

		WidgetMDMV.data.ox = self.props.left;
		WidgetMDMV.data.oy = self.props.top;
		WidgetMDMV.data.omx = e.clientX;
		WidgetMDMV.data.omy = e.clientY;

		self.elements.moveControlBtn.removeEventListener('mousedown', WidgetMDMV );
		document.addEventListener('mousemove', WidgetMMMV );
		document.addEventListener('mouseup',WidgetMUMV );
 	}

	/**
	 * Called by the mousemove event on the document after mousedown event on a move control element of a widget.
	 *
	 * @param {Event} e - The mousemove event object.
	 */
	function WidgetMMMV ( e ) {
		e.preventDefault();
		var self = WidgetMDMV.data.o;

		var x = WidgetMDMV.data.ox + ( e.clientX - WidgetMDMV.data.omx );
		var y = WidgetMDMV.data.oy + ( e.clientY - WidgetMDMV.data.omy );

		self.setTop ( y )
			.setLeft ( x )
		;
	}

	/**
	 * Called by the mouseup event on the document after mousedown event on a move contorl element of a widget.
	 *
	 * @param {Event} e - The mouseup event object.
	 */
	function WidgetMUMV ( e ) {
		e.preventDefault();
		var self = WidgetMDMV.data.o;

		document.removeEventListener('mousemove', WidgetMMMV );
		document.removeEventListener('mouseup',WidgetMUMV );
		self.elements.moveControlBtn.addEventListener('mousedown', WidgetMDMV );
	}

	/**
	 * Called by the mousedown event on e resize-box element of the widget.
	 *
	 * @param {Event} e - The mousedown event object.
	 */
	function WidgetMDRS ( e ) {
		e.preventDefault();
		var self = lvzwebdesktop( e.target );

		WidgetMDRS.d = {};

		WidgetMDRS.d.o = self;
		WidgetMDRS.d.e = e.target;
		WidgetMDRS.d.a = e.target.dataAction;
		WidgetMDRS.d.w = self.elements.container.offsetWidth;
		WidgetMDRS.d.h = self.elements.container.offsetHeight;
		WidgetMDRS.d.t = self.props.top;
		WidgetMDRS.d.l = self.props.left;
		WidgetMDRS.d.cmx = e.clientX;
		WidgetMDRS.d.cmy = e.clientY;

		e.target.removeEventListener('mousedown', WidgetMDRS);
		document.addEventListener('mousemove', WidgetMMRS);
		document.addEventListener('mouseup', WidgetMURS);
	}

	/**
	 * Called by the mousemove event on the document after mousedown event on a resize-box element of a widget.
	 *
	 * @param {Event} e - The mousemove event object.
	 */
	function WidgetMMRS ( e ) {
		e.preventDefault();
		var self = WidgetMDRS.d.o;
		var p,s;

		if ( self.props.resizable !== true ) return;

		if ( WidgetMDRS.d.a == 'resize-right' ) {
			self.setWidth( WidgetMDRS.d.w + (e.clientX - WidgetMDRS.d.cmx ));
		}
		else if ( WidgetMDRS.d.a == 'resize-bottom' ) {
			self.setHeight( WidgetMDRS.d.h + (e.clientY - WidgetMDRS.d.cmy-self.elements.controls.offsetHeight ));
		}
		else if ( WidgetMDRS.d.a == 'resize-bottom-right' ) {
			self.setWidth( WidgetMDRS.d.w + (e.clientX - WidgetMDRS.d.cmx ));
			self.setHeight( WidgetMDRS.d.h + (e.clientY - WidgetMDRS.d.cmy-self.elements.controls.offsetHeight ));
		}
		else if ( WidgetMDRS.d.a == 'resize-top' ) {
			p = WidgetMDRS.d.t + (e.clientY - WidgetMDRS.d.cmy );
			s = WidgetMDRS.d.h + ( WidgetMDRS.d.cmy - e.clientY )-self.elements.controls.offsetHeight;
			if ( ( self.props.minTop === false || p >= self.props.minTop )
				 && ( self.props.maxTop === false || p <= self.props.maxTop )
				 && ( self.props.minHeight === false || s >= self.props.minHeight )
				 && ( self.props.maxHeight === false || s <= self.props.maxHeight )
			   ) self.setTop( p ).setHeight( s );
		}
		else if ( WidgetMDRS.d.a == 'resize-left' ) {
			p = WidgetMDRS.d.l + (e.clientX - WidgetMDRS.d.cmx );
			s = WidgetMDRS.d.w + ( WidgetMDRS.d.cmx - e.clientX );
			if ( ( self.props.minLeft === false || p >= self.props.minLeft )
				 && ( self.props.maxLeft === false || p <= self.props.maxLeft )
				 && ( self.props.minWidth === false || s >= self.props.minWidth )
				 && ( self.props.maxWidth === false || s <= self.props.maxWidth )
			   ) self.setLeft( p ).setWidth( s );
		}
		else if ( WidgetMDRS.d.a == 'resize-top-left' ) {
			p = WidgetMDRS.d.t + (e.clientY - WidgetMDRS.d.cmy );
			s = WidgetMDRS.d.h + ( WidgetMDRS.d.cmy - e.clientY )-self.elements.controls.offsetHeight;
			if ( ( self.props.minTop === false || p >= self.props.minTop )
				 && ( self.props.maxTop === false || p <= self.props.maxTop )
				 && ( self.props.minHeight === false || s >= self.props.minHeight )
				 && ( self.props.maxHeight === false || s <= self.props.maxHeight )
			   ) self.setTop( p ).setHeight( s );

			p = WidgetMDRS.d.l + (e.clientX - WidgetMDRS.d.cmx );
			s = WidgetMDRS.d.w + ( WidgetMDRS.d.cmx - e.clientX );
			if ( ( self.props.minLeft === false || p >= self.props.minLeft )
				 && ( self.props.maxLeft === false || p <= self.props.maxLeft )
				 && ( self.props.minWidth === false || s >= self.props.minWidth )
				 && ( self.props.maxWidth === false || s <= self.props.maxWidth )
			   ) self.setLeft( p ).setWidth( s );
		}
		else if ( WidgetMDRS.d.a == 'resize-top-right' ) {
			p = WidgetMDRS.d.t + (e.clientY - WidgetMDRS.d.cmy );
			s = WidgetMDRS.d.h + ( WidgetMDRS.d.cmy - e.clientY )-self.elements.controls.offsetHeight;
			if ( ( self.props.minTop === false || p >= self.props.minTop )
				 && ( self.props.maxTop === false || p <= self.props.maxTop )
				 && ( self.props.minHeight === false || s >= self.props.minHeight )
				 && ( self.props.maxHeight === false || s <= self.props.maxHeight )
			   ) self.setTop( p ).setHeight( s );

			self.setWidth( WidgetMDRS.d.w + (e.clientX - WidgetMDRS.d.cmx ));
		}
		else if ( WidgetMDRS.d.a == 'resize-bottom-left' ) {
			p = WidgetMDRS.d.l + (e.clientX - WidgetMDRS.d.cmx );
			s = WidgetMDRS.d.w + ( WidgetMDRS.d.cmx - e.clientX );
			if ( ( self.props.minLeft === false || p >= self.props.minLeft )
				 && ( self.props.maxLeft === false || p <= self.props.maxLeft )
				 && ( self.props.minWidth === false || s >= self.props.minWidth )
				 && ( self.props.maxWidth === false || s <= self.props.maxWidth )
			   ) self.setLeft( p ).setWidth( s );

			self.setHeight( WidgetMDRS.d.h + (e.clientY - WidgetMDRS.d.cmy-self.elements.controls.offsetHeight ));
		}
		self.positionResizeElements();
	}

	/**
	 * Called by the mouseup event on the document after mousedown event on a resize-box element of a widget.
	 *
	 * @param {Event} e - The mouseup event object.
	 */
	function WidgetMURS ( e ) {
		e.preventDefault();
		var self = WidgetMDRS.d.o;

		document.removeEventListener('mousemove', WidgetMMRS);
		document.removeEventListener('mouseup', WidgetMURS);
		WidgetMDRS.d.e.addEventListener('mousedown', WidgetMDRS);
	}

	/**
	 * Called on mouse click on the remove control element of a widget.
	 *
	 * @param {Event} e - The click event object.
	 */
	function widgetDelete ( e ) {
		var self = lvzwebdesktop(e.target);
		self.remove();
	}

	/**
	 * Called on mouseover on widget or widget's control bar.
	 *
	 * @param {Event} e - The mouseover event object.
	 */
	function widgetMOVER ( e ) {
		var self = lvzwebdesktop(e.target,false).getParentObject('WidgetObject');
		if ( self === null ) self = lvzwebdesktop(e.target);
		if ( ! self instanceof WidgetObject ) return;
		self.showControlsBar();
	}

	/**
	 * Called on mousedown on widget.
	 *
	 * @param {Event} e - The mousedown event object.
	 */
	function widgetMDOWN ( e ) {
		var self = lvzwebdesktop(e.target,false).getParentObject('WidgetObject');
		if ( self === null ) self = lvzwebdesktop(e.target);
		if ( ! self instanceof WidgetObject ) return;
		if ( e.ctrlKey == true && e.button == 2 ) {
			e.preventDefault();
			self.showControlsBar(true);
			return false;
		}
	}

	/**
	 * Called on mouseout from widget or widget's control bar.
	 *
	 * @param {Event} e - The mouseout event object.
	 */
	function widgetMOUT ( e ) {
		var self = lvzwebdesktop(e.target,false).getParentObject('WidgetObject');
		if ( self === null ) self = lvzwebdesktop(e.target);
		if ( ! self instanceof WidgetObject ) return;

		var el = document.elementFromPoint(e.clientX, e.clientY);
		if ( el === null ) return ;
		el = el.parentNode;
		
		if ( el !== self.elements.controls )
			self.hideControlsBar();
	}

	window.lvzwebdesktop.WidgetObject = WidgetObject;
	window.lvzwebdesktop.constructors.push({
		"selector" : "." + WidgetObject.classNames.container,
		"constructor" : WidgetObject
	});

}
