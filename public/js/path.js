{
	document.currentScript.dataLVZWLIB = true;

	/**
	 * Constructor for creating virtual filesystem objects.
	 * @constructor
	 * @param {HTMLElement} [e] - Element to attach vfs object to (get data).
	 */
	var VFSObject = function ( e ) {
		VFSObject.list.push ( this );

		/**
		 * Type of object.
		 * @public
		 * @type {'vfs'}
		 */
		this.type = 'vfs';

		/**
		 * Associative array for storing vfs properties values.
		 * @public
		 * @type {object}
		 */
		this.props = {};

		/**
		 * Property current path.
		 * @public
		 * @type {string}
		 */
		this.props.currentPath = '/';

		/**
		 * Array for storing history of visited paths.
		 * @public
		 * @type {Array}
		 */
		this.props.pathHistory = [];

		/**
		 * Number of visited path items to keep. If false then don't keep history, same as zero. if true then no limit.
		 * @public
		 * @type {number|boolean}
		 */
		this.props.maxHistoryItems = 100;

		/**
		 * Property for defining the source of data. If props.dataSourceType is "stream" then this is url of stream. if props.dataSourceType is element then this is the element's id. If not set, the it's null.
		 * @public
		 * @type {null|string}
		 */
		this.props.dataSource = null;

		/**
		 * Data source type.
		 * @public
		 * @type {null|'stream'|'element'}
		 */
		this.props.dataSourceType = null;

		/**
		 * Array for storing "changepath" event functions.
		 * @public
		 * @type {Array}
		 */
		this.onchangepath = [];

		/**
		 * Array for storing "pathnotexist" event functions.
		 * @public
		 * @type {Array}
		 */
		this.onpathnotexist = [];

		/**
		 * Method for adding events to vfs object.
		 * @param {string} e - Event name.
		 * @param {function} f - Function to run on event.
		 * @return {PathObject|false} False on error, current vfs object otherwise.
		 */
		this.addEvent = window.lvzwebdesktop.fn.addEvent;

		/**
		 * Method for removing events from vfs object.
		 * @param {string} e - The name of the event to remove.
		 * @param {function} [f] - The function to remove. If no function given then all functions will be removed.
		 * @return {PathObject|false} False on error, current vfs object otherwise.
		 */
		this.removeEvent = window.lvzwebdesktop.fn.removeEvent;

		/**
		 * Method for firing events of vfs object.
		 * @param {string} e - The name of the event to fire.
		 * @param {object} obj - An object with properties to add to event object.
		 * @return {PathObject|false} False on error, current vfs object otherwise.
		 */
		this.fireEvent = window.lvzwebdesktop.fn.fireEvent;

		/**
		 * Method for changing path. Fires event "onchange".
		 * @param {string} path - The new path.
		 * @return {PathObject} Current vfs object.
		 */
		this.setPath = function ( path ) {
			var eobj = {
				"target" : this,
				"newPath" : path,
				"oldPath" : this.props.currentPath
			};
			this.fireEvent('changepath', eobj);
			if ( eobj.preventDefault.a === true ) return this;

			if ( this.props.maxHistoryItems !== false ) {
				if ( this.props.maxHistoryItems !== true &&
					 this.props.maxHistoryItems <= this.props.pathHistory.length
				   ) this.props.pathHistory.shift ();
				this.props.pathHistory.push ( path );
			}
			this.props.currentPath = path;
			return this;
		};

		/**
		 * Alias of setPath method.
		 * @param {string} path - The new path.
		 * @return {PathObject} Current vfs object.
		 */
		this.cd = this.setPath;

		/**
		 * Alias of setPath method.
		 * @param {string} path - The new path.
		 * @return {PathObject} Current vfs object.
		 */
		this.changeDir = this.setPath;

		/**
		 * Method for getting the parent path of current or given path.
		 * @param {string} [p] - If is set, this path will be used instead of currentPath. This will not change current path.
		 * @return {string} Parent path of given or current path.
		 */
		this.getParentPath = function ( p ) {
			var tmpp = '';
			if ( typeof p == 'string' ) tmpp = p;
			else tmpp = this.props.currentPath;

			if ( tmpp.substr(-1) == '/' ) tmpp = tmpp.substr(0,tmpp.length-1);
			tmpp = tmpp.split(/[/]/);
			tmpp.pop();
			return tmpp.join('/')+'/';
		};

		/**
		 * Alias method of getParentPath method.
		 * @param {string} [p] - If is set, this path will be used instead of currentPath. This will not change current path.
		 * @return {string} Parent path of given or current path.
		 */
		this.dirname = this.getParentPath;

		/**
		 * Method for getting the directory/file name from current or given path.
		 * @param {string} [p] - If is set, this path will be used instead of currentPath. This will not change current path.
		 * @return {string} The file/directory name of current or given path.
		 */
		this.getItemsName = function ( p ) {
			var tmpp ='';
			if ( typeof p == 'string' ) tmpp = p;
			else tmpp = this.props.currentPath;
			if ( tmpp.substr(-1) == '/' ) tmpp = tmpp.substr(0,tmpp.length-1);
			return tmpp.pop();
		};

		/**
		 * Alias method of getItemsName method.
		 * @param {string} [p] - If is set, this path will be used instead of currentPath. This will not change current path.
		 * @return {string} The file/directory name of current or given path.
		 */
		this.basename = this.getItemsName;

		/**
		 * Method for checking if an item exists.
		 * @param {string} item - The item to check.
		 * @return {boolean|null} Returns true if an item exists, false if not and null on error.
		 */
		this.itemExists = function ( item ) {

			if ( this.props.dataSourceType == 'element' ) {
				var datael = this.elements.container.querySelector( "[data-path=\"" + this.props.dataSource + "\"],[path=\"" + this.props.dataSource + "\"]");
				var data = JSON.parse( datael.innerText );
				if ( item.substr(-1) == '/' )
					return data.indexOf ( item ) > -1 || data.indexOf ( item.substr(0,item.length -1) ) > -1;
				return data.indexOf ( item ) > -1 || data.indexOf ( item + '/' ) > -1;
			}

			console.error ( "A data source and type must me set first.");
			return null;
		};

		this.getFSItems = function ( ) {

			if ( this.props.dataSourceType == 'stream' ) {
				var data = window.lvzwebdesktop.getFileS( this.props.dataSource );

				if ( data === false ) {
					console.warning ( "Problem getting data" );
					return false;
				}

				return true;
			}

			else if ( this.props.dataSourceType == 'element' ) {
				var datael = this.elements.container.querySelector( "[data-path=\"" + this.props.dataSource + "\"],[path=\"" + this.props.dataSource + "\"]");
				var data = JSON.parse( datael.innerText );
				var m,ret=[],dr=false;

				for ( var i=0; i<data.length; i++ )
					if ( typeof data[i] == 'string' ) {
						if ( data[i] == this.props.currentPath ) dr = true;
						m=data[i].match(RegExp ( '^' + this.props.currentPath + '([^/]+)([/]?)$','') );
						if ( m !== null ) ret.push(data[i]);
					}else {

					}
				if ( dr === false ) {
					var eobj = {
						"target" : this,
						"path" : this.props.currentPath
					};
					this.fireEvent('pathnotexist', eobj);
					if ( eobj.preventDefault.a === true ) return null;

					console.error ( "Path does not exists" );
					return null;
				}
				return ret;
			}

			return false;
		};

		this.addItem  = function ( item ) {

			if ( this.props.dataSourceType == 'element' ) {
				var datael = this.elements.container.querySelector( "[data-path=\"" + this.props.dataSource + "\"],[path=\"" + this.props.dataSource + "\"]");
				var data = JSON.parse( datael.innerText );
				if ( this.itemExists( item ) ) return this;
				data.push ( item );
				datael.innerText = JSON.stringify ( data );
			}

			return this;
		};

		this.removeItem = function ( item ) {

			if ( this.props.dataSourceType == 'element' ) {
				var datael = this.elements.container.querySelector( "[data-path=\"" + this.props.dataSource + "\"],[path=\"" + this.props.dataSource + "\"]");
				var data = JSON.parse( datael.innerText );
				if ( typeof item == 'string' ) {
					if ( item.substr(-1) == '/' ) {
						for ( var i = data.length - 1; i >= 0; i -- )
							if ( typeof data[i] == 'string' ) 
								if ( data[i].substr(0,item.length) == item )
									data.splice(i,1);
					} else {
						var ipos = data.indexOf ( item );
						if ( ipos > -1 ) data.splice(ipos,1);
					}
				}

				else if ( typeof item == 'object' ) {
					var ipos = data.indexOf ( item );
					if ( ipos > -1 ) {
						data.splice ( ipos, 1 );
					}
				}
				datael.innerText = JSON.stringify ( data );
			}

			return this;
		};

		this.copyItemUnderItem = function ( item1, item2 ) {

			if ( this.props.dataSourceType == 'element' ) {
				var datael = this.elements.container.querySelector( "[data-path=\"" + this.props.dataSource + "\"],[path=\"" + this.props.dataSource + "\"]");
				var data = JSON.parse( datael.innerText );
				if ( typeof item1 == 'string' ) {

					if ( typeof item2 == 'string' ) {
						if ( item2.substr(-1) != '/' ) {
							console.error ( 'Not a directory');
							return false;
						}

						var item1p = this.getParentPath (item1);
						var item1n = item1.substr(item1p.length);

						if ( this.itemExists( item2 + item1n ) ) return false;
						
						if ( item1.substr(-1) == '/' )
							for ( var i = 0; i < data.length; i ++ )
								if ( data[i].length > item1.length && data[i].substr(0,item1.length) == item1 )
									data.push (item2 + item1n + data[i].substr(item1.length) );

						item1 = item2 + item1n;
						data.push ( item1 );
					}

					else if ( typeof item2 == 'object' ) {
						item2.children.push ( item1 );
						data.push ( item2 );
					}

				}

				else if ( typeof item1 == 'object' ) {

					if ( typeof item2 == 'string' ) {
						item1.parent = item2;
					}
					else if ( typeof item2 == 'object' ) {
						item1.parent = item2.name;
					}

					data.push ( item1 );
				}

				datael.innerText = JSON.stringify ( data );

			}

			return this;

		};

		/**
		 * Method for checking if "item1" is parent of "item2".
		 * @param {string} item1 - The item to check.
		 * @param {string} item2 - The item to check if "item1" is parent of.
		 * @return {boolean} Returns true if "item1" is parent of "item2". false otherwise.
		 */
		this.isParentOf = function ( item1, item2 ) {
			if ( item2.substr(0,item1.length) == item1 ) return true;
			return false;
		};

		this.elements = {};

		this.elements.container = null;

		if ( e ) {
			this.elements.container = e;

			var tmpattr;

			tmpattr = e.getAttribute('data-source-type')
			if ( tmpattr !== null ) this.props.dataSourceType = tmpattr;

			tmpattr = e.getAttribute('data-source');
			if ( tmpattr !== null ) this.props.dataSource = tmpattr;

			var tmpevent;

			tmpevent = e.getAttribute('onchangepath');
			if ( tmpevent !== null ) this.addEvent('changepath', new Function ('event', tmpevent));

			tmpevent = e.getAttribute('onpathnotexist');
			if ( tmpevent !== null ) this.addEvent ('pathnotexist', new Function ('event', tmpevent));

		}

		else {
			this.elements.container = document.createElement('div');;
		}

	};

	/**
	 * List of all vfs objects.
	 * @type {Array}
	 * @public
	 */
	VFSObject.list = [];

	window.lvzwebdesktop.VFSObject = VFSObject;
	window.lvzwebdesktop.constructors.push({
		"selector" : ".web-desktop-vfs",
		"constructor" : VFSObject
	});
}
