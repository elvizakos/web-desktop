{
	document.currentScript.dataLVZWLIB = true;

	var applytoclassnames1 = [
		'android',
		'android-light',
		'android-dark'
	];

	lvzwebdesktop.WindowObject.addEvent('newwindow', function ( e ) {

		if ( ! applytoclassnames1.some( r=> this.elements.container.className.split(/ /g).indexOf(r) >= 0 ) ) return;

		/**
		 * CSS class-name for window's title iconify element.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.iconifyBtn = lvzwebdesktop.WindowObject.classNames.iconifyBtn;

		/**
		 * CSS class-name for window's title fullscreen element.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.fullscreenBtn = lvzwebdesktop.WindowObject.classNames.fullscreenBtn;

		/**
		 * CSS class-name for window's title splitview element.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.splitviewBtn = lvzwebdesktop.WindowObject.classNames.splitviewBtn;

		/**
		 * CSS class-name for window's title hidetitle element.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.hidetitleBtn = lvzwebdesktop.WindowObject.classNames.hidetitleBtn;

		/**
		 * CSS class-name for window's title transparency element.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.transparencyBtn = lvzwebdesktop.WindowObject.classNames.transparencyBtn;

		/**
		 * CSS class-name for window's title more element.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.moreBtn = lvzwebdesktop.WindowObject.classNames.moreBtn;

		/**
		 * CSS class-name for the window's title active button elements.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.activeTitleBtn = lvzwebdesktop.WindowObject.classNames.activeTitleBtn;

		/**
		 * Iconify title button for the android themes.
		 * @public
		 * @type {HTMLDivElement}
		 */
		this.elements.iconifyBtn = lvzwebdesktop.createBasicElement.call(this,'div',undefined,[
			this.props.classNames.titleControl,
			this.props.classNames.iconifyBtn
		]);
		this.elements.iconifyBtn.dataElement = 'iconifyBtn';
		//*** must create an taskmanager for android first

		/**
		 * Button to make android window to fullscreen.
		 * @public
		 * @type {HTMLDivElement}
		 */
		this.elements.fullscreenBtn = lvzwebdesktop.createBasicElement.call(this,'div',undefined,[
			this.props.classNames.titleControl,
			this.props.classNames.fullscreenBtn
		]);
		this.elements.fullscreenBtn.dataElement = 'fullscreenBtn';
		//***

		/**
		 * Button to add window to a split view.
		 * @public
		 * @type {HTMLDivElement}
		 */
		this.elements.splitviewBtn = lvzwebdesktop.createBasicElement.call(this,'div',undefined,[
			this.props.classNames.titleControl,
			this.props.classNames.splitviewBtn
		]);
		this.elements.splitviewBtn.dataElement = 'splitviewBtn';
		//*** must make tiles manager first

		/**
		 * Button to hide title of window.
		 * @public
		 * @type {HTMLDivElement}
		 */
		this.elements.hidetitleBtn = lvzwebdesktop.createBasicElement.call(this,'div',undefined,[
			this.props.classNames.titleControl,
			this.props.classNames.hidetitleBtn
		]);
		this.elements.hidetitleBtn.dataElement = 'hidetitleBtn';
		//***

		/**
		 * Button to show controls for setting transparency of the window.
		 * @public
		 * @type {HTMLDivElement}
		 */
		this.elements.transparencyBtn = lvzwebdesktop.createBasicElement.call(this,'div',undefined,[
			this.props.classNames.titleControl,
			this.props.classNames.transparencyBtn
		]);
		this.elements.transparencyBtn.dataElement = 'transparencyBtn';
		//***

		/**
		 * Button to show more controls in title bar.
		 * @public
		 * @type {HTMLDivElement}
		 */
		this.elements.moreBtn = lvzwebdesktop.createBasicElement.call(this,'div',undefined,[
			this.props.classNames.titleControl,
			this.props.classNames.moreBtn
		]);
		this.elements.moreBtn.dataElement = 'moreBtn';
		this.elements.moreBtn.dataTitleControls = ['iconify','fullscreen','close','hidetitle','splitview','transparency'];
		this.elements.moreBtn.addEventListener('click',function(e){
			var self = lvzwebdesktop(e.target);
			var btns = e.target.dataTitleControls;
			var hide = [];
			var show = [];
			var nlayout = '';
			if ( e.target.classList.contains ( self.props.classNames.activeTitleBtn ) ) {
				e.target.classList.remove ( self.props.classNames.activeTitleBtn )
			}
			else {
				e.target.classList.add ( self.props.classNames.activeTitleBtn );
			}
			for ( var i=0;i<btns.length;i++){
				if ( self.elements[btns[i]+'Btn'].parentNode !== null ) {
					hide.push(self.elements[btns[i]+'Btn']);
				}
				else {
					show.push(self.elements[btns[i]+'Btn']);
					nlayout+=btns[i]+',';
				}
			}
			self.setLayout(':'+nlayout+'more');
		});

		this.setLayout ( this.props.buttonLayout );

	});

	/**
	 * Default CSS class-name for window's title iconify element.
	 * @public
	 * @type {String}
	 */
	lvzwebdesktop.WindowObject.classNames.iconifyBtn = 'window-iconify-btn';

	/**
	 * Default CSS class-name for window's title fullscreen element.
	 * @public
	 * @type {String}
	 */
	lvzwebdesktop.WindowObject.classNames.fullscreenBtn = 'window-fullscreen-btn';

	/**
	 * Default CSS class-name for window's title splitview element.
	 * @public
	 * @type {String}
	 */
	lvzwebdesktop.WindowObject.classNames.splitviewBtn = 'window-splitview-btn';

	/**
	 * Default CSS class-name for window's title hidetitle element.
	 * @public
	 * @type {String}
	 */
	lvzwebdesktop.WindowObject.classNames.hidetitleBtn = 'window-hidetitle-btn';

	/**
	 * Default CSS class-name for window's title transparency element.
	 * @public
	 * @type {String}
	 */
	lvzwebdesktop.WindowObject.classNames.transparencyBtn = 'window-transparency-btn';

	/**
	 * Default CSS class-name for window's title more element.
	 * @public
	 * @type {String}
	 */
	lvzwebdesktop.WindowObject.classNames.moreBtn = 'window-more-btn';

	/**
	 * Default CSS class-name for window's active title button elements.
	 * @public
	 * @type {String}
	 */
	lvzwebdesktop.WindowObject.classNames.activeTitleBtn = 'active-btn';

}
