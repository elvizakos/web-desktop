{
	document.currentScript.dataLVZWLIB = true;

	/**
	 * @typedef {Object} elementoffset
	 * @property {number} x - The distance of the left border of the element from the left border of document's body element.
	 * @property {number} y - The distance of the top border of the element from the top border of document's body element.
	 * @property {number} z - The Greatest z-index of the given element's ancestors.
	 */

	/**
	 * Constructor for creating toolbar objects.
	 * @constructor
	 * @param {HTMLElement} [e] - Element to attach toolbar to.
	 */
	var ToolbarObject = function ( e ) {

		lvzwebdesktop.basicObject.call(this, 'toolbar');

		/**
		 * Method for attaching this object to another lvzwebdesktop object or an element. Fires "attachto" event.
		 *
		 * @param {HTMLElement|object} item - The lvzwebdesktop or the element to attach this object to.
		 * @param {'top'|'left'|'right'|'bottom'|'center'} [pos] - A string telling the position to attach toolbar to.
		 * @return {false|ToolbarObject} false on error, otherwise the current toolbar object.
		 */
		this.attachTo = function ( item, pos ) {
			var lvzo = null;

			if ( typeof pos == 'undefined' || ! pos ) pos = 'top';

			if ( item instanceof HTMLElement ) {
				if ( typeof item.dataLVZWDESKTOPOBJ == 'object' ) {
					lvzo = item.dataLVZWDESKTOPOBJ;
					if ( typeof lvzo.attachItem == 'function' ) {
						lvzo.attachItem(this.elements.container, pos);
					}
					else {
						console.error ( "Argument must be an lvzwebdesktop object having \"attachItem\" method." );
						return false;
					}
				}
				else {
					var eobj = {
						"target" : this,
						"attachElement" : item,
						"item" : item,
						"position" : pos
					};
					this.fireEvent('attachto',eobj);
					if ( eobj.defaultPrevented ) return this;

					item.appendChild ( this.elements.container );
				}
			}
			else {
				var c = lvzwebdesktop.isPartOf(item);
				if ( c === false || typeof item.attachItem != 'function' ) {
					console.error ( "Argument must be an element or an lvzwebdesktop object. If it is an lvzwebdesktop must having the \"attachItem\" method." );
					return false;
				}
				item.attachItem ( this, pos );
			}
			this.props.isAttached = true;

			return this;
		};

		/**
		 * Method for attaching toolbar to a window.
		 *
		 * @param {WindowObject} w - The window object to attach this toolbar to.
		 * @return {ToolbarObject} This toolbar object.
		 */
		this.attachToWindow = function ( w ) {

			if ( typeof w.toolbar == 'object'
				 && w.toolbr !== null ) return this;

			w.elements.bodycontainer.insertBefore(
				this.elements.container,
				w.elements.bodycontainer.firstElementChild
			);

			w.toolbar = this;
			this.window = w;

			w.fireEvent('itemattached', {
				"target" : w,
				"item" : this
			});

			return this;
		};

		/**
		 * Method for attaching items to toolbar.
		 * Fires "itemattached" event and "attachto" event on object to be attached.
		 * @param {HTMLElement|object} item - An element of an lvzwebdesktop object to attach.
		 * @return {ToolbarObject|false} False on error, otherwise current toolbar object.
		 */
		this.attachItem = function ( item ) {
			var eobj1 = { "target" : this };
			var eobj2 = { "attachTo" : this };

			if ( item instanceof HTMLElement ) {

				if ( typeof item.dataLVZWDESKTOPOBJ == 'object' ) {

					var c = lvzwebdesktop.isPartOf(item.dataLVZWDESKTOPOBJ );
					if ( c !== false
						 && item.dataLVZWDESKTOPOBJ.elements == 'object'
						 && item.dataLVZWDESKTOPOBJ.elements.container instanceof HTMLElement
					   ) {
						eobj2.target = item.dataLVZWDESKTOPOBJ;
						eobj2.attachElement = item;
						eobj1.item = item.dataLVZWDESKTOPOBJ;

						this.elements.body.appendChild ( item );
						item.dataLVZWDESKTOPOBJ.fireEvent ( 'attachto', eobj2 );
					}
					else {
						eobj1.item = item;
						this.elements.body.appendChild ( item );
					}
				}
				else {
					eobj1.item = item;
					this.elements.body.appendChild ( item );
				}
			}
			else {
				var c = window.lvzwebdesktop.isPartOf(item);
				if ( c === false ) {
					console.error ( "Argument must be an element of an lvzwebdesktop object with a container element." );
					return false;
				}
				if ( typeof item.elements == 'object'
					 && item.elements.container instanceof HTMLElement
				   ) {
					eobj2.target = item;
					eobj2.attachElement = item.elements.container;
					eobj1.item = item;

					this.elements.body.appendChild ( item.elements.container );

				}
			}

			item.fireEvent ( 'attachto', eobj1 );
			return this;

		};

		/**
		 * Method for adding items to ToolbarObject.
		 * Fires "additem" event.
		 * @param {HTMLElement|Object} o - The html element or web-object to add to the Toolbalr object.
		 * @return {ToolbarOjbect|false} False on error, this object otherwise.
		 */
		this.addItem = function ( o ) {
			var tmpelem = null;
			if ( o instanceof HTMLElement ) {
				tmpelem = o;
			}
			else if ( typeof o == 'object'
					  && typeof o.elements == 'object'
					  && typeof o.elements.container == 'object' && o.elements.container instanceof HTMLElement
					  && typeof o.elements.container.dataLVZWDESKTOPOBJ == 'object'
					) {
				tmpelem = o.elements.container;
				o.toolbar = this;
			}
			else {
				console.error ( "Argument is not a valid web-desktop object or an element.");
				return false;
			}

			// Check if element is allready in toolbar
			for ( var i = 0; i < this.items.length; i++ )
				if ( this.items[i] === tmpelem )
					return this;

			this.items.push(tmpelem);

			this.elements.body.appendChild ( tmpelem );

			return this.fireEvent('additem', {
				"target" : this,
				"item" : tmpelem
			});
		};

		/**
		 * Method for setting toolbar as editable.
		 * @param {boolean} t - true for set as editable, false for not.
		 * @return {ToolbarObject} This toolbar object.
		 */
		this.setEditable = function ( t ) {

			var ps = this.props.editable;
			this.props.editable = t;

			if ( t ) {
				this.elements.container.classList.add(
					this.props.classNames.editable
				);
			}
			else {
				this.elements.container.classList.remove(
					this.props.classNames.editable
				);
			}

			return this.fireEvent('editablestatechange',{
				"target" : this,
				"previousState" : this.props.editable
			});

		};

		/**
		 * Method for building groups for the toolbar.
		 * @public
		 * @param {Array} d - Array with arrays of elements or web-desktop objects.
		 * @return {ToolbarObject} This toolbar object.
		 */
		this.buildGroups = function ( d ) {
			for ( var i = 0; i < d.length; i++ ) {
				var tmpgroup = new ToolbarGroupObject();
				tmpgroup.toolbar = this;
				this.elements.container.appendChild ( tmpgroup.elements.container );
				for ( var j = 0; j < d[i].length; j ++ )
					tmpgroup.addItem(d[i][j]);
			}
			return this;
		};

		/**
		 * Method for getting the number of hidden toolbar items.
		 * @public
		 * @return {Number} The number of hidden items.
		 */
		this.countHiddenGroups = function () {
			var containerWidth = this.elements.body.offsetWidth;
			var l = 0;
			var ce = this.elements.body.firstElementChild;
			do {
				if ( ce.offsetLeft >= containerWidth ) l ++;
			} while ( (ce = ce.nextElementSibling) !== null );
			return l;
		};

		/**
		 * Method for getting the hidden toolbar items.
		 * @public
		 * @return {Array} An array of the hidden items.
		 */
		this.hiddenGroups = function () {
			var containerWidth = this.elements.body.offsetWidth;
			var r = [];
			var ce = this.elements.body.firstElementChild;
			do {
				if ( ce.offsetLeft >= containerWidth ) r.push(ce);
			} while ( (ce = ce.nextElementSibling) !== null );
			return r;
		};

		/**
		 * The window object that the toolbar belongs to or null.
		 * @public
		 * @type {WindowObject|null}
		 */
		this.window = null;

		/**
		 * Property to set that the toolbar is already attached to something.
		 * @public
		 * @type {boolean}
		 */
		this.props.isAttached = false;

		/**
		 * Array for storing the items of toolbar object.
		 * @public
		 * @type {Array}
		 */
		this.items = [];

		/**
		 * Property for setting if toolbar is editable.
		 * @public
		 * @type {Boolean}
		 */
		this.props.editable = true;

		/**
		 * CSS Class name of toolbar container element.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.container = 'web-desktop-toolbar';

		/**
		 * CSS Class name of toolbar container element for when toolbar is editable.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.editable = 'wd-toolbar-editable';

		/**
		 * CSS Class name of toolbar body element.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.body = 'wd-toolbar-body';

		/**
		 * CSS Class name of toolbar button to show the hidden elements.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.showHiddenBtn = 'wd-toolbar-showhidden-btn';

		/**
		 * CSS Class name of toolbar hidden elements list element.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.showHiddenItems = 'wd-toolbar-showhidden-items';

		/**
		 * CSS class name for toolbar's items containers.
		 * @public
		 * @type {string}
		 */
		this.props.classNames.toolbarItemContainer = 'wd-toolbar-item-container';

		/**
		 * Array for storing event functions for the "additem" event.
		 * @public
		 * @type {Array}
		 */
		this.onadditem = [];

		/**
		 * Array for storing event function for the "attachto" event.
		 * @public
		 * @type {Array}
		 */
		this.onattachto = [];

		/**
		 * Array for storing event functions for the "editablestatechange" event.
		 * @public
		 * @type {Array}
		 */
		this.oneditablestatechange = [];

		/**
		 * Container element.
		 * @public
		 * @type {HTMLElement}
		 */
		this.elements.container = lvzwebdesktop.createBasicElement.call(this,'div',e,[
			this.props.classNames.container
		]);

		/**
		 * Body element.
		 * @public
		 * @type {HTMLElement}
		 */
		this.elements.body = lvzwebdesktop.createBasicElement.call(this,'div',undefined,[
			this.props.classNames.body
		]);

		ce = this.elements.container.lastElementChild;
		var pce;
		while ( ce !== null ) {
			pce = ce.previousElementSibling;
			this.elements.body.insertBefore(ce,this.elements.body.firstElementChild);
			ce=pce;
		}
		this.elements.container.appendChild(this.elements.body);

		/**
		 * Element to show the hidden items of the toolbar.
		 * @public
		 * @type {HTMLElement}
		 */
		this.elements.showHiddenBtn = lvzwebdesktop.createBasicElement.call(this,'div',undefined,[
			this.props.classNames.showHiddenBtn
		]);
		this.elements.showHiddenBtn.addEventListener('click', toolbarShowHidden);
		this.elements.container.appendChild(this.elements.showHiddenBtn);

		/**
		 * Element to show hidden items of the toolbar.
		 * @public
		 * @type {HTMLElement}
		 */
		this.elements.showHiddenItems = lvzwebdesktop.createBasicElement.call(this,'div',undefined,[
			this.props.classNames.showHiddenItems
		]);

		var attr;

		attr = this.elements.container.getAttribute('onitemadd');
		if ( attr !== null ) this.addEvent ( 'itemadd', new Function ( 'event', attr ) );

		var w = lvzwebdesktop(this.elements.container,false).getParentObject('WindowObject');
		if ( w !== null ) w.toolbar = this;

		lvzwebdesktop.endBasicObject.call(this);

		if ( this.elements.container.classList.contains(this.props.classNames.editable) )
			this.props.editable = true;
		else
			this.props.editable = false;

		this.addEvent('attachto', function ( e ){
			var self = e.target;
			var w = lvzwebdesktop(e.positionElement);
			w.addEvent('resize', function ( e ) {
				var self = e.target.toolbar;
				if ( typeof self == 'object' ) {
					if ( self.countHiddenGroups() > 0 ) {
						self.elements.showHiddenBtn.style.display = 'block'; 
					}
					else {
						self.elements.showHiddenBtn.style.display = 'none';
					}
				}
			});
		} );

		if ( lvzwebdesktop.constructorsReady == true ) {
			var w = this.getParentOfType('WindowObject');

			if ( typeof w == 'object' && w instanceof lvzwebdesktop.WindowObject ) {
				w.attachItem(this, 'top' );
			}
		}

	}; 

	/**
	 * List with all toolbar objects.
	 * @public
	 * @type {Array}
	 */
	ToolbarObject.list = [];

	/**
	 * Constructor for creating toolbar group objects.
	 * @constructor
	 * @param {HTMLElement} [e] - Element to attack toolbar group to.
	 */
	var ToolbarGroupObject = function ( e ) {

		lvzwebdesktop.basicObject.call(this,'toolbargroup');

		/**
		 * Array for storing the items of the toolbar group object.
		 * @public
		 * @type {Array}
		 */
		this.items = [];

		/**
		 * The toolbar that the group belongs to.
		 * @public
		 * @type {ToolbarObject|null}
		 */
		this.toolbar = null;

		/**
		 * CSS Class name of toolbar group container element.
		 * @public
		 * @type {string}
		 */
		this.props.classNames.container = 'wd-toolbar-group';

		/**
		 * CSS Class name of toolbar group grab element.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.grab = 'wd-toolbar-group-grab';

		/**
		 * CSS Class name of toolbar group body element.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.body = 'wd-toolbar-group-body';

		/**
		 * CSS Class name of the element that replaces toolbar group during moving the group.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.positionElement = 'wd_toolbar_group_position';

		/**
		 * CSS Class name for the group container element while moving.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.moving = 'moving';
		/**
		 * Array for storing event functions for the "additem" event.
		 * @public
		 * @type {Array}
		 */
		this.onadditem = [];

		/**
		 * Array for storing event functions for the "removeitem" event.
		 * @public
		 * @type {Array}
		 */
		this.onremoveitem = [];

		/**
		 * Method for adding items to ToolbarGroupObject.
		 *
		 * @param {HTMLElement|Object} o - The html element or web-object to add to the toolbar group.
		 * @return {ToolbarGroupObject|false} False on error, this object on success.
		 */
		this.addItem = function ( o ) {
			var tmpelem = null;
			if ( o instanceof HTMLElement ) {
				tmpelem = o;
			}
			else {
				return this;
				// console.error ( "Argument is not a valid web-desktop object or an element." );
				// return false;
			}

			// Check if element is allready in toolbar group.
			if ( this.items.indexOf ( tmpelem ) > -1 ) return this;

			this.items.push( tmpelem );

			this.elements.body.appendChild ( tmpelem );

			return this.fireEvent('additem', {
				"target" : this,
				"item" : tmpelem
			});
		};

		/**
		 * Method for removing items from ToolbarGroupObject.
		 *
		 * @public
		 * @param {HTMLElement|Object} o - The html element, or web-object or items order number to remove.
		 * @return {ToolbarGroupObject|false} False on error, this object otherwise.
		 */
		this.removeItem = function ( o ) {
			var tmpelem = null;
			var tmporder = null;

			if ( typeof o == 'number' ) {
				if ( typeof this.items[o] != 'undefined' ) {
					tmpelem = this.items[o];
					tmporder = o;
				}
				else {
					console.error ( "Argument out of range." );
					return false;
				}
			}
			else if ( typeof o == 'object' ) {
				if ( o instanceof HTMLElement ) {
					if ( this.items.indexOf(o) > -1 ) {
						tmpelem = o;
						tmporder = this.items.indexOf(o);
					}
					else {
						console.error ( "Element not a member of toolbar group." );
						return false;
					}
				}
				else if ( typeof o.elements != 'undefined'
						  && typeof o.elements.container != 'undefined'
						  && typeof o.elements.container.dataLVZWDESKTOPOBJ == 'object'
						  && this.items.indexOf(o.elements.container) > -1
						) {
					tmpelem = o.elements.container;
					tmporder = this.items.indexOf(o.elements.container);
				}
				else {
					console.error ( "Argument is not a valid web-desktop object, an html element or the number of the order of an item." );
					return false;
				}
			}
			else {
				console.error ( "Argument is not a valid web-desktop object, an html element or the number of the order of an item." );
				return false;
			}

			tmpelem.remove();
			this.items.splice(tmporder,1);

			return this.fireEvent('removeitem', {
				"target" : this,
				"order" : tmporder,
				"item" : tmpelem
			});
		};

		/**
		 * Method for getting the items of the group that they are hidden.
		 *
		 * @public
		 * @return {Array} An array of the items that are not visible. 
		 */
		this.hiddenItems = function () {
			var cwidth = this.elements.body.offsetWidth;
			var r = [];
			var ce = this.elements.body.firstElementChild;
			do {
				if ( ce.offsetLeft >= cwidth ) r.push(ce);
			} while ( (ce = ce.nextElementSibling) !== null );
			return r;
		};

		/**
		 * Container element.
		 * @public
		 * @type {HTMLElement}
		 */
		this.elements.container = lvzwebdesktop.createBasicElement.call(this,'div',e,[
			this.props.classNames.container
		]);

		/**
		 * Toolbar group grab element.
		 * @public
		 * @type {HTMLElement}
		 */
		this.elements.grab = lvzwebdesktop.createBasicElement.call(this,'div',undefined,[
			this.props.classNames.grab
		]);
		this.elements.grab.addEventListener('mousedown', toolbargroup_md);
		
		/**
		 * Toobar group body element.
		 * @public
		 * @type {HTMLElement}
		 */
		this.elements.body = lvzwebdesktop.createBasicElement.call(this,'div',undefined,[
			this.props.classNames.body
		]);

		if (typeof  e != 'undefined') for ( var i=e.children.length-1; i>=0; i-- ) {
			this.items.unshift(e.children[i]);
			this.elements.body.insertBefore(e.children[i],this.elements.body.firstElementChild);
			this.fireEvent('additem', {
				"target" : this,
				"item" : e.children[i]
			});
		}

		this.elements.container.appendChild(this.elements.grab);
		this.elements.container.appendChild(this.elements.body);

		var attr;

		attr = this.elements.container.getAttribute('onitemadd');
		if ( attr !== null ) this.addEvent('itemadd', new Functions ( 'event', attr ) );

		if ( lvzwebdesktop.constructorsReady == true ) {

			var t = this.getParentOfType('ToolbarObject');

			if ( typeof t == 'object' && t instanceof lvzwebdesktop.ToolbarObject ) {
				this.toolbar = t;
			}
		}
	};

	/**
	 * List with all toolbar group objects.
	 * @public
	 * @type {Array}
	 */
	ToolbarGroupObject.list = [];

	/**
	 * Function for getting the offset position of an element.
	 * @param {HTMLElement} e - The element to get it's position.
	 * @return {elementoffset} The offset properties of the give element.
	 */
	function elementOffset ( e ) {
		var ret = {x:0,y:0,z:0};
		var o = e;
		while ( o.nodeName != '#document' ) {
			ret.x+=o.offsetLeft;
			ret.y+=o.offsetTop;
			if ( o.style.zIndex > ret.z) ret.z = o.style.zIndex;
			o = o.parentNode;
		}
		return ret;
	}

	/**
	 * Function for controlling the mousedown events on toolbar group grab element.
	 * @param {Event} e - Event object.
	 */
	function toolbargroup_md ( e ) {
		if ( e.button !== 0 ) return ;
		e.preventDefault();
		var self = e.target.dataToolbarGroupObject;
		toolbargroup_md.currenttg = self;
		toolbargroup_md.currentel = e.target.parentNode;
		toolbargroup_md.currentelnel = toolbargroup_md.currentel.nextElementSibling;

		var m = elementOffset(toolbargroup_md.currentel);

		if ( toolbargroup_md.relem === null ) {
			toolbargroup_md.relem = document.createElement('div');
			toolbargroup_md.relem.classList.add(self.props.classNames.positionElement);
		}

		toolbargroup_md.currentel.classList.add(self.props.classNames.moving);

		toolbargroup_md.relem.style.display = 'inline-block';
		toolbargroup_md.relem.style.width = toolbargroup_md.currentel.offsetWidth + 'px';
		toolbargroup_md.relem.style.height = toolbargroup_md.currentel.offsetHeight + 'px';

		toolbargroup_md.currentel.parentNode.insertBefore(toolbargroup_md.relem, toolbargroup_md.currentel);

		m.w = toolbargroup_md.currentel.style.width;
		m.h = toolbargroup_md.currentel.style.height;
		m.ow = toolbargroup_md.currentel.offsetWidth;
		m.oh = toolbargroup_md.currentel.offsetHeight;

		toolbargroup_md.currentel.style.position = 'absolute';
		toolbargroup_md.currentel.style.width = m.ow + 'px';
		toolbargroup_md.currentel.style.height = m.oh + 'px';

		document.body.appendChild ( toolbargroup_md.currentel );

		toolbargroup_md.m = {
			mx : e.clientX,
			my : e.clientY,
			ex : m.x,
			ey : m.y,
			dx : e.clientX - m.x,
			dy : e.clientY - m.y,
			oz : toolbargroup_md.currentel.style.zIndex,
			mw : m.w,
			mh : m.h
		};

		toolbargroup_md.currentel.style.zIndex = m.z + 1;

		document.addEventListener('mouseup', toolbargroup_mu);
		document.addEventListener('mousemove', toolbargroup_mm);
		toolbargroup_md.currentel.removeEventListener('mousedown', toolbargroup_md);
	}

	toolbargroup_md.relem = null;
	toolbargroup_md.currenttg = null;
	toolbargroup_md.currentel = null;
	toolbargroup_md.currentelnel = null;
	toolbargroup_md.m = {};

	/**
	 * Function for controlling the mousemove events on document after mousedown on a toolbar group grab element.
	 * @param {Event} e - Event object.
	 */
	function toolbargroup_mm ( e ) {
		var self = toolbargroup_md.currenttg;
		e.preventDefault();
		toolbargroup_md.currentel.style.left = (e.clientX) + 'px';
		toolbargroup_md.currentel.style.top = (e.clientY) + 'px';

		var d = toolbargroup_md.currentel.style.display;
		toolbargroup_md.currentel.style.display = 'none';
		var elematp = document.elementFromPoint(e.clientX, e.clientY);
		toolbargroup_md.currentel.style.display = d;

		var o = elematp;
		if ( o !== null ) while ( o.nodeName != "#document" ) {
			if ( typeof o.dataToolbarGroupObject == 'object'
				 && self.toolbar !== null
				 && o.dataToolbarGroupObject.toolbar === self.toolbar
			   ) {
				o = o.dataToolbarGroupObject.elements.container;
				var m = elementOffset(o);
				if ( e.clientX < m.x + (o.offsetWidth / 2) ) {
					o.parentNode.insertBefore(toolbargroup_md.relem,o);
				}
				else {
					o.parentNode.insertBefore(toolbargroup_md.relem,o.nextElementSibling);
				}
				break;
			}
			o = o.parentNode;
		}
	}

	/**
	 * Function for controlling the mouseup events on document after mousedown on a toolbar group grab element.
	 * @param {Event} e - Event object.
	 */
	function toolbargroup_mu ( e ) {
		var self = toolbargroup_md.currenttg;

		toolbargroup_md.currentel.style.position = '';
		toolbargroup_md.relem.style.display = 'none';

		toolbargroup_md.currentel.style.width = toolbargroup_md.m.mw;
		toolbargroup_md.currentel.style.height = toolbargroup_md.m.mh;
		toolbargroup_md.currentel.classList.remove(self.props.classNames.moving);

		toolbargroup_md.relem.parentNode.insertBefore(toolbargroup_md.currentel, toolbargroup_md.relem);

		document.removeEventListener('mouseup', toolbargroup_mu);
		document.removeEventListener('mousemove', toolbargroup_mm);
		toolbargroup_md.currenttg.elements.grab.addEventListener('mousedown', toolbargroup_md);
	}


	function toolbarShowHidden ( e ) {
		var self = lvzwebdesktop(e.target);

		toolbarShowHidden.d = {};
		toolbarShowHidden.d.o = self;

		self.elements.showHiddenBtn.removeEventListener('click', toolbarShowHidden);
		document.addEventListener('mousedown', toolbarShowHiddenH );

		var items = self.hiddenGroups();

		toolbarShowHidden.d.i = items;

		for ( var i=0; i<items.length; i++ ) {
			self.elements.showHiddenItems.appendChild(items[i]);
		}

		self.elements.showHiddenItems.style.position = 'absolute';
		self.elements.showHiddenItems.style.zIndex = '100';
		self.elements.showHiddenItems.style.top = e.clientY + 'px';
		self.elements.showHiddenItems.style.left = e.clientX + 'px';
		self.elements.showHiddenItems.style.display = 'block';
		self.elements.showHiddenItems.tabIndex = 0;

		document.body.appendChild(self.elements.showHiddenItems);
	}

	function toolbarShowHiddenH ( e ) {
		var self = toolbarShowHidden.d.o;
		var o = e.target;

		self.elements.showHiddenItems.style.display = 'none';
		for (var i = 0; i < toolbarShowHidden.d.i.length; i ++ ) {
			self.elements.body.appendChild(toolbarShowHidden.d.i[i]);
		}
		document.removeEventListener('mousedown', toolbarShowHiddenH );
		self.elements.showHiddenBtn.addEventListener('click', toolbarShowHidden);
	}

	// Properly attach object to it's parent after loading the constructors.
	if ( lvzwebdesktop.constructorsReady === false ) {

		lvzwebdesktop.fn.addEvent.call(lvzwebdesktop, 'constructorsready', function ( e ) {
			var self = null;
			for ( var i = 0 ; i < e.target.ToolbarObject.list.length; i ++ ) {
				self = e.target.ToolbarObject.list[i];
				var w = self.getParentOfType('WindowObject');

				if ( self.props.isAttached === false && typeof w == 'object' && w instanceof lvzwebdesktop.WindowObject ) {
					if ( self.elements.container.classList.contains('top-orientation'))
						self.attachTo ( w, 'top' );
					else if ( self.elements.container.classList.contains('left-orientation'))
						self.attachTo ( w, 'left' );
					else if ( self.elements.container.classList.contains('right-orientation'))
						self.attachTo ( w, 'right' );
					else
						self.attachTo ( w, 'bottom' );
					w.toolbar = self;
				}
			}

			for ( var i = 0; i < e.target.ToolbarGroupObject.list.length; i ++ ) {
				self = e.target.ToolbarGroupObject.list[i];
				var t = self.getParentOfType('ToolbarObject');

				if ( typeof t == 'object' && t instanceof lvzwebdesktop.ToolbarObject ) {
					self.toolbar = t;
				}
			}

		});

	}

	window.lvzwebdesktop.ToolbarObject = ToolbarObject;
	window.lvzwebdesktop.constructors.push({
		"selector" : ".web-desktop-toolbar,web-toolbar",
		"constructor" : ToolbarObject
	});

	window.lvzwebdesktop.ToolbarGroupObject = ToolbarGroupObject;
	window.lvzwebdesktop.constructors.push({
		"selector" : ".wd-toolbar-group",
		"constructor" : ToolbarGroupObject
	});
}
