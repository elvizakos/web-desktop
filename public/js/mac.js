{
	document.currentScript.dataLVZWLIB = true;

	var applytoclassnames = [
		'macos7'
	];

	lvzwebdesktop.WindowObject.addEvent('newwindow', function ( e ) {

		if ( ! applytoclassnames.some( r=> this.elements.container.className.split(/ /g).indexOf(r) >= 0 ) ) return;

		/**
		 * CSS class-name for window's body, scrollbars and resize button container element.
		 * @type {string}
		 * @public
		 */
		this.props.classNames.bodyscrollandresize = lvzwebdesktop.WindowObject.classNames.bodyscrollandresize;

		/**
		 * CSS class-name for window's body and right scrollbar container element.
		 * @type {string}
		 * @public
		 */
		this.props.classNames.bodyandrightscroll = lvzwebdesktop.WindowObject.classNames.bodyandrightscroll;

		/**
		 * CSS class-name for window's right scroll-bar container element.
		 * @type {string}
		 * @public
		 */
		this.props.classNames.rightscrollbar = lvzwebdesktop.WindowObject.classNames.rightscrollbar;

		/**
		 * CSS class-name for window's bottom scroll-bar container element.
		 * @type {string}
		 * @public
		 */
		this.props.classNames.bottomscrollbar = lvzwebdesktop.WindowObject.classNames.bottomscrollbar;

		/**
		 * CSS class-name for window's resize button element.
		 * @type {string}
		 * @public
		 */
		this.props.classNames.macresizeBtn = lvzwebdesktop.WindowObject.classNames.macresizeBtn;

		/**
		 * Container element for body, scrollbars and resize button.
		 * @type {HTMLDivElement}
		 * @public
		 */
		this.elements.bodyscrollandresize = document.createElement('div');
		this.props.resizeElement = this.elements.bodyscrollandresize;
		this.elements.bodyscrollandresize.className = this.props.classNames.bodyscrollandresize;
		this.elements.bodyscrollandresize.dataElement = 'bodyscrollandresize';
		this.elements.bodyscrollandresize.dataWindowObject = this;
		this.elements.bodyscrollandresize.dataLVZWDESKTOPOBJ = this;

		/**
		 * Container element for body and right scrollbar.
		 * @type {HTMLDivElement}
		 * @public
		 */
		this.elements.bodyandrightscroll = document.createElement('div');
		this.elements.bodyandrightscroll.className = this.props.classNames.bodyandrightscroll;
		this.elements.bodyandrightscroll.dataElement = 'bodyandrightscroll';
		this.elements.bodyandrightscroll.dataWindowObject = this;
		this.elements.bodyandrightscroll.dataLVZWDESKTOPOBJ = this;

		/**
		 * Container element for the right scrollbar.
		 * @type {HTMLDivElement}
		 * @public
		 */
		this.elements.rightscrollbar = document.createElement('div');
		this.elements.rightscrollbar.className = this.props.classNames.rightscrollbar;
		this.elements.rightscrollbar.dataElement = 'rightscrollbar';
		this.elements.rightscrollbar.dataWindowObject = this;
		this.elements.rightscrollbar.dataLVZWDESKTOPOBJ = this;

		/**
		 * Container element for the bottom scrollbar.
		 * @type {HTMLDivElement}
		 * @public
		 */
		this.elements.bottomscrollbar = document.createElement('div');
		this.elements.bottomscrollbar.className = this.props.classNames.bottomscrollbar;
		this.elements.bottomscrollbar.dataElement = 'bottomscrollbar';
		this.elements.bottomscrollbar.dataWindowObject = this;
		this.elements.bottomscrollbar.dataLVZWDESKTOPOBJ = this;

		/**
		 * Resize button for the classic mac themes.
		 * @type {HTMLDivElement}
		 * @public
		 */
		this.elements.macresizeBtn = document.createElement('div');
		this.elements.macresizeBtn.className = this.props.classNames.macresizeBtn;
		this.elements.macresizeBtn.dataElement = 'macresizeBtn';
		this.elements.macresizeBtn.dataWindowObject = this;
		this.elements.macresizeBtn.dataLVZWDESKTOPOBJ = this;
		this.elements.macresizeBtn.addEventListener('mousedown', lvzwebdesktop.borderResizeBegin);

		this.elements.centercont.insertBefore (
			this.elements.bodyscrollandresize,
			this.elements.bottombordercontainer
		);

		this.elements.bodyandrightscroll.appendChild(this.elements.bodycontainer);
		this.elements.bodyandrightscroll.appendChild(this.elements.rightscrollbar);
		this.elements.bodyscrollandresize.appendChild(this.elements.bodyandrightscroll);
		this.elements.bodyscrollandresize.appendChild(this.elements.bottomscrollbar);
		this.elements.bodyscrollandresize.appendChild(this.elements.macresizeBtn);

		this.elements.bodycontainer.style.height = '';
		this.elements.bodycontainer.style.width = '';

		this.elements.leftbordercont.style.width = '';
		this.elements.leftbordercont.style.border = '';
		this.elements.rightbordercont.style.width = '';
		this.elements.rightbordercont.style.border = '';

		this.props.theme.border['border-weight'] = '0px';
		this.props.theme.border['corner-height'] = '0px';
		this.props.theme.border['corner-width'] = '0px';
		this.props.theme.border['width'] = '0px';
		this.resize();
	});

	/**
	 * Default CSS class-name for window's body,scrollbars and resize button container element.
	 * @type {string}
	 * @public
	 */
	lvzwebdesktop.WindowObject.classNames.bodyscrollandresize = 'body-scroll-and-resize';

	/**
	 * Default CSS class-name for window's body and right scrollbar container element.
	 * @type {string}
	 * @public
	 */
	lvzwebdesktop.WindowObject.classNames.bodyandrightscroll = 'body-rightscroll';

	/**
	 * Default CSS class-name for window's right scroll-bar container element.
	 * @type {string}
	 * @public
	 */
	lvzwebdesktop.WindowObject.classNames.rightscrollbar = 'right-scrollbar';

	/**
	 * Default CSS class-name for window's bottom scroll-bar container element.
	 * @type {string}
	 * @public
	 */
	lvzwebdesktop.WindowObject.classNames.bottomscrollbar = 'bottom-scrollbar';

	/**
	 * Defautl CSS class-name for window's resize button element.
	 * @type {string}
	 * @public
	 */
	lvzwebdesktop.WindowObject.classNames.macresizeBtn = 'macresizebtn window-title-control';

}
