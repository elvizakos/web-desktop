(function () {
	document.currentScript.dataLVZWLIB = true;

	/**
	 * Constructor for creating visual window editor. 
	 * @constructor
	 * @param {HTMLElement} [e] - Element to attach editor to.
	 */
	var VisualeditorObject = function ( e ) {

		if ( this instanceof Window ) return new VisualeditorObject();

		lvzwebdesktop.basicObject.call(this,'visualeditor');

		this.generateXML = function () {};

		this.generateHTML5 = function () {
			var libraries=[
				'form-controls',
				'window',
				'desktop',
				'menu',
				'toolbar',
				'statusbar'
			];
			var theme = 'test';

			// var cssPath = 'http:/localhost:8080/css/';
			var cssPath = 'file:///home/elvizakos/Projects/JavaScript/web-desktop/public/css/';
			var cssfiles = [
				"theme-test",
				"toolbar/test-theme",
				"menu/test-theme",
				"formelements/test-theme",
				"statusbar/test-theme",
				"icons/test-theme",
				"panel/test-theme",
				"taskbar/test-theme",
				"widget/test-theme",
				"timer/test-theme",
			];
				
			// var librariesPath = 'js/';
			var librariesPath = 'file:///home/elvizakos/Projects/JavaScript/web-desktop/public/js/';
			// var librariesPath = 'http://localhost:8080/js/';
			var ret = "<!DOCTYPE html>\n";
			ret += '<html><head><meta charset="UTF-8"/><script src="'+librariesPath+'lvzwebdesktop.js"></script>';
			for (var i=0;i<libraries.length;i++) {
				ret+='<script src="'+librariesPath+libraries[i]+'.js"></script>';
			}
			ret += '<link rel="stylesheet" href="'+cssPath+'main.css" type="text/css" />';
			for ( i=0;i<cssfiles.length;i++) {
				ret+='<link rel="stylesheet" href="'+cssPath+cssfiles[i]+'.css" type="text/css" />';
			}
			ret+='</head><body class="ts-light"><div class="desktop" style="padding:0;margin:0;">';
			ret+=this.editedWindow.codeHTML();
			ret+='</div></body></html>';
			return ret;
		};

		this.generatePython3GTK3 = function(){
			var ret='#!/usr/bin/env python3\n';

			ret+="import gi\n";
			ret+="gi.require_version(\"Gtk\", \"3.0\")\n";
			ret+="from gi.repository import Gtk\n";

			ret+=this.editedWindow.codePython3GTK3();

			return ret;
		};

		/**
		 * Property to store all visual controls in this editor.
		 * @public
		 * * @type {Array}
		 */
		this.controls = [];

		/**
		 * CSS classname of the container element.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.container = 'visual-editor';

		/**
		 * Container element of the visual editor.
		 * @public
		 * @type {HTMLElement}
		 */
		this.elements.container = lvzwebdesktop.createBasicElement.call(this,'div',e,[
			this.props.classNames.container
		]);

		/**
		 * The window that hosts the visual editor or null if not in a window.
		 * @public
		 * @type {WindowObject|null}
		 */
		this.window = lvzwebdesktop(this.elements.container,false).getParentObject('WindowObject');

		/**
		 * The window that is edited or null if there is not one.
		 * @public
		 * @type {WindoweditorObject|null}
		 */
		this.editedWindow = null;

		// Add toolbar, menubar and buttons to add controls, if it's a window
		if ( this.window && this.window !== null ) {
			var tmpmi;
			var tmpsm;
			var cell;

			this.window.elements.bodycontainer.style.position = 'relative';

			this.selectControl = (new ListboxObject());
			for ( var i=0; i<VisualcontrolObject.list.length; i++) {
				this.selectControl.addItem(
					VisualcontrolObject.list[i].constructor.name,
					i
				);
			}
			this.selectControl.elements.container.style.display = 'block';
			this.window.attachItem(this.selectControl,'left');
			this.selectControl.selectControl = function ( c ) {
				for (var i=0;i<this.elements.container.children.length;i++)
					if ( this.elements.container.children[i].dataControl === c ) {
						this.selectItem(i);
					}
			};
			this.selectControl.elements.container.addEventListener('change', selectcontrolchange);

			this.properties = (new TableObject())
				.addColumn()
				.addColumn()
			;

			this.properties.elements.container.style.width = '200px';

			for ( var i=0; i < 100; i ++ ) {
				this.properties.addRow();
			}

			for ( i=0;i<this.properties.rows.length;i++) {
				cell = this.properties.getCell(i,1);
				cell.props.editable = true;
				cell.addEvent('captionchange',function(e){
					if( typeof e.target.controlprops == 'undefined' ) return;
					if( typeof e.target.controlprops.setValue == 'function' ) {
						if ( typeof e.target.vcontrol == 'undefined' ) return;
						e.target.controlprops.setValue(
							e.target.vcontrol,
							e.target.getValue()
						);
					}
				});
				cell.elements.container.addEventListener('keydown', visualcontrolKD );
				cell.elements.listselect.addEventListener('change', visualcontrolCH );
				this.properties.rows[i].style.display = 'none';
			}

			this.elements.propcontainer = lvzwebdesktop.createBasicElement.call(this,'div',undefined,[]);
			this.elements.propcontainer.style.height = 'calc(100% - '+this.selectControl.elements.container.offsetHeight+'px)';
			this.elements.propcontainer.style.overflow = 'scroll';
			this.elements.propcontainer.appendChild(this.properties.elements.container);

			this.window.attachItem(this.elements.propcontainer,'left');

			this.menu = new lvzwebdesktop.MenubarObject();
			this.menu.attachTo(this.window,'top');
			this.menu.visualeditor = this;
			this.menu.windoweditor = this.window;

			// File menu
			tmpmi = this.menu.createMenuItem('File');
			this.menu.addMenuItem(tmpmi);

			// Edit menu
			tmpmi = this.menu.createMenuItem('Edit');
			this.menu.addMenuItem(tmpmi);

			// View menu
			tmpmi = this.menu.createMenuItem('View');
			this.menu.addMenuItem(tmpmi);
			tmpsm = new lvzwebdesktop.MenuObject();
			tmpsm.elements.container.classList.add('test');
			tmpsm.visualeditor = this;
			tmpsm.windoweditor = this.window;
			this.menu.addSubmenu(tmpsm,tmpmi);
			tmpsm.addItem('Show toolbar', function ( e ) {
				console.log ( e ) ;
			});
			tmpsm.addSeparator();
			tmpsm.addItem('Get HTML code',function ( e ) {
				var self = lvzwebdesktop(e.target.parentNode);
				var sourceCode = self.visualeditor.generateHTML5();
				var mime = 'application/octet-stream';
				var file = 'data:'+mime+';Content-Disposition:attachment;filename="file.html";base64,' + btoa(sourceCode);

				window.open(file, "_blank") || window.location.replace(file);
			});
			tmpsm.addItem('Get Python3 code',function ( e ) {
				var self = lvzwebdesktop(e.target.parentNode);
				var sourceCode = self.visualeditor.generatePython3GTK3();
				var mime = 'application/octet-stream';
				var file = 'data:'+mime+';Content-Disposition:attachment;filename="file.html";base64,' + btoa(sourceCode);

				window.open(file, "_blank") || window.location.replace(file);
			});

			// Help menu
			tmpmi = this.menu.createMenuItem('Help');
			tmpmi.style.cssFloat = 'right';
			this.menu.addMenuItem(tmpmi);

			// Add toolbar
			this.toolbar = new lvzwebdesktop.ToolbarObject();
			this.toolbar.elements.container.classList.add('top-orientation');
			this.toolbar.attachTo(this.window,'top');

			for ( var i=0; i<window.lvzwebdesktop.windowVisualControls.length; i++) {
				var controlbtn = new lvzwebdesktop.ButtonObject();
				controlbtn.addEvent('click',visualeditoraddcontrol);
				controlbtn.elements.container.classList.add('wd-toolbar-square-button');
				controlbtn.props.constructor = window.lvzwebdesktop.windowVisualControls[i].constructor;
				controlbtn.visualeditor = this;

				if ( typeof window.lvzwebdesktop.windowVisualControls[i].name == 'string' )
					controlbtn.elements.container.title = window.lvzwebdesktop.windowVisualControls[i].name;
				else
					controlbtn.elements.container.title =  controlbtn.props.constructor.name;

				if ( typeof window.lvzwebdesktop.windowVisualControls[i].icon == 'string' ) {
					controlbtn.elements.container.style.backgroundImage = "url('"+window.lvzwebdesktop.windowVisualControls[i].icon+"')";

					controlbtn.elements.container.style.height = window.lvzwebdesktop.windowVisualControls[i].iconh + 'px';
					controlbtn.elements.container.style.width  = window.lvzwebdesktop.windowVisualControls[i].iconw + 'px';
				}

				if ( typeof window.lvzwebdesktop.windowVisualControls[i].init == 'function' )
					window.lvzwebdesktop.windowVisualControls[i].init(controlbtn);

				this.toolbar.addItem(controlbtn);
			}

		}
	};

	/**
	 * List of all VisualeditorObject instances.
	 * @public
	 */
	VisualeditorObject.list = [];

	function visualeditoraddcontrol ( e ) {
		var self = e.target;
		var controlobj = new self.props.constructor();

		if ( controlobj.props.minWidth && typeof controlobj.setWidth == 'function' )
			controlobj.setWidth(controlobj.props.minWidth);

		if ( controlobj.props.minHeight && typeof controlobj.setHeight == 'function' )
			controlobj.setHeight(controlobj.props.minHeight);

		if ( visualcontrolFocus.s !== null
			 && typeof visualcontrolFocus.s.control.props.addChildrenOnEdit != 'undefined'
			 && visualcontrolFocus.s.control.props.addChildrenOnEdit !== false
		   ) {


			if ( typeof visualcontrolFocus.s.control.props.addChildrenOnEdit == 'string'
				 && visualcontrolFocus.s.control.props.addChildrenOnEdit.split(',').indexOf(controlobj.type) > -1
			   ) {

				if ( typeof visualcontrolFocus.s.control.addItem == 'function' ) {
					visualcontrolFocus.s.control.addItem(controlobj,null);
					var vc = (new VisualcontrolObject(controlobj));
					visualcontrolFocus.s.elements.itemcontainer.appendChild(vc.elements.container);
					visualcontrolFocus.s.elements.vail.style.display = 'none';
					addvisualcontroltoeditorlistbox(controlobj,self.visualeditor);
				}

			}
			else if ( visualcontrolFocus.s.props.addChildrenOnEdit === true ) {
			}
		}
		else {
			self.visualeditor.editedWindow.props.window.elements.bodycontainer.appendChild(controlobj.elements.container);
			(new VisualcontrolObject(controlobj));
			addvisualcontroltoeditorlistbox(controlobj,self.visualeditor);
		}

	}

	/**
	 * Function for adding controls to the listbox of the visual editor.
	 * @param {Object} control - The control to add.
	 * @param {VisualeditorObject} editor - The visual editor.
	 */
	function addvisualcontroltoeditorlistbox ( control, editor ) {
		var o = editor.selectControl.addItem( control.constructor.name,editor.controls.length);
		o.dataControl = control;
		editor.selectControl.selectItem(editor.controls.length);
		editor.controls.push ( control );
	}

	/**
	 * Function to control the change on editor controls listbox.
	 * @param {Event} e - The event object.
	 */
	function selectcontrolchange ( e ) {
		var self = lvzwebdesktop(e.target);
		var o = false;
		for (var i=0;i<self.elements.container.children.length;i++)
			if ( self.elements.container.children[i].selected === true ) {
				o = self.elements.container.children[i].dataControl;
			}
		if ( o === false ) return;
		if ( typeof o.visualcontrol != 'undefined' ) {
			buildPropertiesTable ( o );
		}
		else {
			windoweditorclickwindow ({"target":o.elements.bodycontainer});
		}
	}

	/**
	 * Constructor for creating editor for visual editing windows.
	 * @constructor
	 * @param {HTMLElement} [e] - Element to attach visual editor to.
	 */
	var WindoweditorObject = function ( e ) {

		lvzwebdesktop.basicObject.call(this,'windoweditor');

		/**
		 * Method for generating HTML code of window.
		 * @return {String} The html code for the creation of this window.
		 */
		this.codeHTML = function ( ) {
			// Here take code from objects in window
			var htmlCode = '';

			var themes = [];
			themes.push("web-desktop-window");

			var ret = '<div class="'+themes.join(" ")+'"';

			if ( typeof this.props.id == 'string' )
				ret += "\n     id=\"" + this.props.id + "\"";
			if ( typeof this.props.window.props.title == 'string' )
				ret += "\n     caption=\""+this.props.window.props.title+"\"";
			if ( typeof this.props.icon == 'string' )
				ret += "\n     icon=\""+this.props.icon+"\"";

			if ( typeof this.props.window.props.top == 'number' )
				ret += "\n     top=\""+this.props.window.props.top+"\"";
			if ( typeof this.props.window.props.minTop == 'number' )
				ret += "\n     min-top=\""+this.props.window.props.minTop+"\"";
			if ( typeof this.props.window.props.maxTop == 'number' )
				ret += "\n     max-top=\""+this.props.window.props.maxTop+"\"";

			if ( typeof this.props.window.props.bottom == 'number' )
				ret += "\n     bottom=\""+this.props.window.props.bottom+"\"";
			if ( typeof this.props.window.props.minBottom == 'number' )
				ret += "\n     min-bottom=\""+this.props.window.props.minBottom+"\"";
			if ( typeof this.props.window.props.maxBottom == 'number' )
				ret += "\n     max-bottom=\""+this.props.window.props.maxBottom+"\"";

			if ( typeof this.props.window.props.left == 'number' )
				ret += "\n     left=\""+this.props.window.props.left+"\"";
			if ( typeof this.props.window.props.minLeft == 'number' )
				ret += "\n     min-left=\""+this.props.window.props.minLeft+"\"";
			if ( typeof this.props.window.props.maxLeft == 'number' )
				ret += "\n     max-left=\""+this.props.window.props.maxLeft+"\"";

			if ( typeof this.props.window.props.right == 'number' )
				ret += "\n     right=\""+this.props.window.props.right+"\"";
			if ( typeof this.props.window.props.minRight == 'number' )
				ret += "\n     min-right=\""+this.props.window.props.minRight+"\"";
			if ( typeof this.props.window.props.maxRight == 'number' )
				ret += "\n     max-right=\""+this.props.window.props.maxRight+"\"";

			if ( typeof this.props.window.props.height == 'number' )
				ret += "\n     height=\""+this.props.window.props.height+"\"";
			if ( typeof this.props.window.props.minHeight == 'number' )
				ret += "\n     min-height=\""+this.props.window.props.minHeight+"\"";
			if ( typeof this.props.window.props.maxHeight == 'number' )
				ret += "\n     max-height=\""+this.props.window.props.maxHeight+"\"";

			if ( typeof this.props.window.props.width == 'number' )
				ret += "\n     width=\""+this.props.window.props.width+"\"";
			if ( typeof this.props.window.props.minWidth == 'number' )
				ret += "\n     min-width=\""+this.props.window.props.minWidth+"\"";
			if ( typeof this.props.window.props.maxWidth == 'number' )
				ret += "\n     max-width=\""+this.props.window.props.maxWidth+"\"";

			if ( this.props.window.props.showintaskbar == false )
				ret += "\n     show-in-taskbar=\"false\"";
			if ( this.props.window.props.resizable == false )
				ret += "\n     resizable=\"false\"";
			if ( this.props.window.props.disableresize == true )
				ret += "\n     disable-resize=\"true\"";
			if ( this.props.window.props.maximizable == false )
				ret += "\n     maximizable=\"false\"";
			if ( this.props.window.props.minimizable == false )
				ret += "\n     minimizable=\"false\"";
			if ( this.props.window.props.alwaysontop == true )
				ret += "\n     always-on-top=\"true\"";

			if ( this.props.window.props.buttonLayout != false )
				ret += "\n     button-layout=\""+this.props.buttonLayout+"\"";

			if ( visualcontrolFocus.s !== null ) visualcontrolFocus.s.endResize();

			var c = this.props.window.elements.bodycontainer.firstElementChild;
			var o;
			while( c !== null ) {
				o = lvzwebdesktop(c);
				if ( o !== false && typeof o.codeHTML == 'function' )
					htmlCode+=o.codeHTML();
				else
					htmlCode+=c.outerHTML;

				c = c.nextElementSibling;
			}

			if ( visualcontrolFocus.s !== null &&
				 visualcontrolFocus.s.elements.container.parentNode &&
				 visualcontrolFocus.s.control.props.resizableOnEdit ) visualcontrolFocus.s.beginResize();

			return ret+'>'+htmlCode+'</div>';
		};

		/**
		 * Method for generating JavaScript code of window.
		 * @return {String} The JavaScript code for creating the window.
		 */
		this.codeJavaScript = function ( ) {

			var ret="var w=(new lvzwebdesktop.WindowObject())";

			if ( typeof this.props.caption == 'string' )
				ret+="\n	.setTitle(\""+this.props.caption+'")';

			if ( typeof this.props.icon == 'string' )
				ret+="\n	.setIcon(\""+this.props.icon+'")';

			if ( typeof this.props.top == 'number' )
				ret+="\n	.setTop("+this.props.top+')';

			if ( typeof this.props.left == 'number' )
				ret+="\n	.setLeft("+this.props.left+')';

			if ( typeof this.props.bottom == 'number' )
				ret+="\n	.setBottom("+this.props.bottom+')';

			if ( typeof this.props.right == 'number' )
				ret+="\n	.setRight("+this.props.right+')';

			if ( typeof this.props.height == 'number' )
				ret+="\n	.setHeight("+this.props.height+')';

			if ( typeof this.props.width == 'number' )
				ret+="\n	.setWidth("+this.props.width+')';

			// this.setThemeClass(theme)
			return ret;
		};

		/**
		 * Method for generating Python code of window using the gtk3 library.
		 * @return {String} The python code for creating the window.
		 */
		this.codePython3GTK3 = function ( ) {
			// https://athenajc.gitbooks.io/python-gtk-3-api/content/gtk-group/gtkwindow.html
			// https://python-gtk-3-tutorial.readthedocs.io/en/latest/introduction.html
			// https://stackoverflow.com/questions/20907897/proper-way-of-building-gtk3-applications-in-python
			// https://blog.digitaloctave.com/posts/python/gtk3/16-gtk3-custom-signals-example.html
			var ret = '';

			ret+='class Window(Gtk.Window):';
			ret+="\n	def __init__(self):";
			ret+="\n		Gtk.Window.__init__(self)";
			if ( typeof this.props.caption == 'string' )
				ret+="\n		self.set_title(\""+this.props.caption+"\")";
			if ( typeof this.props.height == 'number' && typeof this.props.width == 'number' )
			    ret+="\n		self.set_default_size("+this.props.width+","+this.props.height+")"

			ret+="\n		self.connect(\"destroy\", Gtk.main_quit)";

			ret+="\nwindow = Window()"
			ret+="\nwindow.show()"
			ret+="\nGtk.main()"
			return ret;
		};

		/**
		 * Method for generating Python code of window using TKinger library.
		 * @return {String} The python code for creating the window.
		 */
		this.codePythonTKINTER = function ( ) {
			// https://likegeeks.com/python-gui-examples-tkinter-tutorial/
			// https://www.geeksforgeeks.org/python-gui-tkinter/
			// https://www.javatpoint.com/python-tkinter
			var ret = '';

			ret+='window = Tk()';
			if ( typeof this.props.caption == 'string' )
				ret+="\nwindow.title(\""+this.props.caption+"\")";

			if ( typeof this.props.height == 'number' && typeof this.props.width == 'number' )
				ret+="\nwindow.geometry("+this.props.width+'x'+this.props.height+")"

			ret+="\nwindow.mainloop()";

			return ret;
		};

		this.codeJavaScript2Python = function () {
			// https://www.codeconvert.ai/javascript-to-python-converter
		};

		/**
		 * Method to build the window object to be edited.
		 * @return {WindoweditorObject} This object.
		 */
		this.buildWindow = function () {
			this.props.window = new lvzwebdesktop.WindowObject(false,false);
			var vobj;
			var veobj;
			addvisualcontroltoeditorlistbox(this.props.window,this.visualEditor);
			for ( var i=0;i<this.items.length;i++) {
				this.props.window.elements.bodycontainer.appendChild(this.items[i]);
				vobj = lvzwebdesktop(this.items[i]);
				if ( vobj !== false ) {
					veobj = new VisualcontrolObject(vobj);
					addvisualcontroltoeditorlistbox(vobj,this.visualEditor);
				}
			}

			this.elements.midcentercell.appendChild(this.props.window.elements.container);
			this.props.window.props.resizable = false;
			this.props.window.props.maximizable = false;
			this.props.window.props.minimizable = false;
			this.props.window.props.movable = false;
			this.props.window
				.setWidth(this.props.width)
				.setHeight(this.props.height)
				.buildTheme()
				.decorate()
				.setTitle(this.props.caption)
				.setLayout(this.props.buttonLayout)
				.setTop(10)
				.setLeft(10)
				.addEvent('menu',function(e){e.preventDefault();})
				.addEvent('close',function(e){e.preventDefault();})
			;
			this.props.window.windoweditor = this;
			this.props.window.elements.bodycontainer.addEventListener('click',windoweditorclickwindow);
			return this;
		};

		/**
		 * Property to set the edited window's id.
		 * @public
		 * @type {false|String}
		 */
		this.props.id = false;

		/**
		 * Property to store the window object to be edited.
		 * @public
		 * @type {null|WindowObject}
		 */
		this.props.window = null;

		/**
		 * Property to set the edited window's title.
		 * @public
		 * @type {false|String}
		 */
		this.props.caption = 'Form1';

		/**
		 * Property to set the edited window's icon.
		 * @public
		 * @type {false|String}
		 */
		this.props.icon = false;

		/**
		 * Property to set the edited window's top position.
		 * @public
		 * @type {false|Number}
		 */
		this.props.top = false;

		/**
		 * Property to set the edited window's minimum top position.
		 * @public
		 * @type {false|Number}
		 */
		this.props.minTop = false;

		/**
		 * Property to set the edited window's maximum top position.
		 * @public
		 * @type {false|Number}
		 */
		this.props.maxTop = false;

		/**
		 * Property to set the edited window's bottom position.
		 * @public
		 * @type {false|Number}
		 */
		this.props.bottom = false;

		/**
		 * Property to set the edited window's minimum distance from the bottom of it's container.
		 * @public
		 * @type {false|Nubmer}
		 */
		this.props.minBottom = false;

		/**
		 * Property to set the edited window's maximum distance from the bottom of it's container.
		 * @public
		 * @type {false|Number}
		 */
		this.props.maxBottom = false;

		/**
		 * Property to set the edited window's distance from the left of it's container.
		 * @public
		 * @type {false|Number}
		 */
		this.props.left = false;

		/**
		 * Property to set the edited window's minimum distance from the left of it's container.
		 * @public
		 * @type {false|Number}
		 */
		this.props.minLeft = false;

		/**
		 * Property to set the edited window's maximum distance from the left of it's container.
		 * @public
		 * @type {false|Number}
		 */	
		this.props.maxLeft = false;

		/**
		 * Property to set the edited window's distance from the right of it's container.
		 * @public
		 * @type {false|Number}
		 */
		this.props.right = false;

		/**
		 * Property to set the edited window's minimum distance from the right of it's container.
		 * @public
		 * @type {false|Number}
		 */
		this.props.minRight = false;

		/**
		 * Property to set the edited window's maximum distance from the right of it's container.
		 * @public
		 * @type {false|Number}
		 */
		this.props.maxRight = false;

		/**
		 * Property to set the edited window's height.
		 * @public
		 * @type {false|Number}
		 */
		this.props.height = 200;

		/**
		 * Property to set the minimum height of the edited window.
		 * @public
		 * @type {false|Number}
		 */
		this.props.minHeight = false;

		/**
		 * Property to set the maximum height of the edited window.
		 * @public
		 * @type {false|Number}
		 */
		this.props.maxHeight = false;

		/**
		 * Property to set the edited window's width.
		 * @public
		 * @type {false|Number}
		 */
		this.props.width = 300;

		/**
		 * Property to set the edited window's minimum width.
		 * @public
		 * @type {false|Number}
		 */
		this.props.minWidth = false;

		/**
		 * Property to set the edited window's maximum width.
		 * @public
		 * @type {false|Number}
		 */
		this.props.maxWidth = false;

		/**
		 * Property to set if the edited window will be visible in taskbar.
		 * @public
		 * @type {boolean}
		 */
		this.props.showintaskbar = true;

		/**
		 * Property to set if the edited window will be resizable.
		 * @public
		 * @type {boolean}
		 */
		this.props.resizable = true;

		/**
		 * Property to disable resize for the edited window.
		 * @public
		 * @type {boolean}
		 */
		this.props.disableresize = false;

		/**
		 * Property to set if edited window will be maximizable.
		 * @public
		 * @type {boolean}
		 */
		this.props.maximizable = true;

		/**
		 * Property to set if edited window will be minimizable.
		 * @public
		 * @type {boolean}
		 */
		this.props.minimizable = true;

		/**
		 * Property to set if the edited window will be always on top of other windows.
		 * @public
		 * @type {boolean}
		 */
		this.props.alwaysontop = false;

		/**
		 * Property to set the button layout of the edited window.
		 * @public
		 * @type {false|String}
		 */
		this.props.buttonLayout = 'menu:minimize,maximizeRestore,close';

		/**
		 * CSS classname of the container element.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.container = 'wd-window-editor-container';

		/**
		 * CSS classname of the top row container element.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.toprowcontainer = 'wd-top-row-container';

		/**
		 * CSS classname of the middle row container element.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.midrowcontainer = 'wd-mid-row-container';

		/**
		 * CSS classname of the bottom row container element.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.bottomrowcontainer = 'wd-bottom-row-container';

		/**
		 * CSS classname of the resize boxes.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.resizebox = 'wd-resizebox';

		/**
		 * CSS classname of the cell elements.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.editorcell = 'wd-cell';

		/**
		 * CSS classname of the top left resize box.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.topleftresizebox = 'wd-resize-topleft';

		/**
		 * CSS classname of the top center resize box.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.topcenterresizebox = 'wd-resize-topcenter';

		/**
		 * CSS classname of the top right resize box.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.toprightresizebox = 'wd-resize-topright';

		/**
		 * CSS classname of the middle left resize box.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.middleleftresizebox = 'wd-resize-midleft';

		/**
		 * CSS classname of the middle right resize box.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.middlerightresizebox = 'wd-resize-midright';

		/**
		 * CSS classname of the bottom left resize box.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.bottomleftresizebox = 'wd-resize-bottomleft';

		/**
		 * CSS classname of the bottom center resize box.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.bottomcenterresizebox = 'wd-resize-bottomcenter';

		/**
		 * CSS classname of the bottom right resize box.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.bottomrightresizebox = 'wd-resize-bottomright';

		this.items=[];
		if ( e ) for ( var i = 0; i < e.childNodes.length; i++ ) this.items.push(e.childNodes[i]);

		/**
		 * Container element of the window editor.
		 * @public
		 * @type {HTMLDivElement}
		 */
		this.elements.container = lvzwebdesktop.createBasicElement.call(this,'div',e,[
			this.props.classNames.container
		]);

		/**
		 * Top row container.
		 * @public
		 * @type {HTMLDivElement}
		 */
		this.elements.toprowcontainer = lvzwebdesktop.createBasicElement.call(this,'div',undefined,[
			this.props.classNames.toprowcontainer
		]);
		this.elements.container.appendChild(this.elements.toprowcontainer);

		/**
		 * Top left cell element.
		 * @public
		 * @type {HTMLDivElement}
		 */
		this.elements.topleftcell = lvzwebdesktop.createBasicElement.call(this,'div',undefined,[
			this.props.classNames.editorcell
		]);
		this.elements.toprowcontainer.appendChild(this.elements.topleftcell);

		/**
		 * Top center cell element.
		 * @public
		 * @type {HTMLDivElement}
		 */
		this.elements.topcentercell = lvzwebdesktop.createBasicElement.call(this,'div',undefined,[
			this.props.classNames.editorcell
		]);
		this.elements.topcentercell.style.textAlign = 'center';
		this.elements.toprowcontainer.appendChild(this.elements.topcentercell);

		/**
		 * Top right cell element.
		 * @public
		 * @type {HTMLDivElement}
		 */
		this.elements.toprightcell = lvzwebdesktop.createBasicElement.call(this,'div',undefined,[
			this.props.classNames.editorcell
		]);
		this.elements.toprowcontainer.appendChild(this.elements.toprightcell);

		/**
		 * Middle row container.
		 * @public
		 * @type {HTMLDivElement}
		 */
		this.elements.midrowcontainer = lvzwebdesktop.createBasicElement.call(this,'div',undefined,[
			this.props.classNames.midrowcontainer
		]);
		this.elements.container.appendChild(this.elements.midrowcontainer);

		/**
		 * Middle left cell element.
		 * @public
		 * @type {HTMLDivElement}
		 */
		this.elements.midleftcell = lvzwebdesktop.createBasicElement.call(this,'div',undefined,[
			this.props.classNames.editorcell
		]);
		this.elements.midrowcontainer.appendChild(this.elements.midleftcell);

		/**
		 * Middle center cell element.
		 * @public
		 * @type {HTMLDivElement}
		 */
		this.elements.midcentercell = lvzwebdesktop.createBasicElement.call(this,'div',undefined,[
			this.props.classNames.editorcell
		]);
		this.elements.midrowcontainer.appendChild(this.elements.midcentercell);

		if ( lvzwebdesktop.constructorsReady === true ) this.buildWindow();

		/**
		 * Middle right cell element.
		 * @public
		 * @type {HTMLDivElement}
		 */
		this.elements.midrightcell = lvzwebdesktop.createBasicElement.call(this,'div',undefined,[
			this.props.classNames.editorcell
		]);
		this.elements.midrowcontainer.appendChild(this.elements.midrightcell);

		/**
		 * Bottom row container.
		 * @public
		 * @type {HTMLDivElement}
		 */
		this.elements.bottomrowcontainer = lvzwebdesktop.createBasicElement.call(this,'div',undefined,[
			this.props.classNames.bottomrowcontainer
		]);
		this.elements.container.appendChild(this.elements.bottomrowcontainer);

		/**
		 * Bottom left cell element.
		 * @public
		 * @type {HTMLDivElement}
		 */
		this.elements.bottomleftcell = lvzwebdesktop.createBasicElement.call(this,'div',undefined,[
			this.props.classNames.editorcell
		]);
		this.elements.bottomrowcontainer.appendChild(this.elements.bottomleftcell);

		/**
		 * Bottom center cell element.
		 * @public
		 * @type {HTMLDivElement}
		 */
		this.elements.bottomcentercell = lvzwebdesktop.createBasicElement.call(this,'div',undefined,[
			this.props.classNames.editorcell
		]);
		this.elements.bottomcentercell.style.textAlign = 'center';
		this.elements.bottomrowcontainer.appendChild(this.elements.bottomcentercell);

		/**
		 * Bottom right cell element.
		 * @public
		 * @type {HTMLDivElement}
		 */
		this.elements.bottomrightcell = lvzwebdesktop.createBasicElement.call(this,'div',undefined,[
			this.props.classNames.editorcell
		]);
		this.elements.bottomrowcontainer.appendChild(this.elements.bottomrightcell);

		/**
		 * Top left resize box element.
		 * @public
		 * @type {HTMLDivElement}
		 */
		this.elements.topleftresizebox = lvzwebdesktop.createBasicElement.call(this,'div',undefined,[
			this.props.classNames.resizebox,
			this.props.classNames.topleftresizebox
		]);
		this.elements.topleftcell.appendChild(this.elements.topleftresizebox);

		/**
		 * Top center resize box element.
		 * @public
		 * @type {HTMLDivElement}
		 */
		this.elements.topcenterresizebox = lvzwebdesktop.createBasicElement.call(this,'div',undefined,[
			this.props.classNames.resizebox,
			this.props.classNames.topcenterresizebox
		]);
		this.elements.topcentercell.appendChild(this.elements.topcenterresizebox);

		/**
		 * Top right resize box element.
		 * @public
		 * @type {HTMLDivElement}
		 */
		this.elements.toprightresizebox = lvzwebdesktop.createBasicElement.call(this,'div',undefined,[
			this.props.classNames.resizebox,
			this.props.classNames.toprightresizebox
		]);
		this.elements.toprightcell.appendChild(this.elements.toprightresizebox);

		/**
		 * Middle left resize box element.
		 * @public
		 * @type {HTMLDivElement}
		 */
		this.elements.middleleftresizebox = lvzwebdesktop.createBasicElement.call(this,'div',undefined,[
			this.props.classNames.resizebox,
			this.props.classNames.middleleftresizebox
		]);
		this.elements.midleftcell.appendChild(this.elements.middleleftresizebox);

		/**
		 * Middle right resize box element.
		 * @public
		 * @type {HTMLDivElement}
		 */
		this.elements.middlerightresizebox = lvzwebdesktop.createBasicElement.call(this,'div',undefined,[
			this.props.classNames.resizebox,
			this.props.classNames.middlerightresizebox
		]);
		this.elements.middlerightresizebox.dataDirection = 'right';
		this.elements.middlerightresizebox.addEventListener('mousedown', windoweditorresizeboxmdown);
		this.elements.midrightcell.appendChild(this.elements.middlerightresizebox);

		/**
		 * Bottom left resize box element.
		 * @public
		 * @type {HTMLDivElement}
		 */
		this.elements.bottomleftresizebox = lvzwebdesktop.createBasicElement.call(this,'div',undefined,[
			this.props.classNames.resizebox,
			this.props.classNames.bottomleftresizebox
		]);
		this.elements.bottomleftcell.appendChild(this.elements.bottomleftresizebox);

		/**
		 * Bottom center resize box element.
		 * @public
		 * @type {HTMLDivElement}
		 */
		this.elements.bottomcenterresizebox = lvzwebdesktop.createBasicElement.call(this,'div',undefined,[
			this.props.classNames.resizebox,
			this.props.classNames.bottomcenterresizebox
		]);
		this.elements.bottomcenterresizebox.dataDirection = 'down';
		this.elements.bottomcenterresizebox.addEventListener('mousedown', windoweditorresizeboxmdown);
		this.elements.bottomcentercell.appendChild(this.elements.bottomcenterresizebox);

		/**
		 * Bottom right resize box element.
		 * @public
		 * @type {HTMLDivElement}
		 */
		this.elements.bottomrightresizebox = lvzwebdesktop.createBasicElement.call(this,'div',undefined,[
			this.props.classNames.resizebox,
			this.props.classNames.bottomrightresizebox
		]);
		this.elements.bottomrightresizebox.dataDirection = 'down right';
		this.elements.bottomrightresizebox.addEventListener('mousedown', windoweditorresizeboxmdown);
		this.elements.bottomrightcell.appendChild(this.elements.bottomrightresizebox);

		// this.buildWindow();
	};

	/**
	 * List of all WindoweditorObject instances.
	 * @public
	 * @type {Array}
	 */
	WindoweditorObject.list = [];

	/**
	 * Function to build properties table for the given control.
	 * @param {Object} o - The control object to build properties table.
	 */
	function buildPropertiesTable ( o ) {
		var weobj;
		var pobj;
		var j,k;
		var lst;
		if ( typeof o.control == 'object' ) o = o.control;
		weobj = o.visualcontrol.getParentOfType('WindoweditorObject');
		if ( weobj !== false && weobj !== null ) {
			pobj =  weobj.visualEditor.properties;

			if ( visualcontrolFocus.p !== pobj ) {
				// Clear Properties table.
				for ( var i = 0 ; i < pobj.rows.length; i ++ ) {
					pobj.getCell(i,0).setCaption('');
					//#pobj.getCell(i,1).setCaption('');
					pobj.rows[i].style.display = 'none';
				}
				if ( typeof o.constructor.editProperties == 'object'  ) {

					visualcontrolFocus.p = pobj;
					// Set properties to the properties table.
					j=0;
					var cell;
					for ( var i in o.constructor.editProperties ) {
						pobj.getCell(j,0).setCaption(i);
						pobj.rows[j].style.display = '';
						cell = pobj.getCell(j,1);
						cell.control = o.control;
						cell.vcontrol = o;
						cell.controlprops = o.constructor.editProperties[i];
						if ( typeof o.constructor.editProperties[i].getValue == 'function' ) {
							if ( o.constructor.editProperties[i].type == 'boolean' ) {
								cell.setList([
									{'label':'false','value':false},
									{'label':'true','value':true}
								], o.constructor.editProperties[i].getValue(o));
							}
							else if ( o.constructor.editProperties[i].type == 'number' ) {
								cell.setNumber(o.constructor.editProperties[i].getValue(o));
							}
							else if ( o.constructor.editProperties[i].type == 'string' ) {
								cell.setString(o.constructor.editProperties[i].getValue(o));
							}
							else if ( o.constructor.editProperties[i].type == 'string|false' ) {
								cell.setStringFalse(o.constructor.editProperties[i].getValue(o));
							}
							else if ( o.constructor.editProperties[i].type == 'number|false' ) {
								cell.setNumberFalse(o.constructor.editProperties[i].getValue(o));
							}
							else if ( o.constructor.editProperties[i].type == 'stringlist' ) {
								lst = o.constructor.editProperties[i].list.split(/[|]/);
								for ( k = 0;k<lst.length;k++)lst[k]={"label":lst[k],
																	 "value":lst[k]};
								cell.setList( lst,o.constructor.editProperties[i].getValue(o));
							}
							else {
								console.warn ( i ) ;
								console.log  ( o.constructor.editProperties[i].type );
								console.log ( o.constructor.editProperties[i] );
								cell.setCaption(o.constructor.editProperties[i].getValue(o));
							}
						}
						j++;
					}
				}
			}
		}
	}

	/**
	 * Function to build properties table for the given window.
	 * @param {WindowObject} w - The WindowObject to build properties table for.
	 */
	function buildPropertiesTableW ( w ) {
		var weobj = w.getParentOfType('WindoweditorObject');
		var j = 0;
		var pobj = weobj.visualEditor.properties;
		var cell;
		var lst;
		var k;
		pobj.control = {"control" : w };

		// Clear properties
		for ( var i = 0 ; i < pobj.rows.length; i ++ ) {
			pobj.getCell(i,0).setCaption('');
			// pobj.getCell(i,1).setCaption('');
			pobj.rows[i].style.display = 'none';
		}

		for ( var i in w.constructor.editProperties ) {
			pobj.getCell(j,0).setCaption(i);
			pobj.rows[j].style.display = '';
			if ( typeof w.constructor.editProperties[i].getValue == 'function' ) {
				cell = pobj.getCell(j,1);
				cell.vcontrol = w;
				if ( w.constructor.editProperties[i].type == 'boolean' ) {
					cell.setList([
						{'label':'false','value':false},
						{'label':'true','value':true}
					], w.constructor.editProperties[i].getValue(w));
				}
				else if ( w.constructor.editProperties[i].type == 'number' ) {
					cell.setNumber(w.constructor.editProperties[i].getValue(w));
				}
				else if ( w.constructor.editProperties[i].type == 'string' ) {
					cell.setString(w.constructor.editProperties[i].getValue(w));
				}
				else if ( w.constructor.editProperties[i].type == 'string|false' ) {
					cell.setStringFalse(w.constructor.editProperties[i].getValue(w));
				}
				else if ( w.constructor.editProperties[i].type == 'number|false' ) {
					cell.setNumberFalse(w.constructor.editProperties[i].getValue(w));
				}
				else if ( w.constructor.editProperties[i].type == 'stringlist' ) {
					lst = w.constructor.editProperties[i].list.split(/[|]/);
					for ( k = 0;k<lst.length;k++)lst[k]={"label":lst[k],
														 "value":lst[k]};
					cell.setList( lst,w.constructor.editProperties[i].getValue(w));
				}
				else {
					console.log ( "=========================================" );
					console.warn ( i );
					console.log ( w.constructor.editProperties[i].type ) ;
					console.log  ( w.constructor.editProperties[i] );
					console.log ( "=========================================" );
					cell.setCaption(w.constructor.editProperties[i].getValue(w));
				}
			}
			cell.controlprops = w.constructor.editProperties[i];
			j++;
		}

		weobj.visualEditor.selectControl.selectControl(w); 
	}

	/**
	 * Functions to control click events on edited window.
	 * @param {Event} e - Event object.
	 */
	function windoweditorclickwindow (e) {
		var self = lvzwebdesktop(e.target);
		if ( visualcontrolFocus.s !== null ){
			visualcontrolFocus.s.elements.container.classList.remove(visualcontrolFocus.s.props.classNames.selected);
			visualcontrolFocus.s.endResize();
			visualcontrolFocus.s = null;
			visualcontrolFocus.p = null;
		}
		buildPropertiesTableW(self);
	}

	/**
	 * Function to control mousedown event on resize boxes for the window editor.
	 * @param {Event} e - The event object.
	 */
	function windoweditorresizeboxmdown ( e ) {
		var self = lvzwebdesktop(e.target);

		if ( typeof e.target.dataDirection == 'undefined' ) return;

		windoweditorresizeboxmdown.self = self;
		windoweditorresizeboxmdown.direction = e.target.dataDirection;
		windoweditorresizeboxmdown.mx = e.clientX;
		windoweditorresizeboxmdown.my = e.clientY;
		windoweditorresizeboxmdown.ww = parseFloat(self.props.width);
		windoweditorresizeboxmdown.wh = parseFloat(self.props.height);

		document.addEventListener('mousemove', windoweditorresizeboxmmove );
		document.addEventListener('mouseup', windoweditorresizeboxmup );
	}

	windoweditorresizeboxmdown.direction = '';
	windoweditorresizeboxmdown.self = null;
	windoweditorresizeboxmdown.mx = false;
	windoweditorresizeboxmdown.ww = false;
	windoweditorresizeboxmdown.my = false;
	windoweditorresizeboxmdown.wh = false;

	/**
	 * Function to control mousemove event after mousedown on a resize box of window editor.
	 * @param {Event} e - The event object.
	 */
	function windoweditorresizeboxmmove ( e ) {
		e.preventDefault();
		var self = windoweditorresizeboxmdown.self;

		if ( windoweditorresizeboxmdown.direction == 'right' ) {
			var tmpwidth = windoweditorresizeboxmdown.ww + ( e.clientX - windoweditorresizeboxmdown.mx  );
			self.props.width = tmpwidth;
			self.props.window.props.width = tmpwidth;
			self.props.window.resize();
		}
		else if ( windoweditorresizeboxmdown.direction == 'down' ) {
			var tmpheight = windoweditorresizeboxmdown.wh + ( e.clientY - windoweditorresizeboxmdown.my  );
			self.props.height = tmpheight;
			self.props.window.props.height = tmpheight;
			self.props.window.resize();
		}
		else if ( windoweditorresizeboxmdown.direction == 'down right' ) {
			var tmpwidth = windoweditorresizeboxmdown.ww + ( e.clientX - windoweditorresizeboxmdown.mx  );
			var tmpheight = windoweditorresizeboxmdown.wh + ( e.clientY - windoweditorresizeboxmdown.my  );
			self.props.width = tmpwidth;
			self.props.height = tmpheight;
			self.props.window.props.width = tmpwidth;
			self.props.window.props.height = tmpheight;
			self.props.window.resize();
		}
	}

	/**
	 * Function to control mouseup event after mousdown on a resize box of window editor.
	 * @param {Event} e - The event object.
	 */
	function windoweditorresizeboxmup ( e ) {
		e.preventDefault();
		document.removeEventListener('mousemove', windoweditorresizeboxmmove );
		document.removeEventListener('mouseup', windoweditorresizeboxmup );
	}

	/**
	 * Constructor for creating control editor objects.
	 * @public
	 * @param {Object} wdobject - A web-desktop object for creating .
	 */
	var VisualcontrolObject = function ( wdobject ) {

		lvzwebdesktop.basicObject.call(this,'visualcontrol');

		this.codeHTML = function () {
			return this.control.elements.container.outerHTML;
			// return this.elements.itemcontainer.outerHTML;
		};

		this.codePyhton3GTK3 = function () {
			var ret = '';

			if ( this.control instanceof lvzwebdesktop.LabelObject ) {
				ret += 'label = Gtk.Label(label="'+JSON.stringify(this.control.elements.container.innerText)+'")';
			}
			// label = Gtk.Label(label="This is a normal label")
			// label.set_markup("Go to <a href=\"https://www.gtk.org\" "
            //      "title=\"Our website\">GTK+ website</a> for more")
			// label = Gtk.Label()
			// label.set_text("This is a left-justified label.\nWith multiple lines.")
			// label.set_justify(Gtk.Justification.LEFT)

			return ret;
		};

		this.beginResize = function () {
			this.elements.container.parentNode.appendChild(this.elements.resizeboxTopLeft);
			this.elements.container.parentNode.appendChild(this.elements.resizeboxTopCenter);
			this.elements.container.parentNode.appendChild(this.elements.resizeboxTopRight);
			this.elements.container.parentNode.appendChild(this.elements.resizeboxMiddleLeft);
			this.elements.container.parentNode.appendChild(this.elements.resizeboxMiddleRight);
			this.elements.container.parentNode.appendChild(this.elements.resizeboxBottomLeft);
			this.elements.container.parentNode.appendChild(this.elements.resizeboxBottomCenter);
			this.elements.container.parentNode.appendChild(this.elements.resizeboxBottomRight);

			this.elements.resizeboxTopLeft.style.display = 'block';
			this.elements.resizeboxTopCenter.style.display = 'block';
			this.elements.resizeboxTopRight.style.display = 'block';
			this.elements.resizeboxMiddleLeft.style.display = 'block';
			this.elements.resizeboxMiddleRight.style.display = 'block';
			this.elements.resizeboxBottomLeft.style.display = 'block';
			this.elements.resizeboxBottomCenter.style.display = 'block';
			this.elements.resizeboxBottomRight.style.display = 'block';

			this.placeResizeBoxes();
			return this;
		};

		this.endResize = function () {
			this.elements.resizeboxTopLeft.style.display = 'none';
			this.elements.resizeboxTopCenter.style.display = 'none';
			this.elements.resizeboxTopRight.style.display = 'none';
			this.elements.resizeboxMiddleLeft.style.display = 'none';
			this.elements.resizeboxMiddleRight.style.display = 'none';
			this.elements.resizeboxBottomLeft.style.display = 'none';
			this.elements.resizeboxBottomCenter.style.display = 'none';
			this.elements.resizeboxBottomRight.style.display = 'none';
			this.elements.resizeboxTopLeft.remove();
			this.elements.resizeboxTopCenter.remove();
			this.elements.resizeboxTopRight.remove();
			this.elements.resizeboxMiddleLeft.remove();
			this.elements.resizeboxMiddleRight.remove();
			this.elements.resizeboxBottomLeft.remove();
			this.elements.resizeboxBottomCenter.remove();
			this.elements.resizeboxBottomRight.remove();
			return this;
		};

		this.placeResizeBoxes = function () {
			var t = this.elements.container.offsetTop;
			var l = this.elements.container.offsetLeft;
			var h = this.elements.container.offsetHeight;
			var w = this.elements.container.offsetWidth;

			this.elements.resizeboxTopLeft.style.top = (t-this.elements.resizeboxTopLeft.offsetHeight) + 'px';
			this.elements.resizeboxTopLeft.style.left = (l-this.elements.resizeboxTopLeft.offsetWidth) + 'px';

			this.elements.resizeboxTopCenter.style.top = (t-this.elements.resizeboxTopLeft.offsetHeight) + 'px';
			this.elements.resizeboxTopCenter.style.left = (l + w/2 - this.elements.resizeboxTopCenter.offsetWidth / 2) + 'px';

			this.elements.resizeboxTopRight.style.top = (t-this.elements.resizeboxTopLeft.offsetHeight) + 'px';
			this.elements.resizeboxTopRight.style.left = (l+w) + 'px';

			this.elements.resizeboxMiddleLeft.style.top = (t + h/2 -this.elements.resizeboxTopLeft.offsetHeight/2) + 'px';
			this.elements.resizeboxMiddleLeft.style.left = (l-this.elements.resizeboxTopCenter.offsetWidth) + 'px';

			this.elements.resizeboxMiddleRight.style.top = (t + h/2 -this.elements.resizeboxTopLeft.offsetHeight/2) + 'px';
			this.elements.resizeboxMiddleRight.style.left = (l+w) + 'px';

			this.elements.resizeboxBottomLeft.style.top = (t+h) + 'px';
			this.elements.resizeboxBottomLeft.style.left = (l - this.elements.resizeboxBottomLeft.offsetWidth) + 'px';

			this.elements.resizeboxBottomCenter.style.top = (t+h) + 'px';
			this.elements.resizeboxBottomCenter.style.left = (l+w/2-this.elements.resizeboxBottomCenter.offsetWidth/2) + 'px';

			this.elements.resizeboxBottomRight.style.top = (t+h) + 'px';
			this.elements.resizeboxBottomRight.style.left = (l+w) + 'px';
		};

		this.remove = function () {
			this.elements.container.remove();
			return this;
		};

		wdobject.fireEvent('visualeditorstart',{
			"target" : wdobject,
			"editorObject" : this
		});

		/**
		 * CSS class name of the container element of the visual control.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.container = 'wd-visual-control';

		/**
		 * CSS class name of the vail element of the visual control.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.vail = 'wd-vc-vail';

		/**
		 * CSS classname of the resize boxes.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.resizebox = 'wd-resizebox';

		/**
		 * CSS class name of the container element of the selected visualcontrol object.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.selected = 'selected';

		/**
		 * The lvzwebdesktop object that will be edited.
		 * @public
		 * @type {Object}
		 */
		this.control = wdobject;
		this.control.visualcontrol = this;

		/**
		 * Container element of the visual control object.
		 * @public
		 * @type {HTMLDivElement}
		 */
		this.elements.container = lvzwebdesktop.createBasicElement.call(this,'div',undefined,[
			this.props.classNames.container
		]);
		this.elements.container.tabIndex = 1;
		this.elements.container.title = this.control.type;
		this.elements.container.style.position = 'absolute';
		this.elements.container.addEventListener('focus',visualcontrolFocus);
		this.elements.container.addEventListener('mousedown',visualcontrolMD);

		this.elements.container.addEventListener('click',function(e){
			e.preventDefault();
			e.stopPropagation();
			return false;
		});

		/**
		 * Vail element of the visual control object.
		 * @public
		 * @type {HTMLDivElement}
		 */
		this.elements.vail = lvzwebdesktop.createBasicElement.call(this,'div',undefined,[
			this.props.classNames.vail
		]);
		this.elements.vail.style.position = 'absolute';
		// this.elements.vail.style.backgroundColor = 'rgba(0,0,0,0.1)';

		if (typeof wdobject.elements.container == 'object' ) {

			wdobject.elements.container.parentNode.insertBefore(this.elements.container, wdobject.elements.container);
			//this.elements.container.appendChild(wdobject.elements.container);

			/**
			 * A copy of the container element of the control to be edited.
			 * @public
			 * @type {HTMLElement}
			 */
			this.elements.itemcontainer = wdobject.elements.container.cloneNode(true);
			this.elements.itemcontainer.dataLVZWDESKTOPOBJ = this;
			this.elements.itemcontainer.dataVisualcontrolObject = this;

			// wdobject.elements.container.replaceWith(this.elements.itemcontainer);

			wdobject.elements.container.remove();
			this.elements.container.appendChild(this.elements.itemcontainer);

			this.elements.vail.style.width = this.elements.itemcontainer.offsetWidth + 'px';
			this.elements.vail.style.height = this.elements.itemcontainer.offsetHeight + 'px';
			this.elements.vail.style.top = '0px';

		}
		this.elements.vail.style.width = '100%';
		this.elements.vail.style.height = '100%';
		this.elements.container.appendChild(this.elements.vail);

		/**
		 * Top left resize box element.
		 * @public
		 * @type {HTMLDivElement}
		 */
 		this.elements.resizeboxTopLeft = lvzwebdesktop.createBasicElement.call(this,'div',undefined,[
			this.props.classNames.resizebox
		]);
		this.elements.resizeboxTopLeft.dataDirection = 'up left';
		this.elements.resizeboxTopLeft.style.display = 'none';
		this.elements.resizeboxTopLeft.style.position = 'absolute';
		this.elements.resizeboxTopLeft.style.cursor = 'nw-resize';
		this.elements.resizeboxTopLeft.tabIndex = 1;
		this.elements.resizeboxTopLeft.addEventListener('mousedown', visualcontrolRMD);

		/**
		 * Top center resize box element.
		 * @public
		 * @type {HTMLDivElement}
		 */
		this.elements.resizeboxTopCenter = lvzwebdesktop.createBasicElement.call(this,'div',undefined,[
			this.props.classNames.resizebox
		]);
		this.elements.resizeboxTopCenter.dataDirection = 'up';
		this.elements.resizeboxTopCenter.style.display = 'none';
		this.elements.resizeboxTopCenter.style.position = 'absolute';
		this.elements.resizeboxTopCenter.style.cursor = 'n-resize';
		this.elements.resizeboxTopCenter.tabIndex = 1;
		this.elements.resizeboxTopCenter.addEventListener('mousedown', visualcontrolRMD);

		/**
		 * Top right resize box element.
		 * @public
		 * @type {HTMLDivElement}
		 */
		this.elements.resizeboxTopRight = lvzwebdesktop.createBasicElement.call(this,'div',undefined,[
			this.props.classNames.resizebox
		]);
		this.elements.resizeboxTopRight.dataDirection = 'up right';
		this.elements.resizeboxTopRight.style.display = 'none';
		this.elements.resizeboxTopRight.style.position = 'absolute';
		this.elements.resizeboxTopRight.style.cursor = 'ne-resize';
		this.elements.resizeboxTopRight.tabIndex = 1;
		this.elements.resizeboxTopRight.addEventListener('mousedown', visualcontrolRMD);

		/**
		 * Middle left resize box element.
		 * @public
		 * @type {HTMLDivElement}
		 */
		this.elements.resizeboxMiddleLeft = lvzwebdesktop.createBasicElement.call(this,'div',undefined,[
			this.props.classNames.resizebox
		]);
		this.elements.resizeboxMiddleLeft.dataDirection = 'left';
		this.elements.resizeboxMiddleLeft.style.display = 'none';
		this.elements.resizeboxMiddleLeft.style.position = 'absolute';
		this.elements.resizeboxMiddleLeft.style.cursor = 'w-resize';
		this.elements.resizeboxMiddleLeft.tabIndex = 1;
		this.elements.resizeboxMiddleLeft.addEventListener('mousedown', visualcontrolRMD);

		/**
		 * Middle right resize box element.
		 * @public
		 * @type {HTMLDivElement}
		 */
		this.elements.resizeboxMiddleRight = lvzwebdesktop.createBasicElement.call(this,'div',undefined,[
			this.props.classNames.resizebox
		]);
		this.elements.resizeboxMiddleRight.dataDirection = 'right';
		this.elements.resizeboxMiddleRight.style.display = 'none';
		this.elements.resizeboxMiddleRight.style.position = 'absolute';
		this.elements.resizeboxMiddleRight.style.cursor = 'e-resize';
		this.elements.resizeboxMiddleRight.tabIndex = 1;
		this.elements.resizeboxMiddleRight.addEventListener('mousedown', visualcontrolRMD);

		/**
		 * Bottom left resize box element.
		 * @public
		 * @type {HTMLDivElement}
		 */
		this.elements.resizeboxBottomLeft = lvzwebdesktop.createBasicElement.call(this,'div',undefined,[
			this.props.classNames.resizebox
		]);
		this.elements.resizeboxBottomLeft.dataDirection = 'down left';
		this.elements.resizeboxBottomLeft.style.display = 'none';
		this.elements.resizeboxBottomLeft.style.position = 'absolute';
		this.elements.resizeboxBottomLeft.style.cursor = 'sw-resize';
		this.elements.resizeboxBottomLeft.tabIndex = 1;
		this.elements.resizeboxBottomLeft.addEventListener('mousedown', visualcontrolRMD);

		/**
		 * Bottom center resize box element.
		 * @public
		 * @type {HTMLDivElement}
		 */
		this.elements.resizeboxBottomCenter = lvzwebdesktop.createBasicElement.call(this,'div',undefined,[
			this.props.classNames.resizebox
		]);
		this.elements.resizeboxBottomCenter.dataDirection = 'down';
		this.elements.resizeboxBottomCenter.style.display = 'none';
		this.elements.resizeboxBottomCenter.style.position = 'absolute';
		this.elements.resizeboxBottomCenter.style.cursor = 's-resize';
		this.elements.resizeboxBottomCenter.tabIndex = 1;
		this.elements.resizeboxBottomCenter.addEventListener('mousedown', visualcontrolRMD);

		/**
		 * Bottom right resize box element.
		 * @public
		 * @type {HTMLDivElement}
		 */
		this.elements.resizeboxBottomRight = lvzwebdesktop.createBasicElement.call(this,'div',undefined,[
			this.props.classNames.resizebox
		]);
		this.elements.resizeboxBottomRight.dataDirection = 'down right';
		this.elements.resizeboxBottomRight.style.display = 'none';
		this.elements.resizeboxBottomRight.style.position = 'absolute';
		this.elements.resizeboxBottomRight.style.cursor = 'se-resize';
		this.elements.resizeboxBottomRight.tabIndex = 1;
		this.elements.resizeboxBottomRight.addEventListener('mousedown', visualcontrolRMD);
	};

	/**
	 * List with all VisualcontrolObject instances.
	 * @public
	 * @type {Array}
	 */
	VisualcontrolObject.list = [];

	/**
	 * Event function to control a control element when it got focus.
	 * @param {Event} e - Event data object.
	 */
	function visualcontrolFocus ( e ) {
		var self = lvzwebdesktop(e.target);
		var weobj = self.getParentOfType('WindoweditorObject');
		var pobj;
		var j = 0;

		if ( visualcontrolFocus.s !== null ){
			visualcontrolFocus.s.elements.container.classList.remove(visualcontrolFocus.s.props.classNames.selected);
			visualcontrolFocus.s.endResize();
			visualcontrolFocus.s = null;
			visualcontrolFocus.p = null;
		}
		self.elements.container.classList.add(self.props.classNames.selected);

		weobj.visualEditor.selectControl.selectControl(self.control);

		if ( weobj !== false && weobj !== null ) {
			buildPropertiesTable (self.control) ;
		}
		visualcontrolFocus.s = self;
		if ( self.control.props.resizableOnEdit ) {
			self.beginResize();
		}
		document.body.addEventListener('keyup', visualcontrolKey );
	}

	visualcontrolFocus.s = null;
	visualcontrolFocus.p = null;

	/**
	 * Event function to control when clicking away from a control element.
	 * @param {Event} e - Event data object.
	 */
	function visualcontrolDocClick ( e ) {
		var self = visualcontrolFocus.s;
		if ( typeof e.target.dataLVZWDESKTOPOBJ == 'undefined'
			 || e.target.dataLVZWDESKTOPOBJ !== self
		   ) {
			document.body.removeEventListener('click', visualcontrolDocClick );
			document.body.removeEventListener('keyup', visualcontrolKey );
			if ( self.control.props.resizableOnEdit ) {
				self.endResize();
			}
			visualcontrolFocus.s = null;
		}
	}

	/**
	 * Event function to control key press.
	 * @param {Event} e - Event data object.
	 */
	function visualcontrolKey ( e ) {
		var self = visualcontrolFocus.s;

		if ( ! self ) return;

		// Delete key
		if ( e.keyCode == 46 ) {
			if ( self.control.props.resizableOnEdit ) {
				self.endResize();
			}
			self.remove();
		}
	}

	/**
	 * Event function to control mousedown on resize boxes of a control element.
	 * @param {Event} e - Event data object.
	 */
	function visualcontrolRMD ( e ) {
		// e.preventDefault();
		e.stopPropagation();
		var self = lvzwebdesktop(e.target);
		var weobj = self.getParentOfType('WindoweditorObject');
		var pobj;

		if ( weobj !== false ) {
			pobj =  weobj.visualEditor.properties;
			visualcontrolRMD.p = pobj;
		}
		else {
			visualcontrolRMD.p = null;
		}

		visualcontrolRMD.s = self;
		visualcontrolRMD.mx = e.clientX;
		visualcontrolRMD.my = e.clientY;
		visualcontrolRMD.w = self.elements.container.offsetWidth;
		visualcontrolRMD.h = self.elements.container.offsetHeight;
		visualcontrolRMD.t = self.elements.container.offsetTop;
		visualcontrolRMD.l = self.elements.container.offsetLeft;
		visualcontrolRMD.d = e.target.dataDirection;

		document.addEventListener('mousemove',visualcontrolRMM);
		document.addEventListener('mouseup',visualcontrolRMU);
	}

	visualcontrolRMD.s = null;
	visualcontrolRMD.mx = null;
	visualcontrolRMD.my = null;
	visualcontrolRMD.w = null;
	visualcontrolRMD.h = null;
	visualcontrolRMD.t = null
	visualcontrolRMD.l = null;
	visualcontrolRMD.d = null;

	/**
	 * Event function to control mousemove after mousedown on resize box.
	 * @param {Event} e - Event data object.
	 */
	function visualcontrolRMM ( e ) {
		var self = visualcontrolRMD.s;
		var mx;
		var my;
		var nw;
		var nh;
		var nt;
		var nl;
		var directions = visualcontrolRMD.d.split(/ /);

		if ( directions.indexOf('up') > -1 ) {
			my=visualcontrolRMD.my-e.clientY;
			nt=visualcontrolRMD.t-my;
			nh=visualcontrolRMD.h+my;
			if ( typeof self.control.props.maxHeight == 'number' && self.control.props.maxHeight > nh ) nh = self.control.props.maxHeight;
			if ( typeof self.control.props.maxTop == 'number' && self.control.props.maxTop > nt ) nt = self.control.props.maxTop;

			self.top = nt;
			self.height = nh;
			self.elements.container.style.top = nt+'px';
			self.elements.container.style.height = nh+'px';
			self.elements.vail.style.height = nh+'px';
			self.elements.itemcontainer.style.height = nh+'px';

			if ( typeof self.control.setTop == 'function' ) self.control.setTop(nt);
			if ( typeof self.control.setHeight == 'function' ) self.control.setHeight(nh);

			if ( visualcontrolRMD.p !== null ) {
				var w = visualcontrolRMD.p.getRowByCell('top',0);
				if ( w !== false ) visualcontrolRMD.p.getCell(w,1).setValue(nt);
				var w = visualcontrolRMD.p.getRowByCell('height',0);
				if ( w !== false ) visualcontrolRMD.p.getCell(w,1).setValue(nh);
			}
		}
		else if ( directions.indexOf('down') > -1 ) {
			my=e.clientY-visualcontrolRMD.my;
			nh=visualcontrolRMD.h+my;
			if ( typeof self.control.props.maxHeight == 'number' && self.control.props.maxHeight > nh ) nh = self.control.props.maxHeight;

			self.height = nh;
			self.elements.container.style.height = nh+'px';
			self.elements.vail.style.height = nh+'px';
			self.elements.itemcontainer.style.height = nh+'px';

			if ( typeof self.control.setHeight == 'function' ) self.control.setHeight(nh);

			if ( visualcontrolRMD.p !== null ) {
				var w = visualcontrolRMD.p.getRowByCell('height',0);
				if ( w !== false ) visualcontrolRMD.p.getCell(w,1).setValue(nh);
			}
		}

		if ( directions.indexOf('left') > -1 ) {
			mx=visualcontrolRMD.mx-e.clientX;
			nw=visualcontrolRMD.w+mx;
			nl=visualcontrolRMD.l-mx;
			if ( typeof self.control.props.maxWidth == 'number' && self.control.props.maxWidth > nw )
				nw = self.control.props.maxWidth;
			if ( typeof self.control.props.maxLeft == 'number' && self.control.props.maxLeft > nl )
				nl = self.control.props.maxLeft;

			self.left = nl;
			self.width = nw;
			self.elements.container.style.left = nl+'px';
			self.elements.container.style.width = nw+'px';
			self.elements.vail.style.width = nw+'px';
			self.elements.itemcontainer.style.width = nw+'px';

			if ( typeof self.control.setLeft == 'function' ) self.control.setLeft(nl);
			if ( typeof self.control.setWidth == 'function' ) self.control.setWidth(nw);

			if ( visualcontrolRMD.p !== null ) {
				var w = visualcontrolRMD.p.getRowByCell('left',0);
				if ( w !== false ) visualcontrolRMD.p.getCell(w,1).setValue(nl);
				var w = visualcontrolRMD.p.getRowByCell('width',0);
				if ( w !== false ) visualcontrolRMD.p.getCell(w,1).setValue(nw);
			}
		}
		else if ( directions.indexOf('right') > -1 ) {
			mx=e.clientX-visualcontrolRMD.mx;
			nw=visualcontrolRMD.w+mx;
			if ( typeof self.control.props.maxWidth == 'number' && self.control.props.maxWidth > nw )
				nw = self.control.props.maxWidth;
			self.width = nw;
			self.elements.container.style.width = nw+'px';
			self.elements.vail.style.width = nw+'px';
			self.elements.itemcontainer.style.width = nw+'px';

			if ( typeof self.control.setWidth == 'function' ) self.control.setWidth(nw);

			if ( visualcontrolRMD.p !== null ) {
				var w = visualcontrolRMD.p.getRowByCell('width',0);
				if ( w !== false ) visualcontrolRMD.p.getCell(w,1).setValue(nw);
			}
		}

		self.placeResizeBoxes();
	}

	/**
	 * Event function to control mouseup after mousedown on resize box.
	 * @param {Event} e - Event data object.
	 */
	function visualcontrolRMU ( e ) {
		e.stopPropagation();
		e.preventDefault();
		var self = visualcontrolRMD.s;
		visualcontrolRMD.p = null;
		document.removeEventListener('mousemove',visualcontrolRMM);
		document.removeEventListener('mouseup',visualcontrolRMU);
		return false;
	}

	/**
	 * Event function to control mousedown on control.
	 * @param {Event} e - Event data object.
	 */
	function visualcontrolMD ( e ) {
		// e.preventDefault();
		// e.stopPropagation();
		var self = lvzwebdesktop(e.target);
		if ( ! self || self === null ) return;
		var weobj = self.getParentOfType('WindoweditorObject');
		if ( ! weobj || weobj === null ) return;
		var pobj;

		visualcontrolMD.s = self;
		visualcontrolMD.mx = e.screenX;
		visualcontrolMD.my = e.screenY;
		visualcontrolMD.t = self.elements.container.offsetTop;
		visualcontrolMD.l = self.elements.container.offsetLeft;
		if ( weobj !== false ) {
			pobj =  weobj.visualEditor.properties;
			visualcontrolMD.p = pobj;
		}
		else {
			visualcontrolMD.p = null;
		}

		document.addEventListener('mousemove',visualcontrolMM);
		document.addEventListener('mouseup',visualcontrolMU);
	}

	visualcontrolMD.s = null;
	visualcontrolMD.p = null;
	visualcontrolMD.mx = null;
	visualcontrolMD.my = null;
	visualcontrolMD.t = null;
	visualcontrolMD.l = null;

	/**
	 * Event function to control mousemove after mousedown on control element.
	 * @param {Event} e - Event data object.
	 */
	function visualcontrolMM ( e ) {
		var self = visualcontrolMD.s;
		var nt= (e.screenY-visualcontrolMD.my) + visualcontrolMD.t;
		var nl= (e.screenX-visualcontrolMD.mx) + visualcontrolMD.l; 
		// self.elements.container.style.top = nt+'px';
		// self.elements.container.style.left = nl+'px';
		// self.top = nt;
		// self.left = nl;
		// if ( typeof self.control.setTop == 'function' ) self.control.setTop(nt);
		// if ( typeof self.control.setLeft == 'function' ) self.control.setLeft(nl);

		if ( visualcontrolMD.p !== null ) {
			var w;
			w = visualcontrolMD.p.getRowByCell('top',0);
			if ( w !== false ) visualcontrolMD.p.getCell(w,1).setValue(nt);
			w = visualcontrolMD.p.getRowByCell('left',0);
			if ( w !== false ) visualcontrolMD.p.getCell(w,1).setValue(nl);
		}

		self.placeResizeBoxes();
	}

	/**
	 * Event function to control mouseup after mousedonw on control element.
	 * @param {Event} e - Event data object.
	 */
	function visualcontrolMU ( e ) {
		var self = visualcontrolMD.s;
		visualcontrolRMD.p = null;
		document.removeEventListener('mousemove',visualcontrolMM);
		document.removeEventListener('mouseup',visualcontrolMU);
	}

	/**
	 * Event function t control keydown events on control properties table.
	 * @param {Event} e - Event data object.
	 */
	function visualcontrolKD ( e ) {
		var self = lvzwebdesktop(e.target);
		if ( typeof self.table.control != 'undefined' ) {

			if ( e.keyCode == 13
				 && typeof self.controlprops.setValue == 'function'
			   ) {
				e.preventDefault();
				e.stopPropagation();
				self.controlprops.setValue(self.table.control.control, self.getValue() );
			}

		}

	}

	function visualcontrolCH ( e ) {
		var self = lvzwebdesktop(e.target);
		if ( typeof self.controlprops.setValue == 'function' ) {
			self.controlprops.setValue(self.table.control.control, self.getValue() );
		}
	}

	function getParentClass ( e, c ) {
		if ( !e ) return false;
		o = e.parentNode;
		while ( o && o !== null && o.nodeName != '#document' ) {
			if ( o.classList.contains(c) ) return o;
			o=o.parentNode;
		}
		return false;
	}

	// Properly attach object to it's parent after loading the contructors.
	if ( lvzwebdesktop.constructorsReady === false ) {
		lvzwebdesktop.fn.addEvent.call(lvzwebdesktop,'constructorsready', function ( e ) {
			var self;
			var veobj;
			var pc;
			var control;

			for ( var i=0;i<lvzwebdesktop.constructors.length;i++ ) {
				if ( lvzwebdesktop.constructors[i].constructor === WindoweditorObject ) {
					for ( var j=0;j<lvzwebdesktop.constructors[i].constructor.list.length;j++ ) {
						self = lvzwebdesktop.constructors[i].constructor.list[j];
						veobj = lvzwebdesktop(self.elements.container,false).getParentObject('VisualeditorObject');
						self.visualEditor = veobj;
						veobj.editedWindow = self;
						if ( self.props.window === null ) self.buildWindow();
					}
				}
			}

		});
	}

	window.lvzwebdesktop.VisualeditorObject = VisualeditorObject;
	window.lvzwebdesktop.constructors.push({
		"selector" : ".visual-editor",
		"constructor" : VisualeditorObject
	});

	window.lvzwebdesktop.WindoweditorObject = WindoweditorObject;
	window.lvzwebdesktop.constructors.push({
		"selector" : ".wd-window-editor-container",
		"constructor" : WindoweditorObject
	});

	window.lvzwebdesktop.VisualcontrolObject = VisualcontrolObject;
	window.lvzwebdesktop.constructors.push({
		"selector" : false,
		"constructor" : VisualcontrolObject
	});

})();
