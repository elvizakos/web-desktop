{
	document.currentScript.dataLVZWLIB = true;

	/**
	 * Constructor for creating button objects.
	 * @constructor
	 * @param {HTMLElement} [e] - Element to attach button object to.
	 */
	var ButtonObject = function ( e ) {

		lvzwebdesktop.basicObject.call(this, 'button');

		// Add common methods
		lvzwebdesktop.addMethods.call(this,"setTop,setLeft,setWidth,setHeight");

		/**
		 * Method for disabling button.
		 * @return {ButtonObject} This button object.
		 */
		this.disable = function ( ) {
			this.elements.container.disabled = true;
			return this;
		};

		/**
		 * Method for enabling button.
		 * @return {ButtonObject} This button object.
		 */
		this.enable = function ( ) {
			this.elements.container.disabled = false;
			return this;
		};

		/**
		 * Button's top position in number of pixels.
		 * @type {Number|false}
		 * @public
		 */
		this.props.top = 0;

		/**
		 * Button's left position in number of pixles.
		 * @type {Number|false}
		 * @public
		 */
		this.props.left = false;

		/**
		 * Button's width in number of pixels.
		 * @type {Number|false}
		 * @public
		 */
		this.props.width = false;

		/**
		 * Button's height in number of pixels.
		 * @type {Number|false}
		 * @public
		 */
		this.props.height = false;

		/**
		 * Set if this object can be resized during edit.
		 * @public
		 * @type {boolean}
		 */
		this.props.resizableOnEdit = true;

		/**
		 * CSS class-name for button element.
		 * @type {string}
		 * @public
		 */
		this.props.classNames.container = ButtonObject.classNames.container;

		/**
		 * Array for storing event functions for clicking button.
		 * @type {Array}
		 * @public
		 */
		this.onclick = [];

		/**
		 * Array for storing event functions for moving button.
		 * @type {Array}
		 * @public
		 */
		this.onmove = [];

		/**
		 * Array for storing event functions for resizing button.
		 * @type {Array}
		 * @public
		 */
		this.onresize = [];

		/**
		 * button element.
		 * @type {HTMLButtonElement|HTMLInputElement}
		 * @public
		 */
		this.elements.container = null;

		if ( typeof e == 'object' ) {
			if ( e instanceof HTMLElement ) {

				this.props.elementId = !e.id ? false : e.id;

				if ( e.nodeName != 'BUTTON' && ! ( e.nodeName = 'INPUT' && e.type == 'button' ) ) {
					var o = e.outerHTML;
					o = o.replace(RegExp('^[<]([^> ]+)(.*)[<][/]([^>]+)[>]$','mu'),'<button$2</button>');
					e.outerHTML = o;
				}

				this.elements.container = e;
				var tmpattr;

				tmpattr = e.getAttribute('onclick');
				if ( tmpattr !== null ) {
					this.addEvent ( 'click', new Function ( 'event', tmpattr ) );
					e.setAttribute ( 'onbuttonclick', tmpattr );
					e.removeAttribute ( 'onclick' );
				}

				tmpattr = e.getAttribute('onmove');
				if ( tmpattr !== null ) this.addEvent ( 'move' , new Function ( 'event' , tmpattr ) );

				tmpattr = e.getAttribute('onresize');
				if ( tmpattr !== null ) this.addEvent ( 'resize' , new Function ( 'event' , tmpattr ) );

				tmpattr = getComputedStyle(document.body).getPropertyValue('--lvzwebdesktop-default-button-theme').trim();
				if ( tmpattr != '' ) {
					for ( var i = 0 ; i < this.elements.container.classList.length ; i ++ ) {
						if ( getComputedStyle(document.body).getPropertyValue('--lvzwebdesktop-theme-button-'+this.elements.container.classList[i]).trim() == 'true' ) {
							this.props.themeClass = this.elements.container.classList[i];
							break;
						}
					}
					if ( this.props.themeClass === null ) {
						this.props.themeClass = tmpattr;
						this.elements.container.classList.add(tmpattr);
					}
				}
				else {
					console.error ("There is no theme loaded for this button.");
				}

			}
			else {
				console.error ( "Invalid object type" );
				return false;
			}
		}
		else {
			this.elements.container = document.createElement('button');
		}

		this.elements.container.classList.add ( this.props.classNames.container );
		this.elements.container.dataLVZWDESKTOPOBJ = this;
		this.elements.container.dataButtonObject = this;
		this.props.height = this.elements.container.offsetHeight;
		this.props.width = this.elements.container.offsetWidth;

		lvzwebdesktop.resizeObserver.observe ( this.elements.container );

		this.elements.container.addEventListener('click', function ( e ) {
			var self = e.target.dataLVZWDESKTOPOBJ;
			var eobj={
				"buttons" : e.buttons,
				"button" : e.button,
				"clientX" : e.clientX,
				"clientY" : e.clientY,
				"layerX" : e.layerX,
				"layerY" : e.layerY,
				"offsetX" : e.offsetX,
				"offsetY" : e.offsetY,
				"pageX" : e.pageX,
				"pageY" : e.pageY,
				"screenX" : e.screenX,
				"screenY" : e.screenY,
				"x" : e.x,
				"y" : e.y,
				"altKey" : e.altKey,
				"ctrlKey" : e.ctrlKey,
				"metaKey" : e.metaKey,
				"shiftKey" : e.shiftKey,
				"target" : self,
				"element" : e.target
			};
			self.fireEvent('click',eobj);
		});

		lvzwebdesktop.endBasicObject.call(this);
	};

	window.lvzwebdesktop.windowVisualControls.push({
		"constructor" : ButtonObject,
		"name" : "button",
		"icon" : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADMAAAAXCAYAAACmnHcKAAABhGlDQ1BJQ0MgcHJvZmlsZQAAKJF9kT1Iw0AcxV9TpVIqInYQUchQHcQuKuJYq1CECqFWaNXB5NIvaNKQpLg4Cq4FBz8Wqw4uzro6uAqC4AeIq4uToouU+L+k0CLWg+N+vLv3uHsHCPUy06yuGKDptplKxMVMdlUMvCKIEfQjgHGZWcacJCXRcXzdw8fXuyjP6nzuz9Gr5iwG+ETiGDNMm3iDeGbTNjjvE4dZUVaJz4knTLog8SPXFY/fOBdcFnhm2Eyn5onDxGKhjZU2ZkVTI54mjqiaTvlCxmOV8xZnrVxlzXvyF4Zy+soy12kOI4FFLEGCCAVVlFCGjSitOikWUrQf7+Afcv0SuRRylcDIsYAKNMiuH/wPfndr5acmvaRQHOh+cZyPUSCwCzRqjvN97DiNE8D/DFzpLX+lDsx+kl5raZEjoG8buLhuacoecLkDDD4Zsim7kp+mkM8D72f0TVlg4BYIrnm9Nfdx+gCkqavkDXBwCIwVKHu9w7t72nv790yzvx+huXK5d0ym3wAAAAZiS0dEAP8A/wD/oL2nkwAAAAlwSFlzAAAuIwAALiMBeKU/dgAAAAd0SU1FB+cKHREPAlskot0AAAAZdEVYdENvbW1lbnQAQ3JlYXRlZCB3aXRoIEdJTVBXgQ4XAAADLElEQVRYw+2YX2hTVxzHP0lb1hdpoGkbinUF05trZsBUfLDV0NDpbO3fgRBDkUEZ9kWGFkEIQUZRXFuV4fNAMv/WP5W2TiZrYYFmBZt2W2q9pAkLYYEbY2jAPrRk6fVBvRImG5SMcbv93u7v/M75/T7nnHvP9x5dNBpVMpmXhMMxZDmN1sxkKkcQajEYtqALBn9W/P4g9fU7EcU6QNEQig5JWmJubgGHYzfF4XAMu/0jRNGsMRAABVE0oygK4XAMvSyn36yIdk0U65DlNHoAnU7TLGr9ejaR/XdgbDYrkcjS/yvj8ZxhZOR2nu/QoQNEoxHtwaTTaVZWVvJ8iUSCbPaPfw/mwoXz2GxW7HYbw8ODrK+vq36v16PGBQLTNDc3AXDlytc8evQd584NqFv12LEeXrxI0d3djsPRoPYLhX6ls7MNi2U7TqeDqalJta2v73POnvXidDowm2txuY6QSj3fOEx9/W7m5n7h5s07jIzc4vr1b/92hk6c+IKWllY8Hi+h0CJmcx0+3zWMxgpGR8fx+wMAZDIZ3G4XJ0/2s7gYpr//NL29nxGL/aaOlUzKjI6OMT09Qy6XY2hocOMwBw9+QklJCaIocurUae7fv1ewbTExMUZjYyNNTU6Kiopoa2unufljxsfH1JiGhkYMBgNVVSbc7h4WFkKFeWcsFpFkMlkwmHg8ztatNXk+QbCQTMrvjTcajeRyucLAJBK/U1PzOnlxcTHLy8t/rZwU5b166q1VVlYRiUT+lGPbtg//mQ/A3bt3WFtbQ5ZlLl0axu3uAWDHDitTUz8gSRLZbJZAYDqvX1mZgWBwNg/IYChjdvaJ+tzaehi//0cePpxAURRmZn5icnKS7u5PCw9TWvoB8XicXbt20tFxGJfrKJ2dXWohHR1dtLe3sH9/A6urq+j174Y7fryPZ88W2bPHzuPH3785e7wMDn7Fvn17SaWeU11dzdWrPi5eHMJqFRgY+BKf7xoVFZUb02iXL3+juN1dmpcyN2482ITaTFG0DfG2fr3JVI4kLWkaRpKWMJnK0QtCLfPzT5GkCKC1vzQdkhRhfv4pglCLbjPdzrwCr8g5svxB3joAAAAASUVORK5CYII=",
		"iconw" : 51,
		"iconh" : 23,
		"init" : function ( b ) { }
	});

	/**
	 * Property for building the edit properties table on control edit.
	 * @public
	 * @type {Object}
	 */
	ButtonObject.editProperties = {
		"id"      : { "type" : "string|false",
					  "default" : "",
					  "getValue" : function(self){return self.props.elementId;},
					  "setValue" : function(self,v){if ( v == '' ) v=false; self.props.elementId=v;self.elements.container.id=v;}},
		"name"    : { "type" : "string|false",
					  "default" : "",
					  "getValue" : function (self){return self.props.name;},
					  "setValue" : function (self,v){ self.props.name=v; }},
		"top" : { "type" : "number|false",
				  "default" : false,
				  "getValue" : window.lvzwebdesktop.fn.visualeditorGetTop,
				  "setValue" :  window.lvzwebdesktop.fn.visualeditorSetTop
				},
		"left" : { "type" : "number|false",
				   "default" : false,
				   "getValue" : lvzwebdesktop.fn.visualeditorGetLeft,
				   "setValue" : lvzwebdesktop.fn.visualeditorSetLeft
				 },
		"height" : { "type" : "number|false",
					 "default" : false,
					 "getValue" : lvzwebdesktop.fn.visualeditorGetHeight,
					 "setValue" : lvzwebdesktop.fn.visualeditorSetHeight
				   },
		"width" : { "type" : "number|false",
					"default" : false,
					"getValue" : lvzwebdesktop.fn.visualeditorGetWidth,
					"setValue" : lvzwebdesktop.fn.visualeditorSetWidth
				  },
		"label"   : { "type" : "string",
					  "default" : '',
					  "getValue" : function(self){return self.elements.container.value;},
					  "setValue" : function(self,v){
						  if ( typeof self.visualcontrol !== 'undefined' ) {
							  self.visualcontrol.elements.itemcontainer.value = v;
						  }
						  self.elements.container.value = v;
					  }},
		// "visible" : { "type" : "boolean", "default" : true },
		// "enabled" : { "type" : "boolean", "default" : true },
		"tool tip" : { "type" : "string",
					   "default" : "",
					   "getValue" : function(self){var t=self.elements.container.getAttribute('title');if(t!==null)return t;return '';},
					   "setValue" : function(self,v){self.elements.container.setAttribute('title',v);}},
		"tab index" : { "type" : "number|false",
						"default" : false
					  }
	};

	/**
	 * List with all button objects.
	 * @type {Array}
	 * @public
	 */
	ButtonObject.list = [];

	/**
	 * Associative array for button object's default class names.
	 * @type {object}
	 * @public
	 */
	ButtonObject.classNames = {};

	/**
	 * Default class name for button object's elements.
	 * @type {string}
	 * @public
	 */
	ButtonObject.classNames.container = 'web-desktop-button';

	/**
	 * Constructor for creating textbox objects.
	 * @constructor
	 * @param {HTMLInputElement} [e] - Element to attach text object to.
	 */
	var TextboxObject = function ( e ) {

		lvzwebdesktop.basicObject.call(this, 'textbox');

		// Add common methods
		lvzwebdesktop.addMethods.call(this,"setTop,setLeft,setWidth,setHeight");

		/**
		 * Method for disabling textbox.
		 * @return {TextboxObject} This textbox object.
		 */
		this.disable = function () {
			this.elements.container.disabled = true;
			return this;
		};

		/**
		 * Method for enabling textbox.
		 * @return {TextboxObject} This textbox object.
		 */
		this.enable = function () {
			this.elements.container.disabled = false;
			return this;
		};

		/**
		 * Method for changing textbox's value.
		 * @param {String} t - The value of the textbox.
		 * @return {TextboxObject} This textbox object.
		 */
		this.setValue = function ( t ) {
			this.elements.container.value = t;
			if ( this.elements.container.nodeName == 'INPUT' ) {
				this.elements.container.setAttribute('value',t);
			}
			else if ( this.elements.container.nodeName == 'TEXTAREA' ){
				this.elements.container.innerText = t;
			}
			return this;
		};

		/**
		 * Textbox value.
		 * @public
		 * @type {string}
		 */
		this.props.value = '';

		
		/**
		 * Textbox top position in number of pixels.
		 * @type {number}
		 * @public
		 */
		this.props.top = 0;

		/**
		 * Textbox left position in number of pixels.
		 * @type {number}
		 * @public
		 */
		this.props.left = 0;

		/**
		 * Textboxe width in number of pixels.
		 * @type {number}
		 * @public
		 */
		this.props.width = 0;

		/**
		 * Textbox height in number of pixels.
		 * @type {number}
		 * @public
		 */
		this.props.height = 0;

		/**
		 * Set if this object can be resized during edit.
		 * @public
		 * @type {boolean}
		 */
		this.props.resizableOnEdit = true;

		/**
		 * CSS class-name for textbox element.
		 * @type {string}
		 * @public
		 */
		this.props.classNames.container = TextboxObject.classNames.container;

		/**
		 * Array for storing event functions for moving textbox.
		 * @type {Array}
		 * @public
		 */
		this.onmove = [];

		/**
		 * Array for storing event functions for resizing textbox.
		 * @type {Array}
		 * @public
		 */
		this.onresize = [];

		/**
		 * Array for storing event functions for when changing textbox value.
		 * @type {Array}
		 * @public
		 */
		this.onchange = [];

		/**
		 * Array for storing event functions for when textbox gets focus.
		 * @type {Array}
		 * @public
		 */
		this.onfocus = [];

		/**
		 * Array for storing event functions for whet textbox looses focus.
		 * @type {Array}
		 * @public
		 */
		this.onblur = [];

		/**
		 * Array for storing event functions for keyup on textbox.
		 * @public
		 * @type {Array}
		 */
		this.onkeyup = [];

		/**
		 * Array for storing event functions for keypress on textbox.
		 * @public
		 * @type {Array}
		 */
		this.onkeypress = [];

		/**
		 * Array for storing event functions for keydown on textbox.
		 * @public
		 * @type {Array}
		 */
		this.onkeydown = [];

		/**
		 * Textbox element.
		 * @type {HTMLInputElement}
		 * @public
		 */
		this.elements.container = null;

		if ( typeof e != 'undefined' ) {
			if ( e instanceof HTMLElement ) {
				this.props.elementId = !e.id ? false : e.id;
				//			if ( e instanceof HTMLInputElement && e.type == 'text' ) {

				if ( e.nodeName != 'TEXTAREA' && ! ( e.nodeName == 'INPUT' && ['text','number'].indexOf(e.type) > -1 ) ) {
					var o = e.outerHTML;
					o = o.replace(RegExp('^[<]([^> ]+)(.*)[<][/]([^>]+)[>]$','mu'),'<textarea$2</textarea>');
					e.outerHTML = o;
				}

				this.elements.container = e;
				

				var tmpattr;

				tmpattr = e.getAttribute('onclick');
				if ( tmpattr !== null ) {
					this.addEvent ( 'click', new Function ( 'event', tmpattr ) );
					e.setAttribute ('ontextboxclick', tmpattr );
					e.removeAttribute('onclick');
				}

				tmpattr = e.getAttribute ('onmove');
				if ( tmpattr !== null ) this.addEvent('move', new Function ( 'event', tmpattr ) );

				tmpattr = getComputedStyle(document.body).getPropertyValue('--lvzwebdesktop-default-textbox-theme').trim();
				if ( tmpattr != '' ) {
					for ( var i = 0 ; i < this.elements.container.classList.length ; i ++ ) {
						if ( getComputedStyle(document.body).getPropertyValue('--lvzwebdesktop-theme-textbox-'+this.elements.container.classList[i]).trim() == 'true' ) {
							this.props.themeClass = this.elements.container.classList[i];
							break;
						}
					}
					if ( this.props.themeClass === null ) {
						this.props.themeClass = tmpattr;
						this.elements.container.classList.add(tmpattr);
					}
				}
				else {
					console.error ("There is no theme loaded for this button.");
				}

			}
			else {
				console.error ( "Invalid element type" );
				return false;
			}
		}
		else {

			this.elements.container = document.createElement('input');
			this.elements.container.type = 'text';

		}
		this.props.width = this.elements.container.offsetWidth;
		this.props.height = this.elements.container.offsetHeight;

		this.elements.container.classList.add ( this.props.classNames.container );
		this.elements.container.dataLVZWDESKTOPOBJ = this;
		this.elements.container.dataTextboxObject = this;
		this.elements.container.addEventListener('change', function ( e ) {
			var self = e.target.dataLVZWDESKTOPOBJ;
			self.fireEvent('change',{
				"target" : self,
				"oldValue" : self.props.value,
				"newValue" : e.target.value
			});
			self.props.value = e.target.value;
		});
		this.elements.container.addEventListener('focus', function ( e ) {
			var self = e.target.dataLVZWDESKTOPOBJ;
			self.fireEvent('focus',{
				"target" : self,
			});
		});
		this.elements.container.addEventListener('blur', function ( e ) {
			var self = e.target.dataLVZWDESKTOPOBJ;
			self.fireEvent('blur',{
				"target" : self,
			});
		});

		this.elements.container.addEventListener('keyup', textboxkeypress);
		this.elements.container.addEventListener('keypress', textboxkeypress);
		this.elements.container.addEventListener('keydown', textboxkeypress);

		lvzwebdesktop.resizeObserver.observe ( this.elements.container );

		lvzwebdesktop.endBasicObject.call(this);
	};

	window.lvzwebdesktop.windowVisualControls.push({
		"constructor" : TextboxObject,
		"name" : "textbox",
		"icon" : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADMAAAAXCAYAAACmnHcKAAABhGlDQ1BJQ0MgcHJvZmlsZQAAKJF9kT1Iw0AcxV9TpVIqInYQUchQHcQuKuJYq1CECqFWaNXB5NIvaNKQpLg4Cq4FBz8Wqw4uzro6uAqC4AeIq4uToouU+L+k0CLWg+N+vLv3uHsHCPUy06yuGKDptplKxMVMdlUMvCKIEfQjgHGZWcacJCXRcXzdw8fXuyjP6nzuz9Gr5iwG+ETiGDNMm3iDeGbTNjjvE4dZUVaJz4knTLog8SPXFY/fOBdcFnhm2Eyn5onDxGKhjZU2ZkVTI54mjqiaTvlCxmOV8xZnrVxlzXvyF4Zy+soy12kOI4FFLEGCCAVVlFCGjSitOikWUrQf7+Afcv0SuRRylcDIsYAKNMiuH/wPfndr5acmvaRQHOh+cZyPUSCwCzRqjvN97DiNE8D/DFzpLX+lDsx+kl5raZEjoG8buLhuacoecLkDDD4Zsim7kp+mkM8D72f0TVlg4BYIrnm9Nfdx+gCkqavkDXBwCIwVKHu9w7t72nv790yzvx+huXK5d0ym3wAAAAZiS0dEAP8A/wD/oL2nkwAAAAlwSFlzAAAuIwAALiMBeKU/dgAAAAd0SU1FB+cKHREPDyWV3mAAAAAZdEVYdENvbW1lbnQAQ3JlYXRlZCB3aXRoIEdJTVBXgQ4XAAACCklEQVRYw+2YIWgyUQDH/9strBgcNzhQuKBfEpTDMQwDxWCzaHF1YFUQk0VQEGXhilXDiig2i5hWBo4ZxCSbiCJYdqyYjnn3/9rxwTdhoNtw+E/vPXg/3o8Hfx7v6P7+gaPRCwzDwL5GEAR4vX9wMhq94Po6ClE821sZTXtDo9HBsWEYey0CAKJ4BsMwcIxflIPMQeYgswOZVCqFu7u7reG74mwl8/r6itVqtTV8V5xPR1Vr/Dflcpmnp6e02WyUJInj8Zgk2e/3eXl5SUmSGA6HOZ1OaZomQ6EQb29vrf3RaJSFQmEj56uiqjX+J0OSiUSC1WrVmmuaRlEUORwOSZKlUonBYJAkOZlMKIoip9MpW60WA4EA1+v1h5yvljn5zO21221cXFzA5/MBAJLJJPL5PHRdh8vlQi6Xw83NDRaLBbrdLgRB+JEC+JTMfD7HYDCAoijWmsfjsR6n6XQaqqoiHo/D7Xb/WJttlCFpjZ1OJxRFQa/X23hz5+fnaDQayGazcDgcH3J+pM3sdjv6/b51kFgshqenJ6tmdV3HbDYDACyXS2QyGTSbTaRSKSSTyY2cb28zknx+fqbX66Usy+x0OiTJx8dHXl1dUZZl+v1+FotFmqbJSCTCSqVCknx/f6eiKKzX6xs5395m+xhVrfHwNjvIfIeMIAjQtLe9ltC0NwiCgKPf9DvzF4fzUR3jKLfqAAAAAElFTkSuQmCC",
		"iconw" : 51,
		"iconh" : 23
	});

	/**
	 * Property for building the edit properties table on control edit.
	 * @public
	 * @type {Object}
	 */
	TextboxObject.editProperties = {
		"id" : { "type" : "string|false",
				 "default" : false,
				 "getValue" : function(self){return self.props.elementId;},
				 "setValue" : function(self,v){if(v=='')v=false;self.props.elementId=v;self.elements.container.id=v;}},
		"name" : { "type" : "string|false",
				   "default" : false,
				   "getValue" : function (self){return self.props.name;},
				   "setValue" : function (self,v){ self.props.name =v; }},
		"top" : { "type" : "number|false",
				  "default" : false,
				  "getValue" : function(self){return self.props.top;},
				  "setValue" : function(self,v){
					  v = parseInt(v);
					  if ( typeof self.visualcontrol !== 'undefined' ) {
						  self.visualcontrol.top = v;
						  self.visualcontrol.elements.container.style.top = v+'px';
					  }
					  self.setTop(v);
				  }},
		"left" : { "type" : "number|false",
				   "default" : false,
				   "getValue" : function(self){return self.props.left;},
				   "setValue" : function(self,v){
					   v = parseInt(v);
					   if ( typeof self.visualcontrol !== 'undefined' ) {
						   self.visualcontrol.left = v;
						   self.visualcontrol.elements.container.style.left = v+'px';
					   }
					   self.setLeft(v);
				   }},
		"height" : { "type" : "number|false",
					 "default" : 20,
					 "getValue" : function(self){return self.props.height;},
					 "setValue" : function(self,v){
						 v = parseInt(v);
						 if ( typeof self.visualcontrol !== 'undefined' ) {
							 self.visualcontrol.height = v;
							 self.visualcontrol.elements.container.style.height = v+'px';
							 self.visualcontrol.elements.vail.style.height = v+'px';
							 self.visualcontrol.elements.itemcontainer.style.height = v+'px';
						 }
						 self.setHeight(v);
					 }},
		"width" : { "type" : "number|false",
					"default" : 100,
					"getValue" : function(self){return self.props.width;},
					"setValue" : function(self,v){
						v = parseInt(v);
						if ( typeof self.visualcontrol !== 'undefined' ) {
							self.visualcontrol.width = v;
							self.visualcontrol.elements.container.style.width = v+'px';
							self.visualcontrol.elements.vail.style.width = v+'px';
							self.visualcontrol.elements.itemcontainer.style.width = v+'px';
						}
						self.setWidth(v);
					}},
		"value"   : { "type" : "string",
					  "default" : '',
					  "getValue" : function(self){return self.elements.container.value;},
					  "setValue" : function(self,v){
						  if ( typeof self.visualcontrol !== 'undefined' ) {
							  self.visualcontrol.elements.itemcontainer.value = v;
							  if ( self.visualcontrol.elements.itemcontainer.nodeName == 'INPUT' ) {
								  self.visualcontrol.elements.itemcontainer.setAttribute('value',v);
							  }
							  else if ( self.visualcontrol.elements.itemcontainer.nodeName == 'TEXTAREA' ){
								  self.visualcontrol.elements.itemcontainer.innerText = v;
							  }
						  }
						  self.setValue(v);
					  }},
		"tool tip" : { "type" : "string|false",
					   "default" : false,
					   "getValue" : function ( self ) { var t=self.elements.container.getAttribute('title');if ( t!== null) return t; return '';},
					   "setValue" : function (self,v) {
						   if ( v === '' ) {
							   self.elements.container.removeAttribute('title');
						   }
						   else {
							   self.elements.container.setAttribute('title',v); 
						   }
					   }}
	};

	/**
	 * List with all textbox objects.
	 * @type {Array}
	 * @public
	 */
	TextboxObject.list = [];

	/**
	 * Associative array for textbox object's default class names.
	 * @type {object}
	 * @public
	 */
	TextboxObject.classNames = {};

	/**
	 * Default class name for textbox object's element.
	 * @type {string}
	 * @public
	 */
	TextboxObject.classNames.container = 'web-desktop-textbox';

	/**
	 * Constructor for creating label objects.
	 * @constructor
	 * @param {HTMLLabelElement} e - Element to create label object from.
	 */
	var LabelObject = function ( e ) {

		lvzwebdesktop.basicObject.call( this, 'label' );

		// Add common methods
		lvzwebdesktop.addMethods.call(this,"setTop,setLeft,setWidth,setHeight");

		/**
		 * Method for changing the label object's caption.
		 * Fires the "captionchange" event.
		 * @param {String} v - The new caption.
		 * @return {LabelObject} This label object.
		 */
		this.setCaption = function ( v ) {
			var eventData = { "target":this,
							  "oldCaption":this.elements.container.innerText,
							  "newCaption":v
							};
			this.fireEvent('captionchange',eventData);
			if ( eventData.defaultPrevented === true ) return this;
			this.elements.container.innerText = v;
			return this;
		};

		/**
		 * Method for changing the label object's caption, alias of the "setCaption" method.
		 * Fires the "captionchange" event.
		 * @param {String} v - The new caption.
		 * @return {LabelObject} This label object.
		 */
		this.setLabel = this.setCaption;

		/**
		 * Array for storing event function for when moving the label.
		 * @public
		 * @type {Array}
		 */
		this.onmove = [];

		/**
		 * Array for storing event function for when change the label's caption.
		 * @public
		 * @type {Array}
		 */
		this.oncaptionchange = [];

		/**
		 * Property to store the label's top position.
		 * @public
		 * @type {false|Number}
		 */
		this.props.top = false;

		/**
		 * Minimum distance of the top border of the label from the top border of its parent element.
		 * @public
		 * @type {false|Number}
		 */
		this.props.minTop = false;

		/**
		 * Maximum distance of the top border of the label from the top border of its parent element.
		 * @public
		 * @type {false|Number}
		 */
		this.props.maxTop = false;

		/**
		 * Property to store the label's left position.
		 * @public
		 * @type {false|Number}
		 */
		this.props.left = false;

		/**
		 * Minimum distance of the left border of the label from the left border of its parent element.
		 * @public
		 * @type {false|Number}
		 */
		this.props.minLeft = false;

		/**
		 * Maximum distance of the left border of the label from the left border of its parent element.
		 * @public
		 * @type {false|Number}
		 */
		this.props.maxLeft = false;

		/**
		 * Property to store the label's width.
		 * @public
		 * @type {false|Number}
		 */
		this.props.width = false;

		/**
		 * Property to store the label's height.
		 * @public
		 * @type {false|Number}
		 */
		this.props.height = false;

		/**
		 * Set if this object can be resized during edit.
		 * @publi
		 * @type {boolean}
		 */
		this.props.resizableOnEdit = true;

		/**
		 * CSS classname of the label's container element.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.container = LabelObject.classNames.container;

		/**
		 * Container element of the label object.
		 * @public
		 * @type {HTMLLabelElement}
		 */
		this.elements.container = lvzwebdesktop.createBasicElement.call(this,'label',e,[
			this.props.classNames.container
		]);

		this.props.height = this.elements.container.offsetHeight;
		this.props.width = this.elements.container.offsetWidth;

		lvzwebdesktop.endBasicObject.call(this);

	};

	window.lvzwebdesktop.windowVisualControls.push({
		"constructor" : LabelObject,
		"name" : "label",
		"icon" : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADMAAAAUCAYAAAAgCAWkAAABhGlDQ1BJQ0MgcHJvZmlsZQAAKJF9kT1Iw0AcxV9TpVIqInYQUchQHcQuKuJYq1CECqFWaNXB5NIvaNKQpLg4Cq4FBz8Wqw4uzro6uAqC4AeIq4uToouU+L+k0CLWg+N+vLv3uHsHCPUy06yuGKDptplKxMVMdlUMvCKIEfQjgHGZWcacJCXRcXzdw8fXuyjP6nzuz9Gr5iwG+ETiGDNMm3iDeGbTNjjvE4dZUVaJz4knTLog8SPXFY/fOBdcFnhm2Eyn5onDxGKhjZU2ZkVTI54mjqiaTvlCxmOV8xZnrVxlzXvyF4Zy+soy12kOI4FFLEGCCAVVlFCGjSitOikWUrQf7+Afcv0SuRRylcDIsYAKNMiuH/wPfndr5acmvaRQHOh+cZyPUSCwCzRqjvN97DiNE8D/DFzpLX+lDsx+kl5raZEjoG8buLhuacoecLkDDD4Zsim7kp+mkM8D72f0TVlg4BYIrnm9Nfdx+gCkqavkDXBwCIwVKHu9w7t72nv790yzvx+huXK5d0ym3wAAAAZiS0dEAP8A/wD/oL2nkwAAAAlwSFlzAAAuIwAALiMBeKU/dgAAAAd0SU1FB+cKHREPJmcnRgwAAAAZdEVYdENvbW1lbnQAQ3JlYXRlZCB3aXRoIEdJTVBXgQ4XAAADAUlEQVRYw+2XT0hiURSHvzcFQlZaFOmm1z+yFrVJjNYSFNKmghYRGYGEYVBJqxZuokVuLEhzI0VERIuiaBVCRVLZw4Uba6VEkCmR8YQiYWYnNDkzzEzNounszj2/c+/77jn3Xp5wfn7+lQ9iX/hA9mV5eZnOzk70ej2BQOC3ku12O0ajEb1ez9PT0199iCRJdHV1YTAYWFhY+DOYwcFB3G73HyU7nU6sVuub7GpLSws7OzuUlJR8ttmHg8n/USAUCrG6usrNzQ0AeXl5DAwM0N7enlMfjUZxOp1EIhHUajVms5menp4XmqOjI7xeL7Isk8lkaGtrY2xsjMLCwveF8fv9VFZW4nQ6EQSBWCzG8PAwKpUKg8HwSu92u5menkaj0bC5ucns7CxqtRqj0QjAwcEBdrsdh8OByWQinU4zOjrK5OQkHo8HQRDer83MZjMjIyPZRURRpLW1la2trZz63t5eRFFEoVDQ399PQ0MDXq83G5+fn0en02EymQBQKpVYLBYkSSIYDL5vZfLz81lcXOT09BRZlhEEgfv7e2pqanLqGxsbX/jNzc1sbGyQTqeRZZlYLEZfX98LjU6nA+Ds7Cxntd8MZmpqing8jsvlQhRFABwOB5eXlzn13/d9cXExAMlkksfHRwD29vY4PDx8lffw8PB+lbm9vUWSJKxWaxbkVybLMgqFIuunUikAysrKkGUZgO7ubmw227+9mjOZTE5xIpH44UQXFxcv/HA4TG1tLUqlkoqKCqqqqnJW1e12v9mZyQmj1Wqpq6tje3ubeDwOQCAQ+OmiPp+P6+trnp+fWVtbIxKJYLFYsvHx8XGCwSB+vz87tr+/z+7uLvX19W8CI9hstq/r6+skEglKS0vp6OhgYmKCq6sr5ubmCIfDaLVampqaSCaTHB8fU15ezsrKCjMzM4RCIVKpFB6PB5fLRTQaRaVSMTQ09OqdOTk5YWlpibu7O4qKitBoNNhsNkRRRJIkHA4H8XicgoICqqur8fl8vwfz+QvwCfMJ8//CfAMrLSHjimY+wgAAAABJRU5ErkJggg==",
		"iconw" : 51,
		"iconh" : 20
	});

	/**
	 * Property for building the edit properties table on control edit.
	 * @public
	 * @type {Object}
	 */
	LabelObject.editProperties = {
		"id" : { "type" : "string",
				 "default" : "",
				 "getValue" : function(self){return self.props.elementId;},
				 "setValue" : function(self,v){if(v=='')v=false;self.props.elementId=v;self.elements.container.id=v;}},
		"name" : { "type" : "string",
				   "default" : "",
				   "getValue" : function (self){return self.props.name;},
				   "setValue" : function (self,v){ self.props.name =v; }},
		"label" : { "type" : "string",
					"default" : "",
					"getValue" : function(self){return self.elements.container.innerText;},
					"setValue" : function(self,v){
						if ( typeof self.visualcontrol != 'undefined' ) {
							self.visualcontrol.elements.itemcontainer.innerText = v; 
						}
						self.setCaption(v);
					}},
		"top" : { "type" : "number",
				  "default" : 0,
				  "getValue" : function(self){return self.props.top;},
				  "setValue" : function(self,v){
					  v = parseInt(v);
					  if ( typeof self.visualcontrol !== 'undefined' ) {
						  self.visualcontrol.top = v;
						  self.visualcontrol.elements.container.style.top = v+'px';
					  }
					  self.setTop(v);
				  }},
		"left" : { "type" : "number",
				   "default" : 0,
				   "getValue" : function(self){return self.props.left;},
				   "setValue" : function(self,v){
					   v = parseInt(v);
					   if ( typeof self.visualcontrol !== 'undefined' ) {
						   self.visualcontrol.left = v;
						   self.visualcontrol.elements.container.style.left = v+'px';
					   }
					   self.setLeft(v);
				   }},
		"height" : { "type" : "number",
					 "default" : 0,
					 "getValue" : function(self){return self.props.height;},
					 "setValue" : function(self,v){
						 v = parseInt(v);
						 if ( typeof self.visualcontrol != 'undefined' ) {
							 self.visualcontrol.elements.itemcontainer.style.height = v+'px';
						 }
						 self.setHeight(v);
					 }},
		"width" : { "type" : "number",
					"default" : 0,
					"getValue" : function(self){return self.props.width;},
					"setValue" : function(self,v){
						v=parseInt(v);
						if ( typeof self.visualcontrol != 'undefined' ) {
							self.visualcontrol.elements.itemcontainer.style.width = v+'px';
						}
						self.setWidth(v);
					}},
		"tool tip" : { "type" : "string",
					   "default" : '',
					   "getValue" : function ( self ) { var t=self.elements.container.getAttribute('title');if ( t!== null) return t; return '';},
					   "setValue" : function (self,v) {
						   if ( v === '' ) {
							   self.elements.container.removeAttribute('title');
						   }
						   else {
							   self.elements.container.setAttribute('title',v); 
						   }
					   }}
	};

	/**
	 * List of all label object instances.
	 * @public
	 * @type {Array}
	 */
	LabelObject.list = [];

	/**
	 * Associative array for label object's default class names.
	 * @public
	 * @type {object}
	 */
	LabelObject.classNames = {};

	/**
	 * Default class name for label object's container elements.
	 * @public
	 * @type {String}
	 */
	LabelObject.classNames.container = 'wd-label';

	/**
	 * Constructor for creating checkbox objects.
	 * @constructor
	 * @param {HTMLElement} [e] - Element to create checkbox object from.
	 */
	var CheckboxObject = function ( e ) {

		lvzwebdesktop.basicObject.call( this, 'checkbox' );

		// Add common methods
		lvzwebdesktop.addMethods.call(this,"setTop,setLeft,setWidth,setHeight");

		/**
		 * Method for setting the checkboxes label.
		 * fire the "labelchange" event.
		 * @public
		 * @param {String} v - The checkboxes new label.
		 * @return {CheckboxObject} This object.
		 */
		this.setLabel = function ( v ) {
			var eventData = { "target" : this,
							  "newValue" : v,
							  "previousValue" : this.elements.label.innerText
							};
			this.fireEvent('labelchange',eventData);
			if ( eventData.defaultPrevented === true ) return this;
			this.elements.label.innerText = v;
			return this;
		};

		/**
		 * Method for setting the checkbox's value.
		 * Fire the "change" event.
		 * @public
		 * @param {boolean} v - The value to set.
		 * @return {CheckboxObject} This object.
		 */
		this.setValue = function ( v ) {
			var eventData = { "target" : this,
							  "newValue"  : v,
							  "previousValue" : this.value
							};
			this.fireEvent('change',eventData);
			if ( eventData.defaultPrevented === true ) return this;
			if ( typeof v == 'boolean' && this.value !== v ) {
				this.elements.checkbox.setAttribute('checked',v===true?'checked':'');
				this.elements.checkbox.checked = v;
			}
			return this;
		};

		/**
		 * Array for storing event functions for when moving the checkbox object.
		 * @public
		 * @type {Array}
		 */
		this.onmove = [];

		/**
		 * Array for storing event functions for when resizing the checkbox object.
		 * @public
		 * @type {Array}
		 */
		this.onresize = [];

		/**
		 * Array for storing event functions for when there is a change in checkbox.
		 * @public
		 * @type {Array}
		 */
		this.onchange = [];

		/**
		 * Array for storing event functions for when the label is changed.
		 * @public
		 * @type {Array}
		 */
		this.onlabelchange = [];

		/**
		 * Array for storing event functions for when this object is ready.
		 * @public
		 * @type {Array}
		 */
		this.onready = [];

		/**
		 * Property for the checkbox's value.
		 * @public
		 * @type {boolean}
		 */
		this.value = false;

		/**
		 * Set if this object can be resized during edit.
		 * @public
		 * @type {boolean}
		 */
		this.props.resizableOnEdit = true;

		/**
		 * Property to store the vertical position of the
		 * checkbox. The value can be a number for absolute position
		 * or false if position is static.
		 * @public
		 * @type {false|Number}
		 */
		this.props.top = false;

		/**
		 * Property to store the horizontal position of the
		 * checkbox. The value can be a number for absolute position
		 * or false for static.
		 * @public
		 * @type {false|Number}
		 */
		this.props.left = false;

		/**
		 * The checkbox's height in number of pixels.
		 * @public
		 * @type {Number}
		 */
		this.props.height = 0;

		/**
		 * The ckeckbox's width in number of pixels.
		 * @public
		 * @type {Number}
		 */
		this.props.width = 0;

		/**
		 * CSS classname of the checkboxes container element.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.container = CheckboxObject.classNames.container;

		/**
		 * CSS classname of the checkbox element.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.checkbox = CheckboxObject.classNames.checkbox;

		/**
		 * CSS classname of the checkboxes label element.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.label = CheckboxObject.classNames.label;

		var cntel=undefined;
		var chkel=undefined;
		var lblel=undefined;
		if ( typeof e == 'object' && e instanceof HTMLElement ) {
			if ( e.nodeName == 'LABEL' ) {
				cntel = e;
				chkel = cntel.querySelector('input[type=checkbox]')
				if ( chkel === null ) chkel = undefined;
				lblel = cntel.querySelector('span');
				if ( lblel === null ) lblel = undefined;
			}
			else if ( e.nodeName == 'INPUT' ) {
				chkel = e;

				if ( (attr = chkel.getAttribute('label')) !== null ) {
					lblel = document.createElement('span');
					lblel.innerText = attr;
				}
				else if ( (attr = chkel.getAttribute('data-label')) !== null ) {
					lblel = document.createElement('span');
					lblel.innerText = attr;
				}
			}
		}

		/**
		 * Container element of the checkbox object.
		 * @public
		 * @type {HTMLLabelElement}
		 */
		this.elements.container = lvzwebdesktop.createBasicElement.call(this,'label',cntel,[
			this.props.classNames.container
		]);

		/**
		 * The checkbox element.
		 * @public
		 * @type {HTMLInputElement}
		 */
		this.elements.checkbox = lvzwebdesktop.createBasicElement.call(this,'input',chkel,[
			this.props.classNames.checkbox
		]);
		this.elements.checkbox.type = 'checkbox';
		this.elements.checkbox.addEventListener('change',checkboxCNG);
		this.elements.container.appendChild(this.elements.checkbox);
		this.value = this.elements.checkbox.checked;

		/**
		 * The label element of the checkbox.
		 * @public
		 * @type {HTMLSpanElement}
		 */
		this.elements.label = lvzwebdesktop.createBasicElement.call(this,'span',lblel,[
			this.props.classNames.label
		]);
		this.elements.container.appendChild(this.elements.label);

		if ( typeof e != 'undefined' ) {
			var attr;

			attr = e.getAttribute('onready');
			if ( attr !== null ) this.addEvent('ready', new Function ( 'event', attr ) );

			attr = e.getAttribute('onchange');
			if ( attr !== null ) this.addEvent('change', new Function ( 'event', attr ) );

			attr = e.getAttribute('onlabelchange');
			if ( attr !== null ) this.addEvent('labelchange', new Function ( 'event', attr ) );

		}

		if ( this.elements.container.style.position == 'absolute' ) {
			this.props.top = this.elements.container.offsetTop;
			this.props.left = this.elements.container.offsetLeft;
		}

		this.props.width = this.elements.container.offsetWidth;
		this.props.height = this.elements.container.offsetHeight;

		lvzwebdesktop.endBasicObject.call(this);

	};

	window.lvzwebdesktop.windowVisualControls.push({
		"constructor" : CheckboxObject,
		"name" : "checkbox",
		"icon" : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADMAAAAXCAYAAACmnHcKAAABhGlDQ1BJQ0MgcHJvZmlsZQAAKJF9kT1Iw0AcxV9TpVIqgnYQUQhSneyiIo61CkWoEGqFVh1MLv2CJg1Ji4uj4Fpw8GOx6uDirKuDqyAIfoC4ujgpukiJ/0sKLWI8OO7Hu3uPu3eA0CgzzeqKAZpeNVOJuJjJroqBVwQxgn6MIiozy5iTpCQ8x9c9fHy9i/Is73N/jl41ZzHAJxLHmGFWiTeIZzarBud94jAryirxOfGESRckfuS64vIb54LDAs8Mm+nUPHGYWCx0sNLBrGhqxNPEEVXTKV/IuKxy3uKslWusdU/+wlBOX1nmOs1hJLCIJUgQoaCGEsqoIkqrToqFFO3HPfxDjl8il0KuEhg5FlCBBtnxg//B726t/NSkmxSKA90vtv0xBgR2gWbdtr+Pbbt5AvifgSu97a80gNlP0uttLXIE9G0DF9dtTdkDLneAwSdDNmVH8tMU8nng/Yy+KQsM3ALBNbe31j5OH4A0dZW8AQ4OgfECZa97vLuns7d/z7T6+wHvFnLY1xAL5wAAAAZiS0dEAP8A/wD/oL2nkwAAAAlwSFlzAAAuIwAALiMBeKU/dgAAAAd0SU1FB+cKHRIqDrgngEgAAAAZdEVYdENvbW1lbnQAQ3JlYXRlZCB3aXRoIEdJTVBXgQ4XAAADqklEQVRYw+2X2UorSRjHf93pGLOJJnSwMYNk0XA4clAhiCII4rV66yv4DsK8hI8giJjruRZcQFQwRtGjyZCMMVETJcfYZq2eC0mPepZZmQvJBwVd9dXSv/r+tUnJZNLgnZjMO7I2TBvm/4DR9ad3AVKvN96xzCTpdXprkiSZCaBYLHJ5efm3BtR1nXQ6TbVaNcuur68pFAr/GkYB+PKlTCJxQaPRMB2GAV1dTj59imCxyNzf37O9vY3X6yWTyTA6Okpvby/Ly8ssLS2ZgH86oKKws7PD1NQUmqYBUKlUODw8ZHZ29r+B+fAhzMePkVdRisV+QYgmYBCLxZibm0NVVSKRCMfHxwQCAYQQnJ6eoigK4XAYIQTn5+fUajUCgQBut5ubmxtyuRwdHR0MDAzgcDhoNpucnJxgGAbd3d1UKhUSiQRWq5XBwUEajQYXFxfU63VCoRCFQoG7uzt6e3vJ5XJEIhGcTufXMjMMsFoVOjttZrLZbOZs12o1stksqqoC0NPTw8TEBAClUgmbzcbGxgaFQoHd3V2KxSKaprG6ukq5XGZ9fZ2BgQFyuRy1Ws2UWyqVor+/H6vVytnZGZqmkUgkSKVSbG1tUa1W8fl8xGIxVFVlc3OTfD5PrVbD4XD8s61ZkiQM4/Wtp5X3er0Eg0F8Ph+NRoODgwPzRx8fH8lms7hcLpxOJ9PT09jtdoQQrK2t4fF4cLlcSJLE0NAQHo+HYDBIOp0mmUyiaRqappHJZJBlmfn5eVZWVhgeHv6mrP8SjNVqJRwOk81mAbi8vGR/f/+5A1k2gQH8fj+hUIhoNMrMzAyqqvL4+IgQgmQyia7ryLLMwsICx8fHFAoFJEkyo1YqlfB6vXg8HnRdR9d1XC4XsixzdXXF+Pg48Xj8mzCWxcXFnyuVKvV6E7u9E11/QtefeHqq8Pnzr/T1+VAUhUAgQDweJ5/Pk8/nGRkZ4fb2lmKxiM/nI51OY7FYiEajxONxbm5ucDgc+P1+FEUxQXp6ekilUiiKQldXF6lUinK5DMD9/T0PDw+MjY3R19fH0dER2WyWyclJHh4eOD8/JxqNsre3h6qquN1uE0QIgXR0lDAAksnfqNcbr0jdbieh0E9fzX5LZq38j75fSvWt/3vWavej/t/Kvl5vPMM4HPZ3cQNQhPj+c6bZbL6aBUmSzCi1QvvSZxgGFovFPKeet/XnddXyCyGe9W3W+6PsZd1W/y/XZatuq+1LE0IgtR9nbZg2TBvmXdjv84fSgmAu9E0AAAAASUVORK5CYII=",
		"iconw" : 51,
		"iconh" : 23
	});

	CheckboxObject.editProperties = {
		"id" : { "type" : "string",
				 "default" : "",
				 "getValue" : function (self) { return self.props.elementId; },
				 "setValue" : function (self,v) { if ( v=='')v=false;self.props.elementId=v;}},
		"name" : { "type" : "string",
				   "default" : "",
				   "getValue" : function (self){return self.props.name;},
				   "setValue" : function (self,v){ self.props.name =v; }},
		"top" : { "type" : "number",
				  "default" : 0,
				  "getValue" : function (self) { return self.props.top;},
				  "setValue" : function (self,v) {
					  v = parseInt(v);
					  if ( typeof self.visualcontrol != 'undefined' ) {
						  self.visualcontrol.top = v;
						  self.visualcontrol.elements.container.style.top = v+'px';
					  }
					  self.setTop(v);
				  }},
		"left" : { "type" : "number",
				   "default" : 0,
				   "getValue" : function(self) { return self.props.left;},
				   "setValue" : function(self,v){
					   v = parseInt(v);
					   if ( typeof self.visualcontrol != 'undefined' ) {
						   self.visualcontrol.left = v;
						   self.visualcontrol.elements.container.style.left = v+'px';
					   }
					   self.setLeft(v);
				   }},
		"width" : { "type" : "number",
					"default" : 0,
					"getValue" : function(self) {return self.props.width; },
					"setValue" : function(self,v) {
						v = parseInt(v);
						if ( typeof self.visualcontrol != 'undefined' ) {
							self.visualcontrol.width = v;
							self.visualcontrol.elements.container.style.width = v+'px';
							self.visualcontrol.elements.vail.style.width = v+'px';
							self.visualcontrol.elements.itemcontainer.style.width = v+'px';
						}
						self.setWidth(v);
					}},
		"height" : { "type" : "number",
					 "default" : 0,
					 "getValue" : function(self) { return self.props.height;},
					 "setValue" : function(self,v) {
						 v = parseInt(v);
						 if ( typeof self.visualcontrol != 'undefined' ) {
							 self.visualcontrol.height = v;
							 self.visualcontrol.elements.container.style.height = v+'px';
							 self.visualcontrol.elements.vail.style.height = v+'px';
							 self.visualcontrol.elements.itemcontainer.style.height = v+'px';
						 }
						 self.setHeight(v);
					 }},
		"label" : { "type" : "string",
					"default" : "",
					"getValue" : function(self) {return self.elements.label.innerText; },
					"setValue" : function(self,v) {
						if ( typeof self.visualcontrol != 'undefined' ) {
							self.visualcontrol.elements.itemcontainer.children[1].innerText = v;
						}
						self.setLabel(v);
					}},
		"checked" : { "type" : "boolean",
					  "default" : false,
					  "getValue" : function(self) { return self.elements.checkbox.checked; },
					  "setValue" : function(self,v){
						  if ( typeof self.visualcontrol != 'undefined' ) {
							  self.visualcontrol.elements.itemcontainer.children[0].checked = v == true;
						  }
						  self.setValue(v);
					  }},
		"tool tip" : { "type" : "string",
					   "default" : "",
					   "getValue" : function(self){var t=self.elements.container.getAttribute('title');if(t!==null)return t;return '';},
					   "setValue" : function(self,v){
						   if (v!=='')
							   self.elements.container.setAttribute('title',v);
						   else
							   self.elements.container.removeAttribute('title');
					   }}
	};

	/**
	 * List with all checkbox objects.
	 * @type {Array}
	 * @public
	 */
	CheckboxObject.list = [];

	CheckboxObject.onready = [];
	CheckboxObject.onload = [];

	/**
	 * Associative array for checkbox object's default classnames.
	 * @public
	 * @type {Object}
	 */
	CheckboxObject.classNames = {};

	/**
	 * Default class name for checkbox container element.
	 * @public
	 * @type {String}
	 */
	CheckboxObject.classNames.container = 'wd-checkbox';

	/**
	 * Default class name for checkbox element.
	 * @public
	 * @type {String}
	 */
	CheckboxObject.classNames.checkbox = 'wd-checkbox-checkbox';

	/**
	 * Default class name for checkbox's label.
	 * @public
	 * @type {String}
	 */
	CheckboxObject.classNames.label = 'wd-checkbox-label';

	/**
	 * Function to control checkbox elements of CheckboxObject objects change events.
	 * @param {Event} e - The event object.
	 */
	function checkboxCNG ( e ) {
		var self = lvzwebdesktop(e.target);
		self.value = self.elements.checkbox.checked;
		self.fireEvent('change',{"target":self,
								 "value" : self.value
								});
	}

	/**
	 * Constructor for creating radio objects.
	 * @constructor
	 * @param {HTMLElement} [e] - Element to create the radio object from.
	 */
	var RadioObject = function ( e ) {

		lvzwebdesktop.basicObject.call(this, 'radio' );

		// Add common methods
		lvzwebdesktop.addMethods.call(this,"setTop,setLeft,setWidth,setHeight");

		/**
		 * Method for setting the label of the radio button.
		 * @param {String} v - The label to be set.
		 * @return {RadioObject} This object.
		 */
		this.setLabel = function ( v ) {
			this.elements.label.innerText = v;
			return this.fireEvent('labelchange',{"target":this});
		};

		/**
		 * Method for setting the value of the radio button.
		 * @param {String} v - The value to be set.
		 * @return {RadioObject} This object.
		 */
		this.setValue = function ( v ) {
			this.elements.radio.value = v;
			this.value = v;
			return this.fireEvent('change',{"target":this});
		};

		/**
		 * Method for setting the radio element of this object to checked.
		 * @return {RadioObject} This object.
		 */
		this.check = function () {
			this.elements.radio.checked = true;
			return this.fireEvent('check',{"target":this});
		};

		/**
		 * Array for storing event functions for when there is a change in value.
		 * @public
		 * @type {Array}
		 */
		this.onchange = [];

		/**
		 * Array for storing event functions for when the label of the object has changed.
		 * @public
		 * @type {Array}
		 */
		this.onlabelchange = [];

		/**
		 * Array for storing event functions for when the radio element is checked.
		 * @public
		 * @type {Array}
		 */
		this.oncheck = [];

		/**
		 * Property for the radio object's value.
		 * @public
		 * @type {string}
		 */
		this.value = false;

		/**
		 * The name of the radio button.
		 * @public
		 * @type {String}
		 */
		this.props.name = null;

		/**
		 * Set if this object can be resized during edit.
		 * @public
		 * @type {boolean}
		 */
		this.props.resizableOnEdit = true;

		/**
		 * Property to store the vertical position of the radio
		 * button. The value can be a number for absolute position or
		 * false for static position.
		 * @public
		 * @type {false|Number}
		 */
		this.props.top = false;

		/**
		 * Property to store the horizontal position of the radio
		 * button. The value can be a number for absolute position or
		 * false for static position.
		 * @public
		 * @type {false|Number}
		 */
		this.props.left = false;

		/**
		 * The radio button's width in number of pixels.
		 * @public
		 * @type {Number}
		 */
		this.props.width = 0;

		/**
		 * The radio button's height in number of pixels.
		 * @public
		 * @type {Number}
		 */
		this.props.height = 0;

		/**
		 * The radio group object that this radio object belongs to or null if doesn't belong to any radio group object.
		 * @public
		 * @type {null|RadiogroupObject}
		 */
		this.group = null;

		/**
		 * CSS classname of the container element.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.container = RadioObject.classNames.container;

		/**
		 * CSS classname of the radio element.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.radio = RadioObject.classNames.radio;

		/**
		 * CSS classname of the label element.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.label = RadioObject.classNames.label;

		var cntel = undefined;
		var radel = undefined;
		var lblel = undefined;
		var attr;

		if ( typeof e == 'object' && e instanceof HTMLElement ) {
			cntel = e;
			radel = cntel.querySelector('input[type=radio]');
			if ( radel === null) {
				radel = undefined;
			}
			else {
				this.value = radel.value;
			}
			lblel = cntel.querySelector('span');
			if ( lblel === null ) lblel = undefined;
		}

		/**
		 * Container element of the radiobutton object.
		 * @public
		 * @type {HTMLLabelElement}
		 */
		this.elements.container = lvzwebdesktop.createBasicElement.call(this,'label',cntel,[
			this.props.classNames.container
		]);

		/**
		 * The radio button element.
		 * @public
		 * @type {HTMLInputElement}
		 */
		this.elements.radio = lvzwebdesktop.createBasicElement.call(this,'input',radel,[
			this.props.classNames.radio
		]);
		this.elements.radio.type = 'radio';
		this.elements.radio.addEventListener('change',radioCNG);
		this.elements.container.appendChild(this.elements.radio);

		/**
		 * The label element of the radio button.
		 * @public
		 * @type {HTMLSpanElement}
		 */
		this.elements.label = lvzwebdesktop.createBasicElement.call(this,'span',lblel,[
			this.props.classNames.label
		]);
		this.elements.container.appendChild(this.elements.label);

		lvzwebdesktop.endBasicObject.call(this);
	};

	window.lvzwebdesktop.windowVisualControls.push({
		"constructor" : RadioObject,
		"name" : "radiobutton",
		"icon" : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADMAAAAXCAYAAACmnHcKAAABhGlDQ1BJQ0MgcHJvZmlsZQAAKJF9kT1Iw0AcxV9TpVIqgnYQUQhSneyiIo61CkWoEGqFVh1MLv2CJg1Ji4uj4Fpw8GOx6uDirKuDqyAIfoC4ujgpukiJ/0sKLWI8OO7Hu3uPu3eA0CgzzeqKAZpeNVOJuJjJroqBVwQxgn6MIiozy5iTpCQ8x9c9fHy9i/Is73N/jl41ZzHAJxLHmGFWiTeIZzarBud94jAryirxOfGESRckfuS64vIb54LDAs8Mm+nUPHGYWCx0sNLBrGhqxNPEEVXTKV/IuKxy3uKslWusdU/+wlBOX1nmOs1hJLCIJUgQoaCGEsqoIkqrToqFFO3HPfxDjl8il0KuEhg5FlCBBtnxg//B726t/NSkmxSKA90vtv0xBgR2gWbdtr+Pbbt5AvifgSu97a80gNlP0uttLXIE9G0DF9dtTdkDLneAwSdDNmVH8tMU8nng/Yy+KQsM3ALBNbe31j5OH4A0dZW8AQ4OgfECZa97vLuns7d/z7T6+wHvFnLY1xAL5wAAAAZiS0dEAP8A/wD/oL2nkwAAAAlwSFlzAAAuIwAALiMBeKU/dgAAAAd0SU1FB+cKHRIpN8wPW4MAAAAZdEVYdENvbW1lbnQAQ3JlYXRlZCB3aXRoIEdJTVBXgQ4XAAADnklEQVRYw+2Xy08iWRTGf5cCUTFIiw+iwcgkKAaNihnZ9Nq/2MTdLNwao8QHWFKKE6CMCNUUU1QVrzsLx0K62870ZpJ2vMs6X53zffeex71C0zTJO1k+3tH6EPMh5r8Q027b70JIt9vDpyjKuxAjhMAXDI69CzF+vzKsGSklpvkXjcYXOp2uB2o2m5RKJe7u7qhWq0j5/bEkpeT09JRyuYxlWViW9V1cr9fj6OgIwzDeJGbbNp1OBwDTNH+uAQwGAy4ubnh8NBDCz+lpgUajCUAoFOLg4IDFxUWOj4+pVCoMBgMcx/ECSilxXZfJyUlM08Q0TdrtNlJKHMfBdd1XO+hHCIFpmjiO42H6/T6u69Lv9zk/P0fTNGzb5vDw0NsY13VxXXfEr+M4Q98A9foXotEonz//DkAy+RuHh3/w6VMYRVGwbZtyuUy9XkdRFAzDQNd1dF0nm81yfX1Np9OhXC6zsbHhnaCu69i2Tb1eJ5PJEIvFvMBXV1ecnZ2RTqcpFousr6+jqiqJRIJms4njOEQiERqNBrVajVqtRrFYRAjB8vIyqqrSarXo9/vs7+8zNzf3fDKtlkUiEfcChcNTTEyMeykVCoWIxWIEAgFmZ2exLIv7+3ts26ZaraKqKplMhu3tbYQQRKNRhBDkcjl2dnZIpVKUSqWRlNjc3GRvb4+LiwsWFhYQQjA7O+v9v7S0xPz8PHNzc8TjcU5OTshms6TTaXK5HCsrK2QyGXZ3d3l8fBymWTg8haaVPPLNpoltOwghAFAUhampKTY3NykWi9ze3rK1tcX8/DwA09PTtFotKpXKCOFoNEqz2cQwDCKRyIitXq9Tq9WIxWIoioLrutRqNa8zWZaFruv4fD6q1SozMzM8PDxgmiZLS0ser5GOpmmaHAwk+byGoviJRiNo2p+kUitEImGenp4olUosLy8TDofJ5/PE43HK5bKX/4lEAlVVCQaD9Ho9xsfHcRyHtbU1CoUCwWCQZDKJz+ej2+1yeXnJ2NgY3W6XjY0Ner0e+XyeYDBIIBBgcXGRQqFAMpnEMAwsy2J1dZVCofBPGSS5ublBURSEEEgpSaVSz2Keixja7Ta9Xp/JyQkCAf+v156HQwdCoclfe9bYtsPExPh3jYPB4Nte7vP9K7uUIOXAq4GXdHipyyFOjsyuF+xr/19jX3MYXme6+N8S8jXxn7ULAUL4vrlyvBD90be3/P8IGwgEPp4AH2I+xPyfxfwNFQLbFg/smNsAAAAASUVORK5CYII=",
		"iconw" : 51,
		"iconh" : 23
	});

	RadioObject.editProperties = {
		"id" : { "type" : "string",
				 "default" : "",
				 "getValue" : function (self) { return self.props.elementId; },
				 "setValue" : function (self,v) { if ( v=='')v=false;self.props.elementId=v;}},
		"name" : { "type" : "string",
				   "default" : "",
				   "getValue" : function (self){return self.props.name;},
				   "setValue" : function (self,v){ self.props.name =v; }},
		"top" : { "type" : "number",
				  "default" : 0,
				  "getValue" : function (self) { return self.props.top;},
				  "setValue" : function (self,v) {
					  v = parseInt(v);
					  if ( typeof self.visualcontrol != 'undefined' ) {
						  self.visualcontrol.top = v;
						  self.visualcontrol.elements.container.style.top = v+'px';
					  }
					  self.setTop(v);
				  }},
		"left" : { "type" : "number",
				   "default" : 0,
				   "getValue" : function(self) { return self.props.left;},
				   "setValue" : function(self,v){
					   v = parseInt(v);
					   if ( typeof self.visualcontrol != 'undefined' ) {
						   self.visualcontrol.left = v;
						   self.visualcontrol.elements.container.style.left = v+'px';
					   }
					   self.setLeft(v);
				   }},
		"width" : { "type" : "number",
					"default" : 0,
					"getValue" : function(self) {return self.props.width; },
					"setValue" : function(self,v) {
						v = parseInt(v);
						if ( typeof self.visualcontrol != 'undefined' ) {
							self.visualcontrol.width = v;
							self.visualcontrol.elements.container.style.width = v+'px';
							self.visualcontrol.elements.vail.style.width = v+'px';
							self.visualcontrol.elements.itemcontainer.style.width = v+'px';
						}
						self.setWidth(v);
					}},
		"height" : { "type" : "number",
					 "default" : 0,
					 "getValue" : function(self) { return self.props.height;},
					 "setValue" : function(self,v) {
						 v = parseInt(v);
						 if ( typeof self.visualcontrol != 'undefined' ) {
							 self.visualcontrol.height = v;
							 self.visualcontrol.elements.container.style.height = v+'px';
							 self.visualcontrol.elements.vail.style.height = v+'px';
							 self.visualcontrol.elements.itemcontainer.style.height = v+'px';
						 }
						 self.setHeight(v);
					 }},
		"label" : { "type" : "string",
					"default" : "",
					"getValue" : function(self) {return self.elements.label.innerText; },
					"setValue" : function(self,v) {
						if ( typeof self.visualcontrol != 'undefined' ) {
							self.visualcontrol.elements.itemcontainer.children[1].innerText = v;
						}
						self.setLabel(v);
					}},
		"checked" : { "type" : "boolean",
					  "default" : false,
					  "getValue" : function(self) { return self.elements.radio.checked; },
					  "setValue" : function(self,v){
						  if ( typeof self.visualcontrol != 'undefined' ) {
							  self.visualcontrol.elements.itemcontainer.children[0].checked = v == true;
						  }
						  self.setValue(v);
					  }},
		"tool tip" : { "type" : "string",
					   "default" : "",
					   "getValue" : function(self){var t=self.elements.container.getAttribute('title');if(t!==null)return t;return '';},
					   "setValue" : function(self,v){
						   if (v!=='')
							   self.elements.container.setAttribute('title',v);
						   else
							   self.elements.container.removeAttribute('title');
					   }}
	};

	/**
	 * List with all radio objects.
	 * @type {Array}
	 * @public
	 */
	RadioObject.list = [];

	RadioObject.onready = [];
	RadioObject.onload = [];

	/**
	 * Associative array for radiobutton's default class names.
	 * @public
	 * @type {Object}
	 */
	RadioObject.classNames = {};

	/**
	 * Default class name for radio container element.
	 * @public
	 * @type {String}
	 */
	RadioObject.classNames.container = 'wd-radio';

	/**
	 * Default classname for radio object's radio element.
	 * @public
	 * @type {String}
	 */
	RadioObject.classNames.radio = 'wd-radio-radio';

	/**
	 * Default classname for radio object's label element.
	 * @public
	 * @type {String}
	 */
	RadioObject.classNames.label = 'wd-radio-label';

	/**
	 * Function to control radio elements of RadioObjects change event.
	 * @param {Event} e - The event object.
	 */
	function radioCNG ( e ) {
		var self = lvzwebdesktop(e.target);
		if ( e.target.checked == true ) {
			self.check();
			if ( self.group !== null ) self.group.setValue(self);
		}
		self.fireEvent('check',{"target":self,
								"value" :self.value});
	}

	/**
	 * Constructor for creating radio button group objects.
	 * @constructor
	 * @param {HTMLElement} [e] - Element to attach radio group object to.
	 */
	var RadiogroupObject = function ( e ) {

		lvzwebdesktop.basicObject.call(this, 'radiogroup' );

		// Add common methods
		lvzwebdesktop.addMethods.call(this,"setTop,setLeft,setWidth,setHeight");

		/**
		 * Method for generating name for the radiogroup object.
		 * @return {String} The generated name.
		 */
		this.createName = function () { return window.lvzwebdesktop.generateUniqueName(this.props.namePrefix); };

		/**
		 * Method for adding a radioobject item to the radiogroup.
		 * Fires the "itemadded" event.
		 * @param {RadioObject} item - The radioobject to add.
		 * @param {Number|null} [order] - The order to add the new
		 * item. If its null or not set, then the item will be added
		 * at the end of the group.
		 * @return {RadiogroupObject} This object.
		 */
		this.addItem = function ( item, order ) {
			if ( ! item instanceof RadioObject ) {
				console.warn("Item has to be RadioObject.");
				return this;
			}

			if ( item.group !== null ) {
				console.warn("Item must not belong to group");
				return this;
			}

			var o = null;
			if ( typeof order != 'undefined' && order !== null && typeof this.elements.container.children[order] != 'undefined' ) o = this.elements.container.children[order];

			this.elements.container.insertBefore(item.elements.container, o );

			if ( this.name === null ) this.name = this.createName();

			item.group = this;
			item.props.name = this.name;
			item.elements.radio.name = this.name;

			var eventData = { "target" : this,
							  "item" : item,
							  "order" : order };

			return this.fireEvent('itemadded', eventData);
		};

		/**
		 * Method for removing radioobject items from the radiogroup.
		 * Fires the "itemremoved" event.
		 * @param {RadioObject} item - The radio object to remove.
		 * @return {RadiogroupObject} This object.
		 */
		this.removeItem = function ( item ) {
			if ( ! item instanceof RadioObject ) {
				console.warn ( "Item has to be a RadioObject." );
				return this;
			}

			if ( item.group !== this ) {
				console.warn ( "Item does not belong to this radio group." );
				return this;
			}

			item.group = null;
			item.props.name = null;
			item.elements.radio.name = '';
			item.elements.container.remove();
			var eventData = { "target" : this,
							  "item" : item
							};
			return this.fireEvent('itemremoved',eventData);
		};

		/**
		 * Method for getting a radio object item by its order in this radio group.
		 * @param {Number} byOrder - The order number of the item to get.
		 * @return {null|RadioObject} Return the radio object item of
		 * the give order number. If there is no item in that order
		 * number or in any other case, return null.
		 */
		this.getItem = function ( byOrder ) {

			if ( typeof byOrder != 'number' && ! byOrder instanceof Number ) {
				console.error ( "Argument must be the number of the order of the RadioObject item." );
				return null;
			}

			byOrder = parseInt(byOrder)

			if ( byOrder < 0 || byOrder >= this.elements.container.children.length) return null;

			return lvzwebdesktop(this.elements.container.children[byOrder]);
		};

		/**
		 * Method for setting the value of the radiogroup.
		 * @param {String|RadioObject} v - A string to set as value of the group or the radio object to take its value.
		 * @return {RadiogroupObject} This object.
		 */
		this.setValue = function ( v ) {
			if ( typeof v == 'object' ) {
				if ( v instanceof RadioObject ) {
					if ( v.group !== this ) {
						console.warn ("This radio doesn't belong to this group.");
						return this;
					}
					this.value = v.elements.radio.value;
				}
			}
			else if ( typeof v == 'string' ) {
				this.value = v;
				var o = this.elements.container.firstElementChild;
				var itm;
				while ( o!==null ) {
					itm = lvzwebdesktop(o);
					if ( itm.elements.radio.value === this.value ) itm.elements.radio.checked = true;
					else itm.elements.radio.checked = false;
					o=o.nextElementSibling;
				}
			}
			return this.fireEvent('change',{"target":this,
											"value":this.value});
		};

		/**
		 * Array for storing event functions for when moving the radiogroup.
		 * @public
		 * @type {Array}
		 */
		this.onmove = [];

		/**
		 * Array for storing event functions for when resizing the radiogroup.
		 * @public
		 * @type {Array}
		 */
		this.onresize = [];

		/**
		 * Array for storing event functions for after an item is added to the group.
		 * @public
		 * @type {Array}
		 */
		this.onitemadded = [];

		/**
		 * Array for storing event functions for after an item has been removed from the group.
		 * @public
		 * @type {Array}
		 */
		this.onitemremoved = [];

		/**
		 * Array for storing event functions for when changing the value of this radio group object.
		 * @public
		 * @type {Array}
		 */
		this.onchange = [];

		/**
		 * Property for radiogroup's value. that is the value of the selected radio button.
		 * @public
		 * @type {String|null}
		 */
		this.value = null;

		/**
		 * The name to be used for the radiobuttons of the radio group.
		 * @public
		 * @type {null|String}
		 */
		this.name = null;

		/**
		 * A string prefix to be used for the name generator.
		 * @public
		 * @type {String}
		 */
		this.props.namePrefix = 'wdrd_';

		/**
		 * Set if this object can be resized during edit.
		 * @public
		 * @type {boolean}
		 */
		this.props.resizableOnEdit = true;

		/**
		 * Set that when this object is selected, inserting objects of
		 * the specified type will be inserted in this object.
		 * @public
		 * @type {false|true|String}
		 */
		this.props.addChildrenOnEdit = 'radio';

		/**
		 * Property to store the vertical position of the
		 * radiogroup. The value can be a number for absolute position
		 * or boolean false for static.
		 * @public
		 * @type {false|Number}
		 */
		this.props.top = false;

		/**
		 * Property to store the horizontal position of the
		 * radiogroup. The value can be a number for absolute position
		 * or boolean false for static.
		 * @public
		 * @type {false|Number}
		 */
		this.props.left = false;

		/**
		 * The radiogroup's height in number of pixels.
		 * @public
		 * @type {Number}
		 */
		this.props.height = 0;

		/**
		 * The radiogroup's width in number of pixels.
		 * @public
		 * @type {Number}
		 */
		this.props.width = 0;

		/**
		 * CSS classname of the radiogroup object's container element.	
		 * @public
		 * @type {String}
		 */
		this.props.classNames.container = RadiogroupObject.classNames.container;

		/**
		 * Container element of the radiogroup object.
		 * @public
		 * @type {HTMLDivElement}
		 */
		this.elements.container = lvzwebdesktop.createBasicElement.call(this,'div',e,[
			this.props.classNames.container
		]);

		lvzwebdesktop.endBasicObject.call(this);
	};

	window.lvzwebdesktop.windowVisualControls.push({
		"constructor" : RadiogroupObject
	});

	RadiogroupObject.editProperties = {
		"id" : { "type" : "string",
				 "default" : "",
				 "getValue" : function (self) { return self.props.elementId; },
				 "setValue" : function (self,v) { if ( v=='')v=false;self.props.elementId=v;}},
		"name" : { "type" : "string",
				   "default" : "",
				   "getValue" : function (self){return self.props.name;},
				   "setValue" : function (self,v){ self.props.name =v; }},
		"top" : { "type" : "number",
				  "default" : 0,
				  "getValue" : function (self) { return self.props.top;},
				  "setValue" : function (self,v) {
					  v = parseInt(v);
					  if ( typeof self.visualcontrol != 'undefined' ) {
						  self.visualcontrol.top = v;
						  self.visualcontrol.elements.container.style.top = v+'px';
					  }
					  self.setTop(v);
				  }},
		"left" : { "type" : "number",
				   "default" : 0,
				   "getValue" : function(self) { return self.props.left;},
				   "setValue" : function(self,v){
					   v = parseInt(v);
					   if ( typeof self.visualcontrol != 'undefined' ) {
						   self.visualcontrol.left = v;
						   self.visualcontrol.elements.container.style.left = v+'px';
					   }
					   self.setLeft(v);
				   }},
		"width" : { "type" : "number",
					"default" : 0,
					"getValue" : function(self) {return self.props.width; },
					"setValue" : function(self,v) {
						v = parseInt(v);
						if ( typeof self.visualcontrol != 'undefined' ) {
							self.visualcontrol.width = v;
							self.visualcontrol.elements.container.style.width = v+'px';
							self.visualcontrol.elements.vail.style.width = v+'px';
							self.visualcontrol.elements.itemcontainer.style.width = v+'px';
						}
						self.setWidth(v);
					}},
		"height" : { "type" : "number",
					 "default" : 0,
					 "getValue" : function(self) { return self.props.height;},
					 "setValue" : function(self,v) {
						 v = parseInt(v);
						 if ( typeof self.visualcontrol != 'undefined' ) {
							 self.visualcontrol.height = v;
							 self.visualcontrol.elements.container.style.height = v+'px';
							 self.visualcontrol.elements.vail.style.height = v+'px';
							 self.visualcontrol.elements.itemcontainer.style.height = v+'px';
						 }
						 self.setHeight(v);
					 }},
		"tool tip" : { "type" : "string",
					   "default" : "",
					   "getValue" : function(self){var t=self.elements.container.getAttribute('title');if(t!==null)return t;return '';},
					   "setValue" : function(self,v){
						   if (v!=='')
							   self.elements.container.setAttribute('title',v);
						   else
							   self.elements.container.removeAttribute('title');
					   }}
	};

	/**
	 * List with all radio group objects.
	 * @type {Array}
	 * @public
	 */
	RadiogroupObject.list = [];

	RadiogroupObject.onready = [];
	RadiogroupObject.onload = [];

	/**
	 * Method for getting the first radiogroup object having the given name.
	 * @param {String} name - The name to search for.
	 * @return {null|RadiogroupObject} The radiogroup object. If none found with the given name then will return null.
	 */
	RadiogroupObject.getFirstWithName = function ( name ) {
		for ( var i=0;i<RadiogroupObject.list.length; i++ )
			if ( RadiogroupObject.list[i].name == name ) return RadiogroupObject.list[i];
		return null;
	};

	/**
	 * Associative array for radiogroup objects default classnames.
	 * @public
	 * @type {Object}
	 */
	RadiogroupObject.classNames = {};

	/**
	 * Default classname for radio groups container elements.
	 * @public
	 * @type {String}
	 */
	RadiogroupObject.classNames.container = 'wd-radio-group';

	/**
	 * Constructor for creating fieldset objects.
	 * @constructor
	 * @param {HTMLElement} [e] - Element to attach fieldset object to.
	 */
	var FieldsetObject = function ( e ) {

		lvzwebdesktop.basicObject.call(this,'fieldset');

		/**
		 * Method for setting the label of the fieldset.
		 * Fires the "labelchange" event.
		 * @param {String} v - The label to be set.
		 * @return {FieldsetObject} This object.
		 */
		this.setLabel = function ( v ) {

			var eventData = { "target" : this,
							  "oldLabel" : this.elements.label.innerText,
							  "newLabel" : v };
			this.fireEvent('labelchange', eventData );
			if ( eventData.defaultPrevented === true ) return this;

			this.elements.label.innerText = v;

			return this;
		};

		/**
		 * Method for attaching lvzwebdesktop objects or html elements
		 * to the fieldset.
		 * Fires the "iteattached" event on this object and "attachto"
		 * event on the attached object if is an lvzwebdesktop object.
		 * @param {HTMLElement|lvzwebdesktopObject} item - Html element or an lvzwebdesktop object to be attached.
		 * @return {FieldsetObject} This object.
		 */
		this.attachItem = function ( item ) {
			var cont;
			var eventData1 = { "target" : this };
			var eventData2 = { "attachTo" : this };

			if ( item instanceof HTMLElement ) {
				cont = item;
				eventData1.item = item;
			}
			else {
				var c = lvzwebdesktop.isPartOf( item );
				if ( c === false ) {
					console.warn( "Argument must be html element or an lvzwebdesktop object." );
					return this;
				}
				eventData2.target = item;
				eventData1.item = item;
				cont = item.elements.container;
				item.fireEvent('attachto',eventData2);
			}

			this.elements.container.appendChild ( cont );
			this.fireEvent('itemattached', eventData1);

			return this;
		};

		/**
		 * Method for enabling the fieldset.
		 * Fires the "enablestatechange" and "enable" events.
		 * @return {FieldsetObject} This object.
		 */
		this.enable = function ( ) {
			var eventData = { "target" : this,
							  "currentState" : this.elements.container.disabled === true ? 'disabled' : 'enabled',
							  'newState' : 'enabled'
							};

			this.fireEvent('enablestatechange',eventData);
			if ( eventData.defaultPrevented === true ) return this;

			eventData = { "target" : this,
						  "currentState" : this.elements.container.disabled === true ? 'disabled' : 'enabled',
						  'newState' : 'enabled'
						};
			this.fireEvent('enable',eventData);

			if ( eventData.defaultPrevented === true ) return this;

			this.elements.container.disabled = false;
			return this;
		};

		/**
		 * Method for disabling the fieldset.
		 * Fires the "enablestatechange" and "disable" events.
		 * @return {FieldsetObject} This object.
		 */
		this.disable = function ( ) {
			var eventData = { "target" : this,
							  "currentState" : this.elements.container.disabled === true ? 'disabled' : 'enabled',
							  'newState' : 'disabled'
							};

			this.fireEvent('enablestatechange',eventData);
			if ( eventData.defaultPrevented === true ) return this;

			eventData = { "target" : this,
						  "currentState" : this.elements.container.disabled === true ? 'disabled' : 'enabled',
						  'newState' : 'disabled'
						};
			this.fireEvent('disable',eventData);

			if ( eventData.defaultPrevented === true ) return this;

			this.elements.container.disabled = true;
			return this;
		};

		/**
		 * Method for getting the enabled state of the fieldset.
		 * @return {boolean} Returns true if the fieldset is disabled or false if it's enabled.
		 */
		this.isDisabled = function () {
			return this.elements.container.disabled;
		};

		/**
		 * Method for getting the enabled state of the fieldset.
		 * @return {boolean} Returns true if the fieldset is enabled or false if it's disabled.
		 */
		this.isEnabled = function () {
			return !this.elements.container.disabled;
		};

		/**
		 * Array for storing event functions for when setting of changing the label of the fieldset.
		 * @public
		 * @type {Array}
		 */
		this.onlabelchange = [];

		/**
		 * Array for storing event functions for when items have been attached.
		 * @public
		 * @type {Array}
		 */
		this.onitemattached = [];

		/**
		 * Array for storing event function for when enabling fieldset.
		 * @public
		 * @type {Array}
		 */
		this.onenable = [];

		/**
		 * Array for storing event functions for when enabling or disabling fieldset.
		 * @public
		 * @type {Array}
		 */
		this.onenablestatechange = [];

		/**
		 * Array for storing event functions for when disabling fieldset.
		 * @public
		 * @type {Array}
		 */
		this.ondisable = [];

		/**
		 * Set if this object can be resized during edit.
		 * @public
		 * @type {boolean}
		 */
		this.props.resizableOnEdit = true;

		/**
		 * CSS class name of the fieldset object's container element.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.container = FieldsetObject.classNames.container;

		/**
		 * CSS class name for the fieldset object's label element.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.label = FieldsetObject.classNames.label;

		/**
		 * Container element of the fieldset object.
		 * @public
		 * @type {HTMLFieldSetElement}
		 */
		this.elements.container = lvzwebdesktop.createBasicElement.call(this,'fieldset',e,[
			this.props.classNames.container
		]);

		var lblel = this.elements.container.querySelector('legend');
		if ( lblel === null ) lblel = undefined;
		/**
		 * Label element of the fieldset object.
		 * @public
		 * @type {HTMLLegendElement}
		 */
		this.elements.label = lvzwebdesktop.createBasicElement.call(this,'legend',lblel,[
			this.props.classNames.label
		]);
		this.elements.container.insertBefore(this.elements.label,this.elements.container.firstElementChild);

		lvzwebdesktop.endBasicObject.call(this);
	};

	window.lvzwebdesktop.windowVisualControls.push({
		"constructor" : FieldsetObject
	});

	/**
	 * List with all fieldset objects.
	 * @type {Array}
	 * @public
	 */
	FieldsetObject.list = [];

	FieldsetObject.onready = [];
	FieldsetObject.onload = [];

	/**
	 * Associative array for fieldset objects default class names.
	 * @public
	 * @type {Object}
	 */
	FieldsetObject.classNames = {};

	/**
	 * Default classname for container elements of fieldset objects.
	 * @public
	 * @type {String}
	 */
	FieldsetObject.classNames.container = 'wd-fieldset';

	/**
	 * Default classname for label elements of fieldset objects.
	 * @public
	 * @type {String}
	 */
	FieldsetObject.classNames.label = 'wd-fieldset-label';

	/**
	 * Constructor for creating scrollbar objects.
	 * @constructor
	 * @param {HTMLElement} [e] - Element to attach scrollbar object to.
	 */
	var ScrollbarObject = function ( e ) {

		lvzwebdesktop.basicObject.call( this, 'scrollbar' );

		/**
		 * Array for storing event functions for when the value on the scrollbar has changed.
		 * @public
		 * @type {Array}
		 */
		this.onchange = [];

		/**
		 * Array for storing event functions for when scrolling the scrollbar.
		 * @public
		 * @type {Array}
		 */
		this.onscroll = [];

		/**
		 * Array for storing event functions for when canceling scrollbar change.
		 * @public
		 * @type {Array}
		 */
		this.oncancelchange = [];

		/**
		 * Property for keeping the scrollbar's scroll value.
		 * @public
		 * @type {Number}
		 */
		this.props.value = 0;

		/**
		 * Property to set the value change step of the scrollbar.
		 * @public
		 * @type {Number}
		 */
		this.props.step = 1;

		/**
		 * Property to set the minimum change step of the scrollbar.
		 * @public
		 * @type {Number}
		 */
		this.props.stepMin = 1;

		/**
		 * Property to set the maximum change step of the scrollbar.
		 * @public
		 * @type {Number}
		 */
		this.props.stepMax = 10;

		/**
		 * Property to set the change interval delay of each step.
		 * @public
		 * @type {Number}
		 */
		this.props.changeTmr = 100;

		/**
		 * Property for setting the max value that the scrollbar can reach.
		 * @public
		 * @type {false|Number}
		 */
		this.props.max = false;

		/**
		 * Property for setting the minimum value that the scrollbar can reach.
		 * @public
		 * @type {false|Number}
		 */
		this.props.min = 0;

		/**
		 * Property for setting the length of the scrollbar.
		 * @public
		 * @type {false|Number}
		 */
		this.props.length = false;

		/**
		 * Property for setting the scrollbar's width. if this is false and the scrollbar is horizontal then the scrollbar will take the "props.length" value as width.
		 * @public
		 * @type {false|Number}
		 */
		this.props.width = false;

		/**
		 * Property for setting the scrollbar's height. if this is false and the scrollbar is vertical then the scrollbar will take the "props.length" value as height.
		 * @public
		 * @type {false|Number}
		 */
		this.props.height = false;

		/**
		 * Property for defining if scrollbar will be attached to it's parent object.
		 * @public
		 * @type {Boolean}
		 */
		this.props.attachToParent = false;

		/**
		 * Property for defining the scroll length of the scrollbar.
		 * @public
		 * @type {false|Number}
		 */
		this.props.scrollLength = false;

		/**
		 * Property for defining the visible length of the scrollbar.
		 * @public
		 * @type {false|Number}
		 */
		this.props.visibleLength = false;

		/**
		 * Property for setting the attach position of the scrollbar.
		 * @public
		 * @type {false|'top'|'left'|'right'|'bottom'}
		 */
		this.props.attachPosition = false;

		/**
		 * Property for setting if the scrollbar button size will be analog to the scroll area.
		 * @public
		 * @type {Boolean}
		 */
		this.props.analogScrollButton = false;

		/**
		 * Set this object as not resizable during edit.
		 * @public
		 * @type {boolean}
		 */
		this.props.resizableOnEdit = true;

		/**
		 * CSS class-name for scrollbar container element.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.container = ScrollbarObject.classNames.container;

		/**
		 * CSS class-name for vertical scrollbar container element.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.vertical = ScrollbarObject.classNames.vertical;

		/**
		 * CSS class-name for horizontal scrollbar container element.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.horizontal = ScrollbarObject.classNames.horizontal;

		/**
		 * CSS class-name for the buttons of the scrollbar.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.button = ScrollbarObject.classNames.button;

		/**
		 * CSS class-name for the increase button.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.increase = ScrollbarObject.classNames.increase;

		/**
		 * CSS class-name for the decrease button.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.decrease = ScrollbarObject.classNames.decrease;

		/**
		 * Method for setting scrollbars scroll position.
		 * @public
		 * @param {Number} v - The scroll value to be set.
		 * @return {ScrollbarObject} This object.
		 */
		this.setScroll = function ( v ) {

			var ov = this.props.value;

			if ( v < this.props.min )
				this.props.value = this.props.min;
			else if ( v > this.props.max )
				this.props.value = this.props.max;
			else
				this.props.value = v;

			return this.fireEvent('change',{
				"target": this,
				"oldValue" : ov,
				"newValue" : this.props.value
			}).fireEvent('scroll',{
				"target": this,
				"oldValue" : ov,
				"newValue" : this.props.value
			}).refresh();

		};

		/**
		 * Method for changing scrollbar's scroll position in relation to it's current value.
		 * @public
		 * @param {Number} v - The scroll value to add to the current value.
		 * @return {ScrollbarObject} This object.
		 */
		this.scroll = function ( v ) {
			var ov = this.props.value;
			var cv = this.props.value + v;

			if ( cv < this.props.min )
				this.props.value = this.props.min;
			else if ( cv > this.props.max )
				this.props.value = this.props.max;
			else
				this.props.value = cv;

			return this.fireEvent('change',{
				"target": this,
				"oldValue" : ov,
				"newValue" : this.props.value
			}).fireEvent('scroll',{
				"target": this,
				"oldValue" : ov,
				"newValue" : this.props.value
			}).refresh();
		};

		/**
		 * Method for setting scrollbars scroll position without firing the change event.
		 * @public
		 * @param {Number} v - The scroll value to be set.
		 * @return {ScrollbarObject} This object.
		 */
		this.setScrollTemp = function ( v ) {

			var ov = this.props.value;

			if ( v < this.props.min )
				this.props.value = this.props.min;
			else if ( v > this.props.max )
				this.props.value = this.props.max;
			else
				this.props.value = v;

			return this.fireEvent('scroll',{
				"target": this,
				"oldValue" : ov,
				"newValue" : this.props.value
			}).refresh();

		};

		/**
		 * Method for resizing the scrollbar.
		 * @public
		 * @return {ScrollbarObject} This object.
		 */
		this.refresh = function () {

			var cv = this.props.value - this.props.min;
			var l = this.props.max - this.props.min;

			if ( this.elements.container.classList.contains(this.props.classNames.vertical) ) {

				var btnl = this.elements.firstButtonContainer.offsetHeight + this.elements.lastButtonContainer.offsetHeight;

				if ( this.props.analogScrollButton == true ) {
					var sah = this.elements.container.offsetHeight - btnl;
					this.elements.scroll.style.height =  (sah / this.props.scrollLength * this.props.visibleLength) + 'px';
				}

				if ( this.props.height !== false )
					this.elements.container.style.height = this.props.height + 'px';
				else
					this.elements.container.style.height = this.props.length + 'px';

				btnl += this.elements.scroll.offsetHeight;
				var marea = this.elements.container.offsetHeight - btnl;

				if ( typeof this.props.max == 'number' ) var p = marea / this.props.max;
				else var p = 1;

				this.elements.beforeScroll.style.width = this.elements.scroll.offsetWidth + 'px';
				this.elements.beforeScroll.style.height = (this.props.value * p) + 'px';

				this.elements.afterScroll.style.width = this.elements.scroll.offsetWidth + 'px';
				this.elements.afterScroll.style.height = (marea - (this.props.value * p) ) + 'px';
			}
			else if ( this.elements.container.classList.contains(this.props.classNames.horizontal) ) {

				var btnl = this.elements.firstButtonContainer.offsetWidth + this.elements.lastButtonContainer.offsetWidth;

				if ( this.props.analogScrollButton == true ) {
					var sah = this.elements.container.offsetWidth - btnl;
					this.elements.scroll.style.width =  (sah / this.props.scrollLength * this.props.visibleLength) + 'px';
				}

				if ( this.props.width !== false )
					this.elements.container.style.width = this.props.width + 'px';
				else
					this.elements.container.style.width = this.props.length + 'px';

				btnl += this.elements.scroll.offsetWidth;
				var marea = this.elements.container.offsetWidth - btnl;

				if ( typeof this.props.max == 'number' ) var p = marea / this.props.max;
				else var p = 1;

				this.elements.beforeScroll.style.height = this.elements.scroll.offsetHeight + 'px';
				this.elements.beforeScroll.style.width = (this.props.value * p) + 'px';

				this.elements.afterScroll.style.height = this.elements.scroll.offsetHeight + 'px';
				this.elements.afterScroll.style.width = (marea - (this.props.value * p) ) + 'px';
			}
		};

		/**
		 * Scrollbar container element.
		 * @public
		 * @type {HTMLElement}
		 */
		this.elements.container = lvzwebdesktop.createBasicElement.call(this,'div',e,[
			this.props.classNames.container
		]);
		this.elements.container.addEventListener('wheel', ScrollbarSCWL);

		/**
		 * Container for all scrollbar buttons before scroll button.
		 * @public
		 * @type {HTMLElement}
		 */
		this.elements.firstButtonContainer = lvzwebdesktop.createBasicElement.call(this,'div');
		this.elements.firstButtonContainer.style.overflow = 'hidden';
		this.elements.container.appendChild(this.elements.firstButtonContainer);

		/**
		 * Scrollbar decrease button element.
		 * @public
		 * @type {HTMLElement}
		 */
		this.elements.decreaseButton = lvzwebdesktop.createBasicElement.call(this,'div',undefined,[
			this.props.classNames.decrease,
			this.props.classNames.button
		]);
		this.elements.firstButtonContainer.appendChild ( this.elements.decreaseButton ) ;
		this.elements.decreaseButton.addEventListener('click', function ( e ) {
			var self = e.target.dataLVZWDESKTOPOBJ;
			var ov = self.props.value;

			if ( self.props.min !== false ) {
				if ( self.props.min < self.props.value )
					self.props.value-=self.props.step;
			}
			else {
				self.props.value -= self.props.step;
			}

			self.fireEvent('change',{
				"target": self,
				"oldValue" : ov,
				"newValue" : self.props.value
			}).fireEvent('scroll',{
				"target": self,
				"oldValue" : ov,
				"newValue" : self.props.value
			});

			self.refresh();
		});

		/**
		 * Area before scroll button.
		 * @public
		 * @type {HTMLElement}
		 */
		this.elements.beforeScroll = lvzwebdesktop.createBasicElement.call(this,'div');
		this.elements.beforeScroll.addEventListener('mousedown', ScrollbarFDMD );
		this.elements.beforeScroll.dataChange = 'decrease';
		this.elements.container.appendChild ( this.elements.beforeScroll );

		/**
		 * Scrollbar value element.
		 * @public
		 * @type {HTMLElement}
		 */
		this.elements.scroll = lvzwebdesktop.createBasicElement.call(this,'div',undefined,[
			this.props.classNames.button
		]);
		this.elements.container.appendChild ( this.elements.scroll ) ;
		this.elements.scroll.addEventListener ( 'mousedown', ScrollbarSCMD );

		/**
		 * Area after scroll button.
		 * @public
		 * @type {HTMLElement}
		 */
		this.elements.afterScroll = lvzwebdesktop.createBasicElement.call(this,'div');
		this.elements.afterScroll.addEventListener('mousedown', ScrollbarFDMD );
		this.elements.afterScroll.dataChange = 'increase';
		this.elements.container.appendChild ( this.elements.afterScroll );

		/**
		 * Container for all buttons after scroll button.
		 * @public
		 * @type {HTMLElement}
		 */
		this.elements.lastButtonContainer = lvzwebdesktop.createBasicElement.call(this,'div');
		this.elements.lastButtonContainer.style.overflow = 'hidden';
		this.elements.container.appendChild(this.elements.lastButtonContainer);
		/**
		 * Scrollbar increase button element.
		 * @public
		 * @type {HTMLElement}
		 */
		this.elements.increaseButton = lvzwebdesktop.createBasicElement.call(this,'div',undefined,[
			this.props.classNames.increase,
			this.props.classNames.button
		])
		this.elements.lastButtonContainer.appendChild ( this.elements.increaseButton ) ;
		this.elements.increaseButton.addEventListener('click', function ( e ) {
			var self = e.target.dataLVZWDESKTOPOBJ;
			var ov = self.props.value;

			if ( self.props.max !== false ) {
				if ( self.props.max > self.props.value )
					self.props.value+=self.props.step;
			}
			else {
				self.props.value += self.props.step;
			}

			self.fireEvent('change',{
				"target": self,
				"oldValue" : ov,
				"newValue" : self.props.value
			}).fireEvent('scroll',{
				"target": self,
				"oldValue" : ov,
				"newValue" : self.props.value
			});

			self.refresh();
		});

		if ( e ) {
			var tmpattr;

			tmpattr = e.getAttribute('value');
			if ( tmpattr !== null ) this.props.value = parseFloat(tmpattr);

			tmpattr = e.getAttribute('step');
			if ( tmpattr !== null ) this.props.step = parseFloat(tmpattr);

			tmpattr = e.getAttribute('max');
			if ( tmpattr !== null ) this.props.max = parseFloat(tmpattr);

			tmpattr = e.getAttribute('min');
			if ( tmpattr !== null ) this.props.min = parseFloat(tmpattr);

			tmpattr = e.getAttribute('length');
			if ( tmpattr !== null ) this.props.length = parseFloat(tmpattr);

			tmpattr = e.getAttribute('width');
			if ( tmpattr !== null ) this.props.width = parseFloat(tmpattr);

			tmpattr = e.getAttribute('height');
			if ( tmpattr !== null ) this.props.height = parseFloat(tmpattr);

			tmpattr = e.getAttribute('attach-to-parent');
			if ( tmpattr !== null ) {
				if ( tmpattr != 'false' )
					this.props.attachToParent = true;
				else
					this.props.attachToParent = false;

				if ( ['top','left','right','bottom'].indexOf(tmpattr) > -1 )
					this.props.attachPosition = tmpattr;

			}

			tmpattr = e.getAttribute('analog-scroll-button');
			if ( tmpattr !== null ) {
				this.props.analogScrollButton = tmpattr === 'true';
			}

			tmpattr = e.getAttribute('onchange');
			if ( tmpattr !== null ) this.addEvent('change', new Function ( 'event', tmpattr ) );

			tmpattr = e.getAttribute('onscroll');
			if ( tmpattr !== null ) this.addEvent('scroll', new Function ( 'event', tmpattr ) );
		}

		if ( this.props.max === false ) {
			this.props.max = this.props.length + this.props.min;
		}

		this.refresh();

		lvzwebdesktop.endBasicObject.call(this);
	};

	window.lvzwebdesktop.windowVisualControls.push({
		"constructor" : ScrollbarObject
	});

	/**
	 * List of all scrollbar objects.
	 * @public
	 * @type {Array}
	 */
	ScrollbarObject.list = [];

	ScrollbarObject.onready = [];
	ScrollbarObject.onload = [];

	/**
	 * Associative array for scrollbar object's default class names.
	 * @public
	 * @type {Object}
	 */
	ScrollbarObject.classNames = {};

	/**
	 * Default class name for scrollbar object's elements.
	 * @public
	 * @type {String}
	 */
	ScrollbarObject.classNames.container = 'web-desktop-scrollbar';

	/**
	 * Default class name for vertical scrollbar.
	 * @public
	 * @type {String}
	 */
	ScrollbarObject.classNames.vertical = 'wd-vertical';

	/**
	 * Default class name for buttons of scrollbars.
	 * @public
	 * @type {String}
	 */
	ScrollbarObject.classNames.button = 'wd-sbutton';

	/**
	 * Default class name for increase buttons of scrollbars.
	 * @public
	 * @type {String}
	 */
	ScrollbarObject.classNames.increase = 'wd-increase';

	/**
	 * Default class name for decrease buttons of scrollbars.
	 * @public
	 * @type {String}
	 */
	ScrollbarObject.classNames.decrease = 'wd-decrease';

	/**
	 * Default class name for horizontal scrollbar.
	 * @public
	 * @type {String}
	 */
	ScrollbarObject.classNames.horizontal = 'wd-horizontal';

	/**
	 * Handle keypress events on textboxes.
	 * @param {Event} e - event object.
	 */
	function textboxkeypress ( e ) {
		var self = e.target.dataLVZWDESKTOPOBJ;
		var eobj = {
			"target" : self,
			"key" : e.key,
			"charCode" : e.charCode,
			"keyCode" : e.keyCode
		};
		self.fireEvent(e.type,eobj);
		if ( eobj.defaultPrevented === true ) e.preventDefault();
	}

	/**
	 * Handle mousedown events on scrollbars.
	 * @param {Event} e - event object.
	 */
	function ScrollbarSCMD ( e ) {
		var self = e.target.dataLVZWDESKTOPOBJ;

		ScrollbarSCMD.mdata = {
			o : self,
			ov : self.props.value,
			v : self.props.value,
			x : e.clientX,
			nx : 0,
			y : e.clientY,
			ny : 0
		};

		self.elements.scroll.removeEventListener('mousedown', ScrollbarSCMD );
		document.addEventListener('mousemove', ScrollbarSCMM );
		document.addEventListener('mouseup', ScrollbarSCMU );
		document.addEventListener('keydown', ScrollbarSCKD );
	}

	/**
	 * Handle keydown events on scollbars.
	 * @param {Event} e - event object.
	 */
	function ScrollbarSCKD ( e ) {
		var self = ScrollbarSCMD.mdata.o;

		if ( e.keyCode == 27 ) {

			self.fireEvent('cancelchange',{
				"target": self,
				"value" : ScrollbarSCMD.mdata.ov,
				"canceledValue" : self.props.value
			});

			self.props.value = ScrollbarSCMD.mdata.ov;
			self.refresh();

			document.removeEventListener('mousemove', ScrollbarSCMM );
			document.removeEventListener('mouseup', ScrollbarSCMU );
			document.removeEventListener('keydown', ScrollbarSCKD );
			self.elements.scroll.addEventListener('mousedown', ScrollbarSCMD );

		}
	}

	/**
	 * Handle mousemove events on scrollbars.
	 * @param {Event} e - event object.
	 */
	function ScrollbarSCMM ( e ) {
		var self = ScrollbarSCMD.mdata.o;

		ScrollbarSCMD.mdata.ny = e.clientY - ScrollbarSCMD.mdata.y;
		ScrollbarSCMD.mdata.nx = e.clientX - ScrollbarSCMD.mdata.x;

		if ( self.elements.container.classList.contains ( self.props.classNames.vertical ) ) {

			var nv = ScrollbarSCMD.mdata.v + ScrollbarSCMD.mdata.ny;
			if ( self.props.min	> nv ) nv = self.props.min;
			if ( self.props.max	< nv ) nv = self.props.max;
			self.setScrollTemp(nv);

		}
		else {
			var nv = ScrollbarSCMD.mdata.v + ScrollbarSCMD.mdata.nx;
			if ( self.props.min	> nv ) nv = self.props.min;
			if ( self.props.max	< nv ) nv = self.props.max;
			self.setScrollTemp(nv);
		}

	}

	/**
	 * Handle mouseup events on scrollbars.
	 * @param {Event} e - event object.
	 */
	function ScrollbarSCMU ( e ) {
		var self = ScrollbarSCMD.mdata.o;

		self.fireEvent('change',{
			"target": self,
			"oldValue" : ScrollbarSCMD.mdata.ov,
			"newValue" : self.props.value
		});

		document.removeEventListener('mousemove', ScrollbarSCMM );
		document.removeEventListener('mouseup', ScrollbarSCMU );
		document.removeEventListener('keydown', ScrollbarSCKD );
		self.elements.scroll.addEventListener('mousedown', ScrollbarSCMD );
	}

	/**
	 * Handle mouse wheel events on scrollbars.
	 * @param {Event} e - event object.
	 */
	function ScrollbarSCWL ( e ) {

		e.preventDefault();
		var self = e.target.dataLVZWDESKTOPOBJ;

		var ov = self.props.value;
		var nv;

		if ( self.elements.container.classList.contains( self.props.classNames.vertical ) ) {
			nv = self.props.value + e.deltaY;
			if ( e.deltaY > 0 ) {
				if ( nv > self.props.max ) nv = self.props.max;
			}
			else {
				if ( nv < self.props.min ) nv = self.props.min;
			}
		}
		else if ( self.elements.container.classList.contains( self.props.classNames.horizontal ) ) {
			nv = self.props.value + e.deltaX;
			if ( e.deltaX > 0 ) {
				if ( nv > self.props.max ) nv = self.props.max;
			}
			else {
				if ( nv < self.props.min ) nv = self.props.min;
			}
		}

		self.props.value = nv;

		self.refresh();

		self.fireEvent('scroll',{
			"target": self,
			"oldValue" : ov,
			"newValue" : self.props.value
		});

		self.fireEvent('change',{
			"target": self,
			"oldValue" : ov,
			"newValue" : self.props.value
		});

	}

	/**
	 * Handle mouse wheel events on elements controlled by scrollbars.
	 * @param {Event} e - event object.
	 */
	function ScrollbarBDWL ( e ) {
		e.preventDefault();
		var w = lvzwebdesktop(e.target,false).getParentObject('WindowObject');

		var s = ['top','left','right','bottom'];
		var self = null;
		var ov;

		for ( var i=0;i<s.length;i++ ) {
			if ( w.scrollbars[s[i]] instanceof lvzwebdesktop.ScrollbarObject ) {

				self = w.scrollbars[s[i]];

				if ( self.elements.container.classList.contains( self.props.classNames.vertical ) ) {

					ov = self.props.value;
					nv = self.props.value + e.deltaY;
					if ( e.deltaY > 0 ) {
						if ( nv > self.props.max ) nv = self.props.max;
					}
					else {
						if ( nv < self.props.min ) nv = self.props.min;
					}
					self.props.value = nv;
					self.refresh();
					self.fireEvent('scroll',{
						"target": self,
						"oldValue" : ov,
						"newValue" : self.props.value
					});
					self.fireEvent('change',{
						"target": self,
						"oldValue" : ov,
						"newValue" : self.props.value
					});
					
				}
				else if ( self.elements.container.classList.contains( self.props.classNames.horizontal ) ) {

					ov = self.props.value;
					nv = self.props.value + e.deltaX;
					if ( e.deltaX > 0 ) {
						if ( nv > self.props.max ) nv = self.props.max;
					}
					else {
						if ( nv < self.props.min ) nv = self.props.min;
					}
					self.props.value = nv;
					self.refresh();
					self.fireEvent('scroll',{
						"target": self,
						"oldValue" : ov,
						"newValue" : self.props.value
					});
					self.fireEvent('change',{
						"target": self,
						"oldValue" : ov,
						"newValue" : self.props.value
					});

				}
				
			}
		}

	}

	function ScrollbarFDMD ( e ) {
		var self = e.target.dataLVZWDESKTOPOBJ;
		ScrollbarChange.o = self;
		ScrollbarChange.t = e.target.dataChange;
		ScrollbarChange.changeInt = setInterval(ScrollbarChange, self.props.changeTmr);
		ScrollbarChange.changeVal = ( ScrollbarChange.t == 'decrease' ) ? -self.props.stepMax : self.props.stepMax;
		document.addEventListener('mouseup', ScrollbarFDMU );
	}

	function ScrollbarFDMU ( e ) {
		clearInterval(ScrollbarChange.changeInt);
		ScrollbarChange.changeInt = null;
		document.removeEventListener('mouseup', ScrollbarFDMU );
	}

	function ScrollbarChange () {
		var self = ScrollbarChange.o;
		self.scroll( ScrollbarChange.changeVal );
	}

	/**
	 * Constructor for creating timer objects.
	 * @constructor
	 * @param {HTMLElement} [e] - Element to attach timer object to.
	 */
	var TimerObject = function ( e ) {

		lvzwebdesktop.basicObject.call(this, 'timer');

		/**
		 * Private method for converting the given delay to number of milliseconds.
		 * @param {Number|String} d - Number of milliseconds or a string representing hours, minutes, seconds and milliseconds
		 * @return {Number} The number of milliseconds.
		 */
		function delayToMilliseconds ( d ) {
			if ( typeof d == 'number' ) return d;
			if ( typeof d != 'string' ) return false;

			// "10h2s".match(/^\s*([0-9]+(?:[.][0-9]+)?\s*(?:(?:h(?:ours?)?)|(?:m(?:inutes?)?)|(?:s(?:econds?)?)|(?:ms|milliseconds?))\s*)+$/m)

			var v = d.match(/[0-9]+(?:[.][0-9]+)?\s*(?:(?:h(?:ours?)?)|(?:m(?:inutes?)?)|(?:s(?:econds?)?)|(?:ms|milliseconds?))+/g);
			var r = 0;
			var m;
			for ( var i = 0; i < v.length; i ++ ) {
				m = v[i].match(/(?:[hms]|ms)$/);
				if ( m !== null ) {
					if      ( m[0] == 'h' )  r+= parseFloat(v[i]) * 3600000;
					else if ( m[0] == 'm' )  r+= parseFloat(v[i]) * 60000;
					else if ( m[0] == 's' )  r+= parseFloat(v[i]) * 1000;
					else if ( m[0] == 'ms' ) r+= parseFloat(v[i]);
				}
			}
			return r;
		}

		/**
		 * Method for starting the timer.
		 * Fires the "start" event.
		 * @param {String|Number} [t] - The delay time of the timer.
		 * @return {TimerObject} This timer object.
		 */
		this.start = function ( t ) {
			if ( typeof t == 'undefined' ) t = this.props.delay;
			else t = delayToMilliseconds(t);
			if ( this.props.repeat === true ) {
				var i = TimerObject.list.indexOf(this);
				this.props.timer = setInterval(timerInterval, t, i);
			}
			else {
				var i = TimerObject.list.indexOf(this);
				this.props.timer = setTimeout(timerTimer, t, i);
			}
			var eventData={"target": this,
						   "timer" : this.props.timer,
						   "delay" : this.props.delay
						  };
			this.fireEvent('start',eventData);
			return this;
		};

		/**
		 * Method for stopping the timer.
		 * Fires the "cancel" event.
		 * @return {TimerObject} This timer object.
		 */
		this.cancel = function () {
			var eventData = {"target" : this,
							 "timer" : this.props.timer,
							 "delay" : this.props.delay
							};
			this.fireEvent('cancel',eventData);
			if ( eventData.defaultPrevented === true ) return this;

			if ( this.props.repeat === true ) {
				clearInterval(this.props.timer);
				this.props.timer = false;
			}
			else {
				clearTimeout(this.props.timer);
				this.props.timer = false;
			}

			return this;
		};

		/**
		 * Method for setting the top position of the timer.
		 * @param {Number} v - The top position in number of pixels.
		 * @return {TimerObject} This timer object.
		 */
		this.setTop = function ( v ) {
			this.elements.container.setAttribute('top',v);
			this.elements.container.style.position = 'absolute';
			this.elements.container.style.top = v + 'px';
			return this;
		};

		/**
		 * Method for setting the left position of the timer.
		 * @param {Number} v - The left position in number of pixels.
		 * @return {TimerObject} This timer object.
		 */
		this.setLeft = function ( v ) {
			this.elements.container.setAttribute('left',v);
			this.elements.container.style.position = 'absolute';
			this.elements.container.style.left = v + 'px';
			return this;
		};

		/**
		 * Array for storing event functions for when the timer starts.
		 * @public
		 * @type {Array}
		 */
		this.onstart = [];

		/**
		 * Array for storing event functions for when firing the "reach" event.
		 * @public
		 * @type {Array}
		 */
		this.onreach = [];

		/**
		 * Array for storing event function for when canceling the timer.
		 * @public
		 * @type {Array}
		 */
		this.oncancel = [];

		/**
		 * Array for storing event functions for when start editing the object on visual editor.
		 * @public
		 * @type {Array}
		 */
		this.onvisualeditorstart = [];

		/**
		 * Property to use for setting the timer.
		 * @public
		 * @type {false|timer}
		 */
		this.props.timer = false;

		/**
		 * Property to set the number of milliseconds for the timer.
		 * @public
		 * @type {Number}
		 */
		this.props.delay = 0;

		/**
		 * When this property is true then the timer will continue after reaching end.
		 * @public
		 * @type {boolean}
		 */
		this.props.repeat = false;

		/**
		 * Set this object as not resizable during edit.
		 * @public
		 * @type {boolean}
		 */
		this.props.resizableOnEdit = false;

		/**
		 * CSS class-name for timer container element.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.container = TimerObject.classNames.container;

		/**
		 * CSS class-name for timer container element when timer has reached delay.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.active = TimerObject.classNames.active;

		/**
		 * Timer container element.
		 * @public
		 * @type {HTMLElement}
		 */
		this.elements.container = lvzwebdesktop.createBasicElement.call(this,'div',e,[
			this.props.classNames.container
		]);
		this.elements.container.addEventListener("animationend", function(e){
			var self = lvzwebdesktop(e.target);
			self.elements.container.classList.remove(self.props.classNames.active);
		});

		var tmpattr;

		tmpattr = this.elements.container.getAttribute('data-delay');
		if ( tmpattr !== null ) this.props.delay = parseFloat(tmpattr);

		tmpattr = this.elements.container.getAttribute('data-repeat');
		if ( tmpattr !== null ) this.props.repeat = tmpattr === 'true';

		tmpattr = this.elements.container.getAttribute('onstart');
		if ( tmpattr !== null ) this.addEvent('start', new Function ( 'event', tmpattr ) );

		tmpattr = this.elements.container.getAttribute('onreach');
		if ( tmpattr !== null ) this.addEvent('reach', new Function ( 'event', tmpattr ) );

		tmpattr = this.elements.container.getAttribute('oncancel');
		if ( tmpattr !== null ) this.addEvent('cancel', new Function ( 'event', tmpattr ) );

		tmpattr = this.elements.container.getAttribute('data-start');
		if ( tmpattr === 'true' ) this.start();

		this.addEvent('visualeditorstart',function(e){
			var self=e.target;
			if ( self.props.repeat === true ) {
				clearInterval(self.props.timer);
				self.props.timer = false;
			}
			else {
				clearTimeout(self.props.timer);
				self.props.timer = false;
			}
		});

		lvzwebdesktop.endBasicObject.call(this);

	};

	window.lvzwebdesktop.windowVisualControls.push({
		"constructor" : TimerObject,
		"name" : "timer",
		"icon" : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABcAAAAXCAYAAADgKtSgAAAcPXpUWHRSYXcgcHJvZmlsZSB0eXBlIGV4aWYAAHjarZtndly3loX/YxQ9BOQwHMS1egY9/P42UKQlWXrPHSxbJKuK9wIn7HBwbfZ//ecx/8E/xRZvYio1t5wt/8QWm+98U+37Z9y/nY337/dD+bznfn7dfL/heSnwNbwfm/+8vnmd793n5/a5ifv6/NeFvu/U+S799Ubvn9fHz6+PzwV9/fVCnxUE9+5s1+cXPhcK/rOi+H6enxXlVstPW1vzc+f4ean+9V8MxeeUXYn8Hb0tJTe+r97GQjyXFnqmb/dC6QX0+4Wvn78+6lmT38EFy98hxLfKoP9C6Lzu+NuGavggH+KHwFt6I93AW1LJElhp+9yo2+9g/hibv2L0h3/+ybYsNzlbH/4ha99ff6mb7+/cH17/lMF31mr+vBF+TqvN319/+7pLXxf6eiN838f/eOc6v+/80+t2uf5jKMyP6T5n1XM3zS56zMQifzb1tZX7HZ8biuL9rcyfYrOhaivf6E/jT7XdTmpq2UmnDb5vzpPW46JjDe64fb9ON1li9NsXvno/jQ/3xUqSmp9BxRD1xx1fQgsrVMpivhoK/nst7t623dtNV+0y7JWPesfFHL/yv/5j/ukHz1EvOWfrd6xYl1d3sgrrSL++8DEy4s4nqOkG+OvPr/8or4EMphvmyga7HeZdYiT3V3GFm+jABxNf4wfP1ucChIhbJxZDn0VnM13lMisq3hfnCGQlQZ2l+xD9IAMuJb9YpI8hZJJDd3Bvfqe4+1Gf/HsZVA3RhBRyKOSmhU6yYkzUT4mVGuoppJhSyqmkmlrqOWR1Xs4lC557CSWWVHIppZrSSq+hxppqrqXW2mpvvgXgOzX6tNXWWu/ctHPlzm93PtD78COMONLIo4w6mhl9Uj4zzjTzLLPONvvyKywafOVVVl1t9e02pbTjTjvvsutuux9K7YQTTzr5FHPqaad/Z+2T1r/9+R9kzX2y5m+m9MHynTVeLeXrEk5wkpQzMuajI+GFrJExCls5s9XF6JU55Qw+oiuSZ5FJyVlOGSODcTufjvvO3Sdzhij+v+TNlHrz5v+vmTNK3T/M3N/z9rusLeHdvBl7baig2kD3nZi7r/wL/f75q/n5hRhOiWd5NuTG3twCtiBKc+fezuEGrRyYgybqbs08Zhszlw2MLOI6jstpr0YLrj5icaeV6nef8XCHMgDIxIJ39+UcGvDUckLvXN+mVWYMKzWTV5uVKvd7gThnj7NPIgsw7ywrh7PL6rPwKr/NRrfv1YY1uZx+CqsDVQC1Ob4TUc/vp362O4vlLrZ3XNnd+d1ICXgQuGGuydXt0hjVnb3Ieth2rdTLisfsNXaBMQbpmCQNIk1r7R5veEqijsCLJZWR+PePX82/+8C/+FrX7M3tFce9UCrHr8HmlIOStmOr+/ATbxR2lNvYjs8cqq7VGkKtK7pt20lOGUqz7+EOMXKpESFKhuB1W0nPcWHoJ9vyaI5s1n1yUIRmO3QUmT4jpUpB2EphEPdgtn6yjdY9LInsJ15oNs5zL+X097I1FV+QYjQJ4jXxISTQ4rdymW4U+BlZQ7o6u4hlrJJZGtvs/DjPKGUNL/iaU1vjC11a12q7ljVTrrve24E8w0Dk/eT3AvSQestAVDpVJX3jKPWYwBTebmzQ0aV2UVmdyKhOS2lLMSrjxM5SEq9SrJlW8Zlf2o2rzHr2PjtECbbbR3/6an7/Rh7EVhDV+yqt1wgkZTYMm40W40TDAWtgQQAbNrdeBpSaw8/qU4HBsg3F0SO0H0C4F6nchXq5IT/+5nImIk/8lNsIgp1cautmFZTrBFd9mxHxPCvtdahk4j4CjBVcoerSmrG3ti4wxVw6MVgrk8M9cgvq/r1YjodLbW1pVMCL5I5w+C5OujYfsmTHSEd4OOYk/OwnWHoa45LpNb1rqF3XY+We/EvKxO5th7YzOwd8MgV3LryRrtvx0z4ccH1TUGWVkFg9eKS69iDzOPNWN8QBYC6FavEbWZhz5olVWNLZuEvVdRArAhd5nJZjUoxAlwqGEXuwhR8ayJHaSj4C+dSEmlBd4dqqFAN4EM6I/nDZsvdWREEfVTZLrPFUTxMWN2IEkRpvr/E2EOLdUI2upLviRuPxVrkf4G0uUHIyndD3kV3effR2a8JL1bdF4C1tw/5r6GN3um31ANyFkuGVQa8EqLSkU9iQGflYZSSB0DWnUaacWrHstVHyowEk80CoQAs0B1gPunSChqDlaJOSJC2hGzpnxXxa3wQRRBroqUl9o1BHCVPQ29MJmw5se4D+C3oaE3pIExSWW6Hc4zSJGhqrquJxLmGSzBsJ0B3Be7GfktUr4+T2KptyLb0kOSr602UkwTFhtkCi5ynkbufCAlHoBTTcUPgETSsCYjQ6b3WaHkF4+YurpkKYgfSextpAbWafHuImnPBtB3tcPhX1Di1Ru9KOlazn1CY6w4MOwY1UCzLkhqdNUlwpyMtPSNBy083FqMzcKT/acbiHU5CMhLZKGpQEEUnUmXeL/BSoHDPWaIuMs5WDsyB4oWEbN6igzcAHXuGWUESRbFoyZyyLl4tP9YAyEKDdw4DsaKEGlAfnM7VS77pIIFxNd9XFDZBu/dxtIUIOJbhSch/s/Hw1v77w49ckSzZcKIvgxg6FoPLOrCBV+OYdlBwWaSKPb6O6i0Nj0e8n+tx3eD2/w4R06eYm7NxjgyO0220VaILILTA73F6LHqm4SqdbX/fbG2OA9ZsS2AyV0WVZXgfWW1kJRnvpaNZAUiRKNyGQYyXuXh6YhLcklB1spgQhSxvke1v52CtuIC7qiMw5o/0Maaek22IjT5lJfXMQGXSNOpgmaGeTIKpEHHTv0F65VOgcYEpk7SAjhX996VK2lfU2eAbhA6DRTVVJTshFYBzdBmnEG1HCB7xHiIasWckIe6Mxq0VukY/08uEgcIATjQPXL/rl6r8rMb8UYG8IZKXB4JFaygHsteduDrxTp1s89g3oNd5siR5cVcmPmVjsIZ4PTqqXV3s1/BI37Zaby0r8e1E04T+kOqkHf4KqvOEvlqmUf0atewpqiq7jkmpE/4kj6ZLXi0gQ9bSSdcSQCNY19BK0IIUZTF2ihR1Q3arJ8aLDrtHrQH8QEwx6pQ58AWpTgyDnQISMNS6g+0YhLF8MbAQagOs14+7dhoQ7InJU+B33U208rlm4EM1d4FStdkGIM9VJlcK/O1nw2qRE8SBzIe0VwSy33SQhBIFSPmnEA0vtWSkbEH1O7ijlSDPLF2g3VFe7LQKaCH0oF8Kxve/Az2zgReo4JQUX/kLjFDxaAZFP2X6wBp/j1TysDto3iC4qnQTMPWxOwCAbYS1RuE/sp+zT7GAntCEqvD2DkPOTiocaPFZh4CBJOwwzdx15Q4pYnMreqGLhOswju+ku3eGt2obK1xzOL6LcZOfvjI/SwfcHGTiKHA7MPXreaYEUrIvIfHCye+kO+LN1rjDixenEbSj1IcHDNs1cks5zo8voEYjV7x1Dx23WqYEIMAnpIm1C2rFt6IXihXUWQIdSIZ0IALjShLV8ZDm9I8zUF4koIy8ft7nRXxcKMxBYD6GqyJh2QDHA/awdJWUqyWDR+4DYlFTe6PCFQpz0kVOmw4IqaIa+gapN6cU1XGxY6Mr6xQTe2b0NPYjwPZnKhrXDJMa4Gvihlk1DijfYlke3TbkiJM6EAe2h+mG/erjsGMgJ01FHa/u1oSKLigTTKVl647IFOk42AWMnrZUuHqC2nApxhGcznrlUQX70zsVYGLnf+FgS8OIT5DK+PEZFuPhXiDKvQfbOwW7L8HGAbo4wd3NEa9I6XMRdXo5HogiKA61bHdaxN1jSYkAQfxh3NK6HcNeKBsM3a96uVSvk1G+X7DY372pnErIuwYI6u9xUp5hJfIRyBI1rPWdt/oUHn5lUPggOmFvwW/wG/nK56XCeWbqf7UIozYQCl1gaKnNtuYJKY6NKZ0cDoCkBMMEdRRgiknak8NRSE9oe8GsCaAhkc7JcciyPjig7K9iOadN2kCQ5hicIHuYkwH+e3zoTReipj4lepy+2eA7j91QrhQ9NkuqDM14akHMN6TIJAQRdT58RA99g5RqQRGSrLVny2kKQWeU+tSqADU2CQpXWgr8BBbRgpcipLhaBAZ/TP+0cLrNBH1q/FkKMTr2CCaENRigTC6UWXLuiFvmV/STDAFAEq3zJ9RHO/Ew/ugcGqhNmi9NujGCWU3CLMJpcVBcZU1PXJ8JsMyIOsKv0hcNbI+nSc8SI3mDyjRDXKZeN4IB1S5+li16JNJoL4fkYtg6vFNSPTsGhT0E05tpA6EgakGFSpSj7kGefmqBRx2G0movFuwPsvksXk3yZAUcMs8xlQfXQzGwNnNZgSb6R/CPGF+0Ac8fLcIGoXU+EqM5ciaZCwtFsqOqj5CpDl4PNr6ScWS0AKssIGOFai4YIWEdufzCMXBixd8bWPtBgyJuByYyYGvlLYAH04vfZDBoCtHWtg1PgqRQslYJ2CZpqeg+1NjoP49lwr+ggwGtn8wT9ERtMR+TFRMS/JolLFnl7TcpyU88T9JhJCivI8MKFAyinhJMGmlJk4+hcDf/h7tDJOXCFJR/qH/8AG3g+Tpvo3CYHd103e6bENkC2EQcGKoHcJ9yGALtiBV7G+vphrxRiITqiwRjmOHbFKPsmJCke1429unpjdm9+hY42cDn5qgg6TfMusoWDoCEadQ3VTk07yDULzCFizANydpjnnEEQ9AW8HHQ0IkyEVjRLhOCmE2ke/zwL1U05BoCXzJx2u4t7WFMgw0VrpPHMw9GkpaAHM0ti5XiGKOCH4alpCpd4/MYnZEMKMti/Qr75UUYgUioQTdSzlW9UUeIb1UDsfPuQK+hInygCvHsXYOQ4pMLlmQugAA5CgntCAyi0SRGMQeIreJo1zcPtoqboDnoBDsGWjGFDzCaGSjHArGAp7ScRTPx8o/xCIHY63aElNH9VPBDhS2GN+5n427wSFAadh7siBCSFz4q00S7cOVC6XJxugG8VJUEZWgplUAayDvKKy2YaA9PVohEhJOE8nKBZwG8EMnlJVJEW1NIWf3rXxsVFEEVOCF+EhhQ7Ux+aex/NcTI1h0dYDpqnjKg2lUcAwilzLHgtcppEGS1V+3YSOrUa0aeEE7B3cUHmuavVNca+hqLI0OTBhf/VINpMAc8NG6LjM2HOewBPDciaX6/d6cvly4kFKs8gAZ9Tzc2tvSn4WVWcB8vTLB8/FlFHqLN6Ld8K1/JxgctyGOxnevh3nKTTOKrNaDOsCULEeGFesHK/m/AmVCEcd2c+qiLtWM6BW0+Zf+RxuZvDOckFqY92zsNqFpLwCAezfRS2kLEeYVX0Nkx3EImnIXgp06ulkhEj5uY0S5MCnQheN3Ikzri0pLHJthWGQnuLdzT9DOICttoo7HM+rGOivBpel67RdJ1WZsGAAJRKgwg1NDWs0AIrzRqk5JPdhxg0AaMANCsla3TQjA7H0fMPU01goHOzer9QipPu9UEyklgcpH+T2sKpEx+U4DENXNj4Vz4xUm5Rv7J5gQ2AlnTxmNKRtPz1s2CWrEwV0+BZu7wmiHo1ZNqvAbvzF6YileGur8RGoNVV7k0G7wM08VEDRRvVVLiYVAJb23hDXNiERcCImGJFEoRqr13hAvUk2dWpzCJtrp2n0H7tSeMQ6LjcqIM774FhVENRPIHM66vHY33fxneNe/CmhQkqWE23YLPZjHzGUBmfa2M0SnVWyUXNlzdJuXMUdVpY2i6VF/5e42ZIhncnHw4fCXSJ0vfby/5uTN2w2fpmlpUclIzqq4ZC4a4YqGlZig++VGo1ZuxsQty3+CYvhN3v7j5zkSHFTkUnIicfiubEHbGKrZOkS0uw9h1sBEy3BjNNB1Gui8lrCYj+xnsrAInIgyOvQHRXmcvILGpAxM00CADYiFl+F+13eMVaoyuS5+PbYKg5Or71rw2bH0s4KrFcHdVR3yEHmWsPUj8iTx2VGy1j1Ry4AntVdw+IUSJbWr6Ete7JhYuwNG2XeY/+QWJolWpxypyOZtsZp6lxayh8O6lzTA2q81Z0pgoAG+qmaWS5Na8oOuuhTYpLGprptIz4yKCWt062TpVWVJyhRug7RHDXWS29LU3liAyrlMvAxsoax4jJCLDPatjc35S2ueAHkuFB4Jx9hZW9gx451KtO1uc0jSQ1Hd9oQIlYyyilOxoFu4o1OgSa6Ddo5E38uDDbdBoZ4aTZhl8acFCFDq5kSUAdLmRuHd0cTa9vE5v1N4HHPXD0WtUdnpcfW6FS66oBrj42HrAkndIiMbaJ9J+DbjPuHyIgrPlX/tKzHqBN8mu/g5Z8pibNyF2uXyP0smnaNyy8551dZze7B6gJ5YX4zgDeBoOJIbhYaWOWgjTx85Yl3vkzKs7WLIoFFen34vruaAKjQzWdG6OuUka2uHsePCKOiHhtKQ3+gzG4SlPUwbBprjE9DtXRPSJD47gWxj0mlWRCftJmNvH7NGh/zqXuJ2ZYLLSB3kJdmLQ1BePihAeCCPKN+TWZpnsK8bx9FnT4P+pturZ/Om6WKTRiSbUC0a8yK7nT9npEB68CzHgnMgutNxp7uEGywHYKpi5As9ZqcRwLfW2AZBpacmXr+GW82eP62Cjwguus+c72ELwjUjyaloFSbEzyAVrEpBmVzL8aOxYd2EtLOJKV7LaoBlZ2qdzLSybl/NC0cp/pVyy177cRWBs4RDbco6L5joqW7KsciS4CRW5ovJgCZWbk9NoZpr8PVFQVDne52nEQhMILFH/S/NfjW5dG27XfaJR3AFsMjmtSJG3LadfjhcCjjXJdzvHUktcRKF/njR9ErbNIgHc5dMRGkISGVTTVKes4cVYMoB28Ii5AlQOv9KRRxkdvCBypYYSCrTnIvLs7rXsxNT8Ft+iMpt/h50lTZ5UtIypRrqPqNOedmLkBUyK8nEYgU0KQ7BqEK5YRCQN6hoVwFXzQvHiKrLMQN0OEobLXs18whdyoQFVTHARJQHBG+RSjU6Ch81PNgwUhkzaGvpN/oxTJh5o1SU46PWxdDylwNcFdtiAPghVa5EJWqaWx6NB3nleux/ThicOSPT6xw5M6cCOqKDvMIfGMte6sdtMnu8F5bKxgQnDhw/2siPpQ+VLxFLeex/gnjxKYX17QAQRwsR2Av1pGBt1nYPRQT8zWoeydTMrFDO6wNCm7iGQ2JmiFcU+a193TuDP0+Hl0IKw78PPjUuaQC7xDGT2ogyKgtbkcQtVUHWDk3gZQGi+yrAlK+tEqtoiQewLRHX1GlUYNP7Ele+kRUFZ3Fw54+kWL0AirbOxubNUCI5qJwZTz3IPAO6bWdAIDhlphZblhahB9knZTR82Xk8zAsvCpNzfTyS1QodWXgjbelPCgubpGe1q4hybsJCo4ynwLJPt39m7KvnwvAIWpU88RqXF0tqgTu4SThFrj7vGNy35vjrzLhj0UTdsR/Ypvc+XzsIvGKBAmMndvTbZoQDV11Bix3jEiTY0kOXF0bI85bWiLKOdxaVJWSeM6KRPBRICWwj09wsFpwn3lTs4SmIO2u+0MNMMi4VIGyGLfIfx4d9vSiX9+BuK3z0SMto57h7nV7XtIV5NsHe65e4x2J/6zdA0w99euAomP7DIPUE+KbR8U8+Up73SssTWQe9CP3voc8lSioD2XPT8nj7jZVvab20gQmweV6eTPWNPrXOieCdV4Dd7QAXZw9yh0JR0LLR3FtV+6zfxiFEVFvetkwZP9oaehA/wdgRDlRJNAJ+Icx+MQrxub8dNr9iZIJ1ZjbXVbVrfRQCqApnFjfl54nu8HP/oNhtT8dIjBeqpJeaHLsmxxg8YQKz4XAEejnqXnAiZ91z2h4vpF9e7tSBXLzxI6a/M0OB1v4h2h814eGjvSjylHpEazNEPRVKvck/uak47YNKIBgpv3TtS2dwMG8OkZPNJJ/32uhe4LcmXRJT2O0msQCjghb4gSF6zggskmo+rH3DS5y1hldHYHG4HSiazzelZQMY+YUzC+9mg3eg6ocE2zF1o4FIesQGshWXQMF0LvndREAxjTfChTHV2t9dekiv5H6QKDg5Ksu6cq3ledCNwB/PscyVbAGyrP6BmrHEBoQsUL07XIvjTz1F/7PmsUKIJC9KlVnUuBI9PFNumiJHt+jzdM15kx6JzrQkii5fT0KhjnZ9KjzUjTizpja17B5V9rjwMxblnaAfPJdpiqRwEiYS60HoQ1QHCvg8QUPbGi9zVvoMth0hkD+rpl7xOyNt1jmaPnZTIWAqvbuk4e+ERyPr2Rpo7wwYZZND4XCVMpun/9UAININ8dbpw0uw9oyHfib6mQ9o541hvk9/Se7ctDA1OsL7jQ1AQQVH9tTdSGjjSodVpEGwX8gbuxaMVOLLu0TNYh2MTbIGnRUiMVLHWujvIlJpPP4O+aqwHP4KpRO0eYQ88qYYeGbEiW4SQUkLdVldVKFwRpHj1PCE/Ouu8h858frPz1awUJwP0e1j0PgJguwKR+27x/nosQchmMVYqacGg4X++BuR4RGJoVQQiT8LcaEDhpdUAw08ZSaXt4HXbRyg0V04a/8+zuNSiekjnpzf530KCw6cEA0PfOuAl4uI8JJKHK9KHcd2EuAD8W8/W0Vd5gUEHv1sUic8u0yPA0YIQDQgTcLMCQgiYaSJpj7zGd5dcOUXbd5HsqWO6s9vsZC8g4QHNdQg5M5z+84NbYNup0C6zRc4m9anz17Ls3Rc943mdELIVd71ESlQJ+6AAfv6hn48qTLu76P50i6UxE6IHg0rjgLBBSb8kd3AdNNSlggfbLlBJQeJS6YtOyrDFqNKN+uQXKZmW4M2Rj3nGukiqROd8B5b4tOfOHvNtNtqcY3qMkGBQCD682tWY+YMCARfDQTVZPKrQ65CxNR02gdQ6xxb4RPh1SE1ZAgqBsjU/0DOZ9zoaaV0rNM+jv4Q89cqEzkVpQAFDiTuQTXFsuVKzV0Jm7diMRf/XdfA+jIFf07LGEetTIS2OxJqcFNuvxutWEBbm2rJGE0/g2kb6d6a02g9N5ZtM4vFKrBr4gvkjYgO/emisFHTLgFJt0lhpjaYgJqstRR812ANrPaemdWiYcdDKdot46WFgVctEhMGxcnY7FbU//VItIRHy9EL0X7qxBFdKluPOk/1Mg4DfafcLwTcNkQ4L04n0oSVAGCHqyhqAAKEvoNU1AsoSMjtOT4tN6gnJPDgjMWPeJtafvvk/L9SASRaYKNATGT51SIZBZza1GdYMrNW15P64q9C1QZUBeIyR0CKiTTmp2JXUG+iYY1RXNI9hdD1yfcrunWwl/LceIQe+0GWGoq3mgToZfXJvifYqJQjJ/PcWEMNNjFpb714hjx47aUma2OhbYFgIfKiKghNCgEPq6plkP4uIwDHjhMPfUTNQDLTpIBnAD1cA39B544FkXgseh1+7hbXVhI6p1Xtq/nuta5j2/fMU7dCUC2Au3qf+7g/C5jmuo4HeeYJCj/t5jF+r7NJ+j69fKGLXfhQgHVUlLo4iIzT73sc4huMQnUGOsmuDITvoFiCFMRqe7W0RRhZaNxvoD10ETJpq3ODjXQ0P9cGmPBtoJNlmxyFXmoXv0mdDlFmNndWy/L1GYPzGI1qkpo/lvORMke0TJykEAAAGEaUNDUElDQyBwcm9maWxlAAB4nH2RPUjDQBiG36aKRSsOdhDpkKE62UVFHGsVilAh1AqtOphc+gdNGpIUF0fBteDgz2LVwcVZVwdXQRD8AXF1cVJ0kRK/SwotYrzjuIf3vvfl7jtAaFaZZvUkAE23zUwqKebyq2LfKwYQohmFKDPLmJOkNHzH1z0CfL+L8yz/uj/HoFqwGBAQiRPMMG3iDeKZTdvgvE8cYWVZJT4nnjDpgsSPXFc8fuNcclngmREzm5knjhCLpS5WupiVTY14mjimajrlCzmPVc5bnLVqnbXvyV8YLugry1ynFUUKi1iCBBEK6qigChtx2nVSLGToPOnjH3X9ErkUclXAyLGAGjTIrh/8D3731ipOTXpJ4STQ++I4H2NA3y7QajjO97HjtE6A4DNwpXf8tSYw+0l6o6PFjoChbeDiuqMpe8DlDjDyZMim7EpBWkKxCLyf0TflgeFboH/N61v7HKcPQJZ6lb4BDg6B8RJlr/u8O9Tdt39r2v37AXR/cqc0oiFZAAANeGlUWHRYTUw6Y29tLmFkb2JlLnhtcAAAAAAAPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4KPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iWE1QIENvcmUgNC40LjAtRXhpdjIiPgogPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4KICA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIgogICAgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iCiAgICB4bWxuczpzdEV2dD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL3NUeXBlL1Jlc291cmNlRXZlbnQjIgogICAgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIgogICAgeG1sbnM6R0lNUD0iaHR0cDovL3d3dy5naW1wLm9yZy94bXAvIgogICAgeG1sbnM6dGlmZj0iaHR0cDovL25zLmFkb2JlLmNvbS90aWZmLzEuMC8iCiAgICB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iCiAgIHhtcE1NOkRvY3VtZW50SUQ9ImdpbXA6ZG9jaWQ6Z2ltcDplMzdhYjNhZS04NTI5LTQ5NTctYjM4NS1mNzAyMzliMGEzMGUiCiAgIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6OWM0NGYxMjUtMzc1MC00NTIzLTg4ZTktMDE5YWI4YjNlMTBmIgogICB4bXBNTTpPcmlnaW5hbERvY3VtZW50SUQ9InhtcC5kaWQ6ZDJjYmVkNTctMmE0ZS00MmUzLWE5MTAtZTI1N2IzOGUzNjA5IgogICBkYzpGb3JtYXQ9ImltYWdlL3BuZyIKICAgR0lNUDpBUEk9IjIuMCIKICAgR0lNUDpQbGF0Zm9ybT0iTGludXgiCiAgIEdJTVA6VGltZVN0YW1wPSIxNjk5NDMyMzM3MDE4Mzk0IgogICBHSU1QOlZlcnNpb249IjIuMTAuMzQiCiAgIHRpZmY6T3JpZW50YXRpb249IjEiCiAgIHhtcDpDcmVhdG9yVG9vbD0iR0lNUCAyLjEwIgogICB4bXA6TWV0YWRhdGFEYXRlPSIyMDIzOjExOjA4VDEwOjMyOjE1KzAyOjAwIgogICB4bXA6TW9kaWZ5RGF0ZT0iMjAyMzoxMTowOFQxMDozMjoxNSswMjowMCI+CiAgIDx4bXBNTTpIaXN0b3J5PgogICAgPHJkZjpTZXE+CiAgICAgPHJkZjpsaQogICAgICBzdEV2dDphY3Rpb249InNhdmVkIgogICAgICBzdEV2dDpjaGFuZ2VkPSIvIgogICAgICBzdEV2dDppbnN0YW5jZUlEPSJ4bXAuaWlkOmEyOWM3OGYwLTliYzQtNDMyZC04NzU1LTczNjQ1MDQwYWNjYyIKICAgICAgc3RFdnQ6c29mdHdhcmVBZ2VudD0iR2ltcCAyLjEwIChMaW51eCkiCiAgICAgIHN0RXZ0OndoZW49IjIwMjMtMTEtMDhUMTA6MzI6MTcrMDI6MDAiLz4KICAgIDwvcmRmOlNlcT4KICAgPC94bXBNTTpIaXN0b3J5PgogIDwvcmRmOkRlc2NyaXB0aW9uPgogPC9yZGY6UkRGPgo8L3g6eG1wbWV0YT4KICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgIAo8P3hwYWNrZXQgZW5kPSJ3Ij8+t0NRRwAAAAZiS0dEAAAAAAAA+UO7fwAAAAlwSFlzAAAuIwAALiMBeKU/dgAAAAd0SU1FB+cLCAggEPPR/RoAAAAZdEVYdENvbW1lbnQAQ3JlYXRlZCB3aXRoIEdJTVBXgQ4XAAADl0lEQVRIx5WVSyh0fxjHPz9m5gxjXrdsGJcwLBiXhVkMUWyULSkLuSwsLJS9YkPZiCYlGwulZEUUslGOS2nMRKFhQWmimcG4zBxmzrv4904x4/L/Lp9zzqen5/k+3yNkWVb5hRRFwel04vf7KS4uprCw8MdvEvil3t/fGR8fZ3h4mOXlZVT15540v4UnJSUxODjIzs4OPT09CCGizx4eHtDr9UiS9Hu4qqp4PB6Ojo5wOBwcHBwQDAbx+XxYrVaqqqrIy8tjZmYGn89HT08PJSUlP8NfXl5YW1tjYWGByspK6urqKCoqQpIkUlNTOTw8ZG5ujpaWFhRFQVVVbm5ufoY/Pj5it9u5uLhgYGCA2tpaEhMT8Xq96HQ6jEYj9fX1aDQaZFkmHA4zNjZGdnb29wtVFIXZ2Vl0Oh0TExNEIhGCwSAAmZmZGI1GAO7u7ggEAkxNTdHU1ITdbicQCHxgic9W3NraYnV1laGhIdLT04lEIgghEEKgqiqKoiBJEpFIBEVR0Ov1hEIhJicnMZlMdHR0xO88EAiwuLiI0WgkMTHxvxcSEqLO8Hg8LC0tRet6vR4ASZJoa2tjZWWFm5ub+PDT01M0Gg0DAwNcX1/H7CIYDPL4+BjXAPn5+VRXV+NwOOLDj4+PsdlsZGRkUFZWFgMIh8P4/f648PPzc1JSUpBlOXpgUXgkEsHpdJKbm/ul77VabXRcn1VaWkpjYyNut5vn5+fYzq+urvD7/bhcLkKhUAzA5/PFrQMIITAYDFxfXxMOh2PhQgj+/PlDRkYGiqLEAEKhEJIkxc2Vh4cH7u/vo5wPRySEwGw2I4TAZDLF7c5qtWKxWD7kyr+R7u7u4na7KSgoQKPRfOxcCEF1dTUXFxdfR2hCAgaDIW69ubkZs9mMxWIhOTk5dizl5eVsbGxEF/J/FA6H2d7exmazxbdicXExOTk57O3t4XQ6fw2en59nc3OT8/NzKisr48MlSaKzs5ORkZHoxn+jmpoaRkdH6evrIy0t7evgqqiooL+/n/X1dZ6enqJh9llPT0/c3t6iKAqyLNPa2kpDQ8P3wQXw9vbG0tISLpeLrq4uvF5vdJb7+/tYrVY8Hg9zc3PodDp0Oh29vb3RRX6b51qtlvb2dkpLS5meniYrK4vX11eysrKQZZnX11dOTk44Ozuju7sbm82GVquNPayf/v6hUIjLy0vcbjderxcAo9GI2WymqKgorjX/6S+KGoGlMNV04gAAAABJRU5ErkJggg==",
		"iconw" : 23,
		"iconh" : 23
	});

	/**
	 * Property for building the edit properties table on control edit.
	 * @public
	 * @type {Object}
	 */
	TimerObject.editProperties = {
		"id" : { "type" : "string|false",
				 "default" : false,
				 "getValue" : function(self){return self.props.elementId;},
				 "setValue" : function(self,v){if(v=='')v=false;self.props.elementId=v;self.elements.container.id=v;}},
		"name" : { "type" : "string|false",
				   "default" : false,
				   "getValue" : function (self){return self.props.name;},
				   "setValue" : function (self,v){ self.props.name =v; }},
		"top" : { "type" : "number",
				  "default" : 0,
				  "getValue" : function(self){return self.visualcontrol.elements.container.offsetTop;},
				  "setValue" : function(self,v){
					  v = parseInt(v);
					  if ( typeof self.visualcontrol !== 'undefined' ) {
						  self.visualcontrol.top = v;
						  self.visualcontrol.elements.container.style.top = v+'px';
					  }
				  }},
		"left" : { "type" : "number",
				   "default" : 0,
				   "getValue" : function(self){return self.visualcontrol.elements.container.offsetLeft;},
				   "setValue" : function(self,v){
					   v = parseInt(v);
					   if ( typeof self.visualcontrol !== 'undefined' ) {
						   self.visualcontrol.left = v;
						   self.visualcontrol.elements.container.style.left = v+'px';
					   }
				   }},
		"delay" : { "type" : "number",
					"default" : 0,
					"getValue" : function (self) { return self.props.delay; },
					"setValue" : function (self,v) {
						v=parseInt(v);
						self.elements.container.setAttribute('data-delay',v);
						self.props.delay = v;
					}},
		"repeat" : { "type" : "boolean",
					 "default" : false,
					 "getValue" : function ( self ) { return self.props.repeat; },
					 "setValue" : function ( self, v ) {
						 self.elements.container.setAttribute('data-repeat',v===true?'true':'false');
						 self.props.repeat = v === true;
					 }},
		"start" : { "type" : "boolean",
					"default" : true,
					"getValue" : function ( self ) { return self.elements.container.getAttribute('data-start') == 'true';},
					"setValue" : function (self,v) {
						self.elements.container.setAttribute('data-start',v!=false?'true':'false');
					}}
	};

	/**
	 * List with all timer objects.
	 * @type {Array}
	 * @public
	 */
	TimerObject.list = [];

	/**
	 * Associative array for timer object's default class names.
	 * @public
	 * @type {Object}
	 */
	TimerObject.classNames = {};

	/**
	 * Default class name for timer objects container elements.
	 * @public
	 * @type {String}
	 */
	TimerObject.classNames.container = 'wd-timer';

	/**
	 * Default class name for timer objects container elements when timer has reached delay.
	 * @public
	 * @type {String}
	 */
	TimerObject.classNames.active = 'active';

	/**
	 * Function for controlling repeating timer for when it has reached delay.
	 * @param {Number} o - The index number of the "TimerObject" in "TimerObject.list" array.
	 */
	function timerInterval ( o ) {
		var self  = TimerObject.list[o];
		var eventData = { "target" : self };
		self.elements.container.classList.add(self.props.classNames.active);
		self.fireEvent('reach',eventData);
	}

	/**
	 * Function for controlling non repeating timer for when it has reached end.
	 * @param {Number} o - The index number of the "TimerObject" in "TimerObject.list" array.
	 */
	function timerTimer (o) {
		var self = TimerObject.list[o];
		var eventData = { "target" : self };
		self.elements.container.classList.add(self.props.classNames.active);
		self.fireEvent('reach',eventData);
		clearTimeout(self.props.timer);
		self.props.timer = false;
	}

	/**
	 * Constructor for creating table objects.
	 * @constructor
	 * @param {HTMLTableElement} e - Element to use for creating the table objector to attach to.
	 */
	var TableObject = function ( e ) {

		lvzwebdesktop.basicObject.call ( this, 'table' );

		// Add common methods
		lvzwebdesktop.addMethods.call(this,'attachTo,setTop,setLeft,setWidth,setHeight');

		/**
		 * Method for adding rows.
		 * Fires the 'addrow" event.
		 * @param {Number} [before] - If is set then add the row before that position (0 is the first position).
		 * @return {TableObject} This object.
		 */
		this.addRow = function ( before ) {

			if ( typeof before == 'undefined' || before == null ) before = this.rows.length-1;
			var ebefore = this.rows[before];
			var eventData = { "target" : this };
			this.fireEvent('addrow',eventData);
			if ( eventData.defaultPrevented === true ) return this;

			var row = document.createElement('tr');
			row.dataLVZWDESKTOPOBJ = this;
			row.dataTableObject = this;
			var col;
			this.rows.splice( before,0,row );
			for ( var i = 0; i < this.props.numberOfColumns; i ++ ) {
				col = new TablecellObject();
				col.table = this;
				col.elements.container.innerText = '';
				row.appendChild(col.elements.container)
			}
			this.props.numberOfRows++;
			this.elements.container.insertBefore(row,ebefore);

			return this;
		};

		/**
		 * Method for removing rows.
		 * Fires the "removerow" and "rowremoved" events.
		 * @param {Number|HTMLRowElement} atpos - The position number of the row or the row element to be removed.
		 * @return {TableObject} This object.
		 */
		this.removeRow = function ( atpos ) {
			var rowel;
			if ( typeof atpos == 'number' ) {
				if ( atpos >= 0 && atpos < this.props.numberOfRows ) {
					rowel = this.elements.container.children[atpos];
				}
				else {
					console.warn("Out of range.");
					return this;
				}
			}
			else if ( atpos instanceof HTMLRowElement && atpos.parentNode === this.elements.container ) {
				rowel = atpos.previousElementSibling;
				atpos = -1;
				var o = rowel;
				while (o !== null ){
					o=o.previousElementSibling;
					atpos++;
				}
			}
			else {
				console.warn("Unknown argument type. Must be a number or a row element.");
				return this;
			}
			var eventData = { "target" : this,
							  "rowElement" : rowel,
							  "rowPosition" : atpos
							};
			this.fireEvent('removerow',eventData);
			if ( eventData.defaultPrevented === true ) return this;
			lvzwebdesktop.removeElement(rowel);
			eventData = { "target" : this,
						  "rowElement" : rowel,
						  "rowPosition" : atpos };
			return this.fireEvent('rowremoved',eventData);
		};

		/**
		 * Method for adding columns.
		 * Fires the 'addcolumn' event.
		 * @param {Number|null} [before] - If is set the add the column before that position.
		 * @return {TableObject} This object.
		 */
		this.addColumn = function ( before ) {
			var eventData;
			if ( this.rows.length == 0 ) {
				eventData = { "target" : this };
				this.fireEvent('addcolumn',eventData );
				if ( eventData.defaultPrevented === true ) return this;
				this.props.numberOfColumns++;
				return this;
			}
			if ( typeof before == 'undefined' || before == null ) before = this.rows[0].children.length-1 ;
			var ebefore;
			var col;
			eventData = { "target" : this };
			this.fireEvent('addcolumn',eventData );
			if ( eventData.defaultPrevented === true ) return this;
			this.numberOfColumns++;
			for ( var i = 0;i<this.rows.length; i ++ ) {
				col=new TablecellObject();
				col.table = this;
				ebefore = this.rows[i].children[before];
				this.rows[i].insertBefore(col.elements.container, ebefore);
			}

			return this;
		};

		/**
		 * Method for removing colunmns.
		 * Fires the "removecolumn" and "columnremoved" events.
		 * @param {Number|HTMLTableCellElement} atpos - The column
		 * position or a column element at desired position to remove
		 * the column.
		 * @return {TableObject} This object.
		 */
		this.removeColumn = function ( atpos ) {
			var colel=[];
			var o;
			if ( typeof atpos == 'number' ) {
				if ( atpos < 0 || atpos >= this.props.numberOfColumns ) {
					console.warn("Out of range.");
					return this;
				}
			}
			else if ( atpos instanceof HTMLRowElement && atpos.parentNode === this.elements.container ) {
				o = atpos.previousElementSibling;
				atpos = -1;
				while ( o !== null ) {
					o=o.previousElementSibling;
					atpos++;
				}
			}
			else {
				console.warn ("Unknown argument type.");
				return this;
			}
			for (o=0;o<this.container.children.length;o++)
				colel.push(this.container.children[o].children[atpos]);

			var eventData = { "target" : this,
							  "columnPosition" : atpos,
							  "columnElements" : colel };
			this.fireEvent('removecolumn',eventData);
			if ( eventData.defaultPrevented === true ) return this;

			for (o=0;o<colel.length;o++)lvzwebdesktop.removeElement(colel[o]);

			var eventData = { "target" : this,
							  "columnPosition" : atpos,
							  "columnElements" : colel };
			return this.fireEvent('columnremoved',eventData);
		};

		/**
		 * Method for getting a cell by giving the row column number.
		 * @param {Number} row - The row number of the cell to returned.
		 * @param {Number} column - The column number of the cell to be returned.
		 * @return {TablecellObject|null} The table cell object or null.
		 */
		this.getCell = function ( row, column ) {
			var ret;
			if ( row < 0 || row >= this.rows.length ) {
				console.warn ( "Row out of range" );
				return null;
			}
			ret = this.rows[row];
			if ( typeof ret == 'undefined' || column < 0 || column >= ret.children.length ) {
				console.warn ( "Column out of range" );
				return null;
			}
			this.currentRow = row;
			this.currentColumn = column;
			return lvzwebdesktop(ret.children[column]);
		};

		/**
		 * Method for getting a row element by giving the contents of a cell.
		 * @param {String} v - The contents to match.
		 * @param {Number} p - The column to look for match.
		 * @return {Number|false} The number of the row of false if there is no match.
		 */
		this.getRowByCell = function ( v, p ) {
			if ( this.rows.length == 0 ) return null;
			if ( p < 0 || p > this.rows[0].children.length ) {
				console.warn( "Out of range" );
				return null;
			}
			for ( var i=0;i<this.rows.length;i++ ) {
				if ( this.rows[i].children[p].innerText == v ) {
					this.currentRow = i;
					this.currentColumn = p;
					return i;
				}
			}

			return false;;
		};

		/**
		 * Method for selecting the first row that matches the given string at given column.
		 * @param {String} v - The string to match.
		 * @param {Number} p - The column to look for match.
		 * @return {TableObject} This object.
		 */
		this.selectRowByCell = function ( v, p ) {
			var ret;
			if ( this.rows.length == 0 ) return this;
			if ( p < 0 || p > this.rows[0].children.length ) {
				console.warn( "Out of range" );
				return this;
			}
			for ( var i=0;i<this.rows.length;i++ ) {
				if ( this.rows[i].children[p].innerText == v ) {
					this.currentRow = i;
					this.currentColumn = p;
					return this;
				}
			}

			return this;
		};

		/**
		 * Array for storing event functions for when adding rows.
		 * @public
		 * @type {Array}
		 */
		this.onaddrow = [];

		/**
		 * Array for storing event functions for when removing rows.
		 * @public
		 * @type {Array}
		 */
		this.onremoverow = [];

		/**
		 * Array for storing event functions for when a row has been removed.
		 * @public
		 * @type {Array}
		 */
		this.onrowremoved = [];

		/**
		 * Array for storing event functions for when adding columns.
		 * @public
		 * @type {Array}
		 */
		this.onaddcolumn = [];

		/**
		 * Array for storing event functions for when removing columns.
		 * @public
		 * @type {Array}
		 */
		this.onremovecolumn = [];

		/**
		 * Array for storing event functions for when column has been removed.
		 * @public
		 * @type {Array}
		 */
		this.oncolumnremoved = [];

		/**
		 * List of the table rows.
		 * @public
		 * @type {Array}
		 */
		this.rows = [];

		/**
		 * The number of the selected row of the table.
		 * @public
		 * @type {Number}
		 */
		this.currentRow = 0;

		/**
		 * The number of the selected column of the table. 
		 * @public
		 * @type {Number}
		 */
		this.currentColumn = 0;

		/**
		 * Set if this object can be resized during edit.
		 * @public
		 * @type {boolean}
		 */
		this.props.resizableOnEdit = true;

		/**
		 * Number of rows of the table.
		 * @public
		 * @type {Number}
		 */
		this.props.numberOfRows = 0;

		/**
		 * Number of columns of the table.
		 * @public
		 * @type {Number}
		 */
		this.props.numberOfColumns = 0;

		/**
		 * CSS class name of the container element.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.container = TableObject.classNames.container;

		/**
		 * Container element of the table object.
		 * @public
		 * @type {HTMLTableElement}
		 */
		this.elements.container = lvzwebdesktop.createBasicElement.call(this,'table',e,[
			this.props.classNames.container
		]);

		var o = this.elements.container.firstElementChild;
		if ( o !== null ) {
			if ( o.nodeName == 'TBODY' )
				this.props.numberOfColumns = o.firstElementChild.children.length;
			else
				this.props.numberOfColumns = o.children.length;
		}
		while (o !== null) {
			if ( o instanceof HTMLTableSectionElement && o.nodeName == 'TBODY' ) o = o.firstElementChild;
			if ( o instanceof HTMLRowElement ) {
				o.dataLVZWDESKTOPOBJ = this;
				o.dataTableObject = this;
				this.rows.push( o );
				this.props.numberOfRows++;
			}
			o = o.nextElementSibling;
		}

		lvzwebdesktop.endBasicObject.call(this);

	};

	window.lvzwebdesktop.windowVisualControls.push({
		"constructor" : TableObject,
		"name" : "table",
		"icon" : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADMAAAAXCAYAAACmnHcKAAABhGlDQ1BJQ0MgcHJvZmlsZQAAKJF9kT1Iw0AcxV9bpVIqChYUEcxQneyiIo61CkWoEGqFVh1MLv2CJg1Jiouj4Fpw8GOx6uDirKuDqyAIfoC4ujgpukiJ/0sKLWI8OO7Hu3uPu3eAv1FhqtkVB1TNMtLJhJDNrQrBV4Qwin4MYlZipj4niil4jq97+Ph6F+NZ3uf+HL1K3mSATyCOM92wiDeIZzYtnfM+cYSVJIX4nHjCoAsSP3JddvmNc9FhP8+MGJn0PHGEWCh2sNzBrGSoxNPEUUXVKN+fdVnhvMVZrdRY6578heG8trLMdZojSGIRSxAhQEYNZVRgIUarRoqJNO0nPPzDjl8kl0yuMhg5FlCFCsnxg//B727NwtSkmxROAN0vtv0xBgR3gWbdtr+Pbbt5AgSegSut7a82gNlP0uttLXoE9G0DF9dtTd4DLneAoSddMiRHCtD0FwrA+xl9Uw4YuAVCa25vrX2cPgAZ6ip1AxwcAuNFyl73eHdPZ2//nmn19wPvBHLYnwusPwAAAAZiS0dEAP8A/wD/oL2nkwAAAAlwSFlzAAAuIwAALiMBeKU/dgAAAAd0SU1FB+cKHxAbEsP2x1AAAAAZdEVYdENvbW1lbnQAQ3JlYXRlZCB3aXRoIEdJTVBXgQ4XAAAA5UlEQVRYw+2WMW7FMAxDaVmZi6JINi/OBXL/MRdI9gJevMXIYK+JpZ5Cy695AeJRgEi37/vvsixfMJKqIueMEAKcc1Y2uK6r8jzPP+u6flsZ9d5Ra0WMEd57s8BUlZmI4L03Tc05B++9KQwRgfBBGjADZsAMmM8Qiwie5zHrGRGBiOB9X4iIWc+ICDjnjNaaWVqqivu+cZ6naTHXWsEhBMQYTS9zHAe2bQMRmQWWUgITEaZpguU2IyIw85gz4zUPmAEzYP7pnOm9m/aMqqL3broyRARcSrmdc6+lUWsNKSXTOVNKqX9xvn4FcmMAPwAAAABJRU5ErkJggg==",
		"iconw" : 51,
		"iconh" : 23
	});

	/**
	 * Static method for preparing table control for edit.
	 * @param {VisualcontrolObject} self - The visual control object.
	 * @param {Object} control - The lvzwebdesktop control to be prepared for edit.
	 */
	TableObject.editInit = function ( self, control ) {
		console.log ( control ) ;
		control.addRow().addRow().addColumn().addColumn();
	};

	/**
	 * Property for building the edit properties table on control edit.
	 * @public
	 * @type {Object}
	 */
	TableObject.editProperties = {
		"id" : { "type" : "string|false",
				 "default": false,
				 "getValue":function(self){return self.props.elementId;},
				 "setValue":function(self,v){
					 self.props.elementId=v===''?false:v;
					 self.elements.container.setAttribute('id',v);
				 }},
		"name" : { "type" : "string|false",
				   "default" : false,
				   "getValue" : function (self){return self.props.name;},
				   "setValue" : function (self,v){ self.props.name=v===''?false:v; }},
		"top" : { "type" : "number|false",
				  "default" : false,
				  "getValue" : lvzwebdesktop.fn.visualeditorGetTop,
				  "setValue" : lvzwebdesktop.fn.visualeditorSetTop
				},
		"left" : { "type" : "number|false",
				   "default" : false,
				   "getValue" : lvzwebdesktop.fn.visualeditorGetLeft,
				   "setValue" : lvzwebdesktop.fn.visualeditorSetLeft
				 },
		"height" : { "type" : "number|false",
					 "default" : false,
					 "getValue" : lvzwebdesktop.fn.visualeditorGetHeight,
					 "setValue" : lvzwebdesktop.fn.visualeditorSetHeight
				   },
		"width" : { "type" : "number|false",
					"default" : false,
					"getValue" : lvzwebdesktop.fn.visualeditorGetWidth,
					"setValue" : lvzwebdesktop.fn.visualeditorSetWidth
				  },

		"rows" : { "type" : "number",
				   "default" : 2,
				   "getValue" : function(self){return self.props.numberOfRows;},
				   "setValue" : function(self,v){
					   if ( v > 0) self.setNumberOfRows(v);
				   }},
		"columns" : { "type" : "number",
					  "default" : 2,
					  "getValue" : function(self){return self.props.numberOfColumns;},
					  "setValue" : function(self,v){
						  if ( v > 0) self.setNumberOfColumns(v);
					  }}
	};

	/**
	 * List of all table instances.
	 * @public
	 * @type {Array}
	 */
	TableObject.list = [];

	TableObject.onready = [];
	TableObject.onload = [];

	/**
	 * Associative array for table's default class names.
	 * @public
	 * @type {Object}
	 */
	TableObject.classNames = {};

	/**
	 * Default class name for table object's container elements.
	 * @public
	 * @type {String}
	 */
	TableObject.classNames.container = 'wd-table';

	/**
	 * Constructor for creating cells for table object.
	 * @constructor
	 * @param {HTMLTableCellElement} e - The element to create the cell object.
	 */
	var TablecellObject = function ( e ) {

		lvzwebdesktop.basicObject.call(this,'tablecell');

		/**
		 * Method for setting or changing the caption of the cell.
		 * Fires the "captionchange" event.
		 * @param {String} v - The new caption to set.
		 * @return {TablecellObject} This object.
		 */
		this.setCaption = function ( v ) {
			var eventData = { "target" : this,
							  "newCaption" : v,
							  "captionType" : "string",
							  "currentCaption" : this.elements.container.innerText
							};
			this.fireEvent('captionchange',eventData);
			if ( eventData.defaultPrevented === true ) return this;
			this.elements.container.innerText = v;
			if (this.elements.listselect.parentNode === this.elements.container ) this.elements.listselect.remove();
			if (this.elements.check.parentNode === this.elements.container ) this.elements.check.remove();
			if (this.elements.input.parentNode === this.elements.container ) this.elements.input.remove();
			return this;
		};

		/**
		 * Method for adding input element and  setting a string value to the cell.
		 * Fires the "captionchange" event.
		 * @param {String} v - The new value.
		 * @return {TablecellObject} This object.
		 */
		this.setString = function ( v ) {
			var eventData = { "target" : this,
							  "newCaption" : v,
							  "captionType" : "string",
							  "currentCaption" : this.elements.input.value
							};
			this.fireEvent('captionchange',eventData);
			if ( eventData.defaultPrevented === true ) return this;
			this.elements.input.type = 'text';
			this.elements.input.value = v;
			this.elements.check.style.display = 'none';
			this.elements.listselect.style.display = 'none';
			if (this.elements.listselect.parentNode === this.elements.container ) this.elements.listselect.remove();
			if (this.elements.input.parentNode !== this.elements.container )this.elements.container.appendChild(this.elements.input);
			this.props.type = 'string';
			return this;
		};

		/**
		 * Method for adding input and checkbox elements and setting a
		 * string value to the cell.
		 * Fires the "captionchange" event.
		 * @param {String|boolean} v - If is string then this will be
		 * the new value. If it's boolean it will (un)check the
		 * checkbox.
		 * @return {TablecellObject} This object.
		 */
		this.setStringFalse = function ( v ) {
			var eventData = { "target" : this,
							  "newCaption" : v,
							  "captionType" : typeof v,
							  "currentCaption" : this.elements.input.value,
							  "checked" : this.elements.check.checked
							};
			this.fireEvent('captionchange',eventData);
			if ( eventData.defaultPrevented === true ) return this;
			this.elements.input.type = 'text';
			this.elements.check.style.display = '';
			this.elements.listselect.style.display = 'none';
			if ( typeof v == 'string' ) {
				this.elements.check.checked = true;
				this.elements.input.disabled = false;
				this.elements.input.value = v;
			}
			else if ( typeof v == 'boolean' || typeof v == 'undefined' ) {
				this.elements.check.checked = v;
				this.elements.input.disabled = !v;
			}
			if (this.elements.listselect.parentNode === this.elements.container ) this.elements.listselect.remove();
			if (this.elements.check.parentNode !== this.elements.container ) {
				this.elements.container.appendChild(this.elements.check);
				this.elements.container.appendChild(this.elements.input);
			}
			this.props.type = 'string|false';
			return this;
		};

		/**
		 * Method for adding input element and setting a numeric value to the cell.
		 * Fires the "captionchange" event.
		 * @param {Number} v - The new value.
		 * @return {TablecellObject} This object.
		 */
		this.setNumber = function ( v ) {
			var eventData = { "target" : this,
							  "newCaption" : v,
							  "captionType" : "number",
							  "currentCaption" : this.elements.input.value
							};
			this.fireEvent('captionchange',eventData);
			if ( eventData.defaultPrevented === true ) return this;
			this.elements.listselect.style.display = 'none';
			this.elements.input.type = 'number';
			this.elements.input.value = v;
			this.elements.check.style.display = 'none';
			if (this.elements.listselect.parentNode === this.elements.container ) this.elements.listselect.remove();
			if (this.elements.input.parentNode !== this.elements.container )this.elements.container.appendChild(this.elements.input);
			this.props.type = 'number';
			return this;
		};

		/**
		 * Method for adding input and checkbox elements and setting a
		 * numeric value to the cell.
		 * Fires the "captionchange" event.
		 * @param {Number|boolean} v - If is number then this will be
		 * the new value. If it's boolean it will (un)check the
		 * checkbox.
		 * @return {TablecellObject} This object.
		 */
		this.setNumberFalse = function ( v ) {
			var eventData = { "target" : this,
							  "newCaption" : v,
							  "captionType" : typeof v,
							  "currentCaption" : this.elements.input.value,
							  "checked" : this.elements.check.checked
							};
			this.fireEvent('captionchange',eventData);
			if ( eventData.defaultPrevented === true ) return this;
			this.elements.input.type = 'number';
			this.elements.check.style.display = '';
			this.elements.listselect.style.display = 'none';
			if ( typeof v == 'number' ) {
				this.elements.check.checked = true;
				this.elements.input.disabled = false;
				this.elements.input.value = v;
			}
			else if ( typeof v == 'boolean' || typeof v == 'undefined' ) {
				this.elements.check.checked = v;
				this.elements.input.disabled = !v;
			}
			if ( this.elements.listselect.parentNode === this.elements.container ) {
				this.elements.listselect.remove();
			}
			if (this.elements.check.parentNode !== this.elements.container ) {
				this.elements.container.appendChild(this.elements.check);
				this.elements.container.appendChild(this.elements.input);
			}
			this.props.type = 'number|false';
			return this;
		};

		/**
		 * Method for adding list selection element, options and select a value.
		 * Fires the "captionchange" event.
		 * @param {Array} list - The list to add to select element.
		 * @param {String} [value] - The value to select in select list element.
		 * @return {TablecellObject} This object.
		 */
		this.setList = function ( list, value ) {
			var eventData = { "target" : this,
							  "list" : list===false?this.props.list:list,
							  "newCaption" : value,
							  "captionType" : 'list',
							  "currentCaption" : this.elements.listselect.value,
							  "checked" : null
							};
			this.fireEvent('captionchange',eventData);
			if ( eventData.defaultPrevented === true ) return this;

			if ( list === false ) {
				list = this.props.list;
			}
			else {
				this.props.list = list;
			}
			var l = list.length - this.elements.listselect.children.length;
			var tmpopt;
			var i;
			this.props.type = 'list';

			if ( l > 0 ) {
				for ( i=0; i<l; i++ ) {
					tmpopt = lvzwebdesktop.createBasicElement.call(this,'option',undefined,[]);
					this.elements.listselect.appendChild(tmpopt);
				}
			}
			else if ( l < 0 ) {
				l=Math.abs(l);
				for ( i=0;i<l;i++ ) {
					this.elements.listselect.children[this.elements.listselect.children.length-1+i].style.display = 'none';
				}
			}

			for ( i=0; i<list.length; i++ ) {
				this.elements.listselect.children[i].innerText = list[i].label;
				this.elements.listselect.children[i].value = list[i].value;
				if ( typeof value != 'undefined' && value ===list[i].value ) {
					this.elements.listselect.children[i].selected = true;
				}
			}

			// Remove input and checkbox elements if are present
			if ( this.elements.input.parentNode === this.elements.container ) this.elements.input.remove();
			if ( this.elements.check.parentNode === this.elements.container ) this.elements.check.remove();

			// Add select element
			this.elements.container.appendChild(this.elements.listselect);

			return this;
		};

		/**
		 * Method for setting a value to the cell.
		 * Fires the "captionchange" event.
		 * @param {String|Number|boolean} value - The value to set to the table cell.
		 * @return {TablecellObject} This object.
		 */
		this.setValue = function ( value ) {
			if ( this.props.type == 'string' ) {
				return this.setString(value);
			}
			else if ( this.props.type == 'string|false' ) {
				return this.setStringFalse(value);
			}
			else if ( this.props.type == 'number' ) {
				return this.setNumber(value);
			}
			else if ( this.props.type == 'number|false' ) {
				return this.setNumberFalse(value);
			}
			else if ( this.props.type == 'list' ) {
				return this.setList(false,value);
			}
			return this.setCaption(value);
		};

		/**
		 * Method for getting the value of the cell.
		 * @return {String|Number} The value of the cell.
		 */
		this.getValue = function () {
			if ( this.props.type == 'string' ) {
				return this.elements.input.value;
			}
			else if ( this.props.type == 'string|false' ) {
				return this.elements.check.checked === false ? false : this.elements.input.value;
			}
			else if ( this.props.type == 'number' ) {
				return parseInt(this.elements.input.value);
			}
			else if ( this.props.type == 'number|false' ) {
				return this.elements.check.checked === false ? false : parseFloat(this.elements.input.value);
			}
			else if ( this.props.type == 'list' ) {
				return this.elements.listselect.value;
			}
			return this.elements.container.innerText;
		};

		/**
		 * Array for storing event functions for when changing the caption of the cell.
		 * @public
		 * @type {Array}
		 */
		this.oncaptionchange = [];

		/**
		 * Set if this object can be resized during edit.
		 * @public
		 * @type {boolean}
		 */
		this.props.resizableOnEdit = false;

		/**
		 * Set of the cell is editable.
		 * @public
		 * @type {boolean}
		 */
		this.props.editable = false;

		/**
		 * Set the data type of the cell.
		 * @public
		 * @type {String}
		 */
		this.props.type = 'string';

		/**
		 * If data type is "fromlist", then this holds the possible values.
		 * @public
		 * @type {null|Array}
		 */
		this.props.list = null;

		/**
		 * CSS class name of the container element.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.container = TablecellObject.classNames.container;

		/**
		 * CSS class name of the input element.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.input = TablecellObject.classNames.input;

		/**
		 * CSS class name of the checkbox element.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.check = TablecellObject.classNames.check

		/**
		 * CSS class name of the list select element.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.listselect = TablecellObject.classNames.listselect;

		/**
		 * Container element of the table cell object.
		 * @public
		 * @type {HTMLTableCellElement}
		 */
		this.elements.container = lvzwebdesktop.createBasicElement.call(this,'td',e,[
			this.props.classNames.container
		]);
		this.elements.container.addEventListener('click',tablecellfocus);
		this.elements.container.addEventListener('blur',tablecellblur);

		/**
		 * Input element to use for inserting data.
		 * @public
		 * @type {HTMLInputElement}
		 */
		this.elements.input = lvzwebdesktop.createBasicElement.call(this,'input',undefined,[
			this.props.classNames.input
		]);
		// this.elements.input.addEventListener('keyup', tablecellinputkeyup );

		/**
		 * Checkbox element for inserting false on string-or-false type.
		 * @public
		 * @type {HTMLInputElement}
		 */
		this.elements.check = lvzwebdesktop.createBasicElement.call(this,'input',undefined,[
			this.props.classNames.check
		]);
		this.elements.check.type = 'checkbox';
		this.elements.check.addEventListener('change',function(e) {
			var self = lvzwebdesktop(e.target);
			self.elements.input.disabled = !e.target.checked;
		});

		/**
		 * Select element to use for inserting list ar boolean.
		 * @public
		 * @type {HTMLSelectElement}
		 */
		this.elements.listselect = lvzwebdesktop.createBasicElement.call(this,'select',undefined,[
			this.props.classNames.listselect
		]);

		lvzwebdesktop.endBasicObject.call(this);

	};

	/**
	 * List of all table cell instances.
	 * @public
	 * @type {Array}
	 */
	TablecellObject.list = [];

	TablecellObject.onready = [];
	TablecellObject.onload = [];

	/**
	 * Associative array for table cell's default class names.
	 * @public
	 * @type {Object}
	 */
	TablecellObject.classNames = {};

	/**
	 * Default class name for table cells container element.
	 * @public
	 * @type {String}
	 */
	TablecellObject.classNames.container = 'wd-table-cell';

	/**
	 * Default class name for table input elements.
	 * @public
	 * @type {String}
	 */
	TablecellObject.classNames.input = 'wd-table-input';

	/**
	 * Default class name for table checkbox elements.
	 * @public
	 * @type {String}
	 */
	TablecellObject.classNames.check = 'wd-table-check';

	/**
	 * Default class name for table list select elements.
	 * @public
	 * @type {String}
	 */
	TablecellObject.classNames.listselect = 'wd-table-list';

	function tablecellfocus ( e ) {
		var self = lvzwebdesktop(e.target);
		tablecellfocus.s=self;
		if ( self.props.editable ) {
			self.props.tmpvalue = self.elements.container.innerText;
		}
		
	}

	tablecellfocus.s = null;

	function tablecellblur ( e ) {
		var self = tablecellfocus.s;
		tablecellfocus.s = null;
		self.elements.container.contentEditable = false ;
	}

	function tablecellinputkeyup ( e ) {
		var self = lvzwebdesktop(e.target);
		if ( e.keyCode == 13 ) {
			self.setValue(self.elements.input.value);
		}
	}

	/**
	 * Constructor for creating listbox objects.
	 * @constructor
	 * @param {HTMLSelectElement} [e] - Select element to create listbox object from.
	 */
	var ListboxObject = function ( e ) {

		lvzwebdesktop.basicObject.call(this,'listbox');

		// Add common methods
		lvzwebdesktop.addMethods.call(this,'setTop,setLeft,setWidth,setHeight');

		/**
		 * Method for adding options to listbox.
		 * Fires the "additem" and "itemadded" events.
		 * @param {String} label - The option's label.
		 * @param {String} [value] - The option's value.
		 * @param {Number|null|false} [position] - The position to
		 * insert the new item. If is false, null or not set then the
		 * item will be inserted to the end of the list.
		 * @return {HTMLOptionElement} The generated option element.
		 */
		this.addItem = function ( label, value, position ) {
			var eventData = {"target" : this,
							 "itemLabel" : label,
							 "itemValue" : value,
							 "itemPosition" : position };
			this.fireEvent('additem',eventData);
			if ( eventData.defaultPrevented === true ) return this;
			var opt = lvzwebdesktop.createBasicElement.call(this,'option',undefined,[]);
			opt.innerText = label;
			if ( typeof value == 'string' ) opt.setAttribute('value',value);
			if ( typeof position == 'undefined' || position === false || position === null ) {
				this.elements.container.appendChild(opt);
			}
			else {
				if ( position < 0) position = 0;
				if ( position > this.elements.container.children.length-1 ) position = this.elements.container.children.length-1;
				this.elements.container.insertBefore(opt,this.elements.container.children[position]);
			}
			var eventData = {"target" : this,
							 "itemLabel" : label,
							 "itemValue" : value,
							 "itemPosition" : position,
							 "item" : opt };
			this.fireEvent('itemadded',eventData);
			return opt;
		};

		/**
		 * Method for removing an item.
		 * @param {Number} position - The position of the item that is about to be removed.
		 * @return {ListboxObject} This object.
		 */
		this.removeItem = function ( position ) {
			var eventData = { "target" : this,
							  "itemPosition" : position,
							  "item" : this.elements.container.children[position] };
			this.fireEvent('removeitem', eventData);
			if ( eventData.defaultPrevented === true ) return this;
			if ( typeof this.elements.container.children[position] != 'undefined' )
				lvzwebdesktop.removeElement.call(this,this.elements.container.children[position]);
			this.fireEvent('itemremoved', eventData);
			return this;
		};

		/**
		 * Method for moving an item one position up.
		 * @param {Number|HTMLOptionElement} item - The item to move.
		 * @return {ListboxObject} This listbox object.
		 */
		this.moveItemUp = function ( item ) {
			var itmel;
			if ( item instanceof HTMLOptionElement ) {
				itmel = item;
				for ( var i=0;i<this.elements.children.length;i++)
					if ( this.elements.children[i] === itmel ) item = i;
			}
			else if ( typeof item == 'number' ) {
				if ( item < 0 ) item = 0;
				if ( item > this.elements.container.children.length - 1 ) item = this.elements.container.children.length - 1;
				itmel = this.elements.container.children[item];
			}
			var eventData = { "target" : this,
							  "item" : itmel,
							  "previousPosition" : item };

			this.fireEvent('itemmove',eventData);
			if ( eventData.defaultPrevented === true ) return this;

			this.elements.container.insertBefore(itmel,itmel.previousElementSibling);

			this.fireEvent('itemmoved',eventData);
			return this;
		};

		/**
		 * Method for moving an item one position down
		 * @param {Number|HTMLOptionElement} item - The item to move.
		 * @return {ListboxObject} This listbox object.
		 */
		this.moveItemDown = function ( item ) {
			var itmel;
			if ( item instanceof HTMLOptionElement ) {
				itmel = item;
				for ( var i=0;i<this.elements.children.length;i++)
					if ( this.elements.children[i] === itmel ) item = i;
			}
			else if ( typeof item == 'number' ) {
				if ( item < 0 ) item = 0;
				if ( item > this.elements.container.children.length - 1 ) item = this.elements.container.children.length - 1;
				itmel = this.elements.container.children[item];
			}
			var eventData = { "target" : this,
							  "item" : itmel,
							  "previousPosition" : item };

			this.fireEvent('itemmove',eventData);
			if ( eventData.defaultPrevented === true ) return this;

			var a = itmel.nextElementSibling;
			if ( a !== null ) a = a.nextElementSibling;
			this.elements.container.insertBefore(itmel,a);

			this.fireEvent('itemmoved',eventData);
			return this;
		};

		/**
		 * Method for selecting an item in the listbox.
		 * @param {Number|HTMLOptionElement} item - The item's position or the item's html element.
		 * @return {HTMLOptionElement|null} The selected html element of the item or null.
		 */
		this.selectItem = function ( item ) {
			var itmel;
			var i;
			if ( typeof item == 'number' ) {
				if ( typeof this.elements.container.children[item] != 'undefined' ) {
					itmel = this.elements.container.children[item];
				}
				else {
					console.warn('out of range.');
					return null;
				}
			}
			else if ( item instanceof HTMLOptionElement ) {
				itmel = item;
				item = this.elements.container.children.indexOf(itmel);
				if ( item < 0 ) {
					console.warn('Item not a member of the listbox.');
					return null;
				}
			}
			else {
				console.warn('unknown type of argument.');
				return null;
			}
			for ( i=0;i<this.elements.container.children.length;i++)
				if ( this.elements.container.children[i] !== itmel ) {
					this.elements.container.children[i].removeAttribute('selected');
				}
			itmel.setAttribute('selected',true );
			return itmel;
		};

		/**
		 * Method for getting the number of items in listbox.
		 * @return {Number} The number of items.
		 */
		this.countItems = function () {
			return this.elements.container.children.length;
		};

		/**
		 * Array for storing event functions for when adding items to
		 * listbox.
		 * @public
		 * @type {Array}
		 */
		this.onadditem = [];

		/**
		 * Array for storing event functions for when an item has been
		 * added to the listbox.
		 * @public
		 * @type {Array}
		 */
		this.onitemadded = [];

		/**
		 * Array for storing event functions for when an item is about to be removed.
		 * @public
		 * @type {Array}
		 */
		this.onremoveitem = [];

		/**
		 * Array for storing event functions for when an item has been removed.
		 * @public
		 * @type {Array}
		 */
		this.onitemremoved = [];

		/**
		 * Array for storing event functions for when moving items.
		 * @public
		 * @type {Array}
		 */
		this.onmoveitem = [];

		/**
		 * Array for storing event functions for when an item has been moved.
		 * @public
		 * @type {Array}
		 */
		this.onitemmoved = [];

		/**
		 * Set if the object can be resized during edit.
		 * @public
		 * @type {boolean}
		 */
		this.props.resizableOnEdit = true; 

		/**
		 * Property to store the vertical position of the listbox. The
		 * value can be a number for absolute position or false for
		 * static position.
		 * @public
		 * @type {false|Number}
		 */
		this.props.top = 0;

		/**
		 * Property to store the horizontal position of the
		 * listbox. The value can be a number for absolute position of
		 * false for static position.
		 * @public
		 * @type {false|Number}
		 */
		this.props.left = 0;

		/**
		 * The listbox's width in number of pixels.
		 * @public
		 * @type {Number}
		 */
		this.props.width = 0;

		/**
		 * The listbox's height in number of pixels.
		 * @public
		 * @type {Number}
		 */
		this.props.height = 0;

		/**
		 * The size of visible items.
		 * @public
		 * @type {Number}
		 */
		this.props.size = 1;

		/**
		 * Set if the listbox will show multiple items.
		 * @public
		 * @type {boolean}
		 */
		this.props.multiline = false;

		/**
		 * CSS class name of the container element.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.container = ListboxObject.classNames.container;

		/**
		 * Container element of the listbox object.
		 * @public
		 * @type {HTMLSelectElement}
		 */
		this.elements.container = lvzwebdesktop.createBasicElement.call(this,'select',e,[
			this.props.classNames.container
		]);

		var attr;

		attr = this.elements.container.getAttribute('size');
		if (attr !== null ) {
			this.props.size = parseInt(attr);
			this.props.multiline = true;
		}

		for ( var i=0;i<this.elements.container.children.length;i++) {
			this.elements.container.children[i].dataLVZWDESKTOPOBJ = this;
			this.elements.container.children[i].dataListboxObject = this;
		}

		lvzwebdesktop.endBasicObject.call(this);
	};

	window.lvzwebdesktop.windowVisualControls.push({
		"constructor" : ListboxObject,
		"name" : "listbox",
		"icon" : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADMAAAAXCAYAAACmnHcKAAABhGlDQ1BJQ0MgcHJvZmlsZQAAKJF9kTtIw1AUhv+mSn1UHewg4pChOtlFRRxrFYpQIdQKrTqY3PQhNGlIUlwcBdeCg4/FqoOLs64OroIg+ABxdXFSdJESz00KLWK8cLgf/73/z7nnAkK9zDSrIw5oum2mkwkxm1sRQ6/oRQj9VN0ys4xZSUrBd33dI8D3uxjP8r/35+pT8xYDAiJxnBmmTbxOPL1pG5z3iSOsJKvE58TjJjVI/Mh1xeM3zkWXBZ4ZMTPpOeIIsVhsY6WNWcnUiKeIo6qmU76Q9VjlvMVZK1dZs0/+wnBeX17iOtUIkljAIiSIUFDFBsqwEaNdJ8VCms4TPv5h1y+RSyHXBhg55lGBBtn1g//B79lahckJLymcADpfHOdjFAjtAo2a43wfO07jBAg+A1d6y1+pAzOfpNdaWvQIGNgGLq5bmrIHXO4AQ0+GbMquFKQSCgXg/Yy+KQcM3gI9q97cmuc4fQAyNKvUDXBwCIwVKXvN591d7XP7905zfj8TXHKAZHCNgAAAAAZiS0dEAAAAAAAA+UO7fwAAAAlwSFlzAAAuIwAALiMBeKU/dgAAAAd0SU1FB+cLBw4KKgZYhGQAAAAZdEVYdENvbW1lbnQAQ3JlYXRlZCB3aXRoIEdJTVBXgQ4XAAABEElEQVRYw+3YsU7CUBQG4N+bQ2Jy8boBZaideAFcMZjg3DfB3fAoJO6SGEZJpO0qO8udmErbWGQhJeHG66SLji6nuf92xm/5z8k5i6LEar3B4XAE10h5jl4vAKVpgSDw0em02GKyrECaFiBjPuF5bXCO57WRZQUEUQN1CFEDAjWKwziMw9QUs99/oKqqnznPc76YJImxWr0BAMryHZPJA4QQPDH9/jXm82fsdiUWixeEYQhrLU+M719hMLjBbPaEOI4wHN7yxVhrMRrdYbl8xXh8j2bz4vcVwKmtlLrEdPoIKeXfJw23+lVKuT3jMA7zXxhjTrWAGHMCEQlstzn7HwCRAHW7LWi9wXqt2WK+vzNfjkZZ0B9QHDsAAAAASUVORK5CYII=",
		"iconw" : 51,
		"iconh" : 23
	});

	/**
	 * Property for building the edit properties table on control edit.
	 * @public
	 * @type {Object}
	 */
	ListboxObject.editProperties = {
		"id" : { "type" : "string",
				 "default" : "",
				 "getValue" : function(self){return self.props.elementId;},
				 "setValue" : function(self,v){if(v=='')v=false;self.props.elementId=v;self.elements.container.id=v;}},
		"name" : { "type" : "string",
				   "default" : "",
				   "getValue" : function (self){return self.props.name;},
				   "setValue" : function (self,v){ self.props.name =v; }},
		"top" : { "type" : "number",
				  "default" : 0,
				  "getValue" : function(self){return self.props.top;},
				  "setValue" : function(self,v){
					  v = parseInt(v);
					  if ( typeof self.visualcontrol !== 'undefined' ) {
						  self.visualcontrol.top = v;
						  self.visualcontrol.elements.container.style.top = v+'px';
					  }
					  self.setTop(v);
				  }},
		"left" : { "type" : "number",
				   "default" : 0,
				   "getValue" : function(self){return self.props.left;},
				   "setValue" : function(self,v){
					   v = parseInt(v);
					   if ( typeof self.visualcontrol !== 'undefined' ) {
						   self.visualcontrol.left = v;
						   self.visualcontrol.elements.container.style.left = v+'px';
					   }
					   self.setLeft(v);
				   }},
		"height" : { "type" : "number",
					 "default" : 20,
					 "getValue" : function(self){return self.props.height;},
					 "setValue" : function(self,v){
						 v = parseInt(v);
						 if ( typeof self.visualcontrol !== 'undefined' ) {
							 self.visualcontrol.height = v;
							 self.visualcontrol.elements.container.style.height = v+'px';
							 self.visualcontrol.elements.vail.style.height = v+'px';
							 self.visualcontrol.elements.itemcontainer.style.height = v+'px';
						 }
						 self.setHeight(v);
					 }},
		"width" : { "type" : "number",
					"default" : 100,
					"getValue" : function(self){return self.props.width;},
					"setValue" : function(self,v){
						v = parseInt(v);
						if ( typeof self.visualcontrol !== 'undefined' ) {
							self.visualcontrol.width = v;
							self.visualcontrol.elements.container.style.width = v+'px';
							self.visualcontrol.elements.vail.style.width = v+'px';
							self.visualcontrol.elements.itemcontainer.style.width = v+'px';
						}
						self.setWidth(v);
					}},
		"tool tip" : { "type" : "string",
					   "default" : '',
					   "getValue" : function ( self ) { var t=self.elements.container.getAttribute('title');if ( t!== null) return t; return '';},
					   "setValue" : function (self,v) {
						   if ( v === '' ) {
							   self.elements.container.removeAttribute('title');
						   }
						   else {
							   self.elements.container.setAttribute('title',v); 
						   }
					   }}
	};

	/**
	 * List with all listbox instances.
	 * @public
	 * @type {Array}
	 */
	ListboxObject.list = [];

	ListboxObject.onready = [];
	ListboxObject.onload = [];

	/**
	 * Associative array for listboxes default class names.
	 * @public
	 * @type {Object}
	 */
	ListboxObject.classNames = {};

	/**
	 * Default class name for listboxes container elements.
	 * @public
	 * @type {String}
	 */
	ListboxObject.classNames.container = 'wd-listbox';

	/**
	 * Constructor for creating tabbar objects.
	 * @constructor
	 * @param {HTMLElement} e - The element to create the tabbar from.
	 */
	var TabbarObject = function ( e ) {

		lvzwebdesktop.basicObject.call(this,'tabbar');

		// Add common methods
		lvzwebdesktop.addMethods.call(this,'attachTo,setTop,setLeft');

		/**
		 * Method for adding tabs.
		 * Fires the "onaddtab" and "ontabadded" events.
		 * @param {HTMLElement} [el] - An element to build the tab from.
		 * @return {TabbarObject} This object.
		 */
		this.addTab = function ( el ) {

			var eventData = { "target" : this };
			this.fireEvent('addtab',eventData);
			if ( eventData.defaultPrevented === true ) return this;

			if ( typeof el != 'undefined') {
				// var tablbl = el.querySelector('.'+this.props.classNames.tablabel);
				// var tabbody = el.querySelector('.'+this.props.classNames.tabbody);
				var tablbl = lvzwebdesktop.createBasicElement.call(this,'div',el.children[0],[
					this.props.classNames.tablabel
				]);

				var tabbody = lvzwebdesktop.createBasicElement.call(this,'div',el.children[1],[
					this.props.classNames.tabbody
				]);

				lvzwebdesktop.removeElement.call(this,el);
			}
			else {
				var tablbl = lvzwebdesktop.createBasicElement.call(this,'div',undefined,[
					this.props.classNames.tablabel
				]);

				var tabbody = lvzwebdesktop.createBasicElement.call(this,'div',undefined,[
					this.props.classNames.tabbody
				]);
			}

			tablbl.addEventListener('click',tabbarlabelclick);

			tabbody.dataLabelElement = tablbl;
			tablbl.dataBodyElement = tabbody;

			this.elements.labelcontainer.appendChild(tablbl);
			this.elements.bodycontainer.appendChild(tabbody);

			if ( this.activeTabBody === null ) this.activateTab(tablbl);

			this.fireEvent('tabadded',{ "target" : this,
										"tabLabelElement" : tablbl,
										"tabBodyElement" : tabbody
									  });

			return this;
		};

		/**
		 * Method for setting the label of a tab.
		 * Fires the "onsetlabel" event.
		 * @param {String} label - The label to set to the tab.
		 * @param {HTMLElement|Number} [tab] - The tab number or tab
		 * label element or tab body element to set the label to.
		 * @return {TabbarObject} This object.
		 */
		this.setTabLabel = function ( label, tab ) {
			var tabbody;
			var tablbl;

			if ( typeof tab != 'undefined' ) {
				if ( tab instanceof HTMLElement ) {
					if ( typeof tab.dataLabelElement != 'undefined' ){
						tabbody = tab;
						tablbl = tab.dataLabelElement;
					}
					else if ( typeof tab.dataBodyElement != 'undefined' ) {
						tabbody = tab.dataBodyElement;
						tablbl = tab;
					}
					else {
						console.warn ( "Argument isn't tab body, tab label or tab number" );
						return this;
					}
				}
				else if ( typeof tab == 'number' ) {
					if ( tab >= 0
						 && tab < this.elements.labelcontainer.children.length
						 && typeof this.elements.labelcontainer.children[tab] != 'undefined'
					   ) {
						tabbody = this.elements.labelcontainer.children[tab].dataBodyElement;
						tablbl = this.elements.labelcontainer.children[tab]
					}
					else {
						console.warn ("Argument out of range.");
						return this;
					}
				}
				else {
					console.warn ( "Argument isn't tab body, tab label or tab number" );
					return this;
				}
			}
			else {
				if ( this.activeTabBody === null || this.activeTabLabel === null ) {
					console.warn("There is no active tab.");
					return this;
				}
				tabbody = this.activeTabBody;
				tablbl = this.activeTabLabel;
			}

			var eventData = { "target" : this,
							  "label" : label,
							  "tabBodyElement" : tabbody,
							  "tabLabelElement" : tablbl
							};
			this.fireEvent('setlabel',eventData);
			if ( eventData.defaultPrevented === true ) return this;
			this.activeTabBody = tabbody;
			this.activeTabLabel = tablbl;
			this.activeTabLabel.innerText = label;
			
			return this;
		};

		/**
		 * Method for removing tabs.
		 * Fires the "onremovetab" and "ontabremoved" events.
		 * @public
		 * @param {HTMLElement|number} [tab] - The tab order
		 * number (beginning from 0) or the tab label element, or the
		 * tab body element.
		 * @return {TabbarObject} This object.
		 */
		this.removeTab = function ( tab ) {
			var tabbody;
			var tablbl;

			if ( typeof tab == 'undefined' ) {
				tabbody = this.activeTabBody;
				tablbl = this.activeTabLabel;
			}
			else if ( tab instanceof HTMLElement ) {
				if ( typeof tab.dataLabelElement != 'undefined' ) {
					tabbody = tab;
					tablbl = tab.dataLabelElement;
				}
				else if ( typeof tab.dataBodyElement != 'undefined' ) {
					tabbody = tab.dataBodyElement;
					tablbl = tab;
				}
				else {
					console.warn ( "Argument not a tab body or label element." );
					return this;
				}
			}
			else if ( typeof tab == 'number' ) {

				if ( tab >= 0
					 && tab < this.elements.labelcontainer.children.length
					 && typeof this.elements.labelcontainer.children[tab] != 'undefined'
				   ){
					tabbody = this.elements.labelcontainer.children[tab].dataBodyElement;
					tablbl = this.elements.labelcontainer.children[tab]
				}

			}
			else {
				console.warn ("Unknown argument type.");
				return this;
			}

			var eventData = { "target" : this,
							  "tabBodyElement" : tabbody,
							  "tabLabelElement" : tablbl };
			this.fireEvent('removetab',eventData);
			if ( eventData.defaultPrevented === true ) return this;

			this.previousTab();

			lvzwebdesktop.removeElement.call(this,tablbl);
			lvzwebdesktop.removeElement.call(this,tabbody);

			return this.fireEvent('tabremoved',{ "target" : this });
		};

		/**
		 * Method for deactivating the active tab.
		 * @return {TabbarObject} This tabbar object.
		 */
		this.deactivateActiveTab = function () {
			if ( this.activeTabBody === null ) return this;
			var tabbody = this.activeTabBody;
			var tablbl = this.activeTabLabel;
			this.activeTabBody.classList.remove(this.props.classNames.active);
			this.activeTabLabel.classList.remove(this.props.classNames.active);
			this.activeTabBody = this.activeTabLabel = null;

			return this.fireEvent('tabdeactivated',{"target" : this,
													"tabBodyElement" : tabbody,
													"tabLabelElement" : tablbl}); 
		};

		/**
		 * Method for activating a tab.
		 * @param {HTMLElement|number} tab - The tab label or body
		 * element or order number (beginning from 0) to activate.
		 * @return {TabbarObject} This object.
		 */
		this.activateTab = function ( tab ) {
			var tablbl;
			var tabbody;
			if ( tab instanceof HTMLElement ) {
				if ( typeof tab.dataLabelElement != 'undefined' ) {
					tabbody = tab;
					tablbl = tab.dataLabelElement;
				}
				else if ( typeof tab.dataBodyElement != 'undefined' ) {
					tabbody = tab.dataBodyElement;
					tablbl = tab;
				}
				else {
					console.warn("Argument is not a tab element.");
					return this;
				}
			}
			else if ( typeof tab == 'number' ) {

				if ( tab >= 0
					 && tab < this.elements.labelcontainer.children.length
					 && typeof this.elements.labelcontainer.children[tab] != 'undefined'
				   ) {

					tabbody = this.elements.labelcontainer.children[tab].dataBodyElement;
					tablbl = this.elements.labelcontainer.children[tab]
				}
				else {
					console.warn("Out of range");
					return this;
				}

			}
			else {
				console.warn("Unknown type of argument.");
				return this;
			}

			var eventData = { "target" : this,
							  "tabBodyElement" : tabbody,
							  "tabLabelElement" : tablbl};
			this.fireEvent('activatetab',eventData);
			if ( eventData.defaultPrevented === true ) return this;

			this.deactivateActiveTab();

			this.activeTabBody = tabbody;
			this.activeTabLabel = tablbl;
			tabbody.classList.add(this.props.classNames.active);
			tablbl.classList.add(this.props.classNames.active);

			this.fireEvent('tabactivated',{ "target" : this,
											"tabLabelElement" : this.activeTabLabel,
											"tabBodyElement" : this.activeTabBody });
			return this;
		};

		/**
		 * Method for activating the next tab of the current active tab.
		 * @param {Number} [n] - Activate tab after 'n' tabs from the current active tab.
		 * @return {TabbarObject} This object.
		 */
		this.nextTab = function ( n ) {
			var nextlbl = this.activeTabLabel;
			if ( typeof n != 'number' ) n = 1;
			for ( var i = 0; i<n; i++ ) {
				nextlbl = nextlbl.nextElementSibling;
				if ( nextlbl === null ) nextlbl = this.elements.labelcontainer.firstElementChild;
				if ( nextlbl === null ) return this;
			}
			this.deactivateActiveTab();
			return this.activateTab(nextlbl);
		};

		/**
		 * Method for activating the previous tab of the current active tab.
		 * @param {Number} [n] - Activate tab before 'n' tabs from the current active tab.
		 * @return {TabbarObject} This object.
		 */
		this.previousTab = function ( n ) {
			var prevlbl = this.activeTabLabel;
			if ( typeof n != 'number' ) n = 1;
			for ( var i = 0; i<n; i++ ) {
				prevlbl = prevlbl.previousElementSibling;
				if ( prevlbl === null ) prevlbl = this.elements.labelcontainer.lastElementChild;
				if ( prevlbl === null ) return this;
			}
			this.deactivateActiveTab();
			return this.activateTab(prevlbl);
		};

		/**
		 * Array for storing event functions for when attaching tabbar to another object.
		 * @public
		 * @type {Array}
		 */
		this.onattachto = [];

		/**
		 * Array for storing event functions for when moving this object.
		 * @public
		 * @type {Array}
		 */
		this.onmove = [];

		/**
		 * Array for storing event functions for when resizing this object.
		 * @public
		 * @type {Array}
		 */
		this.onresize = [];

		/**
		 * Array for storing event functions for when adding tabs.
		 * @public
		 * @type {Array}
		 */
		this.onaddtab = [];

		/**
		 * Array for storing event functions for when a tab has been added.
		 * @public
		 * @type {Array}
		 */
		this.ontabadded = [];

		/**
		 * Array for storing event functions for when removing tabs.
		 * @public
		 * @type {Array}
		 */
		this.onremovetab = [];

		/**
		 * Array for storing event functions for when a tab has been removed.
		 * @public
		 * @type {Array}
		 */
		this.ontabremoved = [];

		/**
		 * Array for storing event functions for when activating a tab.
		 * @public
		 * @type {Array}
		 */
		this.onactivatetab = [];

		/**
		 * Array for storing event functions for when a tab has been activated.
		 * @public
		 * @type {Array}
		 */
		this.ontabactivated = [];

		/**
		 * Array for storing event functions for when a tab has been deactivated.
		 * @public
		 * @type {Array}
		 */
		this.ontabdeactivated = [];

		/**
		 * Array for storing event functions for when setting label to tab.
		 * @public
		 * @type {Array}
		 */
		this.onsetlabel = [];

		/**
		 * Property to store the active tab body.
		 * @public
		 * @type {HTMLElement|null}
		 */
		this.activeTabBody = null;

		/**
		 * Property to store the active tab label.
		 * @public
		 * @type {HTMLElement|null}
		 */
		this.activeTabLabel = null;

		/**
		 * Set if the object can be resized during edit.
		 * @public
		 * @type {boolean}
		 */
		this.props.resizableOnEdit = true;

		/**
		 * Property to store the vertical position of the tabbar. The
		 * vaue can be a number for absolute position or false for
		 * static position.
		 * @public
		 * @type {false|Number}
		 */
		this.props.top = false;

		/**
		 * Property to store the horizontal position of the
		 * tabbar. The value can be a number for absolute position or
		 * false for static position.
		 * @public
		 * @type {boolean}
		 */
		this.props.left = false;

		/**
		 * The tabbar's width in number of pixels. If the value is
		 * false then it will automatically be adjusted.
		 * @public
		 * @type {false|Number}
		 */
		this.props.width = false;

		/**
		 * The tabbar's height in number of pixels. If the value is
		 * false then it will automatically be adjusted.
		 * @public
		 * @type {false|Number}
		 */
		this.props.height = false;

		/**
		 * CSS class name of the active tab's label and body elements.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.active = TabbarObject.classNames.active;

		/**
		 * CSS class name of the container element.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.container = TabbarObject.classNames.container;

		/**
		 * CSS class name of the container of the  label elements of tabs.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.labelcontainer = TabbarObject.classNames.labelcontainer;

		/**
		 * CSS class name of the container of the body elements of tabs.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.bodycontainer = TabbarObject.classNames.bodycontainer;

		/**
		 * CSS class name of the tab container element.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.tabcontainer = TabbarObject.classNames.tabcontainer;

		/**
		 * CSS class name of the tab label elememnt.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.tablabel = TabbarObject.classNames.tablabel;

		/**
		 * CSS class name of the tab body element.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.tabbody = TabbarObject.classNames.tabbody;

		/**
		 * Container element of the tabbar object.
		 * @public
		 * @type {HTMLElement}
		 */
		this.elements.container = lvzwebdesktop.createBasicElement.call(this,'div',e,[
			this.props.classNames.container
		]);

		var tabs = [];
		for (var i=0;i<this.elements.container.children.length;i++) tabs.push(this.elements.container.children[i]);

		/**
		 * Container element of the tabs label elements.
		 * @public
		 * @type {HTMLDivElement}
		 */
		this.elements.labelcontainer = lvzwebdesktop.createBasicElement.call(this,'div',undefined,[
			this.props.classNames.labelcontainer
		]);
		this.elements.container.appendChild(this.elements.labelcontainer);

		/**
		 * Container element of the body elements of the tabs.
		 * @public
		 * @type {HTMLDivElement}
		 */
		this.elements.bodycontainer = lvzwebdesktop.createBasicElement.call(this,'div',undefined,[
			this.props.classNames.bodycontainer
		]);
		this.elements.container.appendChild(this.elements.bodycontainer);

		for ( i=0;i<tabs.length;i++) this.addTab(tabs[i]);

		lvzwebdesktop.endBasicObject.call(this);
	};

	/**
	 * List with all tab bar object instances.
	 * @public
	 * @type {Array}
	 */
	TabbarObject.list = [];

	/**
	 * Array to store event functions for when a TabbarObject is ready.
	 * @public
	 * @type {Array}
	 */
	TabbarObject.onready = [];

	/**
	 * Array to store event functions for when a TabbarObject is loading.
	 * @public
	 * @type {Array}
	 */
	TabbarObject.onload = [];

	/**
	 * Associative array for tabbar object's default class names.
	 * @public
	 * @type {Object}
	 */
	TabbarObject.classNames = {};

	/**
	 * Defautl class name for the  label and body elements of active tabs of tabbar objects.
	 * @public
	 * @type {String}
	 */
	TabbarObject.classNames.active = 'active';

	/**
	 * Default class name for the container elements of tabbar objects.
	 * @public
	 * @type {String}
	 */
	TabbarObject.classNames.container = 'wd-tabbar';

	/**
	 * Defautl class name for the container of tab label elements.
	 * @public
	 * @type {String}
	 */
	TabbarObject.classNames.labelcontainer = 'wd-tabbar-label-container';

	/**
	 * Default class name for the container of tab body elements.
	 * @public
	 * @type {String}
	 */
	TabbarObject.classNames.bodycontainer = 'wd-tabbar-body-container';

	/**
	 * Default class name for the container elements of tabs of tabbar objects.
	 * @public
	 * @type {String}
	 */
	TabbarObject.classNames.tabcontainer = 'wd-tab';

	/**
	 * Default class name for the label elements of tabs of tabbar objects.
	 * @public
	 * @type {String}
	 */
	TabbarObject.classNames.tablabel = 'wd-tab-label';

	/**
	 * Defautl class name for the body elements of tabs of tabbar objects.
	 * @public
	 * @type {String}
	 */
	TabbarObject.classNames.tabbody = 'wd-tab-body';

	/**
	 * Function for controlling the click event on tab label.
	 * @param {Event} e - The event object.
	 */
	function tabbarlabelclick ( e ) {
		var self = lvzwebdesktop(e.target);
		self.activateTab(e.target);
	}

	// Properly attach object to it's parent after loading the contructors.
	if ( lvzwebdesktop.constructorsReady === false ) {

		lvzwebdesktop.fn.addEvent.call(lvzwebdesktop, 'constructorsready', function ( e ) {
			var self = null;
			var o;

			for ( var i = 0 ; i < e.target.ScrollbarObject.list.length; i ++ ) {
				self = e.target.ScrollbarObject.list[i];
				o = self.elements.container.classList.contains( self.props.classNames.vertical)
					? 'right' : 'bottom';

				if ( self.props.attachToParent == true ) {
					var w = self.getParentOfType('WindowObject');

					if ( self.props.attachPosition !== false ) o = self.props.attachPosition;

					if ( typeof w == 'object'
						 && w instanceof lvzwebdesktop.WindowObject
					   ) {
						if ( typeof w.scrollbars == 'undefined' ) {
							w.scrollbars = {};

							w.addEvent('resize', function ( e ) {
								var self = e.target;

								var sh = self.elements.bodycontainer.scrollHeight;
								var oh = self.elements.bodycontainer.offsetHeight;
								var sw = self.elements.bodycontainer.scrollWidth;
								var ow = self.elements.bodycontainer.offsetWidth;

								if ( typeof self.scrollbars['right'] != 'undefined' ) {
									self.elements.bodycontainer.style.overflowY = 'hidden';
									self.scrollbars['right'].props.length = oh;
									var sl = ( sh > oh ) ? sh - oh : 0;
									self.scrollbars['right'].props.max = sl;
									self.scrollbars['right'].props.height = oh;

									self.scrollbars['right'].props.visibleLength = oh;
									self.scrollbars['right'].props.scrollLength = sh;
									self.scrollbars['right'].refresh();
								}
								if ( typeof self.scrollbars['bottom'] != 'undefined' ) {
									self.elements.bodycontainer.style.overflowX = 'hidden';
									self.scrollbars['bottom'].props.length = ow;
									var sl = ( sw > ow ) ? sw - ow : 0;
									self.scrollbars['bottom'].props.max = sl;
									self.scrollbars['bottom'].props.width = ow;

									self.scrollbars['bottom'].props.visibleLength = ow - 1;
									self.scrollbars['bottom'].props.scrollLength = sw;
									self.scrollbars['bottom'].refresh();
								}
							});

							w.elements.bodycontainer.addEventListener('wheel', ScrollbarBDWL );
						}
						w.scrollbars[o] = self;

						if ( o == 'bottom' || o == 'top' ) {
							self.window = w;
							self.addEvent('scroll', function ( e ) {
								var self = e.target;
								var w = self.window;
								w.elements.bodycontainer.scrollLeft = e.newValue;
							}).addEvent('cancelchange', function ( e ) {
								var self = e.target;
								var w = self.window;
								w.elements.bodycontainer.scrollLeft = e.value;
							});
						}
						else if ( o == 'left' || o == 'right' ) {
							self.window = w;
							self.addEvent('scroll', function ( e ) {
								var self = e.target;
								var w = self.window;
								w.elements.bodycontainer.scrollTop = e.newValue;
							}).addEvent('cancelchange', function ( e ) {
								var self = e.target;
								var w = self.window;
								w.elements.bodycontainer.scrollTop = e.value;
							});
						}

						w.attachItem(self,o);

					}
				}
			}

			for ( var i = 0 ; i < e.target.RadiogroupObject.list.length; i ++ ) {
				self = e.target.RadiogroupObject.list[i];

				if ( self.name === null ) self.name = self.createName();

				for ( var j = 0; j < self.elements.container.children.length; j++ ) {
					o = lvzwebdesktop(self.elements.container.children[j]);
					if ( o.group === null ) {
						o.group = self;
						o.props.name = self.name;
						o.elements.radio.name = self.name;
					}
				}
			}
		});

	}

	window.lvzwebdesktop.ButtonObject = ButtonObject;
	window.lvzwebdesktop.constructors.push({
		"selector" : "." + ButtonObject.classNames.container,
		"constructor" : ButtonObject
	});

	window.lvzwebdesktop.CheckboxObject = CheckboxObject;
	window.lvzwebdesktop.constructors.push({
		"selector" : "." + CheckboxObject.classNames.container,
		"constructor" : CheckboxObject
	});

	window.lvzwebdesktop.RadioObject = RadioObject;
	window.lvzwebdesktop.constructors.push({
		"selector" : "." + RadioObject.classNames.container,
		"constructor" : RadioObject
	});

	window.lvzwebdesktop.RadiogroupObject = RadiogroupObject;
	window.lvzwebdesktop.constructors.push({
		"selector" : "." + RadiogroupObject.classNames.container,
		"constructor" : RadiogroupObject
	});

	window.lvzwebdesktop.FieldsetObject = FieldsetObject;
	window.lvzwebdesktop.constructors.push({
		"selector" : "." + FieldsetObject.classNames.container,
		"constructor" : FieldsetObject
	});

	window.lvzwebdesktop.TextboxObject = TextboxObject;
	window.lvzwebdesktop.constructors.push({
		"selector" : "." + TextboxObject.classNames.container,
		"constructor" : TextboxObject
	});

	window.lvzwebdesktop.ScrollbarObject = ScrollbarObject;
	window.lvzwebdesktop.constructors.push({
		"selector" : "." + ScrollbarObject.classNames.container,
		"constructor" : ScrollbarObject
	});

	window.lvzwebdesktop.TimerObject = TimerObject;
	window.lvzwebdesktop.constructors.push({
		"selector" : "." + TimerObject.classNames.container,
		"constructor" : TimerObject
	});

	window.lvzwebdesktop.TableObject = TableObject;
	window.lvzwebdesktop.constructors.push({
		"selector" : "." + TableObject.classNames.container,
		"constructor" : TableObject
	});

	window.lvzwebdesktop.TablecellObject = TablecellObject;
	window.lvzwebdesktop.constructors.push({
		"selector" : "." + TablecellObject.classNames.container,
		"constructor" : TablecellObject
	});

	window.lvzwebdesktop.ListboxObject = ListboxObject;
	window.lvzwebdesktop.constructors.push({
		"selector" : "." + ListboxObject.classNames.container,
		"constructor" : ListboxObject
	});

	window.lvzwebdesktop.TabbarObject = TabbarObject;
	window.lvzwebdesktop.constructors.push({
		"selector" : "." + TabbarObject.classNames.container,
		"constructor" : TabbarObject
	});

}
