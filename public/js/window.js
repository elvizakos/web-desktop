{
	document.currentScript.dataLVZWLIB = true;

	/**
	 * Constructor for  Argument objects.
	 *
	 * @constructor
	 * @param {HTMLElement} [e] - The element to attach window to.
	 * @param {DesktopObject} [desktop] - Desktop object that window object will appear into.
	 */
	var WindowObject = function ( e, desktop ) {

		lvzwebdesktop.basicObject.call(this,'window');

		/**
		 * The group object. if not in group then set false.
		 * @type {false|String}
		 * @public
		 */
		this.props.group = false;

		/**
		 * Has the window's top border distance from the top of the desktop object it
		 * belongs to, in pixels.
		 * @type {number}
		 * @public
		 */
		this.props.top = 0;

		/**
		 * For storing the window's top position when the shift key is pressed and sticky property is true.
		 * @type {false|number}
		 * @public
		 */
		this.props.stickyTop = false;

		/**
		 * The minimum top position of the window. If flase, no minimum limit.
		 * @type {number|false}
		 * @public
		 */
		this.props.minTop = false;

		/**
		 * The maximum top position for the window. If false, no maximum limit.
		 * @type {number|false}
		 * @public
		 */
		this.props.maxTop = false;

		/**
		 * Has the window's left border distance from the left of the desktop object it
		 * belongs to, in pixels.
		 * @type {number}
		 * @public
		 */
		this.props.left = 0;

		/**
		 * For storing the window's left position when the shift key is pressed and sticky property is true.
		 * @type {false|number}
		 * @public
		 */
		this.props.stickyLeft = false;

		/**
		 * The minimum left position of the window. If false, no minimum limit.
		 * @type {number|false}
		 * @public
		 */
		this.props.minLeft = false;

		/**
		 * The maximum left position of the window. If false, no maximum limit.
		 * @type {number|false}
		 * @public
		 */
		this.props.maxLeft = false;

		/**
		 * Has the value in pixels of window's inner width (contents area).
		 * @type {number}
		 * @public
		 */
		this.props.width = 300;

		/**
		 * The minimum width for window in pixels.
		 * @type {number}
		 * @public
		 */
		this.props.minWidth = 200;

		/**
		 * The maximum width for window in pixels or false for no maximum width.
		 * @type {number|false}
		 * @public
		 */
		this.props.maxWidth = false;

		/**
		 * Has the value in pixels of window's inner height (contents area).
		 * @type {number}
		 * @public
		 */
		this.props.height = 200;

		/**
		 * The minimum height for window in pixels.
		 * @type {number}
		 * @public
		 */
		this.props.minHeight = 20;

		/**
		 * The maximum height for window in pixels or false for no maximum height.
		 * @type {number|false}
		 * @public
		 */
		this.props.maxHeight = false;

		/**
		 * Is true if window is resized to match content size.
		 * @type {boolean}
		 * @public
		 */
		this.props.matchContentSize = false;

		/**
		 * For when matching window size to content size this takes the window's height before. Used for toggling matching size.
		 * @type {false|number}
		 * @public
		 */
		this.props.prevHeight = false;

		/**
		 * For when matching window size to content size this takes the window's width before. Used for toggling matching size.
		 * @type {false|number}
		 * @public
		 */
		this.props.prevWidth = false;

		/**
		 * The application icon to use for this window.
		 * @type {string|false}
		 * @public
		 */
		this.props.icon = false;

		/**
		 * Sets if should replace menu icon with the application icon.
		 * @type {boolean}
		 * @public
		 */
		this.props.setAppIconOnMenuIcon = true;

		/**
		 * String of window's title.
		 * @type {string}
		 * @public
		 */
		this.props.title = '';

		/**
		 * Title position.
		 * @type {string}
		 * @public
		 */
		this.props.titlePosition = 'top';

		/**
		 * Window's owner object (usually a desktop object).
		 * @type {object}
		 * @public
		 */
		this.props.owner = null;

		/**
		 * Windows state.
		 * @type {('normal' | 'minimized' | 'maximized' )}
		 * @public
		 */
		this.props.state = 'normal';

		/**
		 * True if window has focus. False Otherwise.
		 * @type {boolean}
		 * @public
		 */
		this.props.focused = false;

		/**
		 * Defines if window can take focus.
		 * @public
		 * @type {boolean}
		 */
		this.props.canHaveFocus = true;

		/**
		 * True if border is disabled, false otherwise.
		 * @type {boolean}
		 * @public
		 */
		this.props.disableBorder = false;

		/**
		 * True if window is disabled (can't react with any controls in it). false otherwise.
		 * @type {boolean}
		 * @public
		 */
		this.props.disabled = false;

		/**
		 * Sets if the window object is movable.
		 * @type {boolean}
		 * @public
		 */
		this.props.movable = true;

		/**
		 * Sets if the window object is resizable.
		 * @type {boolean}
		 * @public
		 */
		this.props.resizable = true;

		/**
		 * Sets if the window object is maximizable.
		 * @type {boolean}
		 * @public
		 */
		this.props.maximizable = true;

		/**
		 * Sets if the window object can be minimized.
		 * @type {boolean}
		 * @public
		 */
		this.props.minimizable = true;

		/**
		 * Sets the title button layout.
		 * @type {string}
		 * @public
		 */
		this.props.buttonLayout = 'menu:minimize,maximizeRestore,close';

		/**
		 * Data of JSON theme in use. Must be null if css theme is in use.
		 * @type {object}
		 * @public
		 */
		this.props.theme = null;

		/**
		 * If true then the window will use the vail for when moving so that less cpu is used.
		 * @type {boolean}
		 * @public
		 */
		this.props.useMovingVail = false;

		/**
		 * If it's true it disables application menu.
		 * @type {boolean}
		 * @public
		 */
		this.props.disableAppMenu = false;

		/**
		 * Default method name for setting window's width.
		 * @type {string}
		 * @public
		 */
		this.props.setWidthDefaultMethod = "setInnerWidth";

		/**
		 * Default method name for setting window's height.
		 * @type {string}
		 * @public
		 */
		this.props.setHeightDefaultMethod = "setInnerHeight";

		/**
		 * Set if the window will appear in taskbars if are any.
		 * @public
		 * @type {boolean}
		 */
		this.props.showInTaskbar = true;

		/**
		 * Class name for window container with focus.
		 * @type {string}
		 * @public
		 */
		this.props.classNames.hasfocus = WindowObject.classNames.hasfocus;

		/**
		 * Class name for blured window container element.
		 * @type {string}
	 	 * @public
		 */
		this.props.classNames.blured = WindowObject.classNames.blured;

		/**
		 * Class name for window contrainer that is maximized.
		 * @type {String}
		 * @public
		 */
		this.props.classNames.maximized = WindowObject.classNames.maximized;

		/**
		 * Class name for window container element for minimized window.
		 * @type {string}
		 * @public
		 */
		this.props.classNames.minimized = WindowObject.classNames.minimized;

		/**
		 * Class name for window disabled parts.
		 * @type {string}
		 * @public
		 */
		this.props.classNames.disabled = WindowObject.classNames.disabled;

		/**
		 * Class name for window's disable vail.
		 * @type {string}
		 * @public
		 */
		this.props.classNames.disableVail = WindowObject.classNames.disableVail;

		/**
		 * CSS class-name for window container.
		 * @type {string}
		 * @public
		 */
		this.props.classNames.container = WindowObject.classNames.container;

		/**
		 * CSS class-name for left border container.
		 * @type {string}
		 * @public
		 */
		this.props.classNames.leftbordercont = WindowObject.classNames.leftbordercont;

		/**
		 * CSS class-name for left top border box.
		 * @type {string}
		 * @public
		 */
		this.props.classNames.lefttopbrdrbox = WindowObject.classNames.lefttopbrdrbox;

		/**
		 * CSS class-name for left top corner border part (the element below left-top box).
		 * @type {string}
		 * @public
		 */
		this.props.classNames.leftbordertopcrnr = WindowObject.classNames.leftbordertopcrnr;

		/**
		 * CSS class-name for left center border part.
		 * @type {string}
		 * @public
		 */
		this.props.classNames.leftbordercenter = WindowObject.classNames.leftbordercenter;

		/**
		 * CSS class-name for left bottom corner border part ( the element above left-bottom box ).
		 * @type {string}
		 * @public
		 */
		this.props.classNames.leftborderbtmcrnr = WindowObject.classNames.leftborderbtmcrnr;

		/**
		 * CSS class-name for left bottom border box.
		 * @type {string}
		 * @public
		 */
		this.props.classNames.leftbtmbrdrbox = WindowObject.classNames.leftbtmbrdrbox;

		/**
		 * CSS class-name for center container.
		 * @type {string}
		 * @public
		 */
		this.props.classNames.centercont = WindowObject.classNames.centercont;

		/**
		 * CSS class-name for top border container.
		 * @type {string}
		 * @public
		 */
		this.props.classNames.topbordercontainer = WindowObject.classNames.topbordercontainer;

		/**
		 * CSS class-name for top border left corner part.
		 * @type {string}
		 * @public
		 */
		this.props.classNames.topborderlftcrnr = WindowObject.classNames.topborderlftcrnr;

		/**
		 * CSS class-name for top border center part.
		 * @type {string}
		 * @public
		 */
		this.props.classNames.topbordercenter = WindowObject.classNames.topbordercenter;

		/**
		 * CSS class-name for top border right corner part.
		 * @type {string}
		 * @public
		 */
		this.props.classNames.topborderrghtcrnr = WindowObject.classNames.topborderrghtcrnr;

		/**
		 * CSS class-name for body and title container.
		 * @type {string}
		 * @public
		 */
		this.props.classNames.bodytitlecontainer = WindowObject.classNames.bodytitlecontainer;

		/**
		 * CSS class-name for title container element.
		 * @type {string}
		 * @public
		 */
		this.props.classNames.titlecontainer = WindowObject.classNames.titlecontainer;

		/**
		 * CSS class-name for title left controls container element.
		 * @type {string}
		 * @public
		 */
		this.props.classNames.titleleftcontrols = WindowObject.classNames.titleleftcontrols;

		/**
		 * CSS class-name for title center container ( window title text).
		 * @type {string}
		 * @public
		 */
		this.props.classNames.titlecenter = WindowObject.classNames.titlecenter;

		/**
		 * CSS class-name for title right controls container element.
		 * @type {string}
		 * @public
		 */
		this.props.classNames.titlerightcontrols = WindowObject.classNames.titlerightcontrols;

		/**
		 * CSS class-name for body container (window contents).
		 * @type {string}
		 * @public
		 */
		this.props.classNames.bodycontainer = WindowObject.classNames.bodycontainer;

		/**
		 * CSS class-name for bottom border container element.
		 * @type {string}
		 * @public
		 */
		this.props.classNames.bottombordercontainer = WindowObject.classNames.bottombordercontainer;

		/**
		 * CSS class-name for bottom border left corner part element.
		 * @type {string}
		 * @public
		 */
		this.props.classNames.bottomborderlftcrnr = WindowObject.classNames.bottomborderlftcrnr;

		/**
		 * CSS class-name for bottom border center element.
		 * @type {string}
		 * @public
		 */
		this.props.classNames.bottombordercenter = WindowObject.classNames.bottombordercenter;

		/**
		 * CSS class-name for bottom border right corner part element.
		 * @type {string}
		 * @public
		 */
		this.props.classNames.bottomborderrghtcrnr = WindowObject.classNames.bottomborderrghtcrnr;

		/**
		 * CSS class-name for right border container element.
		 * @type {string}
		 * @public
		 */
		this.props.classNames.rightbordercont = WindowObject.classNames.rightbordercont;

		/**
		 * CSS class-name for right top border box element.
		 * @type {string}
		 * @public
		 */
		this.props.classNames.righttopbrdrbox = WindowObject.classNames.righttopbrdrbox;

		/**
		 * CSS class-name for right top border corner part element.
		 * @type {string}
		 * @public
		 */
		this.props.classNames.rightbordertopcrnr = WindowObject.classNames.rightbordertopcrnr;

		/**
		 * CSS class-name for right border center part element.
		 * @type {string}
		 * @public
		 */
		this.props.classNames.rightbordercenter = WindowObject.classNames.rightbordercenter;

		/**
		 * CSS class-name for right border bottom corner element.
		 * @type {string}
		 * @public
		 */
		this.props.classNames.rightborderbtmcrnr = WindowObject.classNames.rightborderbtmcrnr;

		/**
		 * CSS class-name for right bottom border box element.
		 * @type {string}
		 * @public
		 */
		this.props.classNames.rightbtmbrdrbox = WindowObject.classNames.rightbtmbrdrbox;

		/**
		 * CSS class-name for title buttons.
		 * @type {String}
		 * @public
		 */
		this.props.classNames.titleControl = WindowObject.classNames.titleControl;

		/**
		 * CSS class-name for title button when pressing mouse key.
		 * @type {String}
		 * @public
		 */
		this.props.classNames.titleControlMouseDown = WindowObject.classNames.titleControlMouseDown;

		/**
		 * CSS class-name for menuBtn.
		 * @type {string}
		 * @public
		 */
		this.props.classNames.menuBtn = WindowObject.classNames.menuBtn;

		/**
		 * CSS class-name for closeBtn.
		 * @type {string}
		 * @public
		 */
		this.props.classNames.closeBtn = WindowObject.classNames.closeBtn;

		/**
		 * CSS class-name for minimizeBtn.
		 * @type {string}
		 * @public
		 */
		this.props.classNames.minimizeBtn = WindowObject.classNames.minimizeBtn;

		/**
		 * CSS class-name for maximizeRestoreBtn (for non maximized windows).
		 * @type {string}
		 * @public
		 */
		this.props.classNames.maximizeBtn = WindowObject.classNames.maximizeBtn;

		/**
		 * Css class-name for maximizeRestoreBtn (for maximized windows).
		 * @type {string}
		 * @public
		 */
		this.props.classNames.restoreBtn = WindowObject.classNames.restoreBtn;

		/**
		 * CSS class-name for macrestore (for mac restore button).
		 * @type {string}
		 * @public
		 */ 
		this.props.classNames.macrestoreBtn = WindowObject.classNames.macrestoreBtn;

		/**
		 * CSS class-name for the "innercont" (inner container element)
		 * @public
		 * @type {String}
		 */
		this.props.classNames.innercont = WindowObject.classNames.innercont;

		/**
		 * CSS class-name for the "innercontup" (inner container up element)
		 * @public
		 * @type {String}
		 */
		this.props.classNames.innercontup = WindowObject.classNames.innercontup

		/**
		 * CSS class-name for the "innercontleft" (inner container left element)
		 * @public
		 * @type {String}
		 */
		this.props.classNames.innercontleft = WindowObject.classNames.innercontleft;

		/**
		 * CSS class-name for the "innercontright" (inner container right element)
		 * @public
		 * @type {String}
		 */
		this.props.classNames.innercontright = WindowObject.classNames.innercontright;

		/**
		 * CSS class-name for the "innercontbtm" (inner container bottom element)
		 * @public
		 * @type {String}
		 */
		this.props.classNames.innercontbtm = WindowObject.classNames.innercontbtm;

		/**
		 * The base element the for resize.
		 * @type {null|HTMLElement}
		 * @public
		 */
		this.props.resizeElement = null;

		/**
		 * True if the window change size based on the content. False otherwise.
		 * @type {boolean}
		 * @public
		 */
		this.props.resizeOnContent = false;

		/**
		 * The desktop object that the window belongs to.
		 * @type {DesktopObject|null}
		 * @public
		 */
		this.desktop = null;

		/**
		 * Resize observer for resizing window based on content size.
		 * @type {ResizeObserver}
		 * @public
		 */
		this.resizeObserver = new ResizeObserver ( resizeWindowOnContent );

		/**
		 * Array for storing event functions for when opening window's menu.
		 * @public
		 * @type {Array}
		 */
		this.onmenu=[];

		/**
		 * Array for storing event functions for window focus event.
		 * @type {Array}
		 * @public
		 */
		this.onfocus=[];

		/**
		 * Array for storing event functions for window blur (lose focus) event.
		 * @type {Array}
		 * @public
		 */
		this.onblur=[];

		/**
		 * Array for storing event functions for window move event.
		 * @type {Array}
		 * @public
		 */
		this.onmove=[];

		/**
		 * Array for storing event functions for window's resize event.
		 * @type {Array}
		 * @public
		 */
		this.onresize=[];

		/**
		 * Array for storing event functions for window's titlechange event.
		 * @type {Array}
		 * @public
		 */

		this.ontitlechange=[];

		/**
		 * Array for storing event function for window's iconchange event.
		 * @public
		 * @type {Array}
		 */
		this.oniconchange=[];

		/**
		 * Array for storing event functions for window's close event.
		 * @type {Array}
		 * @public
		 */
		this.onclose=[];

		/**
		 * Array for storing event functions for window's minimize event.
		 * @type {ArraY}
		 * @public
		 */
		this.onminimize=[];

		/**
		 * Array for storing event functions for window's maximize event.
		 * @type {Array}
		 * @public
		 */
		this.onmaximize=[];

		/**
		 * Array for storing event functions for window's restore event.
		 * @type {Array}
		 * @public
		 */
		this.onrestore=[];

		/**
		 * Array for storing event functions for window's fold event.
		 * @type {Array}
		 * @public
	 	 */
		this.onfold=[];

		/**
		 * Array for storing event functions for window's unfold event.
		 * @type {Array}
		 * @public
		 */
		this.onunfold=[];

		/**
		* Array for storing event functions for window's mousemove event.
		* @type {Array}
		* @public
		*/
		this.onmousemove = [];

		/**
		 * Array for storing event functions for window's mousedown event.
		 * @type {Array}
		 * @public
		 */
		this.onmousedown = [];

		/**
		 * Arra for storing event functions for window's click event.
		 * @type {Array}
		 * @public
		 */
		this.onclick = [];

		/**
		 * Array for storing event functions for window's itemattach event.
		 * @type {Array}
		 * @public
		 */
		this.onitemattached = [];

		/**
		 * Array for storing event functions for window's keypress event.
		 * @type {Array}
		 * @public
		 */
		this.onkeypress = [];

		/**
		 * Array for storing event functions for window's keyup event.
		 * @type {Array}
		 * @public
		 */
		this.onkeyup = [];

		/**
		 * Array for storing event functions for window's keydown event.
		 * @type {Array}
		 * @public
		 */
		this.onkeydown = [];

		/**
		 * Array for storing event functions for window's onready event.
		 * @type {Array}
		 * @public
		 */
		this.onready = [];

		/**
		 * Array for storing event functions for window's onhide event.
		 * @public
		 * @type {Array}
		 */
		this.onhide = [];

		/**
		 * Array for storing event functions for window's on show event.
		 * @public
		 * @type {Array}
		 */
		this.onshow = [];

		/** Set dynamic properties **/
		try {

			/**
			 * Set dynamic property "Disabled" for enabling or disabling this window.
			 * @type {boolean}
		 	 * @public
			 */
			Object.defineProperty ( this, "Disabled", {
				get: function () { return this.props.disabled; },
				set: function ( v ) {
					if ( !v )
						this.enableWindow();
					else
						this.disableWindow ();
				},
				enumerable: false,
				configurable: false
			});

			/**
			 * Set dynamic property "Top".
			 * @type {number}
			 * @public
			 */
			Object.defineProperty ( this, "Top", {
				get: function () { return this.getTop(); },
				set: function (v){ this.setTop ( v ); },
				enumerable:false,
				configurable:false
			});

			/**
			 * Set dynamic property "Bottom".
			 * @type {number}
			 * @public
			 */
			Object.defineProperty ( this, "Bottom", {
				get: function () { return this.getBottom(); },
				set: function (v){ this.setBottom ( v ); },
				enumerable:false,
				configurable:false
			});

			/**
			 * Set dynamic property "Left".
			 * @type {number}
			 * @public
			 */
			Object.defineProperty ( this, "Left", {
				get: function () { return this.getLeft(); },
				set: function (v){ this.setLeft ( v ); },
				enumerable:false,
				configurable:false
			});

			/**
			 * Set dynamic property "Right".
			 * @type {number}
			 * @public
			 */
			Object.defineProperty ( this, "Right", {
				get: function () { return this.getRight(); },
				set: function (v){ this.setRight ( v ); },
				enumerable:false,
				configurable:false
			});

			/**
			 * Set dynamic property "Height".
			 * @type {number}
			 * @public
			 */
			Object.defineProperty ( this, "Height", {
				get: function () { return this.getHeight(); },
				set: function (v){ this.setHeight ( v ); },
				enumerable:false,
				configurable:false
			});

			/**
			 * Set dynamic property innerHeight.
			 * @type {number}
			 * @public
			 */
			Object.defineProperty ( this, "innerHeight", {
				get: function () { return this.getInnerHeight(); },
				set: function (v){ this.setInnerHeight ( v ); },
				enumerable:false,
				configurable:false
			});

			/**
			 * Set dynamic property outerHeight.
			 * @type {number}
			 * @public
			 */
			Object.defineProperty ( this, "outerHeight", {
				get: function () { return this.getOuterHeight(); },
				set: function (v){ this.setOuterHeight ( v ); },
				enumerable:false,
				configurable:false
			});

			/**
			 * Set dynamic property "Width".
			 * @type {number}
			 * @public
			 */
			Object.defineProperty ( this, "Width", {
				get: function () { return this.getWidth(); },
				set: function (v){ this.setWidth ( v ); },
				enumerable:false,
				configurable:false
			});

			/**
			 * Set dynamic property innerWidth.
			 * @type {number}
			 * @public
			 */
			Object.defineProperty ( this, "innerWidth", {
				get: function () { return this.getInnerWidth(); },
				set: function (v){ this.setInnerWidth ( v ); },
				enumerable:false,
				configurable:false
			});

			/**
			 * Set dynamic property outerWidth.
			 * @type {number}
			 * @public
			 */
			Object.defineProperty ( this, "outerWidth", {
				get: function () { return this.getOuterWidth(); },
				set: function (v){ this.getOuterWidth ( v ); },
				enumerable:false,
				configurable:false
			});

			/**
			 * Set dynamic property "Title".
			 * @type {String}
			 * @public
			 */
			Object.defineProperty ( this, "Title", {
				get: function () { return this.props.title; },
				set: function (v){ this.setTitle ( v ); },
				enumerable:false,
				configurable:false
			});

			/**
			 * Set dynamic property "Icon".
			 * @type {String}
			 * @public
			 */
			Object.defineProperty ( this, "Icon", {
				get: function () { return this.props.icon; },
				set: function (v){ this.setIcon ( v ); },
				enumerable:false,
				configurable:false
			});

			/**
			 * Set dynamic property "Movable".
			 * @type {boolean}
			 * @public
			 */
			Object.defineProperty ( this, "Movable", {
				get: function () { return this.props.movable; },
				set: function (v){
					if ( v ) {
						this.props.movable = true;
						this.elements.container.setAttribute('movable','true');
					}
					else {
						this.props.movable = false;
						this.elements.container.setAttribute('movable','false');
					}
				},
				enumerable:false,
				configurable:false
			});

			/**
			 * Set dynamic property "Resizable".
			 * @type {boolean}
			 * @public
			 */
			Object.defineProperty ( this, "Resizable", {
				get: function () { return this.props.resizable; },
				set: function (v){
					if ( v ) {
						this.props.resizable = true;
						this.elements.container.setAttribute('resizable','true');
						this.enableResize();
					} else {
						this.props.resizable = false;
						this.elements.container.setAttribute('resizable','false');
						this.disableResize();
					}
				},
				enumerable:false,
				configurable:false
			});

			/**
			 * Set dynamic property "State".
			 * @type {('normal'|'minimize'|'minimized'|'maximize'|'maximized')}
			 * @public
			 */
			Object.defineProperty ( this, "State", {
				get: function () { return this.props.state; },
				set: function (v){
					if ( v == 'normal' ) this.restore();
					else if ( v == 'minimized' || v == 'minimize' ) this.minimize();
					else if ( v == 'maximized' || v == 'maximize' ) this.maximize();
				},
				enumerable:false,
				configurable:false
			});

			/**
			 * Set dynamic property "Visible".
			 * @type {boolean}
			 * @public
			 */
			Object.defineProperty ( this, "Visible", {
				get: function () { return this.elements.container.style.display != 'none'; },
				set: function (v) { this.elements.container.style.display = v ? '' : 'none'; },
				enumerable: false,
				configurable: false
			});

			/**
			 * Set dynamic property "Layout".
			 * @type {String}
			 * @public
			 */
			Object.defineProperty ( this, "Layout", {
				get: function () { return this.props.buttonLayout; },
				set: function ( v ) { this.setLayout ( v ); },
				enumerable: false,
				configurable: false
			});

			/**
			 * Set/get dynamic property "Theme".
			 * @type {null|string}
			 * @public
			 */
			Object.defineProperty ( this, "Theme", {
				get: function () { return this.props.themeClass; },
				set: function ( v ) { this.setThemeClass ( v ); },
				enumerable: false,
				configurable: false
			});

/*
	this.sendBack()
	this.normal()
	this.sendFront()

		Object.defineProperty(obj,"Text",{get:function(){return text(obj.elements[0]);},set:function(str){foreachelement(obj.elements,function(el){text(el,str);});},enumerable:false,configurable:false});
*/

		} catch ( e ) { }

		/**
		 * Method for attaching items to window. Fires "itemattached" event and "attachto" event on object to attach.
		 * @param {HTMLElement|object} item - An element or an lvzwebdesktop object to attach.
		 * @param {'top'|'left'|'right'|'bottom'|'center'} [pos] - A string telling the position to attach item to.
		 * @return {WindowObject|false} False on error. Otherwise, current window object.
		 */
		this.attachItem = function ( item, pos ) {
			var eobj = { "target" : this };
			var eobj2 = { "attachTo" : this };

			if ( typeof pos == 'undefined' || ! pos ) pos = 'center';

			var attelem = null;

			if ( pos == 'top-top' ) {
				eobj.position = 'top-top';
				eobj2.position = 'top-top';
				attelem = this.elements.innercontup;
				eobj.positionElement = attelem;
				eobj2.positionElement = attelem;
			}
			else if ( pos == 'top' ) {
				eobj.position = 'top';
				eobj2.position = 'top';
				attelem = this.elements.innercontup;
				eobj.positionElement = attelem;
				eobj2.positionElement = attelem;
			}
			else if ( pos == 'left' ) {
				eobj.position = 'left';
				eobj2.position = 'left';
				attelem = this.elements.innercontleft;
				eobj.positionElement = attelem;
				eobj2.positionElement = attelem;
			}
			else if ( pos == 'right' ) {
				eobj.position = 'right';
				eobj2.position = 'right';
				attelem = this.elements.innercontright;
				eobj.positionElement = attelem;
				eobj2.positionElement = attelem;
			}
			else if ( pos == 'bottom' ) {
				eobj.position = 'bottom';
				eobj2.position = 'bottom';
				attelem = this.elements.innercontbtm;
				eobj.positionElement = attelem;
				eobj2.positionElement = attelem;
			}
			else {
				eobj.position = 'center';
				eobj2.position = 'center';
				attelem = this.elements.bodycontainer;
				eobj.positionElement = attelem;
				eobj2.positionElement = attelem;
			}

			if ( item instanceof HTMLElement ) {
				if ( typeof item.dataLVZWDESKTOPOBJ == 'object' ) {
					var c = window.lvzwebdesktop.isPartOf(item.dataLVZWDESKTOPOBJ);
					if ( c !== false && typeof item.dataLVZWDESKTOPOBJ.elements == 'object' && item.dataLVZWDESKTOPOBJ.elements.container instanceof HTMLElement ) {
						eobj2.target = item.dataLVZWDESKTOPOBJ;
						eobj2.attachElement = item;
						eobj.item = item.dataLVZWDESKTOPOBJ;
						if ( pos == 'top-top' )
							attelem.insertBefore ( item, attelem.firstElementChild );
						else
							attelem.appendChild ( item );
						item.dataLVZWDESKTOPOBJ.fireEvent('attachto', eobj2);
					}
					else {
						eobj.item = item;
						attelem.appendChild ( item );
					}
				}
				else {
					eobj.item = item;
					attelem.appendChild ( item );
				}
			}
			else {
				var c = window.lvzwebdesktop.isPartOf( item );
				if ( c === false ) {
					console.error ( "Argument must be an element or an lvzwebdesktop object with a container element.");
					return false;
				}
				if ( typeof item.elements == 'object' && item.elements.container instanceof HTMLElement ) {
					eobj2.target = item;
					eobj2.attachElement = item.elements.container;
					eobj.item = item;
					attelem.appendChild ( item.elements.container );
					item.fireEvent('attachto', eobj2);
				}
			}
			this.resize();
			this.fireEvent('itemattached', eobj);
			// if ( eobj.preventDefault.a === true ) return this;
			return this;
		};

		/**
		 * Give focus to current window object.
		 * @return {WindowObject} Current window object.
		 */
		this.focus = function () {
			var sct = this.elements.bodycontainer.scrollTop;
			var scl = this.elements.bodycontainer.scrollLeft;
			if ( this.desktop !== null ) this.desktop.giveFocus(this);
			else if ( ! this.elements.container.classList.contains ( this.props.classNames.hasfocus ) ) {

				var tmpd = this.elements.container.parentNode;
				for ( var i = 0; i < tmpd.children.length; i++ ) {
					if ( tmpd.children[i].dataWindowObject && tmpd.children[i].classList.contains ( this.props.classNames.hasfocus ) )
						tmpd.children[i].dataWindowObject.blur();
				}

				this.elements.container.parentNode.appendChild ( this.elements.container );

			}

			var o = this.elements.container.parentNode;
			while ( o && o.nodeName != '#document' ) {
				if ( typeof o.dataWindowObject == 'object' ) {
					o.dataWindowObject.focus ();
				}
				o = o.parentNode;
			}

			// if ( this.desktop !== null && this.desktop.focusedObject === this )
			this.elements.container.classList.add ( this.props.classNames.hasfocus );
			this.elements.container.classList.remove ( this.props.classNames.blured );

			this.elements.bodycontainer.focus();

			this.elements.bodycontainer.scrollTop = sct;
			this.elements.bodycontainer.scrollLeft = scl;

			return this;
		};

		/**
		 * Take focus from current window object. Fires "blur" event.
		 *
		 * @return {WindowObject} Current window object.
		 */
		this.blur = function () {
			if ( this.desktop !== null && this.desktop.focusedObject === this )
				this.desktop.focusedObject = null;

			this.elements.container.classList.add ( this.props.classNames.blured );
			this.elements.container.classList.remove ( this.props.classNames.hasfocus );

			this.fireEvent('blur', {
				"target" : this
			});

			return this;
		};

		/**
		 * Move window to the left edge of the desktop.
		 *
		 * @return {WindowObject} Current window object.
		 */
		this.positionLeft = function () {
			if ( this.props.movable !== true ) return this;
			this.props.left = 0;
			this.position();
			return this;
		};

		/**
		 * Move window to the right edge of the desktop.
		 *
		 * @return {WindowObject} Current window object.
		 */
		this.positionRight = function () {
			if ( this.props.movable !== true ) return this;
			var desktopElement;
			if ( this.desktop !== null ) desktopElement = this.desktop.elements.root;
			else desktopElement = this.elements.container.parentNode;
			this.props.left = desktopElement.offsetWidth - this.elements.container.offsetWidth;
			this.position();
			return this;
		};

		/**
		 * Move window to the horizontal middle of the desktop.
		 *
		 * @return {WindowObject} Current window object.
		 */
		this.positionHMiddle = function () {
			if ( this.props.movable !== true ) return this;
			var desktopElementW;
			if ( this.desktop !== null ) desktopElementW = this.desktop.elements.root.offsetWidth;
			else desktopElementW = this.elements.container.parentNode.offsetWidth;
			this.props.left = (desktopElementW / 2 ) -  (this.elements.container.offsetWidth / 2 );
			this.position();
			return this;
		};

		/**
		 * Move window to the top edge of the desktop.
		 *
		 * @return {WindowObject} Current window object.
		 */
		this.positionTop = function () {
			if ( this.props.movable !== true ) return this;
			this.props.top = 0;
			this.position();
			return this;
		};

		/**
		 * Move window to the bottom edge of the desktop.
		 *
		 * @return {WindowObject} Current window object.
		 */
		this.positionBottom = function () {
			if ( this.props.movable !== true ) return this;
			var desktopElementH;
			if ( this.desktop !== null ) desktopElementH = this.desktop.elements.root.offsetHeight;
			else desktopElementH = this.elements.container.parentNode.offsetHeight;
			this.props.top = desktopElementH - this.elements.container.offsetHeight;
			this.position();
			return this;
		};

		/**
		 * Move window to the vertical middle of the desktop.
		 *
		 * @return {WindowObject} Current window object.
		 */
		this.positionVMiddle = function () {
			if ( this.props.movable !== true ) return this;
			var desktopElementH;
			if ( this.desktop !== null ) desktopElementH = this.desktop.elements.root.offsetHeight;
			else desktopElementH = this.elements.container.parentNode.offsetHeight;
			this.props.top = desktopElementH / 2 - this.elements.container.offsetHeight / 2;
			this.position();
			return this;
		};

		/**
		 * Set window's application icon. Fires "iconchange" Event.
		 *
		 * @param {string} src - The url of the icon to use.
		 * @return {WindowObject} Current window object.
		 */
		this.setIcon = function ( src ) {
			var eventdata = {};
			eventdata.target = this;
			eventdata.oldIcon = this.props.icon;
			this.props.icon = src;
			if ( this.props.setAppIconOnMenuIcon ) this.elements.menuBtn.style.backgroundImage = "url('" + src + "')";
			eventdata.newIcon = this.props.icon;
			eventdata.icon = this.props.icon;
			this.elements.container.setAttribute('icon',src);
			this.fireEvent('iconchange', eventdata);

			return this;
		};

		/**
		 * Set window top position.
		 *
		 * @param {number} t - The vertical position of window in pixels.
		 * @return {WindowObject} Current window object.
		 */
		this.setTop = function ( t ) {
			this.props.top = t;
			if ( this.props.minTop !== false && this.props.top < this.props.minTop ) this.props.top = this.props.minTop;
			if ( this.props.maxTop !== false && this.props.top > this.props.maxTop ) this.props.top = this.props.maxTop;
			this.position();
			return this;
		};

		/**
		 * Get window's vertical distance from the top border of it's container (parent element or desktop).
		 * @return {nubmer} Window's vertical distance from the top border of it's parent container element, in nubmer of pixels.
		 */
		this.getTop = function () {
			return this.props.top;
		};

		/**
		 * Set window's vertical distance in relation width the bottom border of it's parent container element (parent element or desktop).
		 * @param {number} b - The desired vertical distance from the bottom border of parent element, in number of pixels.
		 * @return {WindowObject) Current window object.
		 */
		this.setBottom = function ( b ) {
			this.props.top = this.elements.container.parentNode.offsetHeight - ( b + this.elements.container.offsetHeight);
			if ( this.props.minTop !== false && this.props.top < this.props.minTop ) this.props.top = this.props.minTop;
			if ( this.props.maxTop !== false && this.props.top > this.props.maxTop ) this.props.top = this.props.maxTop;
			this.position();
			return this;
		};

		/**
		 * Get window's bottom position.
		 * @return {number} Window's bottom border distance from the bottom border of it's container (parent element or desktop) in pixels.
		 */
		this.getBottom = function () {
			return this.elements.container.parentNode.offsetHeight - (this.props.top+this.elements.container.offsetHeight);
		};

		/**
		 * Set window left position.
		 *
		 * @param {number} l - The horizontal position of window in pixels.
		 * @return {WindowObject} Current window object.
		 */
		this.setLeft = function ( l ) {
			this.props.left = l;
			if ( this.props.minLeft !== false && this.props.left < this.props.minLeft ) this.props.left = this.props.minLeft;
			if ( this.props.maxLeft !== false && this.props.left > this.props.maxLeft ) this.props.left = this.props.maxLeft;
			this.position();
			return this;
		};

		/**
		 * Get window's left position.
		 *
		 * @return {number} Window's left border distance from the left border of it's container (parent element or desktop) in pixels.
		 */
		this.getLeft = function () {
			return this.props.left;
		};

		/**
		 * Set window's horizontal position in relation width the right border of it's container element ( parent element or desktop ).
		 * @param {number} r - Right position in number of pixels.
		 * @return {WindowObject} Current window object.
		 */
		this.setRight = function ( r ) {
			this.props.left = this.elements.container.parentNode.offsetWidth - ( r + this.elements.container.offsetWidth);
			if ( this.props.minLeft !== false && this.props.left < this.props.minLeft ) this.props.left = this.props.minLeft;
			if ( this.props.maxLeft !== false && this.props.left > this.props.maxLeft ) this.props.left = this.props.maxLeft;
			this.position();
			return this;
		};

		/**
		 * Get window's right position.
		 * @return {number} Window's right border distance from the right border of it's container (parent element or desktop) in pixels.
		 */
		this.getRight = function () {
			return this.elements.container.parentNode.offsetWidth - (this.props.left+this.elements.container.offsetWidth);
		};

		/**
		 * Set window's width.
		 * @param {number} w - The window's desired width.
		 * @return {WindowObject} Current window object.
		 */
		this.setWidth = function ( w ) {
			this.elements.container.setAttribute('width',w);
			return this[this.props.setWidthDefaultMethod](w);
		};

		/**
		 * Get window's width.
		 * @return {number} Window's width in number of pixels.
		 */
		this.getWidth = function ( ) {
			return this['get'+this.props.setWidthDefaultMethod.substr(3)]();
		};

		/**
		 * Set window's inner width.
		 *
		 * @param {number} w - The windows desired inner width in number of pixels.
		 * @return {WindowObject} Current window object.
		 */
		this.setInnerWidth = function ( w ) {
			this.props.width = parseFloat(w);
			this.resize();
			return this;
		};

		/**
		 * Method for setting window's outer width.
		 *
		 * @param {number} w - The window's desired width in pixels.
		 * @return {WindowObject} Current window object.
		 */
		this.setOuterWidth = function ( w ) {
			var relb = parseFloat(getComputedStyle(this.props.resizeElement).getPropertyValue('border-left-width'));
			var rerb = parseFloat(getComputedStyle(this.props.resizeElement).getPropertyValue('border-right-width'));
			var d = this.elements.container.offsetWidth - (relb+rerb+this.props.resizeElement.offsetWidth);
			this.props.width = w-d;
			this.resize();
			return this;
		};

		/**
		 * Method for getting window's outer width.
		 *
		 * @return {number} The window's outer width in number of pixels.
		 */
		this.getOuterWidth = function () {
			return this.elements.container.offsetWidth;
		};

		/**
		 * Method for getting window's inner width.
		 *
		 * @return {number} The window's inner width in number of pixels.
		 */
		this.getInnerWidth = function () {
			return this.props.width;
		};

		/**
		 * Set window's height.
		 * @param {number} h - The window's desired height.
		 * @return {WindowObject} Current window object.
		 */
		this.setHeight = function ( h ) {
			this.elements.container.setAttribute('height',h);
			return this[this.props.setHeightDefaultMethod](h);
		};

		/**
		 * Method for getting window's height.
	 	 * @return {number} Window's height in number of pixels.
		 */
		this.getHeight = function ( ) {
			return this['get'+this.props.setHeightDefaultMethod.substr(3)]();
		};

		/**
		 * Set window's inner height.
		 *
		 * @param {number} h - The window's desired inner height in number of pixels.
		 * @return {WindowObject} Current window object.
		 */
		this.setInnerHeight = function ( h ) {
			this.props.height = h;
			this.resize();
			return this;
		};

		/**
		 * Method for setting window's outer height.
		 *
		 * @param {number} h - The window's desired outer height in pixels.
		 * @return {WindowObject} Current window object.
		 */
		this.setOuterHeight = function ( h ) {
			var retb = parseFloat(getComputedStyle(this.props.resizeElement).getPropertyValue('border-top-width'));
			var rebb = parseFloat(getComputedStyle(this.props.resizeElement).getPropertyValue('border-bottom-width'));
			var d = this.elements.container.offsetHeight - (retb+rebb+this.props.resizeElement.offsetHeight);
			this.props.height = h-d;
			this.resize();
			return this;
		};

		/**
		 * Method for getting window's outer height.
		 *
		 * @return {number} The window's outer height in number of pixels.
		 */
		this.getOuterHeight = function () {
			return this.elements.container.offsetHeight;
		};

		/**
		 * Method for getting window's inner height.
		 *
		 * @return {number} The window's inner height in number of pixels.
		 */
		this.getInnerHeight = function () {
			return this.props.height;
		};

		/**
		 * Set the window's position from the properties top and left and fire the "move" event.
		 *
		 * @return {WindowObject} Current window object.
		 */
		this.position = function () {
			var eventData = {};
			eventData.target = this;

			eventData.previousPosition = {};
			eventData.previousPosition.top = this.elements.container.offsetTop;
			eventData.previousPosition.left = this.elements.container.offsetLeft;

			if ( this.props.minTop !== false && this.props.top < this.props.minTop ) this.props.top = this.props.minTop;
			if ( this.props.maxTop !== false && this.props.top > this.props.maxTop ) this.props.top = this.props.maxTop;
			if ( this.props.minLeft !== false && this.props.left < this.props.minLeft ) this.props.left = this.props.minLeft;
			if ( this.props.maxLeft !== false && this.props.left > this.props.maxLeft ) this.props.left = this.props.maxLeft;

			this.elements.container.style.top = this.props.top + 'px';
			this.elements.container.style.left = this.props.left + 'px';
			this.elements.container.setAttribute('top',this.props.top);
			this.elements.container.setAttribute('left',this.props.left);

			eventData.currentPosition = {};
			eventData.currentPosition.top = this.elements.container.offsetTop;
			eventData.currentPosition.left = this.elements.container.offsetLeft;

			eventData.move={};
			eventData.move.top = eventData.currentPosition.top - eventData.previousPosition.top;
			eventData.move.left = eventData.currentPosition.left - eventData.previousPosition.left;

			this.fireEvent('move', eventData);
			return this;
		};

		/**
		 * Resize the window for the properties width and height and fire "resize" event.
		 *
		 * @return {WindowObject} Current window object.
		 */
		this.resize = function () {

			if ( this.props.matchContentSize )
				this.props.matchContentSize = 
				this.props.prevHeight = 
				this.props.prevWidth =
				false
			;

			if ( ! this.props.theme ) this.buildTheme();
			var eventData = {};

			this.props.width = parseInt(this.props.width);
			this.props.height = parseInt(this.props.height);

			eventData.oldWidth = this.props.resizeElement.offsetWidth;
			eventData.oldHeight = this.props.resizeElement.offsetHeight;

			if ( this.props.width < this.props.minWidth ) this.props.width = this.props.minWidth;
			else if ( this.props.maxWidth !== false && this.props.width > this.props.maxWidth ) this.props.width = this.props.maxWidth;

			if ( this.props.height < this.props.minHeight ) this.props.height = this.props.minHeight;
			else if ( this.props.maxHeight !== false && this.props.height > this.props.maxHeight ) this.props.height = this.props.maxHeight;

			this.props.resizeElement.style.width = this.props.width + 'px';
			this.props.resizeElement.style.height = this.props.height + 'px';

			this.elements.innercontright.style.height = this.elements.bodycontainer.offsetHeight + 'px';
			this.elements.innercontleft.style.height = this.elements.bodycontainer.offsetHeight + 'px';

			var controlsWidth = 0;
			controlsWidth = this.elements.titlerightcontrols.offsetWidth + this.elements.titleleftcontrols.offsetWidth;

			var innerclr = (
				this.elements.innercontright.offsetWidth
					+ this.elements.innercontleft.offsetWidth
			);

			var innercw = (
				this.elements.bodycontainer.offsetWidth
					+ this.elements.innercontright.offsetWidth
					+ this.elements.innercontleft.offsetWidth
			) + 'px';

			var innerch = (
				this.elements.innercontmid.offsetHeight
					+ this.elements.innercontup.offsetHeight
					+ this.elements.innercontbtm.offsetHeight
			) + 'px';

			var ttlbrdrl = parseFloat(getComputedStyle(this.elements.titlecontainer).getPropertyValue('border-left-width'));
			var ttlbrdrr = parseFloat(getComputedStyle(this.elements.titlecontainer).getPropertyValue('border-right-width'));
			var ttlbrdr = ttlbrdrr + ttlbrdrl;
			var ttlw = (this.props.width  - ttlbrdr);

			this.elements.innercontmid.style.width = innercw;
			this.elements.innercontmid.style.height = this.elements.bodycontainer.offsetHeight + 'px';

			this.elements.innercont.style.width = this.elements.innercontmid.offsetWidth + 'px';
			this.elements.innercont.style.height = innerch;

			this.elements.topbordercenter.style.width = "calc(" + (this.props.width+innerclr) + "px - " + this.props.theme.border['corner-width'] + " - " + this.props.theme.border['corner-width'] + " - " + this.props.theme.border['border-weight'] + " - " + this.props.theme.border['border-weight'] +  ")";

			this.elements.bottombordercenter.style.width = "calc(" + (this.props.width+innerclr) + "px - " + this.props.theme.border['corner-width'] + " - " + this.props.theme.border['corner-width'] + " - " + this.props.theme.border['border-weight']  + " - " + this.props.theme.border['border-weight'] + ")";
			
			this.elements.titlecontainer.style.width = (ttlw + innerclr )+"px";
			this.elements.titlecenter.style.width = ((ttlw + innerclr)  - controlsWidth) +"px";

			this.elements.container.style.width = (
				this.elements.centercont.offsetWidth
					+ this.elements.leftbordercont.offsetWidth
					+ this.elements.rightbordercont.offsetWidth
					+ this.elements.innercontright.offsetWidth
					+ this.elements.innercontleft.offsetWidth
					- innerclr
			) + 'px';

			this.elements.innercontup.style.width = innercw;
			this.elements.innercontbtm.style.width = innercw ;

			var brdrh = (
				this.elements.topbordercontainer.offsetHeight +
					this.elements.titlecontainer.offsetHeight +
					this.props.resizeElement.offsetHeight +
					this.elements.innercontup.offsetHeight +
					this.elements.innercontbtm.offsetHeight + 
					this.elements.bottombordercontainer.offsetHeight
			) - (
				this.elements.leftbordertopcrnr.offsetHeight * 2 +
					this.elements.lefttopbrdrbox.offsetHeight * 2
			)
			;

			this.elements.leftbordercenter.style.height = brdrh + 'px';
			this.elements.rightbordercenter.style.height = brdrh + 'px';

			eventData.target = this;
			eventData.newWidth = this.props.width;
			eventData.newHeight = this.props.height;

			this.elements.disableBody.style.top = ( this.elements.topbordercontainer.offsetHeight + this.elements.titlecontainer.offsetHeight)+ 'px';
			this.elements.disableBody.style.left = this.elements.leftbordercont.offsetWidth + 'px';
			this.elements.disableBody.style.height = this.props.resizeElement.offsetHeight + 'px';
			this.elements.disableBody.style.width = this.props.resizeElement.offsetWidth +"px";

			this.fireEvent('resize', eventData);
			return this;
		};

		/**
		 * Method for resizing window based on content size.
		 *
		 * @param {boolean} t - true for resizing window on content, false otherwise.
		 * @return {WindowObject} Current window object.
		 */
		this.resizeOnContent = function ( t ) {

			this.props.resizeOnContent = t;

			if ( this.elements.bodycontainer.children.length > 0  ) {
				if (this.props.resizeOnContent === true) {
					this.resizeObserver.observe(this.elements.bodycontainer.children[0]);
				}
				else {
					this.resizeObserver.unobserve(this.elements.bodycontainer.children[0]);
				}
			}

			// resizeWindowOnContent ( [ { target : this.elements.bodycontainer.children[0] } ] );
			this.matchContentSize();
			return this;

		};

		/**
		 * Sets or changes the window title. Fires "titlechange" event.
		 *
		 * @param {string} t - The new title.
		 * @return {WindowObject} Current window object.
		 */
		this.setTitle = function ( t ) {
			var eventData = {
				"oldTitle" : this.props.title,
				"newTitle" : t,
				"target" : this
			};
			this.props.title = t;
			this.elements.container.setAttribute('caption',t);
			this.elements.titletext.innerText = this.props.title;
			this.fireEvent('titlechange', eventData);
			return this;
		};

		/**
		 * Move title to the bottom of the window.
		 *
		 * @return {WindowObject} Current window object.
		 */
		this.moveTitleToBottom = function () {
			this.props.titlePosition = 'bottom';
			this.elements.centercont.insertBefore (
				this.elements.titlecontainer,
				this.elements.bottombordercontainer
			);
			return this;
		}

		/**
		 * Move title to the top of the window.
		 *
		 * @return {WindowObject} Current window object.
		 */
		this.moveTitleToTop = function () {
			this.props.titlePosition = 'top';
			this.elements.centercont.insertBefore (
				this.elements.titlecontainer,
				this.elements.innercont
			);
			return this;
		}

		/**
		 * Set window to appear behind normal windows.
		 *
		 * @return {WindowObject} Current window object.
		 */
		this.sendBack = function () {
			this.elements.container.style.zIndex = 6;
			return this;
		};

		/**
		 * Set window in normal priority.
		 *
		 * @return {WindowObejct} Current window object.
		 */
		this.normal = function () {
			this.elements.container.style.zIndex = 8;
			return this;
		};

		/**
		 * Set window to appear infront of normal windows.
		 *
		 * @return {WindowObject} Current window object.
		 */
		this.sendFront = function () {
			this.elements.container.style.zIndex = 12;
			return this;
		};

		/**
		 * Run the "close" event and destroy current window.
		 */
		this.close = function () {
			var eventdata = {"target":this};
			this.fireEvent('close',eventdata);
			if ( eventdata.preventDefault.a === true ) return this;
			closeWindow( this );
		};

		/**
		 * Fold window under it's title and run "fold" event.
		 *
		 * @return {WindowObject} Current window object.
		 */
		this.fold = function () {
			this.disableResize();
			this.elements.container.style.height = (this.elements.titlecontainer.offsetHeight) + 'px';
			this.elements.container.style.width = (this.elements.titlecontainer.offsetWidth) + 'px';
			this.props.resizeElement.style.display = 'none';
			this.props.state = 'folded';
			this.fireEvent('fold',{"target":this});
			return this;
		};

		/**
		 * Unfold window. run "unfold" event.
		 *
		 * @return {WindowObject} Current window object.
		 */
		this.unfold = function () {
			this.props.resizeElement.style.display = '';
			this.restore();
			this.fireEvent('unfold',{"target":this});
			return this;
		};

		/**
		 * Toggle window between fold and unfold.
		 *
		 * @return {WindowObject} Current window object.
		 */
		this.toggleFold = function () {
			if ( this.props.state != 'folded' ) return this.fold();
			return this.unfold();
		}

		/**
		 * Run the "minimize" event and minimize current window. Fires "minimize" event.
		 *
		 * @return {WindowObject} Current window object.
		 */
		this.minimize = function () {
			if ( this.props.minimizable === false ) return this;

			var tmpdb = this.props.disableBorder;
			this.disableResize();
			this.props.disableBorder = tmpdb;

			this.elements.container.style.height = (this.elements.titlecontainer.offsetHeight) + 'px';
			var tmpwidth = this.props.width;
			this.props.width = 200;
			this.resize();
			this.props.width = tmpwidth;
			this.props.state = 'minimized';
			this.fireEvent('minimize',{"target":this});
			return this;
		};

		/**
		 * Minimizes window or restores it if it's already minimized.
		 *
		 * @return {WindowObject} Current window object.
		 */
		this.minimizeRestore = function () {
			if ( this.props.state == 'minimized' ) this.restore();
			else return this.minimize();
			return this;
		}

		/**
		 * Method for enabling window (make boyd controls reachable).
		 * @return {WindowObject} Current window object.
		 */
		this.enableWindow = function () {
			this.props.disabled = false;
			this.elements.disableBody.style.display = '';
			this.elements.container.classList.remove ( this.props.classNames.disabled );
			return this;
		};

		/**
		 * Method for disabling window (make body controls unreachable).
		 * @return {WindowObject} Current window object.
		 */
		this.disableWindow = function () {
			this.props.disabled = true;
			this.elements.disableBody.style.display = 'block';
			this.elements.container.classList.add ( this.props.classNames.disabled );
			return this;
		};

		/**
		 * Method for disabling (hiding) resizing borders on window.
		 * @return {WindowObject} Current window object.
		 */
		this.disableResize = function () {
			this.elements.leftbordercont.style.display = 'none';
			this.elements.topbordercontainer.style.display = 'none';
			this.elements.bottombordercontainer.style.display = 'none';
			this.elements.rightbordercont.style.display = 'none';
			this.props.disableBorder = true;
			this.resize();
			return this;
		};

		/**
		 * Method for enabling resizing borders on window.
		 * @return {WindowObject} Current window object.
		 */
		this.enableResize = function () {
			this.elements.leftbordercont.style.display = '';
			this.elements.topbordercontainer.style.display = '';
			this.elements.bottombordercontainer.style.display = '';
			this.elements.rightbordercont.style.display = '';
			this.props.disableBorder = false;
			this.resize();
			return this;
		};

		/**
		 * Method for toggling the appearance of window resize border.
		 * @return {WindowObject} Current window object.
		 */
		this.toggleBorder = function () {
			if ( ! this.props.disableBorder ) {
				this.disableResize();
				//this.props.disableBorder = true;
			} else {
				this.enableResize();
				//this.props.disableBorder = false;
			}
			return this;
		};

		/**
		 * Method for hidding window title.
		 * @return {WindowObject} Current window object.
		 */
		this.hideTitle = function () {
			this.elements.titlecontainer.style.display = 'none';
			this.resize();
			return this;
		};

		/**
		 * Method for showing window title.
		 * @return {WindowObject} Current window object.
		 */
		this.showTitle = function () {
			this.elements.titlecontainer.style.display = '';
			this.resize();
			return this;
		};

		/**
		 * Method for toggling visibility of window title.
		 * @return {WindowObject} Current window object.
		 */
		this.toggleTitle = function () {
			if ( this.elements.titlecontainer.style.display == '' ) this.hideTitle(); else this.showTitle();
			return this;
		};

		/**
		 * Method for changing title buttons layout.
		 * @param {String} layout - String in the form: "comma seperated button names for left side):(comma seperated button names for right side)".
		 *							example: "menu:minimize,maximizeRestore,close"
		 * @return {WindowObject} Current window object.
		 */
		this.setLayout = function ( layout ) {
			var windowControls = layout.split(':');
			if ( windowControls.length == 2 ) {
				this.props.buttonLayout = layout;
				this.elements.container.setAttribute('button-layout',layout);

				// Remove buttons from titleleftcontrols and titlerightcontrols elements.
				for ( var i = 0; i < this.elements.titleleftcontrols.length; i ++ ) this.elements.titleleftcontrols.removeChild ( this.elements.titleleftcontrols[i] );
				for ( i = 0; i < this.elements.titlerightcontrols.length; i ++ )this.elements.titlerightcontrols.removeChild ( this.elements.titlerightcontrols[i] );
				this.elements.titleleftcontrols.innerHTML = '';
				this.elements.titlerightcontrols.innerHTML = '';

				// Add contorls to titleleftcontrols element.
				windowControls[0] = windowControls[0].split(/,/g);
				for ( i = 0 ; i < windowControls[0].length; i ++ ) {
					windowControls[0][i] = windowControls[0][i].trim();
					if ( typeof this.elements[windowControls[0][i] + 'Btn']  != 'undefined' )
						this.elements.titleleftcontrols.appendChild(this.elements[windowControls[0][i] + 'Btn']);
				}

				// Add controls to titlerightcontrols element.
				windowControls[1] = windowControls[1].split(/,/g);
				for ( i = 0 ; i < windowControls[1].length; i ++ ) {
					windowControls[1][i] = windowControls[1][i].trim();
					if ( typeof this.elements[windowControls[1][i] + 'Btn']  != 'undefined' )
						this.elements.titlerightcontrols.appendChild(this.elements[windowControls[1][i] + 'Btn']);
				}
				this.resize();
			}
			
			return this;
		};

		/**
		 * Method for resizing window matching the content's size.
		 * @return {WindowObject} Current window object.
		 */
		this.matchContentSize = function () {
			var tmph = this.props.resizeElement.style.height;
			var tmpw = this.props.resizeElement.style.width;

			var prevHeight = this.props.height;
			var prevWidth = this.props.width;

			this.props.resizeElement.style.display = 'inline-block';
			this.props.resizeElement.style.height = '';
			this.props.resizeElement.style.width = '';
			var innerHeight = this.props.resizeElement.offsetHeight;
			var innerWidth = this.props.resizeElement.offsetWidth;
			this.props.resizeElement.style.display = 'block';
			if ( this.props.minHeight !== false && innerHeight < this.props.minHeight ) innerHeight = this.props.minHeight;
			if ( this.props.maxHeight !== false && innerHeight > this.props.maxHeight ) innerHeight = this.props.maxHeight;

			if ( this.props.minWidth !== false && innerWidth < this.props.minWidth ) innerWidth = this.props.minWidth;
			if ( this.props.maxWidth !== false && innerWidth > this.props.maxWidth ) innerWidth = this.props.maxWidth;

			this.props.height = innerHeight+1;
			this.props.width = innerWidth+1;
			this.props.resizeElement.style.height = innerHeight + 'px';
			this.props.resizeElement.style.width = innerWidth + 'px';

			this.resize();

			this.props.matchContentSize = true;
			this.props.prevHeight = prevHeight;
			this.props.prevWidth = prevWidth;

			return this;
		};

		/**
		 * Toggle between windows size end windows content size.
		 * @return {WindowObject} Current window object.
		 */
		this.toggleContentSize = function () {
			if ( this.props.matchContentSize === true ) {
				this.props.height = this.props.prevHeight;
				this.props.width = this.props.prevWidth;
				this.resize();
				this.props.matchContentSize = false;
				this.props.prevHeight = false;
				this.props.prevWidth  = false;
			}
			else return this.matchContentSize();
			return this;
		};

		/**
		 * Run the "maximize" event and maximize current window. Fires "maximize" event.
		 *
		 * @return {WindowObject} Current window object.
		 */
		this.maximize = function () {
			if ( this.props.maximizable === false ) return this;

			var eobj = {"target":this}
			this.fireEvent('maximize',eobj);
			if ( eobj.defaultPrevented === true ) return this;

			eobj.oldWidth = this.props.resizeElement.offsetWidth;
			eobj.oldHeight = this.props.resizeElement.offsetHeight;

			var tmpdb = this.props.disableBorder;
			this.disableResize();
			this.props.disableBorder = tmpdb;

			if ( this.desktop === null ) {
				this.elements.container.style.position = 'fixed';
			} else {
				this.elements.container.style.position = 'absolute';
			}
			this.elements.container.style.top = '0px';
			this.elements.container.style.left = '0px';
			this.elements.container.style.width = 'calc(100% - 3px)';
			this.elements.container.style.height = 'calc(100% - 3px)';

			this.elements.container.classList.add(this.props.classNames.maximized);

			// Inner container
			this.elements.innercont.style.width = (this.elements.container.offsetWidth - (this.elements.leftbordercont.offsetWidth+this.elements.rightbordercont.offsetWidth) ) + 'px';
			this.elements.innercont.style.height = (this.elements.container.offsetHeight - (this.elements.topbordercontainer.offsetHeight+this.elements.bottombordercontainer.offsetHeight) ) + 'px';

			// this.props.resizeElement.style.width = ( this.elements.container.offsetWidth - 2) + 'px';
			// this.props.resizeElement.style.height = ( this.elements.container.offsetHeight - this.elements.titlecontainer.offsetHeight ) + 'px';

			this.elements.innercontup.style.width = ( this.elements.innercont.offsetWidth) + 'px';
			this.elements.innercontbtm.style.width = ( this.elements.innercont.offsetWith) + 'px';

			// var ich = ( this.elements.container.offsetHeight - this.elements.titlecontainer.offsetHeight - this.elements.innercontup.offsetHeight - this.elements.innercontbtm.offsetHeight );

			this.elements.innercontmid.style.width  = ( this.elements.innercont.offsetWidth) + 'px';
			this.elements.innercontmid.style.height = ( this.elements.innercont.offsetHeight - (this.elements.innercontup.offsetHeight  + this.elements.innercontbtm.offsetHeight )) + 'px';

			// Body
			this.elements.bodycontainer.style.width  = ( this.elements.innercont.offsetWidth  - (this.elements.innercontleft.offsetWidth + this.elements.innercontright.offsetWidth)) + 'px';
			this.elements.bodycontainer.style.height = this.elements.innercontmid.offsetHeight + 'px';

			this.elements.innercontleft.style.height = this.elements.bodycontainer.offsetHeight + 'px';
			this.elements.innercontright.style.height = this.elements.bodycontainer.offsetHeight + 'px';

			var innerclr = (
				this.elements.innercontright.offsetWidth
					+ this.elements.innercontleft.offsetWidth
			);

			var controlsWidth = 0;
			controlsWidth = this.elements.titlerightcontrols.offsetWidth + this.elements.titleleftcontrols.offsetWidth;
			this.elements.titlecontainer.style.width = (innerclr+this.props.resizeElement.offsetWidth)+"px";
			this.elements.titlecenter.style.width = ((innerclr+this.props.resizeElement.offsetWidth) - controlsWidth) +"px";

			this.elements.maximizeRestoreBtn.dataElement = 'restoreBtn';
			this.elements.maximizeRestoreBtn.classList.add( this.props.classNames.restoreBtn );
			this.elements.maximizeRestoreBtn.classList.remove( this.props.classNames.maximizeBtn );
			this.decorateChange(this.elements.maximizeRestoreBtn);

			this.elements.disableBody.style.top = ( this.elements.topbordercontainer.offsetHeight + this.elements.titlecontainer.offsetHeight)+ 'px';
			this.elements.disableBody.style.left = this.elements.leftbordercont.offsetWidth + 'px';
			this.elements.disableBody.style.height = this.elements.innercont.offsetHeight + 'px';
			this.elements.disableBody.style.width = this.elements.innercont.offsetWidth +"px";

			this.props.state = 'maximized';

			eobj.newWidth = this.props.resizeElement.offsetWidth;
			eobj.newHeight = this.props.resizeElement.offsetHeight;
			return this.fireEvent('resize',eobj);

		};

		/**
		 * Run the "restore" event and restore ( unmaximize/unminimize ) current window. Fires "restore" event.
		 *
		 * @return {WindowObject} Current window object.
		 */
		this.restore = function () {
			this.elements.container.style.display = '';
			this.elements.container.style.position = 'absolute';
			this.elements.container.style.top = this.props.top+'px';
			this.elements.container.style.left = this.props.left+'px';
			this.elements.container.style.width = '';
			this.elements.container.style.height = '';
			this.elements.container.classList.remove(this.props.classNames.maximized);

			this.elements.maximizeRestoreBtn.dataElement = 'maximizeBtn';
			this.elements.maximizeRestoreBtn.classList.remove( this.props.classNames.restoreBtn );
			this.elements.maximizeRestoreBtn.classList.add( this.props.classNames.maximizeBtn );
			this.decorateChange(this.elements.maximizeRestoreBtn);

			if ( this.props.disableBorder ) this.disableResize();
			else this.enableResize();

			this.resize();
			this.props.state = 'normal';
			this.fireEvent('restore',{"target":this});
			return this;
		};

		/**
		 * Maximizes the window or if it's already maximized the restores it to original size.
		 *
		 * @return {WindowObject} Current window object.
		 */
		this.maximizeRestore = function () {
			if ( this.elements.maximizeRestoreBtn.dataElement == 'maximizeBtn' ) {
				this.maximize();
			} else {
				this.restore();
			}
			return this;
		}

		/**
		 * Hide window.
		 *
		 * @return {WindowObject} Current window object.
		 */
		this.hide = function () {
			var eventdata = {"target":this};
			this.fireEvent('hide',eventdata);
			if ( eventdata.preventDefault.a === true ) return this;
			this.elements.container.style.display = 'none';
			return this;
		};

		/**
		 * Show window if is hidden.
		 *
		 * @return {WindowObject} Current window object.
		 */
		this.show = function () {
			var eventdata = {"target":this};
			this.fireEvent('show',eventdata);
			if ( eventdata.preventDefault.a === true ) return this;
			this.elements.container.style.display = '';
			return this;
		};

		/**
		 * Return's true if window is hidden or false if it's visible.
		 * @return {boolean}
		 */
		this.isHidden = function () {
			return this.elements.container.style.display == 'none';
		};

		/**
		 * Begin moving the window using the keyboard.
		 *
		 * @return {WindowObject} Current window object.
		 */
		this.keyboardMove = function () {
			if ( this.props.movable !== true ) return this;
			wtitleMove.obj = this;
			wtitleMove.elem = this.elements.container;
			wtitleMove.elemData = offset(this.elements.container);
			wtitleMove.keyboardAction = "move";
			wtitleMove.mousedata = {
				x: 0,
				dx : parseFloat(this.props.left),
				y: 0,
				dy: parseFloat(this.props.top)
			};

			if ( this.props.useMovingVail ) {
				WindowObject.MovingVail.style.display = 'block';
				WindowObject.MovingVail.style.top = this.props.top + 'px';
				WindowObject.MovingVail.style.left = this.props.left + 'px';
			}

			document.body.addEventListener('keydown', windowKeyboard );

			return this;
		}

		/**
		 * Begin resizing the window. If it's on a touch device allow to resize using the touchscreen.
		 *
		 * @return {WindowObject} Current window object.
		 */
		this.resizeBegin = function () {
			this.focus();
			if ( "ontouchstart" in window ) { // Has touchscreen
				this.touchResize();
				this.keyboardResize();
			} else { // Doesn't have touchscreen
				this.keyboardResize();
			}
			return this;
		}

		/**
		 * Begin resizing the window, using the keyboard.
		 *
		 * @return {WindowObject} Current window object.
		 */
		this.keyboardResize = function () {
			if ( this.props.resizable !== true ) return this;
			wtitleMove.obj = this;
			wtitleMove.elem = this.elements.container;
			wtitleMove.elemData = offset(this.elements.container);
			wtitleMove.keyboardAction = "resize";
			wtitleMove.resizeDirection = {
				vertical: false,
				horizontal: false
			};
			wtitleMove.mousedata = {
				x: 0,
				dx : parseFloat(this.props.left),
				y: 0,
				dy: parseFloat(this.props.top)
			};

			if ( this.props.useMovingVail ) {
				WindowObject.MovingVail.style.display = 'block';
				WindowObject.MovingVail.style.top = this.props.top + 'px';
				WindowObject.MovingVail.style.left = this.props.left + 'px';
			}

			document.body.addEventListener('keydown', windowKeyboard );

			return this;
		}

		/**
		 * Begin resizing the window, using the touch screen.
		 *
		 * @return {WindowObject} Current window object.
		 */
		this.touchResize = function () {
			if ( !WindowObject.touchCircles ) {
				WindowObject.touchCircles = {
					NW : document.createElement('div'),
					N : document.createElement('div'),
					NE : document.createElement('div'),
					W : document.createElement('div'),
					E : document.createElement('div'),
					SW : document.createElement('div'),
					S : document.createElement('div'),
					SE : document.createElement('div')
				};
				for ( var i in WindowObject.touchCircles) {
					WindowObject.touchCircles[i].classList.add('touch-circle');
					WindowObject.touchCircles[i].classList.add(i+'-resize');
					WindowObject.touchCircles[i].dataResizeDirection = i;
				}

			}

			var offset = this.elements.container.getBoundingClientRect();
			var top = offset.top + scrollY;
			var left = offset.left + scrollX;

			var vmiddle = top + ( this.elements.container.offsetHeight / 2 ) ;
			var hmiddle = left + ( this.elements.container.offsetWidth / 2 ) ;

			var bottom = top + this.elements.container.offsetHeight;
			var right = left + this.elements.container.offsetWidth;

			document.body.appendChild ( WindowObject.MovingVail );
			WindowObject.MovingVail.style.display = 'block';
			WindowObject.MovingVail.style.width = this.elements.container.offsetWidth + 'px';
			WindowObject.MovingVail.style.height = this.elements.container.offsetHeight + 'px';
			WindowObject.MovingVail.style.top = top + 'px';
			WindowObject.MovingVail.style.left = left + 'px';
			WindowObject.MovingVail.dataWindowObject = this;
			WindowObject.MovingVail.dataLVZWDESKTOPOBJ = this;

			for ( var i in WindowObject.touchCircles) {
				document.body.appendChild ( WindowObject.touchCircles[i] ) ;
				WindowObject.touchCircles[i].style.display = '';
				WindowObject.touchCircles[i].addEventListener ( 'touchstart', touchResizeStart );
			}

			var circwd2 = WindowObject.touchCircles.NW.offsetWidth / 2;
			var circhd2 = WindowObject.touchCircles.NW.offsetHeight / 2;

			WindowObject.touchCircles.NW.style.top = ( top - circhd2) + 'px';
			WindowObject.touchCircles.NW.style.left = (left - circwd2) + 'px';

			WindowObject.touchCircles.N.style.top = ( top - circhd2) + 'px';
			WindowObject.touchCircles.N.style.left = (hmiddle - circwd2) + 'px';

			WindowObject.touchCircles.NE.style.top = ( top - circhd2) + 'px';
			WindowObject.touchCircles.NE.style.left = (right - circwd2) + 'px';

			WindowObject.touchCircles.W.style.top = ( vmiddle - circhd2) + 'px';
			WindowObject.touchCircles.W.style.left = (left - circwd2) + 'px';

			WindowObject.touchCircles.E.style.top = ( vmiddle - circhd2) + 'px';
			WindowObject.touchCircles.E.style.left = (right - circwd2) + 'px';

			WindowObject.touchCircles.SW.style.top = ( bottom - circhd2) + 'px';
			WindowObject.touchCircles.SW.style.left = (left - circwd2) + 'px';

			WindowObject.touchCircles.S.style.top = ( bottom - circhd2) + 'px';
			WindowObject.touchCircles.S.style.left = (hmiddle - circwd2) + 'px';

			WindowObject.touchCircles.SE.style.top = ( bottom - circhd2) + 'px';
			WindowObject.touchCircles.SE.style.left = (right - circwd2) + 'px';

			return this;
		}

		/**
		 * Change decoration for a specific window part (to be used with JSON themes).
		 *
		 * @param {HTMLElement} el - The HTML element to change decoration.
		 * @param {string} cssClass - The cssClass with new css rules.
		 * @return {WindowObject} Current window object.
		 */
		this.decorateChange = function ( el, cssClass ) {
			var n = el.dataElement;
			if ( typeof cssClass == 'string' ) cssClass = n+':'+cssClass; else cssClass = n;
			for ( var i in this.props.theme[cssClass] ) el.style[i] = this.props.theme[cssClass][i];
			return this;
		};

		/**
		 * Add a title control for the current window.
		 *
		 * @param {string} name - The name of the element.
		 * @param {HTMLElement} element - The control's html element.
		 * @param {string} className - The control's css class-name.
		 * @param {function} onclick - The function to run when clicking the control.
		 * @return {WindowObject} Current window object.
		 */
		this.addTitleControl = function ( name, element, className, onclick ) {
			if ( this.elements[name] ) {
				console.error ( "there is already an item with this name" );
				return false;
			}

			if ( element === null ) {
				element = document.createElement('div');
			}
			this.props.classNames[name] = className;
			this.elements[name] = element;
			this.elements[name].dataElement = name;
			this.elements[name].style.overflow = 'hidden';
			this.elements[name].style.cssFloat = 'left';
			this.elements[name].tabstop = true;
			this.elements[name].addEventListener('mousedown', windowControlMDown );
			this.elements[name].addEventListener('mouseup', windowControlMUp );
			this.elements[name].addEventListener('click', windowControlClick );
			this.elements[name].dataWindowObject = this;
			this.elements[name].dataLVZWDESKTOPOBJ = this;
			this.elements[name].classList.add(this.props.classNames.titleControl);
			this.elements[name].classList.add(this.props.classNames[name]);
			this.elements[name].action = onclick;

			this.setLayout( this.props.buttonLayout );
			return this;
		};

		/**
		 * Method for disabling window title controls.
		 * @param {string} c - Comma separated control names.
		 * @return {WindowObject} Current window object.
		 */
		this.disableControls = function ( c ) {
			c = c.split(/,/);
			var elctrl;
			for ( var i = 0 ; i < c.length; i ++ ) {
				elctrl = this.elements[c[i]+'Btn'];
				if ( elctrl instanceof HTMLElement )
					elctrl.classList.add ( this.props.classNames.disabled );
			}

			return this;
		};

		/**
		 * Method for enabling window title controls.
		 * @param {string} c - Comma separated control names.
		 * @return {WindowObject} Current window object.
		 */
		this.enableControls = function ( c ) {
			c = c.split(/,/);
			var elctrl;
			for ( var i = 0 ; i < c.length; i ++ ) {
				elctrl = this.elements[c[i]+'Btn'];
				if ( elctrl instanceof HTMLElement )
					elctrl.classList.remove ( this.props.classNames.disabled );
			}

			return this;
		};

		/**
		 * Decorate window ( to be used with JSON themes ).
		 *
		 * @return {WindowObject} Current window object.
		 */
		this.decorate = function () {

			if ( ! this.props.theme ) return this;

			this.elements.container.style.boxShadow = this.props.theme.shadow;

			// ### Set border cursors

			// set border top left cursor
			this.elements.lefttopbrdrbox.style.cursor = this.props.theme.border['nw-cursor'];
			this.elements.leftbordertopcrnr.style.cursor = this.props.theme.border['nw-cursor'];
			this.elements.topborderlftcrnr.style.cursor = this.props.theme.border['nw-cursor'];

			// set border bottom left cursor
			this.elements.leftbtmbrdrbox.style.cursor = this.props.theme.border['sw-cursor'];
			this.elements.leftborderbtmcrnr.style.cursor = this.props.theme.border['sw-cursor'];
			this.elements.bottomborderlftcrnr.style.cursor = this.props.theme.border['sw-cursor'];

			// Set border top right cursor
			this.elements.righttopbrdrbox.style.cursor = this.props.theme.border['ne-cursor'];
			this.elements.rightbordertopcrnr.style.cursor = this.props.theme.border['ne-cursor'];
			this.elements.topborderrghtcrnr.style.cursor = this.props.theme.border['ne-cursor'];

			// Set border bottom right cursor
			this.elements.rightbtmbrdrbox.style.cursor = this.props.theme.border['se-cursor'];
			this.elements.rightborderbtmcrnr.style.cursor = this.props.theme.border['se-cursor'];
			this.elements.bottomborderrghtcrnr.style.cursor = this.props.theme.border['se-cursor'];

			// Set left border cursor
			this.elements.leftbordercenter.style.cursor = this.props.theme.border['w-cursor'];

			// Set right border cursor
			this.elements.rightbordercenter.style.cursor = this.props.theme.border['e-cursor'];

			// Set top border cursor
			this.elements.topbordercenter.style.cursor = this.props.theme.border['n-cursor'];

			// Set top border cursor
			this.elements.bottombordercenter.style.cursor = this.props.theme.border['s-cursor'];

			// ### Set border color from theme
			this.elements.lefttopbrdrbox.style.backgroundColor = this.props.theme.border['color'];
			this.elements.leftbtmbrdrbox.style.backgroundColor = this.props.theme.border['color'];
			this.elements.righttopbrdrbox.style.backgroundColor = this.props.theme.border['color'];
			this.elements.rightbtmbrdrbox.style.backgroundColor = this.props.theme.border['color'];
			this.elements.leftbordertopcrnr.style.backgroundColor = this.props.theme.border['color'];
			this.elements.leftbordercenter.style.backgroundColor = this.props.theme.border['color'];
			this.elements.leftborderbtmcrnr.style.backgroundColor = this.props.theme.border['color'];
			this.elements.rightbordertopcrnr.style.backgroundColor = this.props.theme.border['color']; 
			this.elements.rightborderbtmcrnr.style.backgroundColor = this.props.theme.border['color']; 
			this.elements.rightbordercenter.style.backgroundColor = this.props.theme.border['color']; 
			this.elements.topborderlftcrnr.style.backgroundColor = this.props.theme.border['color']; 
			this.elements.topbordercenter.style.backgroundColor = this.props.theme.border['color']; 
			this.elements.topborderrghtcrnr.style.backgroundColor = this.props.theme.border['color']; 
			this.elements.bottomborderlftcrnr.style.backgroundColor = this.props.theme.border['color']; 
			this.elements.bottombordercenter.style.backgroundColor = this.props.theme.border['color']; 
			this.elements.bottomborderrghtcrnr.style.backgroundColor = this.props.theme.border['color']; 

			// ### Set borderline style
			this.elements.container.style.border = this.props.theme.border['outer-border'];
			this.elements.leftbordertopcrnr.style.borderRight = this.props.theme.border['outer-border'];
			this.elements.leftbordertopcrnr.style.borderBottom = this.props.theme.border['outer-border'];
			this.elements.leftborderbtmcrnr.style.borderTop = this.props.theme.border['outer-border'];
			this.elements.leftborderbtmcrnr.style.borderRight = this.props.theme.border['outer-border'];
			this.elements.leftbordercenter.style.borderRight = this.props.theme.border['outer-border'];
			this.elements.rightbordertopcrnr.style.borderLeft = this.props.theme.border['outer-border'];
			this.elements.rightbordertopcrnr.style.borderBottom = this.props.theme.border['outer-border'];
			this.elements.rightborderbtmcrnr.style.borderTop = this.props.theme.border['outer-border'];
			this.elements.rightborderbtmcrnr.style.borderLeft = this.props.theme.border['outer-border'];
			this.elements.rightbordercenter.style.borderLeft = this.props.theme.border['outer-border'];
			this.elements.topborderlftcrnr.style.borderRight = this.props.theme.border['outer-border'];
			this.elements.topborderlftcrnr.style.borderBottom = this.props.theme.border['outer-border'];
			this.elements.topbordercenter.style.borderBottom = this.props.theme.border['outer-border'];
			this.elements.topborderrghtcrnr.style.borderLeft = this.props.theme.border['outer-border'];
			this.elements.topborderrghtcrnr.style.borderBottom = this.props.theme.border['outer-border'];
			this.elements.bottomborderlftcrnr.style.borderRight = this.props.theme.border['outer-border'];
			this.elements.bottomborderlftcrnr.style.borderTop = this.props.theme.border['outer-border'];
			this.elements.bottombordercenter.style.borderTop = this.props.theme.border['outer-border'];
			this.elements.bottomborderrghtcrnr.style.borderLeft = this.props.theme.border['outer-border'];
			this.elements.bottomborderrghtcrnr.style.borderTop = this.props.theme.border['outer-border'];

			// ### Set border size
			this.elements.lefttopbrdrbox.style.width = this.props.theme.border['width'];
			this.elements.lefttopbrdrbox.style.height = this.props.theme.border['width'];
			this.elements.leftbtmbrdrbox.style.width = this.props.theme.border['width'];
			this.elements.leftbtmbrdrbox.style.height = this.props.theme.border['width'];
			this.elements.righttopbrdrbox.style.width = this.props.theme.border['width'];
			this.elements.righttopbrdrbox.style.height = this.props.theme.border['width'];
			this.elements.rightbtmbrdrbox.style.width = this.props.theme.border['width'];
			this.elements.rightbtmbrdrbox.style.height = this.props.theme.border['width'];

			
			this.elements.leftbordertopcrnr.style.width = this.props.theme.border['width'];
			this.elements.leftborderbtmcrnr.style.width = this.props.theme.border['width'];
			this.elements.rightbordertopcrnr.style.width = this.props.theme.border['width'];
			this.elements.rightborderbtmcrnr.style.width = this.props.theme.border['width'];

			this.elements.topborderlftcrnr.style.height = this.props.theme.border['width'];
			this.elements.topborderrghtcrnr.style.height = this.props.theme.border['width'];
			this.elements.bottomborderlftcrnr.style.height = this.props.theme.border['width'];
			this.elements.bottomborderrghtcrnr.style.height = this.props.theme.border['width'];

			if ( this.props.theme.border['corner-width'] ) {
				this.elements.topborderlftcrnr.style.width = this.props.theme.border['corner-width'];
				this.elements.topborderrghtcrnr.style.width = this.props.theme.border['corner-width'];
				this.elements.bottomborderlftcrnr.style.width = this.props.theme.border['corner-width'];
				this.elements.bottomborderrghtcrnr.style.width = this.props.theme.border['corner-width'];
			}

			if ( this.props.theme.border['corner-height'] ) {
				this.elements.leftbordertopcrnr.style.height = this.props.theme.border['corner-height'];
				this.elements.leftborderbtmcrnr.style.height = this.props.theme.border['corner-height'];
				this.elements.rightbordertopcrnr.style.height = this.props.theme.border['corner-height'];
				this.elements.rightborderbtmcrnr.style.height = this.props.theme.border['corner-height'];
			}


			this.elements.leftbordercenter.style.width = this.props.theme.border['width'];
			this.elements.rightbordercenter.style.width = this.props.theme.border['width'];

			this.elements.topbordercenter.style.height = this.props.theme.border['width'];
			this.elements.bottombordercenter.style.height = this.props.theme.border['width'];

			// ### Set minimize button style
			// this.elements.titlerightcontrols.appendChild(this.elements.minimizeBtn);
			for ( var i in this.props.theme.minimizeBtn ) this.elements.minimizeBtn.style[i] = this.props.theme.minimizeBtn[i];

			//this.elements.titlerightcontrols.appendChild(this.elements.maximizeRestoreBtn);
			for ( i in this.props.theme.maximizeBtn ) this.elements.maximizeRestoreBtn.style[i] = this.props.theme.maximizeBtn[i];

			// ### Set close button style
			//this.elements.titlerightcontrols.appendChild(this.elements.closeBtn);
			for ( i in this.props.theme.closeBtn ) this.elements.closeBtn.style[i] = this.props.theme.closeBtn[i];

			// ### Set menu button style
			for ( i in this.props.theme.menuBtn ) this.elements.menuBtn.style[i] = this.props.theme.menuBtn[i];

			var windowControls = this.props.buttonLayout.split(':');
			if ( windowControls.length == 2 ) {
				windowControls[0] = windowControls[0].split(/,/g);
				for ( i = 0 ; i < windowControls[0].length; i ++ ) {
					windowControls[0][i] = windowControls[0][i].trim();
					if ( typeof this.elements[windowControls[0][i] + 'Btn']  != 'undefined' )
						this.elements.titleleftcontrols.appendChild(this.elements[windowControls[0][i] + 'Btn']);

					if ( this.elements[windowControls[0][i] + 'Btn'] ) {
						var btncss = getComputedStyle(this.elements[windowControls[0][i] + 'Btn']).getPropertyValue('--double-click');
						if ( btncss !== "" ) {
							btncss = btncss.substr(btncss.indexOf('"')+1);
							btncss = btncss.substr(0,btncss.lastIndexOf('"') );
							this.elements[windowControls[0][i] + 'Btn'].dblclickAction = new Function( btncss );
							this.elements[windowControls[0][i] + 'Btn'].addEventListener('dblclick', windowControlDBLClick);
						}
					}
				}
				windowControls[1] = windowControls[1].split(/,/g);
				for ( i = 0 ; i < windowControls[1].length; i ++ ) {
					windowControls[1][i] = windowControls[1][i].trim();
					if ( typeof this.elements[windowControls[1][i] + 'Btn']  != 'undefined' )
						this.elements.titlerightcontrols.appendChild(this.elements[windowControls[1][i] + 'Btn']);

					if ( this.elements[windowControls[0][i] + 'Btn'] ) {
						var btncss = getComputedStyle(this.elements[windowControls[0][i] + 'Btn']).getPropertyValue('--double-click');
						if ( btncss !== "" ) {
							btncss = btncss.substr(btncss.indexOf('"')+1);
							btncss = btncss.substr(0,btncss.lastIndexOf('"') );
							this.elements[windowControls[0][i] + 'Btn'].dblclickAction = new Function( btncss );
							this.elements[windowControls[0][i] + 'Btn'].addEventListener('dblclick', windowControlDBLClick);
						}
					}
				}

			}

			var controlsWidth = 0;
			controlsWidth = this.elements.titlerightcontrols.offsetWidth + this.elements.titleleftcontrols.offsetWidth;
			// ### Set title styles
			this.elements.titlecenter.style.width = "calc(100% - "+controlsWidth+"px)";
			// this.elements.titlecenter.style.height = this.props.theme.title['height'];
			this.elements.titlecenter.style.textAlign = this.props.theme.title['text-align'];
			// this.elements.titlecenter.style.lineHeight = this.props.theme.title['line-height'];
			this.elements.titlecenter.style.cursor = this.props.theme.title['cursor'];
			this.elements.titlecenter.style.color = this.props.theme.title['color'];
			// this.elements.titlecenter.style.fontWeight = this.props.theme.title['font-weight'];

			controlsWidth = this.elements.titlerightcontrols.offsetWidth +
				this.elements.titlecenter +
				this.elements.titleleftcontrols.offsetWidth;

			this.elements.titlecontainer.style.backgroundColor = this.props.theme.title['background-color'];
			//this.elements.titlecontainer.style.height = this.props.theme.title['height'];
			this.elements.titlecontainer.style.width = controlsWidth+'px';

			return this;
		};

		/**
		 * Build props.theme property if there is no json them.
		 *
		 * @return {WindowObject} Current window object.
		 */
		this.buildTheme = function () {
			var s = this.elements.container.style.display;
			this.elements.container.style.display = '';
			var contcs = window.getComputedStyle(this.elements.container);
			var titleccs = window.getComputedStyle(this.elements.titlecontainer);
			var brdrccs = window.getComputedStyle(this.elements.leftbordercenter);
			var lbrdrccs = window.getComputedStyle( this.elements.leftbordertopcrnr );

			this.props.theme = {};
			this.props.theme.border = {};

			this.props.theme.border['border-weight'] = contcs.getPropertyValue('--border-weight').trim();
			if ( this.props.theme.border['border-weight'] == '' ) this.props.theme.border['border-weight'] = contcs['border-left-width'];

			this.props.theme.border.width = contcs.getPropertyValue('--border-width').trim();
			if ( this.props.theme.border.width == '' ) this.props.theme.border.width =  brdrccs['width'];

			this.props.theme.border['corner-width'] = contcs.getPropertyValue('--border-corner-width').trim();
			if ( this.props.theme.border['corner-width'] == '') this.props.theme.border['corner-width'] = lbrdrccs['height'];

			this.props.theme.border['corner-height'] = contcs.getPropertyValue('--border-corner-height').trim();
			if ( this.props.theme.border['corner-height'] == '' ) this.props.theme.border['corner-height'] = lbrdrccs['height'];


			switch ( contcs.getPropertyValue('--use-title-body-container').trim() ) {
			case 'yes': case 'true':
				this.elements.bodytitlecontainer = document.createElement('div');

				this.elements.bodytitlecontainer.className = this.props.classNames.bodytitlecontainer;
				this.elements.bodytitlecontainer.dataWindowObject = this;
				this.elements.bodytitlecontainer.dataLVZWDESKTOPOBJ = this;

				this.elements.centercont.insertBefore (
					this.elements.bodytitlecontainer,
					this.elements.bottombordercontainer
				);

				if ( this.props.resizeElement.previousElementSibling === this.elements.titlecontainer ) {
					this.elements.bodytitlecontainer.appendChild( this.elements.titlecontainer );
					this.elements.bodytitlecontainer.appendChild( this.props.resizeElement );
				} else if ( this.elements.titlecontainer.previousElementSibling === this.props.resizeElement ) {
					this.elements.bodytitlecontainer.appendChild( this.props.resizeElement );
					this.elements.bodytitlecontainer.appendChild( this.elements.titlecontainer );
				}

				break;

			case 'no': case 'false': default:
				if ( this.props.resizeElement.previousElementSibling === this.elements.titlecontainer ) {

					if ( this.elements.bodytitlecontainer && this.elements.bodytitlecontainer.parentNode ) this.elements.centercont.parentNode.removeChild ( this.elements.bodytitlecontainer );
					this.elements.centercont.insertBefore (
						this.elements.titlecontainer,
						this.elements.bottombordercontainer
					);
					this.elements.centercont.insertBefore (
						this.props.resizeElement,
						this.elements.bottombordercontainer
					);
				} else if ( this.elements.titlecontainer.previousElementSibling === this.props.resizeElement ) {
					this.elements.centercont.insertBefore (
						this.props.resizeElement,
						this.elements.bottombordercontainer
					);
					this.elements.centercont.insertBefore (
						this.elements.titlecontainer,
						this.elements.bottombordercontainer
					);
				}
				break;

			}

			this.props.theme.title = {};

			this.props.theme.title.height = titleccs['height'];

			this.elements.container.style.display = s;

			return this;
		};

		/**
		 * Sets the theme css class name and removes previous one.
		 * @param {string} c - The new theme class name.
		 * @return {WindowObject} Current window object.
		 */
		this.setThemeClass = function ( c ) {
			if ( typeof this.props.themeClass == 'string' )
				this.elements.container.classList.remove ( this.props.themeClass );
			this.props.themeClass = c;
			this.elements.container.classList.add ( this.props.themeClass );
			this.resize();
			return this;
		};

		/**
		 * Window's container element.
		 * @type {HTMLElement}
		 * @public
		 */
		this.elements.container = null;
		if ( typeof e == 'undefined' || e === false ) this.elements.container = document.createElement('div'); else this.elements.container = e;
		this.elements.container.style.position = 'absolute';
		this.elements.container.style.overflow = 'hidden';
		this.elements.container.style.zIndex = 8;
		this.elements.container.classList.add ( this.props.classNames.container );
		this.elements.container.dataWindowObject = this;
		this.elements.container.dataLVZWDESKTOPOBJ = this;

		var disp = this.elements.container.style.display;
		this.elements.container.style.display = '';
		
		this.props.elementId = (!e || !e.id) ? false : e.id;

		/**
		 * Variable to record all nodes inside window to move them on body container.
		 * @type {array}
		 * @private
		 */
		var tmpnodes = [];
		for ( var i = 0; i<this.elements.container.childNodes.length; i++ )tmpnodes.push(this.elements.container.childNodes[i]);

		/**
		 * Container for left border html elements.
		 * @type {HTMLElement}
		 * @public
		 */
		this.elements.leftbordercont = document.createElement('div');
		this.elements.leftbordercont.style.overflow = 'hidden';
		this.elements.leftbordercont.style.cssFloat = 'left';
		this.elements.leftbordercont.dataWindowObject = this;
		this.elements.leftbordercont.dataLVZWDESKTOPOBJ = this;
		this.elements.leftbordercont.className = this.props.classNames.leftbordercont;
		this.elements.container.appendChild(this.elements.leftbordercont);

		/**
		 * HTML element for top left border corner.
		 * @type {HTMLElement}
		 * @public
		 */ 
		this.elements.lefttopbrdrbox = document.createElement('div');
		this.elements.lefttopbrdrbox.style.overflow = 'hidden';
		this.elements.lefttopbrdrbox.dataWindowObject = this;
		this.elements.lefttopbrdrbox.dataLVZWDESKTOPOBJ = this;
		this.elements.lefttopbrdrbox.dataElement = 'lefttopbrdrbox';
		this.elements.lefttopbrdrbox.addEventListener('mousedown', borderResizeBegin);
		this.elements.lefttopbrdrbox.className = this.props.classNames.lefttopbrdrbox;
		this.elements.leftbordercont.appendChild(this.elements.lefttopbrdrbox);

		/**
		 * HTML element for top-left border part.
		 * @type {HTMLElement}
		 * @public
		 */
		this.elements.leftbordertopcrnr = document.createElement('div');
		this.elements.leftbordertopcrnr.style.overflow = 'hidden';
		this.elements.leftbordertopcrnr.dataWindowObject = this;
		this.elements.leftbordertopcrnr.dataLVZWDESKTOPOBJ = this;
		this.elements.leftbordertopcrnr.dataElement = 'leftbordertopcrnr';
		this.elements.leftbordertopcrnr.addEventListener('mousedown', borderResizeBegin);
		this.elements.leftbordertopcrnr.className = this.props.classNames.leftbordertopcrnr;
		this.elements.leftbordercont.appendChild(this.elements.leftbordertopcrnr);

		/**
		 * HTML element for middle part of left border.
		 * @type {HTMLElement}
		 * @public
		 */
		this.elements.leftbordercenter = document.createElement('div');
		this.elements.leftbordercenter.style.overflow = 'hidden';
		this.elements.leftbordercenter.dataWindowObject = this;
		this.elements.leftbordercenter.dataLVZWDESKTOPOBJ = this;
		this.elements.leftbordercenter.dataElement = 'leftbordercenter';
		this.elements.leftbordercenter.addEventListener('mousedown', borderResizeBegin);
		this.elements.leftbordercenter.className = this.props.classNames.leftbordercenter;
		this.elements.leftbordercont.appendChild(this.elements.leftbordercenter);

		/**
		 * HTML element for bottom-left border part.
		 * @type {HTMLElement}
		 * @public
		 */
		this.elements.leftborderbtmcrnr = document.createElement('div');
		this.elements.leftborderbtmcrnr.style.overflow = 'hidden';
		this.elements.leftborderbtmcrnr.dataWindowObject = this;
		this.elements.leftborderbtmcrnr.dataLVZWDESKTOPOBJ = this;
		this.elements.leftborderbtmcrnr.dataElement = 'leftborderbtmcrnr';
		this.elements.leftborderbtmcrnr.addEventListener('mousedown', borderResizeBegin);
		this.elements.leftborderbtmcrnr.className = this.props.classNames.leftborderbtmcrnr;
		this.elements.leftbordercont.appendChild(this.elements.leftborderbtmcrnr);

		/**
		 * HTML element for bottom-left border corner.
		 * @type {HTMLElement}
		 * @public
		 */
		this.elements.leftbtmbrdrbox = document.createElement('div');
		this.elements.leftbtmbrdrbox.style.overflow = 'hidden';
		this.elements.leftbtmbrdrbox.dataWindowObject = this;
		this.elements.leftbtmbrdrbox.dataLVZWDESKTOPOBJ = this;
		this.elements.leftbtmbrdrbox.dataElement = 'leftbtmbrdrbox';
		this.elements.leftbtmbrdrbox.addEventListener('mousedown', borderResizeBegin);
		this.elements.leftbtmbrdrbox.className = this.props.classNames.leftbtmbrdrbox;
		this.elements.leftbordercont.appendChild(this.elements.leftbtmbrdrbox);

		/**
		 * HTML element for center container (all element except left and right borders).
		 * @type {HTMLElement}
		 * @public
		 */
		this.elements.centercont = document.createElement('div');
		this.elements.centercont.style.cssFloat = 'left';
		this.elements.centercont.style.overflow = 'hidden';
		this.elements.centercont.dataWindowObject = this;
		this.elements.centercont.dataLVZWDESKTOPOBJ = this;
		this.elements.centercont.className = this.props.classNames.centercont;
		this.elements.container.appendChild(this.elements.centercont);

		/**
		 * HTML element for top border container.
		 * @type {HTMLElement}
		 * @public
		 */
		this.elements.topbordercontainer = document.createElement('div');
		this.elements.topbordercontainer.style.overflow = 'hidden';
		this.elements.topbordercontainer.dataWindowObject = this;
		this.elements.topbordercontainer.dataLVZWDESKTOPOBJ = this;
		this.elements.topbordercontainer.className = this.props.classNames.topbordercontainer;
		this.elements.centercont.appendChild(this.elements.topbordercontainer);

		/**
		 * HTML element for right part of top-left border.
		 * @type {HTMLElement}
		 * @public
		 */
		this.elements.topborderlftcrnr = document.createElement('div');
		this.elements.topborderlftcrnr.style.overflow = 'hidden';
		this.elements.topborderlftcrnr.style.cssFloat = 'left';
		this.elements.topborderlftcrnr.dataWindowObject = this;
		this.elements.topborderlftcrnr.dataLVZWDESKTOPOBJ = this;
		this.elements.topborderlftcrnr.dataElement = 'topborderlftcrnr';
		this.elements.topborderlftcrnr.addEventListener('mousedown', borderResizeBegin);
		this.elements.topborderlftcrnr.className = this.props.classNames.topborderlftcrnr;
		this.elements.topbordercontainer.appendChild(this.elements.topborderlftcrnr);

		/**
		 * HTML element for center part of top border.
		 * @type {HTMLElement}
		 * @public
		 */
		this.elements.topbordercenter = document.createElement('div');
		this.elements.topbordercenter.style.overflow = 'hidden';
		this.elements.topbordercenter.style.cssFloat = 'left';
		this.elements.topbordercenter.dataWindowObject = this;
		this.elements.topbordercenter.dataLVZWDESKTOPOBJ = this;
		this.elements.topbordercenter.dataElement = 'topbordercenter';
		this.elements.topbordercenter.addEventListener('mousedown', borderResizeBegin);
		this.elements.topbordercenter.className = this.props.classNames.topbordercenter;
		this.elements.topbordercontainer.appendChild(this.elements.topbordercenter);

		/**
		 * HTML element for left part of top-right border.
		 * @type {HTMLElement}
		 * @public
		 */
		this.elements.topborderrghtcrnr = document.createElement('div');
		this.elements.topborderrghtcrnr.style.overflow = 'hidden';
		this.elements.topborderrghtcrnr.style.cssFloat = 'right';
		this.elements.topborderrghtcrnr.dataWindowObject = this;
		this.elements.topborderrghtcrnr.dataLVZWDESKTOPOBJ = this;
		this.elements.topborderrghtcrnr.dataElement = 'topborderrghtcrnr';
		this.elements.topborderrghtcrnr.addEventListener('mousedown', borderResizeBegin);
		this.elements.topborderrghtcrnr.className = this.props.classNames.topborderrghtcrnr;
		this.elements.topbordercontainer.appendChild(this.elements.topborderrghtcrnr);

		/**
		 * HTML element container for title elements.
		 * @type {HTMLElement}
		 * @public
		 */
		this.elements.titlecontainer = document.createElement('div');
		this.elements.titlecontainer.style.overflow = 'hidden';
		this.elements.titlecontainer.dataWindowObject = this;
		this.elements.titlecontainer.dataLVZWDESKTOPOBJ = this;
		this.elements.titlecontainer.className = this.props.classNames.titlecontainer;
		this.elements.centercont.appendChild(this.elements.titlecontainer);

		/**
		 * HTML element, container for left window controls.
		 * @type {HTMLElement}
		 * @public
		 */
		this.elements.titleleftcontrols = document.createElement('div');
		this.elements.titleleftcontrols.style.overflow = 'hidden';
		this.elements.titleleftcontrols.style.cssFloat = 'left';
		this.elements.titleleftcontrols.dataWindowObject = this;
		this.elements.titleleftcontrols.dataLVZWDESKTOPOBJ = this;
		this.elements.titleleftcontrols.className = this.props.classNames.titleleftcontrols;
		this.elements.titlecontainer.appendChild( this.elements.titleleftcontrols );

		/**
		 * HTML element for center title part.
		 * @type {HTMLElement}
		 * @public
		 */
		this.elements.titlecenter = document.createElement('div');
		this.elements.titlecenter.style.overflow = 'hidden';
		this.elements.titlecenter.style.cssFloat = 'left';
		this.elements.titlecenter.dataWindowObject = this;
		this.elements.titlecenter.dataLVZWDESKTOPOBJ = this;
		this.elements.titlecenter.dblclickaction = function ( event ) { this.maximizeRestore(); };
		this.elements.titlecenter.addEventListener('dblclick', function ( e ) {
			var self = e.target.dataWindowObject;
			self.elements.titlecenter.dblclickaction.call( self );
		});
		this.elements.titlecenter.className = this.props.classNames.titlecenter;
		this.elements.titlecontainer.appendChild ( this.elements.titlecenter );

		/**
		 * HTML element for title text.
		 * @type {HTMLSpanElement}
		 * @public
		 */
		this.elements.titletext = document.createElement('span');
		this.elements.titletext.dataWindowObject = this;
		this.elements.titletext.dataLVZWDESKTOPOBJ = this;
		this.elements.titlecenter.appendChild(this.elements.titletext);

		/**
		 * HTML element, container for right window controls.
		 * @type {HTMLElement}
		 * @public
		 */
		this.elements.titlerightcontrols = document.createElement('div');
		this.elements.titlerightcontrols.style.overflow = 'hidden';
		this.elements.titlerightcontrols.style.cssFloat = 'right';
		this.elements.titlerightcontrols.dataWindowObject = this;
		this.elements.titlerightcontrols.dataLVZWDESKTOPOBJ = this;
		this.elements.titlerightcontrols.className = this.props.classNames.titlerightcontrols;
		this.elements.titlecontainer.appendChild( this.elements.titlerightcontrols );

		this.elements.innercont = document.createElement('div');
		this.elements.innercont.style.overflow = 'hidden';
		this.elements.innercont.dataWindowObject = this;
		this.elements.innercont.dataLVZWDESKTOPOBJ = this;
		this.elements.innercont.className = this.props.classNames.innercont;
		this.elements.centercont.appendChild(this.elements.innercont);

		this.elements.innercontup = document.createElement('div');
		this.elements.innercontup.style.overflow = 'hidden';
		this.elements.innercontup.dataWindowObject = this;
		this.elements.innercontup.dataLVZWDESKTOPOBJ = this;
		this.elements.innercontup.className = this.props.classNames.innercontup;
		this.elements.innercont.appendChild(this.elements.innercontup);

		this.elements.innercontmid = document.createElement('div');
		this.elements.innercontmid.style.overflow = 'hidden';
		this.elements.innercontmid.dataWindowObject = this;
		this.elements.innercontmid.dataLVZWDESKTOPOBJ = this;
		this.elements.innercont.appendChild(this.elements.innercontmid);

		this.elements.innercontleft = document.createElement('div');
		this.elements.innercontleft.style.overflow = 'hidden';
		this.elements.innercontleft.style.cssFloat = 'left';
		this.elements.innercontleft.dataWindowObject = this;
		this.elements.innercontleft.dataLVZWDESKTOPOBJ = this;
		this.elements.innercontleft.className = this.props.classNames.innercontleft;
		this.elements.innercontmid.appendChild(this.elements.innercontleft);

		/**
		 * HTML element for window contents.
		 * @type {HTMLElement}
		 * @public
		 */
		this.elements.bodycontainer = document.createElement('div');
		
		this.props.resizeElement = this.elements.bodycontainer;
		this.elements.bodycontainer.className = this.props.classNames.bodycontainer;
		this.elements.bodycontainer.style.cssFloat = 'left';
		this.elements.bodycontainer.style.position = 'absolute';
		this.elements.bodycontainer.tabIndex = 1;
		this.elements.bodycontainer.dataWindowObject = this;
		this.elements.bodycontainer.dataLVZWDESKTOPOBJ = this;
		this.elements.innercontmid.appendChild(this.elements.bodycontainer);

		this.elements.innercontright = document.createElement('div');
		this.elements.innercontright.style.overflow = 'hidden';
		this.elements.innercontright.style.cssFloat = 'right';
		this.elements.innercontright.dataWindowObject = this;
		this.elements.innercontright.dataLVZWDESKTOPOBJ = this;
		this.elements.innercontright.className = this.props.classNames.innercontright;
		this.elements.innercontmid.appendChild(this.elements.innercontright);

		this.elements.innercontbtm = document.createElement('div');
		this.elements.innercontbtm.style.overflow = 'hidden';
		this.elements.innercontbtm.dataWindowObject = this;
		this.elements.innercontbtm.dataLVZWDESKTOPOBJ = this;
		this.elements.innercontbtm.className = this.props.classNames.innercontbtm;
		this.elements.innercont.appendChild(this.elements.innercontbtm);

		/**
		 * HTML element for disabling window's body.
		 * @type {HTMLDivElement}
		 * @public
		 */
		this.elements.disableBody = document.createElement('div');
		this.elements.disableBody.className =  this.props.classNames.disableVail;
		this.elements.disableBody.dataWindowObject = this;
		this.elements.disableBody.dataLVZWDESKTOPOBJ = this;
		this.elements.centercont.appendChild(this.elements.disableBody);

		/**
		 * HTML element, container for bottom border elements.
		 * @type {HTMLElement}
		 * @public
		 */
		this.elements.bottombordercontainer = document.createElement('div');
		this.elements.bottombordercontainer.style.overflow = 'hidden';
		this.elements.bottombordercontainer.dataWindowObject = this;
		this.elements.bottombordercontainer.dataLVZWDESKTOPOBJ = this;
		this.elements.bottombordercontainer.className = this.props.classNames.bottombordercontainer;
		this.elements.centercont.appendChild(this.elements.bottombordercontainer);

		/**
		 * HTML element for right part of bottom-left border.
		 * @type {HTMLElement}
		 * @public
		 */
		this.elements.bottomborderlftcrnr = document.createElement('div');
		this.elements.bottomborderlftcrnr.style.overflow = 'hidden';
		this.elements.bottomborderlftcrnr.style.cssFloat = 'left';
		this.elements.bottomborderlftcrnr.dataWindowObject = this;
		this.elements.bottomborderlftcrnr.dataLVZWDESKTOPOBJ = this;
		this.elements.bottomborderlftcrnr.dataElement = 'bottomborderlftcrnr';
		this.elements.bottomborderlftcrnr.addEventListener('mousedown', borderResizeBegin);
		this.elements.bottomborderlftcrnr.className = this.props.classNames.bottomborderlftcrnr;
		this.elements.bottombordercontainer.appendChild(this.elements.bottomborderlftcrnr);

		/**
		 * HTML element for center part of bottom border.
		 * @type {HTMLElement}
		 * @public
		 */
		this.elements.bottombordercenter = document.createElement('div');
		this.elements.bottombordercenter.style.overflow = 'hidden';
		this.elements.bottombordercenter.style.cssFloat = 'left';
		this.elements.bottombordercenter.dataWindowObject = this;
		this.elements.bottombordercenter.dataLVZWDESKTOPOBJ = this;
		this.elements.bottombordercenter.dataElement = 'bottombordercenter';
		this.elements.bottombordercenter.addEventListener('mousedown', borderResizeBegin);
		this.elements.bottombordercenter.className = this.props.classNames.bottombordercenter;
		this.elements.bottombordercontainer.appendChild(this.elements.bottombordercenter);

		/**
		 * HTML element for left part of bottom-right border.
		 * @type {HTMLElement}
		 * @public
		 */
		this.elements.bottomborderrghtcrnr = document.createElement('div');
		this.elements.bottomborderrghtcrnr.style.overflow = 'hidden';
		this.elements.bottomborderrghtcrnr.style.cssFloat = 'right';
		this.elements.bottomborderrghtcrnr.dataWindowObject = this;
		this.elements.bottomborderrghtcrnr.dataLVZWDESKTOPOBJ = this;
		this.elements.bottomborderrghtcrnr.dataElement = 'bottomborderrghtcrnr';
		this.elements.bottomborderrghtcrnr.addEventListener('mousedown', borderResizeBegin);
		this.elements.bottomborderrghtcrnr.className = this.props.classNames.bottomborderrghtcrnr;
		this.elements.bottombordercontainer.appendChild(this.elements.bottomborderrghtcrnr);

		/**
		 * HTML element to contain elements of right border.
		 * @type {HTMLElement}
		 * @public
		 */
		this.elements.rightbordercont = document.createElement('div');
		this.elements.rightbordercont.style.overflow = 'hidden';
		this.elements.rightbordercont.style.cssFloat = 'right';
		this.elements.rightbordercont.dataWindowObject = this;
		this.elements.rightbordercont.dataLVZWDESKTOPOBJ = this;
		this.elements.rightbordercont.className = this.props.classNames.rightbordercont;
		this.elements.container.appendChild(this.elements.rightbordercont);

		/**
		 * HTML element for top-right border box.
		 * @type {HTMLElement}
		 * @public
		 */
		this.elements.righttopbrdrbox = document.createElement('div');
		this.elements.righttopbrdrbox.style.overflow = 'hidden';
		this.elements.righttopbrdrbox.dataWindowObject = this;
		this.elements.righttopbrdrbox.dataLVZWDESKTOPOBJ = this;
		this.elements.righttopbrdrbox.dataElement = 'righttopbrdrbox';
		this.elements.righttopbrdrbox.addEventListener('mousedown', borderResizeBegin);
		this.elements.righttopbrdrbox.className = this.props.classNames.righttopbrdrbox;
		this.elements.rightbordercont.appendChild(this.elements.righttopbrdrbox);

		/**
		 * HTML element for top-right border part.
		 * @type {HTMLElement}
		 * @public
		 */
		this.elements.rightbordertopcrnr = document.createElement('div');
		this.elements.rightbordertopcrnr.style.overflow = 'hidden';
		this.elements.rightbordertopcrnr.dataWindowObject = this;
		this.elements.rightbordertopcrnr.dataLVZWDESKTOPOBJ = this;
		this.elements.rightbordertopcrnr.dataElement = 'rightbordertopcrnr';
		this.elements.rightbordertopcrnr.addEventListener('mousedown', borderResizeBegin);
		this.elements.rightbordertopcrnr.className = this.props.classNames.rightbordertopcrnr;
		this.elements.rightbordercont.appendChild(this.elements.rightbordertopcrnr);

		/**
		 * HTML element for center part of right border.
		 * @type {HTMLElement}
		 * @public
		 */
		this.elements.rightbordercenter = document.createElement('div');
		this.elements.rightbordercenter.style.overflow = 'hidden';
		this.elements.rightbordercenter.dataWindowObject = this;
		this.elements.rightbordercenter.dataLVZWDESKTOPOBJ = this;
		this.elements.rightbordercenter.dataElement = 'rightbordercenter';
		this.elements.rightbordercenter.addEventListener('mousedown', borderResizeBegin);
		this.elements.rightbordercenter.className = this.props.classNames.rightbordercenter;
		this.elements.rightbordercont.appendChild(this.elements.rightbordercenter);

		/**
		 * HTML element for bottom-right border part.
		 * @type {HTMLElement}
		 * @public
		 */
		this.elements.rightborderbtmcrnr = document.createElement('div');
		this.elements.rightborderbtmcrnr.style.overflow = 'hidden';
		this.elements.rightborderbtmcrnr.dataWindowObject = this;
		this.elements.rightborderbtmcrnr.dataLVZWDESKTOPOBJ = this;
		this.elements.rightborderbtmcrnr.dataElement = 'rightborderbtmcrnr';
		this.elements.rightborderbtmcrnr.addEventListener('mousedown', borderResizeBegin);
		this.elements.rightborderbtmcrnr.className = this.props.classNames.rightborderbtmcrnr;
		this.elements.rightbordercont.appendChild(this.elements.rightborderbtmcrnr);

		/**
		 * HTML element for bottom-right corner box.
		 * @type {HTMLElement}
		 * @public
		 */
		this.elements.rightbtmbrdrbox = document.createElement('div');
		this.elements.rightbtmbrdrbox.style.overflow = 'hidden';
		this.elements.rightbtmbrdrbox.dataWindowObject = this;
		this.elements.rightbtmbrdrbox.dataLVZWDESKTOPOBJ = this;
		this.elements.rightbtmbrdrbox.dataElement = 'rightbtmbrdrbox';
		this.elements.rightbtmbrdrbox.addEventListener('mousedown', borderResizeBegin);
		this.elements.rightbtmbrdrbox.className = this.props.classNames.rightbtmbrdrbox;
		this.elements.rightbordercont.appendChild(this.elements.rightbtmbrdrbox);

		// Window Controls

		/**
		 * HTML element for window menu button.
		 * @type {HTMLElement}
		 * @public
		 */
		this.elements.menuBtn = document.createElement('div');
		this.elements.menuBtn.dataElement = 'menuBtn';
		this.elements.menuBtn.style.overflow = 'hidden';
		this.elements.menuBtn.style.cssFloat = 'left';
		this.elements.menuBtn.addEventListener('mousedown', windowControlMDown );
		this.elements.menuBtn.addEventListener('mouseup', windowControlMUp );
		this.elements.menuBtn.addEventListener('click', windowControlClick );
		this.elements.menuBtn.dataWindowObject = this;
		this.elements.menuBtn.dataLVZWDESKTOPOBJ = this;
		this.elements.menuBtn.classList.add (this.props.classNames.titleControl);
		this.elements.menuBtn.classList.add (this.props.classNames.menuBtn);

		this.elements.menuBtn.action = function ( e ) {
			if ( window.lvzwebdesktop.MenuObject && this.props.disableAppMenu !== true ) {
				var eventData={"target":this};
				this.fireEvent('menu',eventData);
				if ( eventData.defaultPrevented === true ) return;
				var menu = lvzwebdesktop('#appmenu','MenuObject');
				if ( menu ) {
					menu.props.callerElement = e;
					menu.show ( e );
					menu.fixPosition();
				}
			}
		};

		/**
		 * HTML element for window close button.
		 * @type {HTMLElement}
		 * @public
		 */
		this.elements.closeBtn = document.createElement('div');
		this.elements.closeBtn.dataElement = 'closeBtn';
		this.elements.closeBtn.style.overflow = 'hidden';
		this.elements.closeBtn.style.cssFloat = 'left';
		this.elements.closeBtn.addEventListener('mousedown', windowControlMDown);
		this.elements.closeBtn.addEventListener('mouseup', windowControlMUp);
		this.elements.closeBtn.addEventListener('click', windowControlClick);
		this.elements.closeBtn.classList.add (this.props.classNames.titleControl);
		this.elements.closeBtn.classList.add (this.props.classNames.closeBtn);
		this.elements.closeBtn.action = function () {
			this.close();
		};
		this.elements.closeBtn.dataWindowObject = this;
		this.elements.closeBtn.dataLVZWDESKTOPOBJ = this;

		/**
		 * HTML element for window minimize button.
		 * @type {HTMLElement}
		 * @public
		 */
		this.elements.minimizeBtn = document.createElement('div');
		this.elements.minimizeBtn.dataElement = 'minimizeBtn';
		this.elements.minimizeBtn.style.overflow = 'hidden';
		this.elements.minimizeBtn.style.cssFloat = 'left';
		this.elements.minimizeBtn.addEventListener('mousedown', windowControlMDown);
		this.elements.minimizeBtn.addEventListener('mouseup', windowControlMUp);
		this.elements.minimizeBtn.addEventListener('click',windowControlClick);
		this.elements.minimizeBtn.classList.add (this.props.classNames.titleControl);
		this.elements.minimizeBtn.classList.add (this.props.classNames.minimizeBtn);
		this.elements.minimizeBtn.action = function () {
			this.minimizeRestore();
		};
		this.elements.minimizeBtn.dataWindowObject = this;
		this.elements.minimizeBtn.dataLVZWDESKTOPOBJ = this;

		/**
		 * HTML element for window maximize/restore button.
		 * @type {HTMLElement}
		 * @public
		 */
		this.elements.maximizeRestoreBtn = document.createElement('div');
		this.elements.maximizeRestoreBtn.dataElement = 'maximizeBtn';
		this.elements.maximizeRestoreBtn.style.overflow = 'hidden';
		this.elements.maximizeRestoreBtn.style.cssFloat = 'left';
		this.elements.maximizeRestoreBtn.addEventListener('mousedown', windowControlMDown);
		this.elements.maximizeRestoreBtn.addEventListener('mouseup', windowControlMUp);
		this.elements.maximizeRestoreBtn.addEventListener('click', windowControlClick);
		this.elements.maximizeRestoreBtn.classList.add (this.props.classNames.titleControl);
		this.elements.maximizeRestoreBtn.classList.add (this.props.classNames.maximizeBtn);
		this.elements.maximizeRestoreBtn.action = function () {
			if ( this.elements.maximizeRestoreBtn.dataElement == 'maximizeBtn' )
				this.maximize();
			else
				this.restore();
		};
		this.elements.maximizeRestoreBtn.dataWindowObject = this;
		this.elements.maximizeRestoreBtn.dataLVZWDESKTOPOBJ = this;

		/**
		 * HTML element for window restore (classic mac style) button.
		 * @type {HTMLElement}
		 * @public
		 */
		this.elements.macrestoreBtn = document.createElement('div');
		this.elements.macrestoreBtn.dataElement = 'macrestoreBtn';
		this.elements.macrestoreBtn.style.overflow = 'hidden';
		this.elements.macrestoreBtn.style.cssFloat = 'left';
		this.elements.macrestoreBtn.addEventListener('mousedown', windowControlMDown);
		this.elements.macrestoreBtn.addEventListener('mouseup', windowControlMUp);
		this.elements.macrestoreBtn.addEventListener('click', windowControlClick);
		this.elements.macrestoreBtn.classList.add (this.props.classNames.titleControl);
		this.elements.macrestoreBtn.classList.add (this.props.classNames.macrestoreBtn);
		this.elements.macrestoreBtn.action = function () {
			this.toggleContentSize();
		};
		this.elements.macrestoreBtn.dataWindowObject = this;
		this.elements.macrestoreBtn.dataLVZWDESKTOPOBJ = this;
		
		// Add events
		this.elements.container.addEventListener( 'mousedown', mdownWindow );

		//** MOUSE WHEEL EVENTS
		// this.elements.container.addEventListener( 'mousewheel', mWheel ); // FOR INTERNETEXPLORER
		// this.elements.container.addEventListener( 'DOMMouseScroll', mWheel ); // FOR Old firefox
		this.elements.container.addEventListener( 'wheel', mWheel );

		this.elements.titlecenter.addEventListener( 'mousedown', wtitleMdown );

		this.elements.titlecenter.addEventListener( 'touchstart', wtitleMdown );

		this.elements.bodycontainer.addEventListener( 'click', clickWindow );
		this.elements.bodycontainer.addEventListener("mousemove", windowbodymousemove );
		this.elements.bodycontainer.addEventListener("keypress", windowbodykeypress );
		this.elements.bodycontainer.addEventListener("keyup", windowbodykeypress );
		this.elements.bodycontainer.addEventListener("keydown", windowbodykeypress );

		this.elements.container.addEventListener( 'contextmenu', function (e) { e.preventDefault(); return false; } );

		// Move window items to body container
		for ( let i = 0; i<tmpnodes.length; i++ )this.elements.bodycontainer.appendChild(tmpnodes[i]);
		
		if ( this.desktop !== null ) this.desktop.elements.root.appendChild(this.elements.container);


		/** **/
		if ( typeof window.lvzwebdesktop.themes[window.lvzwebdesktop.defaultTheme] != 'undefined'
			 &&
			 typeof window.lvzwebdesktop.themes[window.lvzwebdesktop.defaultTheme].window != 'undefined'
		   ) this.props.theme = window.lvzwebdesktop.themes[window.lvzwebdesktop.defaultTheme].window;


		// Make properties from css rules

		var tmpcssrule;
		tmpcssrule = getComputedStyle(this.elements.container).getPropertyValue('--title-pos').trim();
		if ( tmpcssrule  == 'bottom' ) this.moveTitleToBottom();
		else if ( tmpcssrule  == 'top' ) this.moveTitleToTop();

		tmpcssrule = getComputedStyle(this.elements.container).getPropertyValue('--change-menu-icon').trim();
		if ( tmpcssrule == 'no' ) this.props.setAppIconOnMenuIcon = false;

		tmpcssrule = getComputedStyle(this.elements.container).getPropertyValue('--title-double-click').trim();
		if ( tmpcssrule != '' ) this.elements.titlecenter.dblclickaction = new Function ( 'event', JSON.parse( tmpcssrule ) );
		
		tmpcssrule = getComputedStyle(this.elements.titlecenter).getPropertyValue('--text-bg').trim();
		if ( tmpcssrule != '' ) {
			this.elements.titletext.backgroundColor = tmpcssrule;
			this.elements.titlecenter.appendChild(this.elements.titletext);
		}

		tmpcssrule = getComputedStyle(document.body).getPropertyValue('--lvzwebdesktop-default-theme').trim();
		if ( tmpcssrule != '' ) {
			for ( var i = 0 ; i < this.elements.container.classList.length ; i ++ ) {
				if ( getComputedStyle(document.body).getPropertyValue('--lvzwebdesktop-theme-'+this.elements.container.classList[i]).trim() == 'true' ) {
					this.props.themeClass = this.elements.container.classList[i];
					break;
				}
			}
			if ( this.props.themeClass === null ) {
				this.props.themeClass = tmpcssrule;
				this.elements.container.classList.add(tmpcssrule);
			}
		}
		else {
			console.error ("There is no theme loaded");
		}

		// Make properties from html element's attributes

		var attr;

		attr = this.elements.container.getAttribute('button-layout');
		if ( attr !== null ) this.props.buttonLayout = attr;
		else {
			attr = getComputedStyle(this.elements.container).getPropertyValue('--button-layout');
			if ( attr !== "" ) this.props.buttonLayout = attr;
		}

		attr = this.elements.container.getAttribute('min-top');
		if ( attr !== null ) this.props.minTop = parseFloat(attr);

		attr = this.elements.container.getAttribute('max-top');
		if ( attr !== null ) this.props.maxTop = parseFloat(attr);

		attr = this.elements.container.getAttribute('top');
		if ( attr !== null ) this.setTop(parseFloat(attr));

		attr = this.elements.container.getAttribute('min-left');
		if ( attr !== null ) this.props.minLeft = parseFloat(attr);

		attr = this.elements.container.getAttribute('max-left');
		if ( attr !== null ) this.props.maxLeft = parseFloat(attr);

		attr = this.elements.container.getAttribute('left');
		if ( attr !== null ) this.setLeft(parseFloat(attr));

		attr = this.elements.container.getAttribute('min-width');
		if ( attr !== null ) this.props.minWidth = parseFloat(attr);

		attr = this.elements.container.getAttribute('max-width');
		if ( attr !== null ) this.props.maxWidth = parseFloat(attr);

		attr = this.elements.container.getAttribute('width');
		if ( attr !== null ) this.setWidth(parseFloat(attr));
		else this.setWidth(this.props.width);

		attr = this.elements.container.getAttribute('min-height');
		if ( attr !== null ) this.props.minHeight = parseFloat(attr);

		attr = this.elements.container.getAttribute('max-height');
		if ( attr !== null ) this.props.maxHeight = parseFloat(attr);

		attr = this.elements.container.getAttribute('height');
		if ( attr !== null ) this.setHeight(parseFloat(attr));
		else this.setHeight(this.props.height);

		attr = this.elements.container.getAttribute('icon');
		if ( attr !== null ) this.setIcon ( attr );

		attr = this.elements.container.getAttribute('disabled');
		if ( attr == 'true' ) this.disableWindow ();
		else if ( attr == 'false' ) this.enableWindow ();

		attr = this.elements.container.getAttribute('group-id');
		if ( attr !== null ) {
			var tmpgroup =  window.lvzwebdesktop('#'+attr,'WindowgroupObject');
			if ( tmpgroup ) tmpgroup.addWindow ( this );
			else console.warn ( "Windowgroup with id \"" + attr + "\" does not exist" );
		}

		attr = this.elements.container.getAttribute('always-on-top');
		if ( attr == 'true' ) this.sendFront();

		attr = this.elements.container.getAttribute('disable-controls');
		if ( attr !== null ) this.disableControls(attr);

		this.props.disableAppMenu = this.elements.container.getAttribute('disable-app-menu') == 'true';

		if ( this.elements.container.getAttribute('hide-title') == 'true' ) this.hideTitle();
		if ( this.elements.container.getAttribute('disable-resize') == 'true' ) {
			this.disableResize();
			this.props.disableBorder = true;
			this.props.resizable = false;
		}

		if ( this.elements.container.getAttribute('resizable') == 'false' ) this.props.resizable = false;
		else if ( this.elements.container.getAttribute('resizable') == 'true' ) this.props.resizable = true;

		if (this.elements.container.getAttribute('maximizable') == 'false' ) this.props.maximizable = false;
		else if ( this.elements.container.getAttribute('maximizable') == 'true' ) this.props.maximizable = true;

		attr = this.elements.container.getAttribute('minimizable');
		if ( attr == 'false' ) this.props.minimizable = false;
		else if ( attr  == 'true' ) this.props.minimizable = true;

		if ( this.elements.container.getAttribute('movable') == 'false' ) this.props.movable = false;

		if ( this.elements.container.getAttribute('use-vail') == 'true' )
			this.props.useMovingVail = true;
		else if ( this.elements.container.getAttribute('use-vail') == 'false' )
			this.props.useMovingVail = false;

		attr = this.elements.container.getAttribute('caption');
		if ( attr !== null ) this.setTitle(attr);

		attr = this.elements.container.getAttribute('resize-on-content');
		if ( attr == "true" ) this.resizeOnContent(true);
		else if (attr == "false" ) this.resizeOnContent(false);

		attr = this.elements.container.getAttribute('load-app');
		if ( attr !== null ) {
			lvzwebdesktop(attr).loadApp(this.elements.bodycontainer);
		}

		attr = this.elements.container.getAttribute('no-desktop');
		var nodesktop = false;
		if ( attr == 'true' ) nodesktop=true;
		if ( desktop === false ) nodesktop=true;

		attr = this.elements.container.getAttribute('show-in-taskbar');
		if ( attr == 'false' ) this.props.showInTaskbar = false;

		// Make events from html element's attributes

		var eventstring = "";

		eventstring = this.elements.container.getAttribute('onclose');
		if ( eventstring !== null ) this.addEvent('close',new Function('event',eventstring));

		eventstring = this.elements.container.getAttribute('onminimize');
		if ( eventstring !== null ) this.addEvent('minimize',new Function('event',eventstring));

		eventstring = this.elements.container.getAttribute('onmaximize');
		if ( eventstring !== null ) this.addEvent('maximize',new Function('event',eventstring));

		eventstring = this.elements.container.getAttribute('onrestore');
		if ( eventstring !== null ) this.addEvent('restore',new Function('event',eventstring));

		eventstring = this.elements.container.getAttribute('onresize');
		if ( eventstring !== null ) this.addEvent('resize',new Function('event',eventstring));

		eventstring = this.elements.container.getAttribute('onmove');
		if ( eventstring !== null ) this.addEvent('move',new Function('event',eventstring));

		eventstring = this.elements.container.getAttribute('onmousemove');
		if ( eventstring !== null ) this.addEvent('mousemove',new Function('event',eventstring));

		eventstring = this.elements.container.getAttribute('onmousedown');
		if ( eventstring !== null ) this.addEvent('mousedown',new Function('event',eventstring));

		eventstring = this.elements.container.getAttribute('onclick');
		if ( eventstring !== null ) this.addEvent('click',new Function('event',eventstring));

		eventstring = this.elements.container.getAttribute('onkeypress');
		if ( eventstring !== null ) this.addEvent('keypress',new Function('event',eventstring));

		eventstring = this.elements.container.getAttribute('onkeyup');
		if ( eventstring !== null ) this.addEvent('keyup',new Function('event',eventstring));

		eventstring = this.elements.container.getAttribute('onkeydown');
		if ( eventstring !== null ) this.addEvent('keydown',new Function('event',eventstring));

		eventstring = this.elements.container.getAttribute('onfocus');
		if ( eventstring !== null ) this.addEvent('focus',new Function('event',eventstring));

		eventstring = this.elements.container.getAttribute('onblur');
		if ( eventstring !== null ) this.addEvent('blur',new Function('event',eventstring));

		eventstring = this.elements.container.getAttribute('onitemattached');
		if ( eventstring !== null ) this.addEvent('itemattached',new Function('event',eventstring));

		eventstring = this.elements.container.getAttribute('onshow');
		if ( eventstring !== null ) this.addEvent('show',new Function('event',eventstring));

		eventstring = this.elements.container.getAttribute('onhide');
		if ( eventstring !== null ) this.addEvent('hide',new Function('event',eventstring));

		eventstring = this.elements.container.getAttribute('onready');
		if ( eventstring !== null ) this.addEvent('ready',new Function('event',eventstring));

		if ( typeof desktop != 'undefined' && desktop !== false ) {
			desktop.addObject(this);
		} else if ( nodesktop !== true && window.lvzwebdesktop.getParentDesktop ) {
			var desk = window.lvzwebdesktop.getParentDesktop (this.elements.container);
			if ( desk ) desk.addObject(this);
		}

		if ( this.props.theme ) this.decorate();
		else this.buildTheme();

		this.resize();

		this.elements.container.style.display = disp;


		this.fireEvent('ready',{
			"target" : this
		});

		WindowObject.fireEvent('newwindow',{
			"target" : this
		});

		WindowObject.fireEvent('ready',{
			"target" : this
		});

	};

	/**
	 * Property for building the edit properties table on control edit.
	 * @public
	 * @type {Object}
	 */
	WindowObject.editProperties = {
		"id" : { "type" : "string|false",
				 "default" : false,
				 "getValue" : function(self){return self.props.elementId===false?'':self.props.elementId;},
				 "setValue" : function(self,v){
					 if(v=='') {
						 v=false;
						 self.elements.container.removeAttribute('id');
					 }
					 else {
						 self.elements.container.setAttribute('id',v);
					 }
					 self.props.elementId=v;
					 self.elements.container.id=v;
				 }},
		"icon" : { "type" : "string|false",
				   "default" : false,
				   "getValue" : function(self){return self.props.icon;},
				   "setValue" : function(self,v){
					   self.setIcon(v);
				   }},
		"enabled" : { "type" : "boolean",
					  "default" : true,
					  "getValue" : function(self){return !self.props.disabled;},
					  "setValue" : function(self,v){
						  if ( v==='true' ) {
							  // self.enableWindow();
							  self.props.disabled = false;
							  self.elements.container.removeAttribute('disabled');
						  }
						  else {
							  // self.disableWindow ();
							  self.props.disabled = true;
							  self.elements.container.setAttribute('disabled','true');
						  }
					  }},
		"button layout" : { "type" : "string",
							"default" : "menu:minimize,maximizeRestore,close",
							"getValue" : function (self){return self.props.buttonLayout;},
							"setValue" : function(self,v){
								self.setLayout(v);
							}},
		"title" : { "type" : "string",
					"default" : "",
					"getValue" : function(self){return self.props.title;},
					"setValue" : function(self,v){
						self.setTitle(v);
					}},
		"show-title" : { "type" : "boolean",
						 "default" : true,
						 "getValue" : function(self){return self.elements.titlecontainer.style.display!='none';},
						 "setValue" : function(self,v){
							 if ( self.elements.titlecontainer.style.display!='none') {
								 self.showTitle();
								 self.elements.container.removeAttribute('hide-title')
							 }
							 else {
								 self.hideTitle();
								 self.elements.container.setAttribute('hide-title','true');
							 }
						 }},
		"title-position" : { "type" : "stringlist",
							 "list" : "top|bottom",
							 "default" : "top",
							 "getValue" : function(self){return self.props.titlePosition;},
							 "setValue" : function(self,v){
								 self.props.titlePosition = v;
								 if ( v === 'bottom' ) {
									 self.moveTitleToBottom();
								 }
								 else if ( v === 'top' ) {
									 self.moveTitleToTop();
								 }
							 }},
		"sticky-top" : { "type" : "boolean",
						 "default" : false,
						 "getValue" : function(self){return self.props.stickyTop;},
						 "setValue" : function(self,v){
							 self.props.stickyTop = v==='true'; 
						 }},
		"top" : { "type" : "number",
				  "default" : 0,
				  "getValue" : function(self){return self.getTop();},
				  "setValue" : function(self,v){
					  v = parseInt(v);
					  self.setTop(v);
				  }},
		"max-top" : { "type" : "number",
					  "default" : -1,
					  "getValue" : function(self){return self.props.maxTop===false?-1:self.props.maxTop;},
					  "setValue" : function(self,v){
						  v = parseInt(v);
						  self.props.maxTop = v===-1?false:v;
					  }},
		"min-top" : { "type" : "number",
					  "default" : -1,
					  "getValue" : function(self){return self.props.minTop===false?-1:self.props.minTop;},
					  "setValue" : function(self,v){
						  v = parseInt(v);
						  self.props.minTop = v===-1?false:v;
					  }},
		"bottom" : { "type" : "number",
					 "default" : -1,
					 "getValue" : function(self){return self.getBottom();},
					 "setValue" : function(self,v){
						 self.setBottom(v);
					 }},
		"sticky-left" : { "type" : "boolean",
						  "default" : false,
						  "getValue" : function(self){return self.props.stickyLeft;},
						  "setValue" : function(self,v){
							  self.props.stickyLeft = v==='true';
						  }},
		"left" : { "type" : "number",
				   "default" : 0,
				   "getValue" : function(self){return self.getLeft();},
				   "setValue" : function(self,v){
					   v = parseInt(v);
					   self.setLeft(v);
				   }},
		"max-left" : { "type" : "number",
					   "default" : -1,
					   "getValue" : function(self){return self.props.maxLeft===false?-1:self.props.maxLeft;},
					   "setValue" : function(self,v){
						   v = parseInt(v);
						   self.props.maxLeft = v===-1?false:v;
					   }},
		"min-left" : { "type" : "number",
					   "default" : -1,
					   "getValue" : function(self){return self.props.minLeft===false?-1:self.props.minLeft;},
					   "setValue" : function(self,v){
						   v = parseInt(v);
						   self.props.minLeft = v===-1?false:v;
					   }},
		"right" : { "type" : "number",
					"default" : -1,
					"getValue" : function(self){return self.getRight();},
					"setValue" : function(self,v){
						self.setRight(v);
					}},
		"height" : { "type" : "number",
					 "default" : 0,
					 "getValue" : function(self){return self.getHeight();},
					 "setValue" : function(self,v){
						 v = parseInt(v);
						 self.setHeight(v);
					 }},
		"max-height" : { "type" : "number",
						 "default" : -1,
						 "getValue" : function(self){return self.props.maxHeight===false?-1:self.props.maxHeight;},
						 "setValue" : function(self,v){
							 v = parseInt(v);
							 self.props.maxHeight = v===-1?false:v;
						 }},
		"min-height" : { "type" : "number",
						 "default" : -1,
						 "getValue" : function(self){return self.props.minHeight===false?-1:self.props.minHeight;},
						 "setValue" : function(self,v){
							 v = parseInt(v);
							 self.props.minHeight = v===-1?false:v;
						 }},
		"width" : { "type" : "number",
					"default" : 0,
					"getValue" : function(self){return self.getWidth();},
					"setValue" : function(self,v){
						v=parseInt(v);
						self.setWidth(v);
					}},
		"max-width" : { "type" : "number",
						"default" : -1,
						"getValue" : function(self){return self.props.maxWidth===false?-1:self.props.maxWidth;},
						"setValue" : function(self,v){
							v=parseInt(v);
							self.props.maxWidth = v===-1?false:v;
						}},
		"min-width" : { "type" : "number",
						"default" : -1,
						"getValue" : function(self){return self.props.minWidth===false?-1:self.props.minWidth;},
						"setValue" : function(self,v){
							v=parseInt(v);
							self.props.minWidth = v===-1?false:v;
						}},
		"match-content-size" : { "type" : "boolean",
								 "default" : false,
								 "getValue" : function(self){return self.props.matchContentSize;},
								 "setValue" : function(self,v){
									 self.props.matchContentSize = v==='true';
								 }},
		"resizable" : { "type" : "boolean",
						"default" : true,
						"getValue" : function(self){return self.props.resizable;},
						"setValue" : function(self,v){
							self.Resizable = v!=false;
						}},
		"movable" : { "type" : "boolean",
					  "default" : true,
					  "getValue" : function(self){return self.props.movable;},
					  "setValue" : function(self,v){
						  self.Movable = v != false;
					  }},
		"tool tip" : { "type" : "string",
					  "default" : '',
					  "getValue" : function ( self ) { var t=self.elements.container.getAttribute('title');if ( t!== null) return t; return '';},
					  "setValue" : function (self,v) {
						  if ( v === '' ) {
							  self.elements.container.removeAttribute('title');
						  }
						  else {
							  self.elements.container.setAttribute('title',v); 
						  }
					  }},
		"state" : { "type" : "stringlist",
					"default" : "normal",
					"list" : "normal|minimized|maximized",
					"getValue" : function(self){return self.props.state;},
					"setValue" : function(self,v){
						if ( v == 'normal' )
							self.restore();
						else if ( v == 'minimized' )
							self.minimize();
						else if ( v == 'maximized' )
							self.maximize();
					}},
		"visible" : { "type" : "boolean",
					  "default" : true,
					  "getValue" : function(self){return self.elements.container.style.display != 'none';},
					  "setValue" : function(self,v){
						  self.elements.container.style.display = v ? '' : 'none';
					  }},
		"theme" : { "type" : "string",
					"default" : "",
					"getValue" : function(self){return self.props.themeClass;},
					"setValue" : function(self,v){
						self.setThemeClass(v);
					}}
	};

	/**
	 * List of all window objects for easy access.
	 * @type {Array}
	 * @public
	 */
	WindowObject.list = [];

	/**
	 * Default CSS class-names for all windows.
	 * @type {object}
	 * @public
	 */
	WindowObject.classNames = {};

	/**
	 * Default CSS class-name for the focused window container element.
	 * @type {string}
	 * @public
	 */
	WindowObject.classNames.hasfocus = 'hasfocus';

	/**
	 * Default CSS class-name for blured window container element.
	 * @type {string}
	 * @public
	 */
	WindowObject.classNames.blured = 'blured';

	/**
	 * Default CSS class-name for maximized window container element.
	 * @type {string}
	 * @public
	 */
	WindowObject.classNames.maximized = 'maximized';

	/**
	 * Default CSS class-name for minimized window container element.
	 * @type {string}
	 * @public
	 */
	WindowObject.classNames.minimized = 'minimized';

	/**
	 * Default CSS class-name for disabled parts of window.
	 * @type {string}
	 * @public
	 */
	WindowObject.classNames.disabled = 'disabled';

	/**
	 * Default CSS class-name for disable vail element.
	 * @type {string}
	 * @public
	 */
	WindowObject.classNames.disableVail = 'disableVail';

	/**
	 * Default CSS class-name for the window container element.
	 * @type {string}
	 * @public
	 */
	WindowObject.classNames.container = 'web-desktop-window';

	/**
	 * Default CSS class-name for window's left border container element.
	 * @type {string}
	 * @public
	 */
	WindowObject.classNames.leftbordercont = 'left-border-container';

	/**
	 * Default CSS class-name for window's top left border box element.
	 * @type {string}
	 * @public
	 */
	WindowObject.classNames.lefttopbrdrbox = 'left-top-border-box';

	/**
	 * Defautl CSS class-name for window's top left corner element.
	 * @type {string}
	 * @public
	 */
	WindowObject.classNames.leftbordertopcrnr = 'left-border-top-corner';

	/**
	 * Default CSS class-name for window's left border middle part element.
	 * @type {string}
	 * @public
	 */
	WindowObject.classNames.leftbordercenter = 'left-border-center';

	/**
	 * Default CSS class-name for window's left border bottom corner element.
	 * @type {string}
	 * @public
	 */
	WindowObject.classNames.leftborderbtmcrnr = 'left-border-bottom-corner';

	/**
	 * Default CSS class-name for window's left bottom box element.
	 * @type {string}
	 * @public
	 */
	WindowObject.classNames.leftbtmbrdrbox = 'left-bottom-border-box';

	/**
	 * Default CSS class-name for window's center container element.
	 * @type {string}
	 * @public
	 */
	WindowObject.classNames.centercont = 'center-container';

	/**
	 * Default CSS class-name for window's top border container element.
	 * @type {string}
	 * @public
	 */
	WindowObject.classNames.topbordercontainer = 'top-border-container';

	/**
	 * Default CSS class-name for window's top border left cornert element.
	 * @type {string}
	 * @public
	 */
	WindowObject.classNames.topborderlftcrnr = 'top-border-left-corner';

	/**
	 * Default CSS class-name for window's top border center element.
	 * @type {string}
	 * @public
	 */
	WindowObject.classNames.topbordercenter = 'top-border-center';

	/**
	 * Default CSS class-name for window's top border right corner element.
	 * @type {string}
	 * @public
	 */
	WindowObject.classNames.topborderrghtcrnr = 'top-border-right-corner';

	/**
	 * Default CSS class-name for window's title and body container element.
	 * @type {string}
	 * @public
	 */
	WindowObject.classNames.bodytitlecontainer = 'body-title-container';

	/**
	 * Default CSS class-name for window's title container element.
	 * @type {string}
	 * @public
	 */
	WindowObject.classNames.titlecontainer = 'title-container';

	/**
	 * Default CSS class-name for window's title left controls container element.
	 * @type {string}
	 * @public
	 */
	WindowObject.classNames.titleleftcontrols = 'title-left-controls';

	/**
	 * Default CSS class-name for window's title center element.
	 * @type {string}
	 * @public
	 */
	WindowObject.classNames.titlecenter = 'title-center';

	/**
	 * Default CSS class-name for window's title right controls container element.
	 * @type {string}
	 * @public
	 */
	WindowObject.classNames.titlerightcontrols = 'title-right-controls';

	/**
	 * Default CSS class-name for window's body container element.
	 * @type {string}
	 * @public
	 */
	WindowObject.classNames.bodycontainer = 'body-container';

	/**
	 * Default CSS class-name for window's bottom border container element.
	 * @type {string}
	 * @public
	 */
	WindowObject.classNames.bottombordercontainer = 'bottom-border-container';

	/**
	 * Default CSS class-name for window's bottom border left corner element.
	 * @type {string}
	 * @public
	 */
	WindowObject.classNames.bottomborderlftcrnr = 'bottom-border-left-corner';

	/**
	 * Default CSS class-name for window's bottom border center element.
	 * @type {string}
	 * @public
	 */
	WindowObject.classNames.bottombordercenter = 'bottom-border-center';

	/**
	 * Defautl CSS class-name for window's bottom border right corner element.
	 * @type {string}
	 * @public
	 */
	WindowObject.classNames.bottomborderrghtcrnr = 'bottom-border-right-conrner';

	/**
	 * Default CSS class-name for window's right border container element.
	 * @type {string}
	 * @public
	 */
	WindowObject.classNames.rightbordercont = 'right-border-container';

	/**
	 * Default CSS class-name for window's right border top box element.
	 * @type {string}
	 * @public
	 */
	WindowObject.classNames.righttopbrdrbox = 'right-top-border-box';

	/**
	 * Default CSS class-name for window's right border top corner element.
	 * @type {string}
	 * @public
	 */
	WindowObject.classNames.rightbordertopcrnr = 'right-top-border-corner';

	/**
	 * Default CSS class-name for window's right border middle element.
	 * @type {string}
	 * @public
	 */
	WindowObject.classNames.rightbordercenter = 'right-border-center';

	/**
	 * Default CSS class-name for window's right border bottom corner element.
	 * @type {string}
	 * @public
	 */
	WindowObject.classNames.rightborderbtmcrnr = 'right-border-bottom-corner';

	/**
	 * Defautl CSS class-name for window's right border bottom box element.
	 * @type {string}
	 * @public
	 */
	WindowObject.classNames.rightbtmbrdrbox = 'right-bottom-border-box';

	/**
	 * Defautl CSS class-name for window's title button elements.
	 * @type {string}
	 * @public
	 */
	WindowObject.classNames.titleControl = 'window-title-control';

	/**
	 * Default CSS class-name for window's title button element with active mousedown event.
	 * @type {string}
	 * @public
	 */
	WindowObject.classNames.titleControlMouseDown = 'window-control-m-down';

	/**
	 * Default CSS class-name for window's title menu button element.
	 * @type {string}
	 * @public
	 */
	WindowObject.classNames.menuBtn = 'window-menu-btn';

	/**
	 * Defautl CSS class-name for window's title close button element.
	 * @type {string}
	 * @public
	 */
	WindowObject.classNames.closeBtn = 'window-close-btn';

	/**
	 * Defautl CSS class-name for window's title minimize button element.
	 * @type {string}
	 * @public
	 */
	WindowObject.classNames.minimizeBtn = 'window-minimize-btn';

	/**
	 * Default CSS class-name for window's title maximize button element.
	 * @type {string}
	 * @public
	 */
	WindowObject.classNames.maximizeBtn = 'window-maximize-btn';

	/**
	 * Default CSS class-name for window's restore button element.
	 * @type {string}
	 * @public
	 */
	WindowObject.classNames.restoreBtn = 'window-restore-btn';

	/**
	 * Default CSS class-name for window's mac style restore button element.
	 * @type {string}
	 * @public
	 */
	WindowObject.classNames.macrestoreBtn = 'window-macrestore-btn';

	/**
	 * Default CSS class-name for the "innercont" (inner container element)
	 * @pulbic
	 * @type {String}
	 */
	WindowObject.classNames.innercont = 'window-inner-container';

	/**
	 * Default CSS class-name for the "innercontup" (inner container up element)
	 * @public
	 * @type {String}
	 */
	WindowObject.classNames.innercontup = 'window-inner-container-up';

	/**
	 * Default CSS class-name for the "innercontleft" (inner container left elemetn)
	 * @public
	 * @type {String}
	 */
	WindowObject.classNames.innercontleft = 'window-inner-container-left';

	/**
	 * Default CSS class-name for the "innercontright" (inner container right element)
	 * @public
	 * @type {String}
	 */
	WindowObject.classNames.innercontright = 'window-inner-container-right';

	/**
	 * Default CSS class-name for the "innercontbtm" (inner container bottom element)
	 * @public
	 * @type {String}
	 */
	WindowObject.classNames.innercontbtm = 'window-inner-container-bottom';

	/**
	 * Array to store functions for the newwindow event.
	 * @type {array}
	 * @public
	 */
	WindowObject.onnewwindow=[];

	/**
	 * Array to store functions for the load event.
	 * @type {array}
	 * @public
	 */
	WindowObject.onload=[];

	/**
	 * Array to store functions for the ready event.
	 * @type {array}
	 * @public
	 */
	WindowObject.onready=[];

	/**
	 * Array to store functions for the windowfocus event.
	 * @type {array}
	 * @public
	 */
	WindowObject.onwindowfocus=[];

	/**
	 * Property to set the radius that windows will stick.
	 * @type {Number}
	 * @public
	 */
	WindowObject.stickyRadius = 20;

	/**
	 * Element to appear when a window is moving.
	 * @type {HTMLElement}
	 * @public
	 */
	WindowObject.MovingVail = document.createElement('div');
	WindowObject.MovingVail.className = 'web-desktop-vail';

	/**
	 * Add events for all windows
	 *
	 * @param {string} e - Event name
	 * @param {function} f - Function to run on event.
	 * @return {boolean} false for error, true otherwise.
	 */
	WindowObject.addEvent = function ( e, f ) {
		if ( typeof WindowObject['on'+e] == 'undefined') return false;
		for ( var i = 0; i < WindowObject['on'+e].length; i++ ) if ( WindowObject['on'+e][i] === f) return true;
		WindowObject['on'+e].push(f);
		return true;
	};

	/**
	 * Fire events for all windows.
	 * @param {string} e - Event name to fire.
	 * @param {object} [obj] - Event data object.
	 * @return {boolean} false for error, true otherwise.
	 */
	WindowObject.fireEvent = function ( e, obj ) {
		var cobj;
		if ( obj.target ) cobj = obj.target; else cobj = this;
		obj.type = e;
		if ( typeof WindowObject['on'+e] == 'undefined') return false;
		for ( var i = 0; i < WindowObject['on'+e].length; i++ )WindowObject['on'+e][i].call(cobj, obj);
		return true;
	};

	/**
	 * Method for positioning windows in "windowList" in a cascade formation.
	 *
	 * @param {array} windowList - Array with the list of window objects to cascade.
	 * @param {number} desktopW - Width of desktop.
	 * @param {number} dekstopH - Height of dekstop.
	 * @param {number} stepX - Horizontal step (distance between two windows) in number of pixels.
	 * @param {number} stepY - Vertical step ( distance between two windows ) in nubmer of pixels.
	*/
	WindowObject.cascadeWindows = function ( windowList, desktopW, desktopH, stepX, stepY ) {
		var countWindows = 0;
		var mw = 0;
		var mh = 0;
		var tmpmw = 0;
		var tmpmh = 0;

		var tmpwin = [];
		for ( var i = 0 ; i < windowList.length; i ++ ) {
			tmpmw = windowList.length * stepX + windowList[i].elements.container.offsetWidth;
			if ( tmpmw > mw ) mw = tmpmw;

			tmpmh = windowList.length * stepY + windowList[i].elements.container.offsetHeight;
			if ( tmpmh > mh ) mh = tmpmh;
		}
		var x = desktopW / 2 - tmpmw / 2;
		var y = desktopH / 2 - tmpmh / 2;
		for ( i = 0 ; i < windowList.length; i ++ ) {
			x += stepX;
			y += stepY;
			windowList[i].props.left = x;
			windowList[i].props.top = y;
			windowList[i].elements.container.parentNode.appendChild ( windowList[i].elements.container );
			windowList[i].position();
		}
	};

	/**
	 * Method for positioning windows in "windowList" in a tile formation.
	 *
	 * @param {array} windowList - Array with the list of window objects to cascade.
	 * @param {number} desktopW - Width of desktop.
	 * @param {number} dekstopH - Height of dekstop.
	*/
	WindowObject.tileWindows = function ( windowList, desktopW, desktopH ) {
		var countWindows = 0;
		var uh = 0;
		var dh = 0

		var tmpw = 0;
		var tmph = 0;

		var tmppx = 0;
		var tmppy = 0;

		if ( windowList.length & 1 ) {
			uh = Math.floor ( windowList.length / 2 );
			dh = windowList.length - uh;
		} else {
			uh = windowList.length / 2 ;
			dh = uh;
		}

		tmph = desktopH / 2;
		tmpw = desktopW / uh;
		
		for ( i = 0 ; i < uh; i ++ ) {
			windowList[i].props.left = tmppx;
			windowList[i].props.top = tmppy;
			windowList[i].setOuterHeight(tmph).setOuterWidth(tmpw);
			windowList[i].props.movable = true;
			windowList[i].props.resizable = true;
			windowList[i].resize();
			windowList[i].position();
			tmppx += tmpw;
		}
		tmppy += tmph;
		tmpw = desktopW / dh;
		tmppx = 0;
		for ( i = uh ; i < uh + dh; i ++ ) {
			windowList[i].props.left = tmppx;
			windowList[i].props.top = tmppy;
			windowList[i].setOuterHeight(tmph).setOuterWidth(tmpw);
			windowList[i].props.movable = true;
			windowList[i].props.resizable = true;
			windowList[i].resize();
			windowList[i].position();
			tmppx += tmpw;
		}
	};

	/**
	 * Get distance for top left corner of browser window or a desktop element of an element.
	 *
	 * @param {HTMLElement} e - The element to check.
	 * @return {Object} Object of the form {top:x, left:y} where x is the distance from top in pixels and y the distance from left in pixels.
	 */
	function offset ( e ) {
		var ret={left:e.offsetLeft,top:e.offsetLeft},o=e;
		while ( o && o.nodeName!='#document' && (o.style.position != 'absolute' || o.style.position != 'fixed' ) ) {
			ret.left+=o.offsetLeft;
			ret.top+=o.offsetTop;
			o=o.offsetParent;
		}
			
		return ret;
	}

	/**
	 * Function for closing windows.
	 *
	 * @param {WindowObject} wobj - The window object to close.
	 */
	function closeWindow ( wobj ) {
		wobj.elements.container.remove();

		WindowObject.list.splice(wobj.id,1);
		for ( var i = wobj.id; i < WindowObject.list.length; i ++ )
			WindowObject.list[i].id = i;
		if ( wobj.desktop !== null ) wobj.desktop.removeObject( wobj );

	}

	/**
	 * Function for controling the wheel event for windows.
	 * @param {Event} e - Event data.
	 */
	function mWheel ( e ) {
		var o = e.target, self = false;
		while ( o.nodeName != '#document' ) {
			if ( o.dataWindowObject ) {
				self = o.dataWindowObject;
				break;
			}
			o = o.parentNode;
		}
		if ( self === false ) return;

		if ( e.shiftKey == true ) {
			e.preventDefault();
			var op = 0;
			if ( self.elements.container.style.opacity === "" ) op = 1; else  op = parseFloat(self.elements.container.style.opacity);
			op +=  -(e.deltaY / 100 );
			op = op > 1 ? 1 : op < 0 ? 0 : op;
			self.elements.container.style.opacity = op;
		}
	}

	/**
	 * Function for controling mousedown event on window controls (close, minimize and maximize/restore buttons).
	 * @param {Event} e - Event data object.
	 */
	function windowControlMDown ( e ) {
		e.preventDefault();
		var self = e.target.dataWindowObject;
		if ( e.target.classList.contains ( self.props.classNames.disabled ) ) return;
		windowControlMDown.control = e.target;
		windowControlMDown.control.classList.add ( self.props.classNames.titleControlMouseDown );
		self.decorateChange(e.target, 'mousedown');
		document.body.addEventListener('mouseup',windowControlMUp);
		document.addEventListener('mouseup',windowControlMUp);
	}

	/**
	 * Function for controling mouse up event on window controls (close, minimize, maximize/restore buttons).
	 * @param {Event} e - Event data object.
	 */
	function windowControlMUp ( e ) {
		e.preventDefault();
		if ( ! windowControlMDown.control ) return ;
		var self = windowControlMDown.control.dataWindowObject;
		windowControlMDown.control.classList.remove ( self.props.classNames.titleControlMouseDown );
		self.decorateChange(windowControlMDown.control);
		document.body.removeEventListener('mouseup',windowControlMUp);
		document.removeEventListener('mouseup',windowControlMUp);
	}

	/**
	 * Function for controling mouse click event on window controls (close, minimize, maximize/restore, menu buttons).
	 * @param {Event} e - Event data object.
	 */
	function windowControlClick ( e ) {
		e.preventDefault();
		var self = e.target.dataWindowObject;
		if ( ! e.target.classList.contains ( self.props.classNames.disabled ) )
			e.target.action.call(self,e.target);
	}

	/**
	 * Function for controling mouse double clicks on window controls (close, minimize, maximize/restore and menu buttons).
	 * @param {Event} e - Event data object.
	 */
	function windowControlDBLClick ( e ) {
		e.preventDefault();
		var self = e.target.dataWindowObject;
		e.target.dblclickAction.call(self,e.target);
	}

	/**
	 * Function for controling mouse click event (anywhere) on window.
	 * @param {Event} e - Event data object.
	 */
	function clickWindow ( e ) {
		var o = e.target, self = false;
		var eobj = {
			"target" : e.target,
			"screenX" : e.screenX,
			"screenY" : e.screenY,
			"pageX" : e.pageX,
			"pageY" : e.pageY,
			"offsetX" : e.offsetX,
			"offsetY" : e.offsetY,
			"layerX" : e.layerX,
			"layerY" : e.layerY,
			"clientX" : e.clientX,
			"clientY" : e.clientY,
			"button" : e.button,
			"buttons" : e.buttons,
			"x" : e.x,
			"y" : e.y,
			"which" : e.which
		};
		while ( o && o.nodeName != '#document' ){
			if ( o.dataWindowObject ) {
				var self = o.dataWindowObject;
				eobj.window = self;
				self.fireEvent('click',eobj);
				if ( eobj.defaultPrevented === true ) return;
				self.focus();
				return;
			}
			o = o.parentNode;
		}
	}

	/**
	 * Function for tracking mouse move on window body element.
	 * @param {Event} e - Event data object.
	 */
	function windowbodymousemove ( e ) {
		var self = false, o = e.target;

		while ( o.nodeName != '#document' ){
			if ( o.dataWindowObject ) {
				self = o.dataWindowObject;
				self.fireEvent('mousemove', {
					"target":self,
					"clientX" : e.clientX,
					"clientY" : e.clientY,
					"pageX" : e.pageX,
					"pageY" : e.pageY,
					"screenX" : e.screenX,
					"screenY" : e.screenY,
					"layerX" : e.layerX,
					"layerY" : e.layerY
				});
				return;;
			}
			o = o.parentNode;
		}
	}

	/**
	 * Function for tracking keypress, keyup and keydown events on window body element.
	 * @param {Event} e - Event data object.
	 */
	function windowbodykeypress ( e ) {
		var self = false, o = e.target;
		while ( o.nodeName != '#document' ){
			if ( o.dataWindowObject ) {
				self = o.dataWindowObject;
				var eobj = {
					"target" : e.target,
					"window":self,
					"altKey" : e.altKey,
					"ctrlKey" : e.ctrlKey,
					"metaKey" : e.metaKey,
					"shiftKey" : e.shiftKey,
					"charCode" : e.charCode,
					"code" : e.code,
					"key" : e.key,
					"keyCode" : e.keyCode,
					"which" : e.which
				};
				self.fireEvent( e.type, eobj );
				if ( eobj.defaultPrevented === true ) return;
				self.focus();
				return;
			}
			o = o.parentNode;
		}
	}

	/**
	 * Function for controling mouse down event (anywhere) on window.
	 * @param {Event} e - Event data object.
	 */
	function mdownWindow ( e ) {
		var o = e.target, self = false;
		while ( o.nodeName != '#document' ){
			if ( o.dataWindowObject ) {
				self = o.dataWindowObject;
				self.focus();
				break;
			}
			o = o.parentNode;
		}
		if ( self === false ) return ;

		if ( e.ctrlKey != false ) {

			// BEGIN MOVE WINDOW
			if ( e.button == 0 && self.props.state == 'normal' && self.props.movable == true ) {

				//var self = e.target.dataWindowObject;
				self.elements.titlecenter.removeEventListener('mousedown',wtitleMdown);
				self.elements.container.removeEventListener("mousedown", mdownWindow );

				self.elements.bodycontainer.removeEventListener("mousemove", windowbodymousemove );
				

				document.body.addEventListener('mouseup',wtitleMup);
				document.addEventListener('mouseup',wtitleMup);
				document.body.addEventListener('mousemove', wtitleMove );
				document.addEventListener('mousemove', wtitleMove );
				document.body.addEventListener('keydown', windowKeyboard );

				if ( self.props.useMovingVail ) {
					if ( self.desktop !== null )
						self.desktop.elements.root.appendChild ( WindowObject.MovingVail );
					else 
						document.body.appendChild ( WindowObject.MovingVail );
					WindowObject.MovingVail.style.display = 'none';
					WindowObject.MovingVail.style.width = self.elements.container.offsetWidth + 'px';
					WindowObject.MovingVail.style.height =self.elements.container.offsetHeight + 'px';
					WindowObject.MovingVail.style.top = parseFloat(self.props.top) + 'px';
					WindowObject.MovingVail.style.left = parseFloat(self.props.left) + 'px';
				}

				wtitleMove.obj = self;
				wtitleMove.elem = e.target;
				wtitleMove.elemData = offset(self.elements.container);
				wtitleMove.parentElemData = offset(self.elements.container.parentNode);
				wtitleMove.mousedata = {
					x: e.clientX,
					// dx: e.clientX - wtitleMove.elemData.left + parseFloat(self.props.theme.border.width) + 1,
					dx: parseFloat(self.props.left),
					y: e.clientY,
					// dy: e.clientY - wtitleMove.elemData.top + 1
					dy: parseFloat(self.props.top)
				};
			} else if ( e.button == 2 ) {
				wtitleMove.obj = self;
				document.body.addEventListener('mouseup',wtitleMup);
			}
		}

		else {
			self.fireEvent('mousedown', {
				"target": e.target,
				"window" : self,
				"button" : e.button,
				"buttons" : e.buttons,
				"which"  : e.which,
				"clientX" : e.clientX,
				"clientY" : e.clientY,
				"pageX" : e.pageX,
				"pageY" : e.pageY,
				"screenX" : e.screenX,
				"screenY" : e.screenY,
				"layerX" : e.layerX,
				"layerY" : e.layerY
			});
		}

	}

	/**
	 * Function for controling mouse down event on window's title.
	 * @param {Event} e - Event data object.
	 */
	function wtitleMdown ( e ) {
		e.preventDefault();

		var self = e.target.dataWindowObject;
		self.focus();

		if ( ( ( e.type == 'mousedown' && e.button == 0 ) || ( e.type == 'touchstart' ) ) && self.props.state != 'maximized' && self.props.movable == true ) {
			if ( e.type == 'touchstart' && e.changedTouches.length != 1 ) return;
			self.elements.titlecenter.removeEventListener('mousedown',wtitleMdown);
			self.elements.container.removeEventListener("mousedown", mdownWindow );
			self.elements.bodycontainer.removeEventListener("mousemove", windowbodymousemove );

			document.body.addEventListener('mouseup',wtitleMup);
			document.body.addEventListener('touchend',wtitleMup);
			document.addEventListener('mouseup',wtitleMup);
			document.body.addEventListener('mousemove', wtitleMove );
			document.body.addEventListener('touchmove', wtitleMove );
			document.addEventListener('mousemove', wtitleMove );
			wtitleMove.keyboardAction = "move";
			document.body.addEventListener('keydown', windowKeyboard );

			if ( self.props.useMovingVail ){
				if ( self.desktop !== null ) 
					self.desktop.elements.root.appendChild ( WindowObject.MovingVail );
				else 
					document.body.appendChild ( WindowObject.MovingVail );
				WindowObject.MovingVail.style.display = 'none';
				WindowObject.MovingVail.style.width = self.elements.container.offsetWidth + 'px';
				WindowObject.MovingVail.style.height =self.elements.container.offsetHeight + 'px';
				WindowObject.MovingVail.style.top = self.props.top + 'px';
				WindowObject.MovingVail.style.left = self.props.left + 'px';
			}

			wtitleMove.obj = self;
			wtitleMove.elem = e.target;
			wtitleMove.elemData = offset(wtitleMove.elem);
			if ( e.type == 'mousedown' ) wtitleMove.mousedata = {
				x: e.clientX,
				// dx: e.clientX - wtitleMove.elemData.left + parseFloat(self.props.theme.border.width) + 1,
				dx : parseFloat(self.props.left),
				y: e.clientY,
				//dy: e.clientY - wtitleMove.elemData.top + parseFloat(self.props.theme.border.width) + 1
				dy: parseFloat(self.props.top)
			}; else if ( e.type == 'touchstart' ) wtitleMove.mousedata = {
				x: e.changedTouches[0].pageX,
				dx : parseFloat(self.props.left),
				y: e.changedTouches[0].pageY,
				dy: parseFloat(self.props.top)
			};

		} else if ( e.button == 2 ) {
			wtitleMove.obj = self;
			document.body.addEventListener('mouseup',wtitleMup);
		}

	}

	/**
	 * Function for controling mouse move event on window's title.
	 * @param {Event} e - Event data object.
	 */
	function wtitleMove ( e ) {
		self = wtitleMove.obj;
		if ( self === false ) return;

		if ( e.type == 'mousemove' ) { 
			//self.props.left = e.clientX - wtitleMove.mousedata.dx;
			self.props.left = wtitleMove.mousedata.dx + ( e.clientX - wtitleMove.mousedata.x );
			//self.props.top = e.clientY - wtitleMove.mousedata.dy;
			self.props.top = wtitleMove.mousedata.dy + ( e.clientY - wtitleMove.mousedata.y );
		} else if ( e.type == 'touchmove' ) {
			self.props.left = wtitleMove.mousedata.dx + ( e.changedTouches[0].pageX - wtitleMove.mousedata.x );
			self.props.top = wtitleMove.mousedata.dy + ( e.changedTouches[0].pageY - wtitleMove.mousedata.y );
		}

		if ( self.props.minTop !== false && self.props.top < self.props.minTop ) self.props.top = self.props.minTop;
		if ( self.props.maxTop !== false && self.props.top > self.props.maxTop ) self.props.top = self.props.maxTop;
		if ( self.props.minLeft !== false && self.props.left < self.props.minLeft ) self.props.left = self.props.minLeft;
		if ( self.props.maxLeft !== false && self.props.left > self.props.maxLeft ) self.props.left = self.props.maxLeft;

		if ( ! self.props.useMovingVail ) {
			self.position();
		} else {
			WindowObject.MovingVail.style.display = 'block';
			WindowObject.MovingVail.style.top = self.props.top + 'px';
			WindowObject.MovingVail.style.left = self.props.left + 'px';
		}

		// Sticky windows
		if ( e.shiftKey && self.desktop ) {
			self.props.stickyTop = self.props.top;
			self.props.stickyLeft = self.props.left;
			
			for ( var i = 0; i < self.desktop.objects.length; i ++ )
				if ( self.desktop.objects[i] !== self
					 // && self.desktop.objects[i].type == 'window'
				   ) {
					var otlw = self.desktop.objects[i].props.left + self.desktop.objects[i].elements.container.offsetWidth;
					var otth = self.desktop.objects[i].props.top + self.desktop.objects[i].elements.container.offsetHeight;

					var cwlw = self.props.left + self.elements.container.offsetWidth;
					var cwth = self.props.top + self.elements.container.offsetHeight;

					if ( cwth > self.desktop.objects[i].props.top &&
						 self.props.top < otth ) {
						// Check current window right side with
						// other windows left side.
						if ( cwlw + WindowObject.stickyRadius > self.desktop.objects[i].props.left &&
							 cwlw - WindowObject.stickyRadius < self.desktop.objects[i].props.left
						   ) {
							self.props.stickyLeft = self.desktop.objects[i].props.left - self.elements.container.offsetWidth;
							self.elements.container.style.left = self.props.stickyLeft + 'px';
						}

						// Check current window left side with
						// other windows right side.
						else if ( self.props.left > otlw - WindowObject.stickyRadius &&
								  self.props.left < otlw + WindowObject.stickyRadius )  {
							self.props.stickyLeft = otlw;
							self.elements.container.style.left = self.props.stickyLeft + 'px';
						}

					}

					if ( cwlw > self.desktop.objects[i].props.left &&
						 self.props.left < otlw ) {

						// Check current window top side with
						// other windows bottom side.
						if ( cwth + WindowObject.stickyRadius > self.desktop.objects[i].props.top &&
							 cwth - WindowObject.stickyRadius < self.desktop.objects[i].props.top
						   ) {
							self.props.stickyTop = self.desktop.objects[i].props.top - self.elements.container.offsetHeight;
							self.elements.container.style.top = self.props.stickyTop + 'px';
						}

						// Check current window bottom side with
						// other windows top side.
						else if ( self.props.top > otth - WindowObject.stickyRadius &&
								  self.props.top < otth + WindowObject.stickyRadius )  {
							self.props.stickyTop = otth;
							self.elements.container.style.top = self.props.stickyTop + 'px';
						}
					}
				}
		} else {
			self.props.stickyTop = false;
			self.props.stickyLeft = false;
		}

	}

	/**
	 * Function for controling mouse up event on window's title.
	 * @param {Event} e - Event data object.
	 */
	function wtitleMup ( e ) {
		e.preventDefault();
		var self = wtitleMove.obj;

		if ( e.button == 2 ) {
			if ( window.lvzwebdesktop.MenuObject && self.props.disableAppMenu !== true ) {
				document.body.removeEventListener('mouseup',wtitleMup);
				var menu = lvzwebdesktop('#appmenu','MenuObject');
				if ( menu ) {
					menu.props.callerElement = self.elements.menuBtn;
					menu.show(menu.props.callerElement);
					menu.elements.container.style.position = 'absolute';
					menu.elements.container.style.display = 'block';
					menu.elements.container.style.top = (e.clientY + ( scrollMaxY - (scrollMaxY - scrollY)))+ 'px';
					menu.elements.container.style.left = (e.clientX + ( scrollMaxX - (scrollMaxX - scrollX)))+'px';
					menu.fixPosition();
				}
			}
		} else  {
			if ( e.shiftKey ) {
				self.props.top = self.props.stickyTop;
				self.props.left = self.props.stickyLeft;
			}
			if ( self.props.useMovingVail ){
				WindowObject.MovingVail.style.display = 'none';
				self.position();
			}
			document.body.removeEventListener('mouseup',wtitleMup);
			document.body.removeEventListener('touchend',wtitleMup);
			document.removeEventListener('mouseup',wtitleMup);
			self.elements.titlecenter.addEventListener('mousedown',wtitleMdown);
			self.elements.titlecenter.addEventListener('touchstart',wtitleMdown);
			self.elements.container.addEventListener("mousedown", mdownWindow );
			document.body.removeEventListener('mousemove', wtitleMove );
			document.body.removeEventListener('touchmove', wtitleMove );
			document.removeEventListener('mousemove', wtitleMove );
			document.body.removeEventListener('keydown', windowKeyboard );

			self.elements.bodycontainer.addEventListener("mousemove", windowbodymousemove );
		}
	}

	/**
	 * Function for controling windows with the keyboard.
	 * @param {Event} e - Event data object.
	 */
	function windowKeyboard ( e ) {
		var self = wtitleMove.obj;
		if ( self === false ) return;
		if ( wtitleMove.keyboardAction == "move" ) {
			if ( e.charCode == 0 ) switch ( e.keyCode ) {

				case 13: // e.key == "Enter"
				document.body.removeEventListener('keydown', windowKeyboard );
				if ( self.props.useMovingVail ){
					WindowObject.MovingVail.style.display = 'none';
					self.position();
				}
				return;
				break;

				case 37: // e.key == "ArrowLeft"
				self.props.left -= 10;
				break;

				case 38: // e.key == "ArrowUp"
				self.props.top -= 10;
				break;

				case 39: // e.key == "ArrowRight"
				self.props.left += 10;
				break;

				case 40: // e.key == "ArrowDown"
				self.props.top += 10;
				break;

			}

			if ( self.props.minTop !== false && self.props.top < self.props.minTop ) self.props.top = self.props.minTop;
			if ( self.props.maxTop !== false && self.props.top > self.props.maxTop ) self.props.top = self.props.maxTop;
			if ( self.props.minLeft !== false && self.props.left < self.props.minLeft ) self.props.left = self.props.minLeft;
			if ( self.props.maxLeft !== false && self.props.left > self.props.maxLeft ) self.props.left = self.props.maxLeft;

			if ( ! self.props.useMovingVail )
				self.position();
			else {
				WindowObject.MovingVail.style.display = 'block';
				WindowObject.MovingVail.style.top = self.props.top + 'px';
				WindowObject.MovingVail.style.left = self.props.left + 'px';
			}
			
		} else if ( wtitleMove.keyboardAction == "resize" ) {
			if ( e.charCode == 0 ) switch ( e.keyCode ) {

				case 13: // e.key == "Enter"
				wtitleMove.resizeDirection.horizontal = false;
				wtitleMove.resizeDirection.vertical = false;
				document.body.removeEventListener('keydown', windowKeyboard );
				if ( self.props.useMovingVail ){
					WindowObject.MovingVail.style.display = 'none';
					//self.position();
				}
				return;
				break;

				case 37: // e.key == "ArrowLeft"
				if ( wtitleMove.resizeDirection.horizontal == false  ) {
					wtitleMove.resizeDirection.horizontal = 'left';
					if ( self.props.width >= self.props.minWidth && ( self.props.maxWidth === false || self.props.maxWidth > self.props.width ) ) {
						self.props.width += 10;
						self.props.left -= 10;
					}
				} else if ( wtitleMove.resizeDirection.horizontal == 'left' ) {
					if ( self.props.width >= self.props.minWidth && ( self.props.maxWidth === false || self.props.maxWidth > self.props.width ) ) {
						self.props.width += 10;
						self.props.left -= 10;
					}
				} else if ( wtitleMove.resizeDirection.horizontal == 'right' ) {
					if ( self.props.width >= self.props.minWidth && ( self.props.maxWidth === false || self.props.maxWidth > self.props.width ) ) {
						self.props.width -= 10;
					}
				}
				break;

				case 38: // e.key == "ArrowUp"
				if ( wtitleMove.resizeDirection.vertical == false  ) {
					wtitleMove.resizeDirection.vertical = 'up';
					if ( self.props.height >= self.props.minHeight && ( self.props.maxHeight === false || self.props.maxHeight > self.props.height ) ) {
						self.props.height += 10;
						self.props.top -= 10;
					}
				} else if ( wtitleMove.resizeDirection.vertical == 'up' ) {
					if ( self.props.height >= self.props.minHeight && ( self.props.maxHeight === false || self.props.maxHeight > self.props.height ) ) {
						self.props.height += 10;
						self.props.top -= 10;
					}
				} else if ( wtitleMove.resizeDirection.vertical == 'down' ) {
					if ( self.props.height >= self.props.minHeight && ( self.props.maxHeight === false || self.props.maxHeight > self.props.height ) ) {
						self.props.height -= 10;
					}
				}
				break;

				case 39: // e.key == "ArrowRight"
				if ( wtitleMove.resizeDirection.horizontal == false  ) {
					wtitleMove.resizeDirection.horizontal = 'right';
					if ( self.props.width >= self.props.minWidth && ( self.props.maxWidth === false || self.props.maxWidth > self.props.width ) ) {
						self.props.width += 10;
					}
				} else if ( wtitleMove.resizeDirection.horizontal == 'left' ) {
					if ( self.props.width >= self.props.minWidth && ( self.props.maxWidth === false || self.props.maxWidth > self.props.width ) ) {
						self.props.width -= 10;
						self.props.left += 10;
					}
				} else if ( wtitleMove.resizeDirection.horizontal == 'right' ) {
					if ( self.props.width >= self.props.minWidth && ( self.props.maxWidth === false || self.props.maxWidth > self.props.width ) ) {
						self.props.width += 10;
					}
				}
				break;

				case 40: // e.key == "ArrowDown"
				if ( wtitleMove.resizeDirection.vertical == false  ) {
					wtitleMove.resizeDirection.vertical = 'down';
					if ( self.props.height >= self.props.minHeight && ( self.props.maxHeight === false || self.props.maxHeight > self.props.height ) ) {
						self.props.height += 10;
					}
				} else if ( wtitleMove.resizeDirection.vertical == 'up' ) {
					if ( self.props.height >= self.props.minHeight && ( self.props.maxHeight === false || self.props.maxHeight > self.props.height ) ) {
						self.props.height -= 10;
						self.props.top += 10;
					}
				} else if ( wtitleMove.resizeDirection.vertical == 'down' ) {
				 	if ( self.props.height >= self.props.minHeight && ( self.props.maxHeight === false || self.props.maxHeight > self.props.height ) ) {
						self.props.height += 10;
					}
				}
				break;

			}

			if ( ! self.props.useMovingVail ) {
				self.position();
				self.resize();
			} else {
				WindowObject.MovingVail.style.display = 'block';
				WindowObject.MovingVail.style.top = self.props.top + 'px';
				WindowObject.MovingVail.style.left = self.props.left + 'px';
				WindowObject.MovingVail.style.height = (self.elements.container.offsetHeight - (borderResizeMMove.data.woh - self.props.height)) + 'px';
				WindowObject.MovingVail.style.width = (self.elements.container.offsetWidth - (borderResizeMMove.data.wow - self.props.width)) + 'px';
			}
		}
	}

	/**
	 * Function for controling touch startevent on windows for resize.
	 * @param {Event} e - Event data object.
	 */
	function touchResizeStart ( e ) {
		e.preventDefault();
		var self = WindowObject.MovingVail.dataWindowObject;

		if ( ! self ) return;

		wtitleMove.obj = self;

		touchResizeStart.elemData = offset(e.target);

		var eoffset = self.elements.container.getBoundingClientRect();

		var dw = parseFloat(self.props.width) - self.elements.container.offsetWidth;
		var dh = parseFloat(self.props.height) - self.elements.container.offsetHeight;
		touchResizeStart.data = {
			t : e.target,
			// direction : e.target.dataElement,
			self : self,
			mx : e.changedTouches[0].pageX,
			dx: e.changedTouches[0].pageX - touchResizeStart.elemData.left + parseFloat(self.props.theme.border.width) + 1,
			my : e.changedTouches[0].pageY,
			dy: e.changedTouches[0].pageY - touchResizeStart.elemData.top + parseFloat(self.props.theme.border.width) + 1,
			wt : parseFloat(self.props.top),
			wl : parseFloat(self.props.left),
			ww : parseFloat(self.props.width) - dw,
			wh : parseFloat(self.props.height) - dh,
			wow : self.elements.container.offsetWidth,
			woh : self.elements.container.offsetHeight
		};


		switch ( e.target.dataResizeDirection ) {
		case 'NW':
			wtitleMove.resizeDirection = { horizontal : "left", vertical : "up" };
			break;

		case 'N':
			wtitleMove.resizeDirection = { horizontal : false, vertical : "up" };
			break;

		case 'NE':
			wtitleMove.resizeDirection = { horizontal : "right", vertical : "up" };
			break;

		case 'W':
			wtitleMove.resizeDirection = { horizontal : "left", vertical : false };
			break;

		case 'E':
			wtitleMove.resizeDirection = { horizontal : "right", vertical : false };
			break;

		case 'SW':
			wtitleMove.resizeDirection = { horizontal : "left", vertical : "down" };
			break;

		case 'S':
			wtitleMove.resizeDirection = { horizontal : false, vertical : "down" };
			break;

		case 'SE':
			wtitleMove.resizeDirection = { horizontal : "right", vertical : "down" };
			break;

		}
		document.body.addEventListener('touchmove', touchResizeMove );
		document.body.addEventListener('touchend', touchResizeEnd );

	}

	/**
	 * Function for controling touch move event on windows for resize.
	 * @param {Event} e - Event data object.
	 */
	function touchResizeMove ( e ) {
		e.preventDefault();

		var self = touchResizeStart.data.self;

		var tmptop = self.props.top;
		var tmpleft = self.props.left;
		var tmpwidth = touchResizeStart.data.ww
		var tmpheight = touchResizeStart.data.wh;

		if ( wtitleMove.resizeDirection.horizontal == 'left' ) {
			tmpleft = touchResizeStart.data.wl + (e.changedTouches[0].pageX - touchResizeStart.data.mx);
			tmpwidth = touchResizeStart.data.ww + ( touchResizeStart.data.mx - e.changedTouches[0].pageX );

			if ( tmpwidth >= self.props.minWidth && ( self.props.maxWidth === false || self.props.maxWidth > tmpwidth ) ) {
				self.props.width = tmpwidth;
				self.props.left = tmpleft;
			} else if ( tmpwidth < self.props.minWidth ) {
				self.props.width = self.props.minWidth;
				self.props.left = (touchResizeStart.data.wl + touchResizeStart.data.ww) - self.props.width;
			} else if ( tmpwidth > self.props.maxWidth ) {
				self.props.width = self.props.maxWidth;
				self.props.left = (touchResizeStart.data.wl + touchResizeStart.data.ww) - self.props.width;
			}
		} else if ( wtitleMove.resizeDirection.horizontal == 'right' ) {
			tmpwidth = touchResizeStart.data.ww + ( e.changedTouches[0].pageX - touchResizeStart.data.mx );
			if ( tmpwidth >= self.props.minWidth && ( self.props.maxWidth === false || self.props.maxWidth > tmpwidth ) ) self.props.width = tmpwidth;
			else if ( tmpwidth < self.props.minWidth ) self.props.width = self.props.minWidth;
			else if ( tmpwidth > self.props.maxWidth ) self.props.width = self.props.maxWidth;
		}

		if ( wtitleMove.resizeDirection.vertical == 'up' ) {
			tmptop = touchResizeStart.data.wt + (e.changedTouches[0].pageY - touchResizeStart.data.my);
			tmpheight = touchResizeStart.data.wh + ( touchResizeStart.data.my - e.changedTouches[0].pageY );

			if ( tmpheight >= self.props.minHeight && ( self.props.maxHeight === false || self.props.maxHeight > tmpheight ) ) {
				self.props.height = tmpheight;
				self.props.top = tmptop;
			} else if ( tmpheight < self.props.minHeight ) {
				self.props.height = self.props.minHeight;
				self.props.top = (touchResizeStart.data.wt + touchResizeStart.data.wh) - self.props.height;
			} else if ( tmpheight > self.props.maxHeight ) {
				self.props.height = self.props.maxHeight;
				self.props.top = (touchResizeStart.data.wt + touchResizeStart.data.wh) - self.props.height;
			}
		} else if ( wtitleMove.resizeDirection.vertical == 'down' ) {
			tmpheight = touchResizeStart.data.wh + ( e.changedTouches[0].pageY - touchResizeStart.data.my );

			if ( tmpheight >= self.props.minHeight && ( self.props.maxHeight === false || self.props.maxHeight > tmpheight ) ) self.props.height = tmpheight;
			else if ( tmpheight < self.props.minHeight ) self.props.height = self.props.minHeight;
			else if ( tmpheight > self.props.maxHeight ) self.props.height = self.props.maxHeight;
		}

		self.props.top = tmptop;
		self.props.left = tmpleft;
		self.props.width = tmpwidth;
		self.props.height = tmpheight;

		// tmptop -= touchResizeStart.data.dy ;
		// tmpleft -= touchResizeStart.data.dx ;

		var vmiddle = tmptop + ( tmpheight / 2 ) ;
		var hmiddle = tmpleft + ( tmpwidth / 2 ) ;

		var bottom = tmptop + tmpheight;
		var right = tmpleft + tmpwidth;

		var circwd2 = WindowObject.touchCircles.NW.offsetWidth / 2;
		var circhd2 = WindowObject.touchCircles.NW.offsetHeight / 2;

		WindowObject.touchCircles.NW.style.top = ( tmptop - circhd2) + 'px';
		WindowObject.touchCircles.NW.style.left = (tmpleft - circwd2) + 'px';

		WindowObject.touchCircles.N.style.top = ( tmptop - circhd2) + 'px';
		WindowObject.touchCircles.N.style.left = (hmiddle - circwd2) + 'px';

		WindowObject.touchCircles.NE.style.top = ( tmptop - circhd2) + 'px';
		WindowObject.touchCircles.NE.style.left = (right - circwd2) + 'px';

		WindowObject.touchCircles.W.style.top = ( vmiddle - circhd2) + 'px';
		WindowObject.touchCircles.W.style.left = (tmpleft - circwd2) + 'px';

		WindowObject.touchCircles.E.style.top = ( vmiddle - circhd2) + 'px';
		WindowObject.touchCircles.E.style.left = (right - circwd2) + 'px';

		WindowObject.touchCircles.SW.style.top = ( bottom - circhd2) + 'px';
		WindowObject.touchCircles.SW.style.left = (tmpleft - circwd2) + 'px';

		WindowObject.touchCircles.S.style.top = ( bottom - circhd2) + 'px';
		WindowObject.touchCircles.S.style.left = (hmiddle - circwd2) + 'px';

		WindowObject.touchCircles.SE.style.top = ( bottom - circhd2) + 'px';
		WindowObject.touchCircles.SE.style.left = (right - circwd2) + 'px';

		WindowObject.MovingVail.style.width = tmpwidth + 'px';
		WindowObject.MovingVail.style.height = tmpheight + 'px';
		WindowObject.MovingVail.style.top = tmptop + 'px';
		WindowObject.MovingVail.style.left = tmpleft + 'px';

	}

	/**
	 * Function for controling touch end event on windows.
	 * @param {Event} e - Event data object.
	 */
	function touchResizeEnd ( e ) {
		document.body.removeEventListener('touchmove', touchResizeMove );
		document.body.removeEventListener('touchend', touchResizeEnd );

		for ( i in WindowObject.touchCircles ) WindowObject.touchCircles[i].style.display = 'none';
		WindowObject.MovingVail.style.display = 'none';

		touchResizeStart.data.self.position();
		touchResizeStart.data.self.resize();

	}

	/**
	 * Function for controling mouse down event on window's border.
	 * @param {Event} e - Event data object.
	 */
	function borderResizeBegin ( e ) {
		e.preventDefault();
		var self = e.target.dataWindowObject;
		if ( self.props.resizable !== true ) return;

		document.body.addEventListener('mouseup',borderResizeMUP);
		document.addEventListener('mouseup',borderResizeMUP);
		document.body.addEventListener('mousemove', borderResizeMMove );
		document.addEventListener('mousemove', borderResizeMMove );

		wtitleMove.obj = self;
		wtitleMove.elem = e.target;
		wtitleMove.keyboardAction = "resize";
		wtitleMove.resizeDirection = { horizontal :false, vertical : false };
		document.body.addEventListener('keydown', windowKeyboard );

		borderResizeMMove.elemData = offset(e.target);
		if ( self.props.useMovingVail ){
			if ( self.desktop !== null ) 
				self.desktop.elements.root.appendChild ( WindowObject.MovingVail );
			else 
				document.body.appendChild ( WindowObject.MovingVail );
			//WindowObject.MovingVail.style.display = 'block';
			WindowObject.MovingVail.style.width = self.elements.container.offsetWidth + 'px';
			WindowObject.MovingVail.style.height =self.elements.container.offsetHeight + 'px';
			WindowObject.MovingVail.style.top = self.props.top + 'px';
			WindowObject.MovingVail.style.left = self.props.left + 'px';
		}
		borderResizeMMove.data = {
			t : e.target,
			direction : e.target.dataElement,
			self : self,
			mx : e.clientX,
			dx: e.clientX - borderResizeMMove.elemData.left + parseFloat(self.props.theme.border.width) + 1,
			my : e.clientY,
			dy: e.clientY - borderResizeMMove.elemData.top + parseFloat(self.props.theme.border.width) + 1,
			wt : parseFloat(self.props.top),
			wl : parseFloat(self.props.left),
			ww : parseFloat(self.props.width),
			wh : parseFloat(self.props.height),
			wow : parseFloat(self.props.width),
			woh : parseFloat(self.props.height)
		};
	}

	lvzwebdesktop.borderResizeBegin = borderResizeBegin;

	/**
	 * Function for controling mouse mouve event on window's border.
	 * @param {Event} e - Event data object.
	 */
	function borderResizeMMove ( e ) {
		e.preventDefault();
		var self = borderResizeMMove.data.self;

		if ( self.props.resizable !== true ) return;
		switch ( borderResizeMMove.data.direction ) {

		case 'lefttopbrdrbox':	// LEFT TOP
		case 'leftbordertopcrnr':
		case 'topborderlftcrnr':
			var tmpleft = borderResizeMMove.data.wl + (e.clientX - borderResizeMMove.data.mx);
			var tmpwidth = borderResizeMMove.data.ww + ( borderResizeMMove.data.mx - e.clientX );
			var tmptop = borderResizeMMove.data.wt + (e.clientY - borderResizeMMove.data.my);
			var tmpheight = borderResizeMMove.data.wh + ( borderResizeMMove.data.my - e.clientY );

			wtitleMove.resizeDirection = { horizontal : "left", vertical : "up" };

			if ( tmpheight >= self.props.minHeight && ( self.props.maxHeight === false || self.props.maxHeight > tmpheight ) ) {
				self.props.height = tmpheight;
				self.props.top = tmptop;
			} else if ( tmpheight < self.props.minHeight ) {
				self.props.height = self.props.minHeight;
				self.props.top = (borderResizeMMove.data.wt + borderResizeMMove.data.wh) - self.props.height;
			} else if ( tmpheight > self.props.maxHeight ) {
				self.props.height = self.props.maxHeight;
				self.props.top = (borderResizeMMove.data.wt + borderResizeMMove.data.wh) - self.props.height;
			}

			if ( tmpwidth >= self.props.minWidth && ( self.props.maxWidth === false || self.props.maxWidth > tmpwidth ) ) {
				self.props.width = tmpwidth;
				self.props.left = tmpleft;
			} else if ( tmpwidth < self.props.minWidth ) {
				self.props.width = self.props.minWidth;
				self.props.left = (borderResizeMMove.data.wl + borderResizeMMove.data.ww) - self.props.width;
			} else if ( tmpwidth > self.props.maxWidth ) {
				self.props.width = self.props.maxWidth;
				self.props.left = (borderResizeMMove.data.wl + borderResizeMMove.data.ww) - self.props.width;
			}
			break;		

		case 'leftbordercenter': 	// LEFT
			var tmpleft = e.clientX - borderResizeMMove.data.dx + borderResizeMMove.data.t.offsetWidth;
			var tmpwidth = borderResizeMMove.data.ww + ( borderResizeMMove.data.mx - e.clientX );

			wtitleMove.resizeDirection = { horizontal : "left", vertical : false };

			if ( tmpwidth >= self.props.minWidth && ( self.props.maxWidth === false || self.props.maxWidth > tmpwidth ) ) {
				self.props.width = tmpwidth;
				self.props.left = tmpleft;
			} else if ( tmpwidth < self.props.minWidth ) {
				self.props.width = self.props.minWidth;
				self.props.left = (borderResizeMMove.data.wl + borderResizeMMove.data.ww) - self.props.width;
			} else if ( tmpwidth > self.props.maxWidth ) {
				self.props.width = self.props.maxWidth;
				self.props.left = (borderResizeMMove.data.wl + borderResizeMMove.data.ww) - self.props.width;
			}
			break;

		case 'leftborderbtmcrnr': 	// LEFT BOTTOM
		case 'leftbtmbrdrbox':
		case 'bottomborderlftcrnr':
			var tmpleft = borderResizeMMove.data.wl + (e.clientX - borderResizeMMove.data.mx);
			var tmpwidth = borderResizeMMove.data.ww + ( borderResizeMMove.data.mx - e.clientX );
			var tmpheight = borderResizeMMove.data.wh + ( e.clientY - borderResizeMMove.data.my );

			wtitleMove.resizeDirection = { horizontal : "left", vertical : "down" };

			if ( tmpwidth >= self.props.minWidth && ( self.props.maxWidth === false || self.props.maxWidth > tmpwidth ) ) {
				self.props.width = tmpwidth;
				self.props.left = tmpleft;
			} else if ( tmpwidth < self.props.minWidth ) {
				self.props.width = self.props.minWidth;
				self.props.left = (borderResizeMMove.data.wl + borderResizeMMove.data.ww) - self.props.width;
			} else if ( tmpwidth > self.props.maxWidth ) {
				self.props.width = self.props.maxWidth;
				self.props.left = (borderResizeMMove.data.wl + borderResizeMMove.data.ww) - self.props.width;
			}

			if ( tmpheight >= self.props.minHeight  && ( self.props.maxHeight === false || self.props.maxHeight > tmpheight ) ) self.props.height = tmpheight;
			else if ( tmpheight < self.props.minHeight ) self.props.height = self.props.minHeight;
			else if ( tmpheight > self.props.maxHeight ) self.props.height = self.props.maxHeight;
			break;

		case 'topbordercenter': 	// TOP
			var tmptop = borderResizeMMove.data.wt + (e.clientY - borderResizeMMove.data.my);
			var tmpheight = borderResizeMMove.data.wh + ( borderResizeMMove.data.my - e.clientY );

			wtitleMove.resizeDirection = { horizontal : false, vertical : "up" };

			if ( tmpheight >= self.props.minHeight && ( self.props.maxHeight === false || self.props.maxHeight > tmpheight ) ) {
				self.props.height = tmpheight;
				self.props.top = tmptop;
			} else if ( tmpheight < self.props.minHeight ) {
				self.props.height = self.props.minHeight;
				self.props.top = (borderResizeMMove.data.wt + borderResizeMMove.data.wh) - self.props.height;
			} else if ( tmpheight > self.props.maxHeight ) {
				self.props.height = self.props.maxHeight;
				self.props.top = (borderResizeMMove.data.wt + borderResizeMMove.data.wh) - self.props.height;
			}
			break;

		case 'bottombordercenter':	// BOTTOM
			var tmpheight = borderResizeMMove.data.wh + ( e.clientY - borderResizeMMove.data.my );
			wtitleMove.resizeDirection = { horizontal : false, vertical : "down" };

			if ( tmpheight >= self.props.minHeight && ( self.props.maxHeight === false || self.props.maxHeight > tmpheight ) ) self.props.height = tmpheight;
			else if ( tmpheight < self.props.minHeight ) self.props.height = self.props.minHeight;
			else if ( tmpheight > self.props.maxHeight ) self.props.height = self.props.maxHeight;
			break;

		case 'topborderrghtcrnr':	// RIGHT TOP
		case 'righttopbrdrbox':
		case 'rightbordertopcrnr':
			var tmptop = borderResizeMMove.data.wt + (e.clientY - borderResizeMMove.data.my);
			var tmpwidth = borderResizeMMove.data.ww + ( e.clientX - borderResizeMMove.data.mx );
			var tmpheight = borderResizeMMove.data.wh + ( borderResizeMMove.data.my - e.clientY );

			wtitleMove.resizeDirection = { horizontal : "right", vertical : "up" };

			if ( tmpheight >= self.props.minHeight && ( self.props.maxHeight === false || self.props.maxHeight > tmpheight ) ) {
				self.props.height = tmpheight;
				self.props.top = tmptop;
			} else if ( tmpheight < self.props.minHeight ) {
				self.props.height = self.props.minHeight;
				self.props.top = (borderResizeMMove.data.wt + borderResizeMMove.data.wh) - self.props.height;
			} else if ( tmpheight > self.props.maxHeight ) {
				self.props.height = self.props.maxHeight;
				self.props.top = (borderResizeMMove.data.wt + borderResizeMMove.data.wh) - self.props.height;
			}

			if ( tmpwidth >= self.props.minWidth && ( self.props.maxWidth === false || self.props.maxWidth > tmpwidth ) ) self.props.width = tmpwidth;
			else if ( tmpwidth < self.props.minWidth ) self.props.width = self.props.minWidth;
			else if ( tmpwidth > self.props.maxWidth ) self.props.width = self.props.maxWidth;
			break;

		case 'bottomborderrghtcrnr': // RIGHT BOTTOM
		case 'rightbtmbrdrbox':
		case 'rightborderbtmcrnr':
		case 'macresizeBtn':
			var tmpwidth = borderResizeMMove.data.ww + ( e.clientX - borderResizeMMove.data.mx  );
			var tmpheight = borderResizeMMove.data.wh + ( e.clientY - borderResizeMMove.data.my );
			
			wtitleMove.resizeDirection = { horizontal : "right", vertical : "down" };

			if ( tmpheight >= self.props.minHeight && ( self.props.maxHeight === false || self.props.maxHeight > tmpheight ) ) self.props.height = tmpheight;
			else if ( tmpheight < self.props.minHeight ) self.props.height = self.props.minHeight;
			else if ( tmpheight > self.props.maxHeight ) self.props.height = self.props.maxHeight;

			if ( tmpwidth >= self.props.minWidth && ( self.props.maxWidth === false || self.props.maxWidth > tmpwidth ) ) self.props.width = tmpwidth;
			else if ( tmpwidth < self.props.minWidth ) self.props.width = self.props.minWidth;
			else if ( tmpwidth > self.props.maxWidth ) self.props.width = self.props.maxWidth;
			break;

		case 'rightbordercenter':	// RIGHT
			var tmpwidth = borderResizeMMove.data.ww + ( e.clientX - borderResizeMMove.data.mx  );

			wtitleMove.resizeDirection = { horizontal : "right", vertical : false };

			if ( tmpwidth >= self.props.minWidth && ( self.props.maxWidth === false || self.props.maxWidth > tmpwidth ) ) self.props.width = tmpwidth;
			else if ( tmpwidth < self.props.minWidth ) self.props.width = self.props.minWidth;
			else if ( tmpwidth > self.props.maxWidth ) self.props.width = self.props.maxWidth;
			break;
			
		}
		if ( ! self.props.useMovingVail ) {
			self.position();
			self.resize();
		} else {
			WindowObject.MovingVail.style.display = 'block';
			WindowObject.MovingVail.style.top = self.props.top + 'px';
			WindowObject.MovingVail.style.left = self.props.left + 'px';
			WindowObject.MovingVail.style.height = (self.elements.container.offsetHeight - (borderResizeMMove.data.woh - self.props.height)) + 'px';
			WindowObject.MovingVail.style.width = (self.elements.container.offsetWidth - (borderResizeMMove.data.wow - self.props.width)) + 'px';
		}
	}

	/**
	 * Funciton for controling mouse up event on window's border.
	 * @param {Event} e - Event data object.
	 */
	function borderResizeMUP ( e ) {
		e.preventDefault();
		document.body.removeEventListener('mouseup',borderResizeMUP);
		document.removeEventListener('mouseup',borderResizeMUP);
		document.body.removeEventListener('mousemove', borderResizeMMove );
		document.removeEventListener('mousemove', borderResizeMMove );

		var self = borderResizeMMove.data.self;
		if ( self.props.useMovingVail ){
			WindowObject.MovingVail.style.display = 'none';
			self.position();
			self.resize();
		}
	}

	window.lvzwebdesktop.WindowObject = WindowObject;
	window.lvzwebdesktop.constructors.push({
		"selector" : '.'+WindowObject.classNames.container+",web-window",
		"constructor" : WindowObject
	});

	/**
	 * Called by windowobject resizeObserver for resizing window based on content size.
	 * @param {Event} e - the resize observer data object
	 */
	function resizeWindowOnContent ( e ) {
		var o = e[0].target;
		var w = e[0].target.offsetWidth;
		var h = e[0].target.offsetHeight;
		var self;
		while ( o && o.nodeName != '#document' ) {
			if ( typeof o.dataWindowObject == 'object' ) {
				self = o.dataWindowObject;
				break
			}
			o = o.parentNode;
		}
		if (!self) return false;
		self.matchContentSize();
		return true;
	}

}
