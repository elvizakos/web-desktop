{
	document.currentScript.dataLVZWLIB = true;

	/**
     * Constructor for creating taskbar objects.
     *
     * @constructor
     * @param {HTMLElement} e - Element to attach taskbar to.
     */
	var TaskbarObject = function ( e ) {

		lvzwebdesktop.basicObject.call(this,'taskbar');

		/**
		 * Array with type of objects to show on taskbar.
		 * @type {Array}
		 * @public
		 */
		this.props.showObjects = ['window'];

		/**
		 * CSS class-name for taskbar container.
		 * @type {string}
		 * @public
		 */
		this.props.classNames.container = 'web-desktop-taskbar';

		/**
		 * CSS class-name for taskbar's task button container.
		 * @type {string}
		 * @public
		 */
		this.props.classNames.taskbtn = 'taskbtn';

		/**
		 * CSS class-name for taskbar's task button icon element.
		 * @type {string}
		 * @public
		 */
		this.props.classNames.taskicon = 'taskicon';

		/**
		 * CSS class-name for taskbar's task button label element.
		 * @type {string}
		 * @public
		 */
		this.props.classNames.tasklabel = 'tasklabel';

		/**
		 * CSS class-name for taskbar's focused  task button container.
		 * @type {string}
		 * @public
		 */
		this.props.classNames.focused = 'focused';

		/**
		 * Desktop that the taskbar belongs to or null if no desktop.
		 * @type {DesktopObject|null}
		 * @public
		 */
		this.desktop = null;

		/**
		 * Array for storing event functions for taskbar reorder event.
		 * @type {Array}
		 * @public
		 */
		this.onorderchange = [];

		/**
		 * Array for storing event functions for when taskbar is changing desktop object.
		 * @type {Array}
		 * @public
		 */
		this.onchangedesktop = [];

		/**
		 * Array for storing event functions for when taskbar object is attached to another object. 
		 * @type {Array}
		 * @public
		 */
		this.onattachto = [];

		/**
         * Add an object to this taskbar.
         *
         * @param {object} o - The object to add. o.type must be in this.props.showObjects.
         * @return {TaskbarObject} This taskbar object.
         */
		this.addObject = function ( o ) {
			if ( this.props.showObjects.indexOf ( o.type ) == -1 ) return this;

			if ( typeof o.props.showInTaskbar != 'undefined' &&
				 o.props.showInTaskbar === false) return this;

			for ( var i = 0; i < this.elements.container.children.length; i ++ )
				if ( this.elements.container.children[i].dataTaskObject === o ) return this;

			var tmpcontainer = document.createElement('div');
			tmpcontainer.draggable = true;
			tmpcontainer.dataTaskbarObject = this;
			tmpcontainer.dataLVZWDESKTOPOBJ = this;
			tmpcontainer.dataTaskObject = o;
			tmpcontainer.dataWindowObject = o;
			tmpcontainer.className = this.props.classNames.taskbtn;
			tmpcontainer.addEventListener('click', taskbarbtnClick);
			tmpcontainer.addEventListener('mousedown', taskbarbtnmdown);
			tmpcontainer.addEventListener('dragstart', taskbarbtndrag, false );
			tmpcontainer.addEventListener('dragenter', function ( e ) { e.preventDefault(); } , false );
			tmpcontainer.addEventListener('dragover', function ( e ) { e.preventDefault(); } , false );
			tmpcontainer.addEventListener('drop', taskbarbtndrop, false );
			
			if ( o.props.focus ) tmpcontainer.classList.add ( this.props.classNames.focused );

			var tmpicon = document.createElement('div');
			tmpicon.dataTaskbarObject = this;
			tmpicon.dataLVZWDESKTOPOBJ = this;
			tmpicon.dataTaskObject = o;
			tmpicon.dataWindowObject = o;
			if ( o.props.icon ) tmpicon.style.backgroundImage = "url('" + o.props.icon + "')";
			tmpicon.className = this.props.classNames.taskicon;
			tmpcontainer.appendChild ( tmpicon );

			var tmpcaption = document.createElement('div');
			tmpcaption.dataTaskbarObject = this;
			tmpcaption.dataLVZWDESKTOPOBJ = this;
			tmpcaption.dataTaskObject = o;
			tmpcaption.dataWindowObject = o;
			tmpcaption.innerText = o.props.title;
			tmpcontainer.title = o.props.title;
			tmpcaption.className = this.props.classNames.tasklabel;
			tmpcontainer.appendChild ( tmpcaption );

			if ( !o.taskBars ) {
				o.taskBars = [];
				o
					.addEvent('close', function ( e ) {
						e.target.taskBars.forEach ( function ( t, i ) { t.removeObject ( e.target ); } );
					})
					.addEvent('show', function ( e ) {
						e.target.taskBars.forEach ( function ( t, i ) { t.addObject ( e.target ); } );
					})
					.addEvent('focus', function ( e ) {
						e.target.taskBars.forEach ( function ( t, i ) { t.focusObject ( e.target ); } );
					})
					.addEvent('titlechange', function ( e ) {
						e.target.taskBars.forEach ( function ( t, i ) { t.changeObjectTitle ( e.target ); } );
					})
					.addEvent('minimize', function ( e ) {
						e.target.elements.container.style.display = 'none';
						e.target.taskBars.forEach ( function ( t, i ) { t.giveFocusToPreviousObject () ; } );
					})
					.addEvent('iconchange',function ( e ) {
						o.taskBars.forEach ( function ( t, i ) {
							for ( var j = 0; j < t.elements.container.children.length; j++ ) if ( t.elements.container.children[j].dataWindowObject === e.target ) {
								if ( e.icon ) t.elements.container.children[j].children[0].style.backgroundImage = "url('" + e.icon + "')";
							}
						});
					})
				;
			}

			if ( o.taskBars.indexOf ( this ) < 0 ) o.taskBars.push ( this );

			this.elements.container.appendChild ( tmpcontainer );

			if ( o.props.focused ) this.focusObject ( o );

			return this;
		};

		/**
		 * Remove task button of an object.
		 *
		 * @param {object} o - The owner object of task button.
		 * @return {TaskbarObject} This object.
		 */
		this.removeObject = function ( o ) {
			for ( var i = 0; i < this.elements.container.children.length; i ++ )
				if ( this.elements.container.children[i].dataTaskObject === o ) {
					if ( this.elements.container.children[i] === this.focusedItem ) this.giveFocusToPreviousObject();
					this.elements.container.removeChild( this.elements.container.children[i] );
				}
			return this;
		};

		/**
		 * Set css styles on object focus.
		 *
		 * @param {object} o - The object that has focus.
		 * @return {TaskbarObject} This object.
		 */
		this.focusObject = function ( o ) {

			for ( var i = 0; i < this.elements.container.children.length; i ++ ) if ( this.elements.container.children[i].dataTaskObject.props.focused ) {
				// if ( this.elements.container.children[i].dataTaskObject === o )  {
				this.elements.container.children[i].classList.add ( this.props.classNames.focused );
				this.focusedItem = this.elements.container.children[i];
			} else {
				this.elements.container.children[i].classList.remove (this.props.classNames.focused);
			}
			return this;
		};

		/**
		 * Change a taskbar button label.
		 *
		 * @param {object} o - The owner object of task button.
		 * @return {TaskbarObject} This object.
		 */
		this.changeObjectTitle = function ( o ) {
			for ( var i = 0; i < this.elements.container.children.length; i ++ ) if ( this.elements.container.children[i].dataTaskObject === o )  {
				this.elements.container.children[i].title = o.props.title;
				this.elements.container.children[i].children[1].innerText = o.props.title;
				return this;
			}
			return this;
		};

		/**
		 * Gives focus to previous object in order that the one that currently has focus.
		 * @return {TaskbarObject} This object.
		 */
		this.giveFocusToPreviousObject = function () {

			if ( this.focusedItem && this.focusedItem.previousElementSibling !== null ) {
				this.focusedItem.previousElementSibling.dataTaskObject.focus();
			} else if ( this.focusedItem && this.focusedItem.nextElementSibling !== null ) {
				this.focusedItem.nextElementSibling.dataTaskObject.focus();
			} else {
				if ( this.focusedItem ) this.focusedItem.dataTaskObject.props.focused = false;
				this.focusedItem = null;
				this.focusObject();
			}

			return this;
		};

		if ( typeof e == 'undefined' ) this.elements.container = document.createElement('div'); else this.elements.container = e;
		
		this.elements.container.classList.add (this.props.classNames.container );
		this.elements.container.dataTaskbarObject = this;
		this.elements.container.dataLVZWDESKTOPOBJ = this;
		this.elements.container.addEventListener ( 'contextmenu', function ( e ) { e.preventDefault(); return false; } );

		this.elements.taskButtons = [];

		attr = getComputedStyle(document.body).getPropertyValue('--lvzwebdesktop-default-taskbar-theme').trim();
		if ( attr != '' ) {
			for ( var i = 0 ; i < this.elements.container.classList.length ; i ++ ) {
				if ( getComputedStyle(document.body).getPropertyValue('--lvzwebdesktop-theme-taskbar-'+this.elements.container.classList[i]).trim() == 'true' ) {
					this.props.themeClass = this.elements.container.classList[i];
					break;
				}
			}
			if ( this.props.themeClass === null ) {
				this.props.themeClass = attr;
				this.elements.container.classList.add(attr);
			}
		}
		else {
			console.error ("There is no theme loaded");
		}


		var desk = window.lvzwebdesktop.getParentDesktop ( this.elements.container);
		if ( desk ) {
			// desk.addObject(this,false);
			this.desktop = desk;

			this.desktop.taskbars.push ( this );
			// this.desktop.addEvent ( 'objectadd' , function ( e ) {
			// 	if ( this.props.showObjects.indexOf ( e.target.type ) == -1 ) return false;
			// 	this.addObject ( e.target );
			// });

			for ( var i = 0; i < this.desktop.objects.length; i ++ )
				if ( this.props.showObjects.indexOf ( this.desktop.objects[i].type ) > -1 )
					this.addObject ( this.desktop.objects[i] );

		}

		this.addEvent ( 'changedesktop', function ( e ) {
			var self = e.target;

			if ( e.previousDesktop !== null && typeof e.previousDesktop.taskbar != 'undefined') {
				var tp = e.previousDesktop.taskbar.indexOf ( self );
				e.previousDesktop.taskbar.splice ( tp, 1);
			}

			if ( e.desktop === null ) {
				for ( var i = self.elements.container.children.length - 1 ; i >= 0 ; i -- ) 
					self.elements.container.removeChild( self.elements.container.children[i] );
				return false;
			}

			if ( self.desktop.taskbars.indexOf ( self ) > -1 ) return false;
			self.desktop.taskbars.push ( self );

			for ( var i = self.elements.container.children.length - 1 ; i >= 0 ; i -- ) {
				self.elements.container.removeChild( self.elements.container.children[i] );
			}

			for ( i = 0; i < self.desktop.objects.length; i ++ )
				if ( self.props.showObjects.indexOf ( self.desktop.objects[i].type ) > -1 )
					self.addObject ( self.desktop.objects[i] );

			self.focusObject ( self.focusedWindow );
		});

	}

	/**
	 * List with all taskbar objects.
	 * @type {Array}
	 * @public
	 */
	TaskbarObject.list = [];

	/**
	 * Runs on taskbar button click event.
	 *
	 * @param {Event} e - The click event object.
	 */
	function taskbarbtnClick ( e ) {
		var self = e.target.dataTaskbarObject;
		if ( e.target.dataTaskObject.props.state == 'minimized' ) {
			e.target.dataTaskObject.restore();
		} else if ( e.target.dataTaskObject.props.focused == true ) {
			e.target.dataTaskObject.minimize();
		}
		e.target.dataTaskObject.focus();
	}

	/**
	 * Runs on taskbar button mousedown event.
	 *
	 * @param {Event} e - The event object.
	 */
	function taskbarbtnmdown ( e ) {

		var self = e.target.dataTaskbarObject;
		var wind = e.target.dataTaskObject;

		if ( e.button == 2 ) {

			if ( window.lvzwebdesktop.MenuObject && wind.props.disableAppMenu !== true ) {
				var menu = lvzwebdesktop('#appmenu','MenuObject');
				if ( menu ) {
					menu.props.callerElement = e.target;
					menu.show ( e.target );
				}
			}
		}
	}

	/**
	 * Runs on taskbar button dragstart event.
	 *
	 * @param {Event} e - The event object.
	 */
	function taskbarbtndrag ( e ) {
		taskbarbtndrag.draggedItem = e.target;
	}

	/**
	 * Runs on taskbar button drop event.
	 * 
	 * @param {Event} e - The event object.
	 */
	function taskbarbtndrop ( e ) {
		e.preventDefault();
		var itm = taskbarbtndrag.draggedItem;
		var o = e.target;
		while (o.nodeName != '#document' && !o.classList.contains ( itm.dataTaskbarObject.props.classNames.taskbtn ) ) {
			o = o.parentNode;
		}
		if ( ! o.classList.contains (  itm.dataTaskbarObject.props.classNames.taskbtn ) ) return;

		if ( o.dataTaskbarObject === itm.dataTaskbarObject ) {
			var isAfter = false;
			var ta = o;
			while ( ta.nextElementSibling !== null ) {
				ta = ta.nextElementSibling;
				if ( ta === itm ) { isAfter = true; break; }
			}
			if ( isAfter ) {
				o.parentNode.insertBefore ( itm, o );
			} else {
				o.parentNode.insertBefore ( itm, o.nextElementSibling );
			}

			o.dataTaskbarObject.fireEvent ( 'orderchange', {
				"target" : o.dataTaskbarObject,
				"moveditem" : o,
				"targetItem" : itm,
				"moveToPosition" : isAfter ? 'before' : 'after'
			});

		}
	}

	window.lvzwebdesktop.TaskbarObject = TaskbarObject;
	window.lvzwebdesktop.constructors.push({
		"selector" : ".web-desktop-taskbar,web-taskbar",
		"constructor" : TaskbarObject
	});

}
