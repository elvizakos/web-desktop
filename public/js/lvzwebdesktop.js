{
	document.currentScript.dataLVZWLIB = true;
	// tests on jsdoc: https://jsdoc.app/
	// jsdoc lvzwebdesktop.js

	/**
	 * Method for getting the first found object of type "ofType" of parent elements.
	 *
	 * @callback getParentObject
	 * @param {function|string} [ofType] - The object's constructor or constructor's name. If it's undefined (not set), the method will return the first parent lvzwebdesktop object.
	 * @return {object|null|false} The object of the given type or null if not found. Return false on error.
	 */

	var scriptTags=document.getElementsByTagName('script');
	// var currentScriptURL = scriptTags[scriptTags.length-1].src;
	var currentScriptURL = document.currentScript.src;
	currentScriptURL = currentScriptURL.split(/[?]/);

	/**
	 * Function for getting a remote file or stream (asyncronus).
	 *
	 * @function getFile
	 * @param {string} file - URL of file or stream.
	 * @param {function} f - Function to run on getting data.
	 */
	function getFile ( file, f ) {
		var xht = new XMLHttpRequest();
		xht.onreadystatechange = function ( ) {
			if ( this.readyState == 4 && this.status == 200 ) {
				f( this.responseText );
			}
		};
		xht.open("GET", file, true);
		xht.send();
	}

	/**
	 * Function for getting a remote file or stream (syncronus).
	 *
	 * @function getFileS
	 * @param {string} file - URL of file or stream.
	 * @return {string} The contents of file or stream.
	 */
	function getFileS ( file ) {
		var xht = new XMLHttpRequest();

		xht.open("GET", file, false);
		xht.send(null);

		if ( xht.status == 200 ) {
			return xht.responseText;
		}
		return false;
	}

	function elementLoaded ( tagName, attr, attrval ) {
		var tags = document.getElementsByTagName(tagName);
		for ( var i =0; i<tags.length; i++ )
			if ( tags[i].getAttribute(attr) == attrval ) return tags[i];
		return false;
	}

	/**
	 * The main function for controling lvzwebdesktop objects.
	 *
	 * @function lvzwebdesktop
	 * @memberof lvzwebdesktopmainns
	 * @param {...*} params - Parameters for searching objects.
	 * @return {object}
	 */
	window.lvzwebdesktop = function (params) {
		var i;

		// If one argumet is given
		if ( arguments.length == 1 ) {

			// Get lvzwebdesktop object from element
			if ( arguments[0] instanceof HTMLElement ) {

				var tmpc;
				//for ( i = 0; i < window.lvzwebdesktop.constructors.length; i++ ) {
					//tmpc = arguments[0]['data'+lvzwebdesktop.constructors[i].constructor.name];
					tmpc = arguments[0].dataLVZWDESKTOPOBJ;
					if ( typeof tmpc == 'object' ) return tmpc;
				//}
			}
			else if ( typeof arguments[0] == 'string' ){
				var url = arguments[0];

				return {
					loadApp : function ( addContentHere, params ) {
						if ( url.charAt(url.length -1 ) != '/' ) url+='/';
						var m = JSON.parse(getFileS(url+'manifest.json'));
						var el;
						var p;
						var ret=[];
						if ( typeof m.content_scripts != 'undefined' ) {
							for ( var j=0; j<m.content_scripts.length; j++) {
								if ( typeof m.content_scripts[j].css != 'undefined' ) 
									for ( var i=0;i<m.content_scripts[j].css.length;i++)
										if ( ! elementLoaded ( 'link', 'href', url+m.content_scripts[j].css[i] ) ) {
											el = document.createElement('link');
											el.type = 'text/css';
											el.rel = 'stylesheet';
											el.href = url+m.content_scripts[j].css[i];
											document.head.appendChild( el ) ;
										}

								if ( typeof m.content_scripts[j].js == 'object' ) 
									for ( var i=0;i<m.content_scripts[j].js.length;i++) {

										el =  elementLoaded ( 'script', 'src', url+m.content_scripts[j].js[i].src + "?$=lvzwebdesktop" );
										if ( el === false ) {
											el = document.createElement('script');
											el.dataScript = [];
											el.dataLoaded = false;
											if ( typeof addContentHere == 'object' && addContentHere instanceof HTMLElement ) el.dataContentElement = addContentHere;
											document.head.appendChild( el ) ;
										}
										p = {
											"url" : url,
											"src" : m.content_scripts[j].js[i].src + "?$=lvzwebdesktop",
											"parameters" : params,
											"attr" : params,
											"onload" : m.content_scripts[j].js[i].onload,
											"container" : addContentHere
										};
										el.dataScript.push(p);
										if ( !el.dataLoaded ) {
											el.addEventListener('load', function (e) {
												e.target.dataLoaded = true;
												var self;
												var data = e.target.dataScript;
												for ( var k=data.length-1;k>-1;k--) {
													self = lvzwebdesktop(data[k].container);
													(new Function('app',data[k].onload))(data[k]);
													self.fireEvent('apploaded',{
														"target" : self,
														"data" : data[k]
													});
													e.target.dataScript.splice(k,1);
													if ( typeof self.resizeOnContent == 'function' ) self.resizeOnContent(self.props.resizeOnContent);
												}
											});
											el.src = url+m.content_scripts[j].js[i].src + "?$=lvzwebdesktop";
										}
										else {
											var self;
											var data = el.dataScript;
											for ( var k=data.length-1;k>-1;k--) {
												self = data[k].container.dataLVZWDESKTOPOBJ;
												(new Function('app',data[k].onload))(data[k]);
												self.fireEvent('apploaded',{
													"target" : self,
													"data" : data[k]
												});
												el.dataScript.splice(k,1);
												if ( typeof self.resizeOnContent == 'function' ) self.resizeOnContent(self.props.resizeOnContent);
											}
										}
									}
							}
						}
						return ret;
					}
				};
			}
			return false;
		}

		// If two arguments are given
		else if ( arguments.length == 2 ) {

			// Get lvzwebdesktop object from element defined with a css selector (arg1) of type (arg2)
			if ( typeof arguments[0] == 'string' &&
				 typeof arguments[1] == 'string') {

				var selector = arguments[0];
				var type = arguments[1];
				
				if ( typeof window.lvzwebdesktop[type] == 'undefined'
					 || typeof window.lvzwebdesktop[type].list  == 'undefined'
				   ) return false;

				if ( selector.substr(0,1) == '#' )
					for ( i = 0; i < window.lvzwebdesktop[type].list.length ; i ++ )
						if ( window.lvzwebdesktop[type].list[i].props.elementId == selector.substr(1) ) return window.lvzwebdesktop[type].list[i];
			}

			// if first argument is element
			else if ( arguments[0] instanceof HTMLElement ) {
				var elem = arguments[0];
				return {

					getParentObject : function ( ofType ) {
						var constr = false;
						var o = elem.parentNode;
						if ( typeof ofType == 'function' ) {
							constr = ofType;
						}
						else if ( typeof ofType == 'string' ) {
							for ( var i = 0; i < window.lvzwebdesktop.constructors.length; i ++ )
								if ( window.lvzwebdesktop.constructors[i].constructor.name == ofType ) {
									constr = window.lvzwebdesktop.constructors[i].constructor;
									break;
								}
							if ( constr === false ) {
								console.error ( " Can't find constructor \""+ofType+"\". Make sure that the constructors script is loaded." );
								return false;
							}
						}
						else if ( typeof ofType == 'undefined' ) {
							constr = false;
							return null;
						}
						else {
							console.error ( "Must give constructor or string describing the object" );
							return false;
						}

						if ( o === null ) return null;
						while ( o.nodeName !== '#document' ) {
							if ( ( constr !== false && o.dataLVZWDESKTOPOBJ instanceof constr ) || constr === false ) return o.dataLVZWDESKTOPOBJ;
							o = o.parentNode;
						}

						return null;
					},

					getParentToAttach : function () {
						var o = elem.parentNode;
						while ( o.nodeName !== '#document' ) {
							if ( typeof o.dataLVZWDESKTOPOBJ == 'object' &&
								 typeof o.dataLVZWDESKTOPOBJ.attachItem == 'function'
							   ) {
								return o;
							}
							o = o.parentNode;
						}
						return null;
					}

				};
			}
		}

		return false;
	};

	/**
	 * Static method for generating unique names for elements.
	 *
	 * @param {String} prefix - A prefix to be used for generating the name.
	 * @param {String} suffix - A suffix to be used for generating the name.
	 * @return {String} The generated name.
	 */
	window.lvzwebdesktop.generateUniqueName = function ( prefix, suffix ) {
		var ret = '';
		var n = -1;
		var nel;
		if ( typeof prefix == 'undefined' ) prefix='';
		if ( typeof suffix == 'undefined' ) suffix='';
		while (true ) {
			n++;
			ret=prefix+n+suffix;
			nel = document.getElementsByName(ret);
			if ( nel.length == 0 ) return ret;
		}
	};

	/**
	 * List of controls that can be added inside a window in visual editor.
	 * @public
	 * @type {Array}
	 */
	window.lvzwebdesktop.windowVisualControls = [];

	/**
	 * Method for building the basic properties to elements of an lvzwebdesktop object.
	 *
	 * @functions lvzwebdesktop.createBasicElement
	 * @memberof lvzwebdesktopmainns
	 * @param {String} t - Element's tag name.
	 * @param {HTMLElement} [e] - The element if already exists.
	 * @param {Array} [c] - A list of classnames to add to the element.
	 */
	window.lvzwebdesktop.createBasicElement = function ( t, e, c ) {

		if ( typeof e == 'undefined' ) {
			if ( typeof lvzwebdesktop.removeElement.elements[t] != 'undefined'
				 && lvzwebdesktop.removeElement.elements[t].length > 0 ) {
				e = lvzwebdesktop.removeElement.elements[t].pop();
			}
			else {
				e = document.createElement(t);
			}
		}

		this.props.elementId = !e.id ? false : e.id;

		e.dataLVZWDESKTOPOBJ = this;
		e['data'+this.constructor.name] = this;

		if ( typeof c == 'object' && c instanceof Array )
			for ( var i = 0; i < c.length; i ++ )
				e.classList.add ( c[i] );

		return e;
	};

	/**
	 * Method for building the basic properties and method of an lvzwebdesktop object.
	 *
	 * @function lvzwebdesktop.basicObject
	 * @memberof lvzwebdesktopmainns
	 * @param {String} type - The type of the object.
	 * @example
	 * // Build constructor "ScrollbarObject" and add the basic properties:
	 * var ScrollbarObject = function ( e ) {
	 *  lvzwebdesktop.basicObject.call(this, 'scrollbar' );
	 * };
	 */
	window.lvzwebdesktop.basicObject = function ( type ) {

		var eobj = {
			"target" : this
		};
		lvzwebdesktop.fn.fireEvent.call(this.constructor,'load', eobj );

		this.constructor.list.push( this );

		/**
		 * Defines the type of the object.
		 * @public
		 * @type {String}
		 */
		this.type = type;

		/**
		 * Assciative array for object's properties.
		 * @public
		 * @type {Object}
		 */
		this.props = {};

		/**
		 * The ID of the object. If there is no ID then the value must be false.
		 * @public
		 * @type {String|false}
		 */
		this.props.elementId = false;

		/**
		 * CSS classname for the theme in use.
		 * @public
		 * @type {null|String}
		 */
		this.props.themeClass = null;

		/**
		 * Property to set the elements that have to set the theme class.
		 * @public
		 * @type {Array}
		 */
		this.props.setThemeToElements = [];

		/**
		 * Associative aray for the object elements class names.
		 * @public
		 * @type {Object}
		 */
		this.props.classNames = {};

		/**
		 * Associative array for storing object's elements.
		 * @public
		 * @type {Object}
		 */
		this.elements = {};

		/**
		 * Method for adding events to object.
		 *
		 * @param {string} e - Event name.
		 * @param {function} f - Function to run on event.
		 * @return {mixed} False on error, current object otherwise.
		 */
		this.addEvent = window.lvzwebdesktop.fn.addEvent;

		/**
		 * Method for removing events from object.
		 *
		 * @param {string} e - The name of the event to remove.
		 * @param {function} [f] - The function to remove. If this param is set then method will remove only the function that matches. Otherwise it will clear all functions from event.
		 * @return {mixed} False on error, current object otherwise.
		 */
		this.removeEvent = window.lvzwebdesktop.fn.removeEvent;

		/**
		 * Method for firing events.
		 *
		 * @param {string} e - The name of the event to fire.
		 * @param {object} obj - An object with properties to add to event object.
		 * @return {mixed} False on error, current object otherwise.
		 */
		this.fireEvent = window.lvzwebdesktop.fn.fireEvent;

		/**
		 * Returns the first parent object of given type.
		 *
		 * @param {function|string} type - lvzwebobject constructor or constructor's name.
		 * @return {object|null|false} Return's the first parent lvzwebobject that matches "type" parameter. if not found returns null. on error returns false.
		 */
		this.getParentOfType = function ( type ) {
			if ( typeof this.elements.container != 'undefined' )
				return window.lvzwebdesktop(this.elements.container,'').getParentObject( type );
			return null;
		};

		/**
		 * Method to "destruct" an object.
		 * @return {null} Returns null.
		 */
		this.destruct = function () {
			for ( var i in this.elements ) {
				this.elements[i].remove();
				delete this.elements[i];
			}
			var i = this.__proto__.constructor.list.indexOf(this);
			this.__proto__.constructor.list.splice(i,1);
			return null;
		};

		/**
		 * Method for setting the theme class to each element of the object that must have it.
		 * @param {String} [t] - The theme class-name to set.
		 * @return {object} The lvzwebobject.
		 */
		this.setTheme = function ( t ) {
			if ( typeof t == 'string' ) this.props.themeClass = t;
			if ( this.props.themeClass !== null )
				for ( var i = 0; i < this.props.setThemeToElements.length; i ++ )
					this.props.setThemeToElements[i].classList.add( this.props.themeClass );
			return this;
		};
	};

	/**
	 * Method for adding key parts at the end of creating an object.
	 */
	lvzwebdesktop.endBasicObject = function () {

		var attr;
		attr = getComputedStyle(document.body).getPropertyValue('--lvzwebdesktop-default-'+this.type+'-theme').trim();
		var tmptheme;
		var tmpclass;
		if ( attr != '' ) {
			for (var i=0; i<this.elements.container.classList.length; i ++ ) {
				tmpclass = this.elements.container.classList[i];
				tmptheme = getComputedStyle(document.body).getPropertyValue('--lvzwebdesktop-theme-'+this.type+'-'+tmpclass).trim()
				if ( tmptheme == 'true' ) {
					this.props.themeClass = tmpclass;
					break;
				}
			}
			if ( this.props.themeClass === null ) {
				// this.props.themeClass = attr;
				// this.elements.container.classList.add(attr);
				this.setTheme(attr);
			}
			else {
				this.setTheme();
			}
		}
		else {
			console.warn("There is no theme loaded for " + this.type );
		}

		this.fireEvent('ready',{
			"target" : this
		});
		lvzwebdesktop.fn.fireEvent.call(this.__proto__.constructor,'ready',{
			"target" : this
		});
	};

	/**
	 * Method for storing removed elements for reuse.
	 * @param {HTMLElement} e - The element to remove.
	 * @return {boolean} True on success, false otherwise.
	 */
	lvzwebdesktop.removeElement = function ( e ) {
		if ( ! e instanceof HTMLElement ) return false;
		var nnlc = e.nodeName.toLowerCase();
		if ( typeof lvzwebdesktop.removeElement.elements[nnlc] == 'undefined' )
			lvzwebdesktop.removeElement.elements[nnlc] = [];
		lvzwebdesktop.removeElement.elements[nnlc].push(e);
		e.remove();
		return true;
	};

	/**
	 * Property to store the removed elements by their node name.
	 * @public
	 * @type {Object}
	 */
	lvzwebdesktop.removeElement.elements = {};

	lvzwebdesktop.addMethods = function ( m ) {
		m = m.split(/,/);
		var mo = {};
		for ( var i=0;i<m.length;i++) mo[m[i].trim()]=true;

		if ( mo['attachTo'] === true )
			this.attachTo = window.lvzwebdesktop.fn.attachTo;
		if ( mo['setTop'] === true )
			this.setTop = window.lvzwebdesktop.fn.setTop;
		if ( mo['setLeft'] === true )
			this.setLeft = window.lvzwebdesktop.fn.setLeft;
		if ( mo['setWidth'] === true )
			this.setWidth = window.lvzwebdesktop.fn.setWidth;
		if ( mo['setHeight'] === true )
			this.setHeight = window.lvzwebdesktop.fn.setHeight;

		// if ( mo['detachFrom'] === true )
		// 	this.detachFrom = window.lvzwebdesktop.fn.detachFrom;
		// if ( mo['attachItem'] === true )
		// 	this.attachItem = window.lvzwebdesktop.fn.attachItem;
		// if ( mo['detachItem'] === true )
		// 	this.detachItem = window.lvzwebdesktop.fn.detachItem;
		// if ( mo['loadApp'] === true )
		// 	this.loadApp = window.lvzwebdesktop.fn.loadApp;
	};

	/**
	 * Method for getting a remote file or stream (asyncronus).
	 *
	 * @memberof lvzwebdesktopmainns
	 * @function lvzwebdesktop.getFile
	 * @param {string} file - URL of file or stream.
	 * @param {function} f - Function to run on getting data.
	 */
	window.lvzwebdesktop.getFile = getFile;

	/**
	 * Method for getting a remote file or stream (syncronus).
	 *
	 * @function lvzwebdesktop.getFileS
	 * @param {string} file - URL of file or stream.
	 * @return {string} The contents of file or stream.
	 */
	window.lvzwebdesktop.getFileS = getFileS;

	/**
	 * Checks if an element is between two points.
	 *
	 * @param {HTMLElement} el - the element to check.
	 * @param {number} x1 - the x position of the first point.
	 * @param {number} y1 - the y position of the first point.
	 * @param {number} x2 - the x position of the second point.
	 * @param {number} y2 = the y position of the second point.
	 * @return {boolean} True if the element, or part of it, is between the two points. False otherwise.
	 */
	window.lvzwebdesktop.isBetween = function(el,x1,x2,y1,y2) {
		var o=offset(el),th=o.top+el.offsetHeight,lw=o.left+el.offsetWidth;
		return((o.top>=y1&&o.top<y2)||(o.top<y1&&th>=y1))&&((o.left>=x1&&o.left<x2)||(o.left<x1&&lw>=x1));
	};

	/**
	 * Method for checking if an object has been constructed using an lvzwebdesktop constructor.
	 * @param {object} item - The item to check.
	 * @return {object|false} If "item" is member of an lvzwebdesktop constructor returns that constructor, if not it returns
	 */
	window.lvzwebdesktop.isPartOf = function ( item ) {
		if ( typeof item !== 'object' ) {
			return false;
		}

		for ( var i = 0; i < window.lvzwebdesktop.constructors.length; i ++ )
			if ( item instanceof window.lvzwebdesktop.constructors[i].constructor )
				return window.lvzwebdesktop.constructors[i].constructor;

		return false;
	};

	window.lvzwebdesktop.defaultTheme = 0;
	window.lvzwebdesktop.themes = [];

	/**
	 * Method for loading js files.
	 * @param {string} url - javascript file url, to load.
	 */
	window.lvzwebdesktop.loadjs = function ( url ) {
		var sc = document.createElement('script');
		sc.src = url;
		document.head.appendChild ( sc );
	}

	/**
	 * Method for loading css files.
	 * @param {string} url - css file url, to load.
	 */
	window.lvzwebdesktop.loadcss = function ( url ) {
		var ln = document.createElement('link');
		ln.setAttribute ('rel', 'stylesheet');
		ln.setAttribute ('type', 'text/css' );
		ln.setAttribute ('href', url );
		document.head.appendChild ( ln );
	}

	window.lvzwebdesktop.addThemeFile = function ( f ) {
		getFile( f , function ( contents ) {
			window.lvzwebdesktop.themes.push ( JSON.parse ( contents ) );
		});
	};

	window.lvzwebdesktop.addThemeString = function ( str ) {
		window.lvzwebdesktop.themes.push ( JSON.parse ( str ) );
	};

	window.lvzwebdesktop.constructors = [];

	// Actions after window has been loaded
	window.addEventListener ( 'load', function () {

		// document.addEventListener('keypress', desktopKeyPress);
		document.addEventListener('keyup', lvzwebdesktopKeyUp);
		document.addEventListener('keydown', lvzwebdesktopKeyDown);

		// Call each constructor.
		for ( var i = 0; i < window.lvzwebdesktop.constructors.length; i ++ )
			if ( typeof window.lvzwebdesktop.constructors[i].selector == 'string' ) {

				var elements = document.querySelectorAll(window.lvzwebdesktop.constructors[i].selector);
				for ( var j = 0 ; j < elements.length; j++ )
					if ( typeof elements[j].dataLVZWDESKTOPOBJ == 'undefined' )
						new window.lvzwebdesktop.constructors[i].constructor( elements[j] );

				window.lvzwebdesktop.constructors[i].loaded = true;

			}

		lvzwebdesktop.fn.fireEvent.call(lvzwebdesktop, 'constructorsready', {"target": lvzwebdesktop});

		lvzwebdesktop.constructorsReady = true; 

		// document.body.addEventListener('click',CheckClicks);
		document.addEventListener('mouseup',CheckClicks);

	} );

	window.lvzwebdesktop.constructorsReady = false;

	window.lvzwebdesktop.onclick = [];

	window.lvzwebdesktop.onconstructorsready = [];

	window.lvzwebdesktop.fn = {};

	window.lvzwebdesktop.fn.addEvent = function ( e, f ) {
		if ( typeof this['on'+e] == 'undefined') return false;
		for ( var i = 0; i < this['on'+e].length; i++ ) if ( this['on'+e][i] === f) return this;
		this['on'+e].push(f);
		return this;
	};

	window.lvzwebdesktop.fn.removeEvent = function ( e, f ) {
		if ( typeof this['on'+e] == 'undefined' ) return false;
		if ( typeof f == 'undefined' ) { this['on'+e]=[]; return this; }
		for ( var i = 0; i < this['on'+e].length; i++ )
			if ( this['on'+e][i] === f ) {
				this['on'+e].splice(i,1);
				return this;
			}
		return this;
	};

	window.lvzwebdesktop.fn.fireEvent = function ( e, obj ) {
		obj.type = e;
		obj.timeStamp = Date.now();
		obj.preventDefault = function () { obj.defaultPrevented = true; obj.preventDefault.a = true; };
		obj.defaultPrevented = false;
		obj.preventDefault.a = false;
		obj.stopPropagation = function () { obj.stopPropagation.a = true; };
		obj.stopPropagation.a = false;

		if ( typeof this['on'+e] == 'undefined') return false;
		for ( var i = 0; i < this['on'+e].length; i++ )
			if ( obj.stopPropagation.a == false )
				this['on'+e][i].call(this, obj);
		return this;
	};

	window.lvzwebdesktop.fn.attachTo = function (item, pos) {
		var lvzo = null;

		if ( typeof pos == 'undefined' || ! pos ) pos = this.props.defaultPosition;

		if ( item instanceof HTMLElement ) {
			if ( typeof item.dataLVZWDESKTOPOBJ == 'object' ) {
				lvzo = item.dataLVZWDESKTOPOBJ;
				if ( typeof lvzo.attachItem == 'function' ) {
					lvzo.attachItem(this,pos);
				}
				else {
					console.warn ( "Argument must be an lvzwebdesktop object having \"attachItem\" method or just an html element." );
					return this;
				}
			}
			else {
				var eventData = { "target" : this,
								  "attachElement" : item,
								  "item" : item,
								  "position" : pos
								};
				this.fireEvent('attachto',eventData);
				if ( eventData.defaultPrevented ) return this;
				item.appendChild(this.elements.container);
			}
		}
		else {
			var c = lvzwebdesktop.isPartOf(item);
			if ( c === false || typeof item.attachItem != 'function' ) {
				console.warn ( "Argument must be an lvzwebdesktop object having \"attachItem\" method or just an html element." );
				return this;
			}
			item.attachItem ( this, pos );
		}
		return this;
	};

	window.lvzwebdesktop.fn.visualeditorGetTop = function(self){return self.props.top;};

	window.lvzwebdesktop.fn.visualeditorSetTop = function(self,v){
		if ( typeof self.visualcontrol !== 'undefined' ){
			self.visualcontrol.top = v;
			if ( v !== false )
				self.visualcontrol.elements.container.style.top = v+'px';
			else
				self.visualcontrol.elements.container.style.top = '';
		}
		if ( typeof self.setTop == 'function' ) {
			self.setTop(v);
		}
		else {
			if ( v !== false ) {
				self.elements.container.style.top = v+'px';
			}
			else {
				self.elements.container.style.top = '';
			}
		}
	};

	window.lvzwebdesktop.fn.visualeditorGetLeft = function(self){return self.props.left;};

	window.lvzwebdesktop.fn.visualeditorSetLeft = function(self,v){
		if ( typeof self.visualcontrol !== 'undefined' ){
			self.visualcontrol.left = v;
			if ( v !== false )
				self.visualcontrol.elements.container.style.left = v+'px';
			else
				self.visualcontrol.elements.container.style.left = '';
		}
		if ( typeof self.setLeft == 'function' ) {
			self.setLeft(v);
		}
		else {
			if ( v !== false ) {
				self.elements.container.style.left = v+'px';
			}
			else {
				self.elements.container.style.left = '';
			}
		}
	};

	window.lvzwebdesktop.fn.visualeditorGetWidth = function(self){return self.props.width;};

	window.lvzwebdesktop.fn.visualeditorSetWidth = function(self,v){
		if ( typeof self.visualcontrol !== 'undefined' ){
			self.visualcontrol.width = v;
			if ( v !== false )
				self.visualcontrol.elements.container.style.width = v+'px';
			else
				self.visualcontrol.elements.container.style.width = '';
		}
		if ( typeof self.setWidth == 'function' ) {
			self.setWidth(v);
		}
		else {
			if ( v !== false ) {
				self.elements.container.style.width = v+'px';
			}
			else {
				self.elements.container.style.width = '';
			}
		}
	};

	window.lvzwebdesktop.fn.visualeditorGetHeight = function(self){return self.props.height;};

	window.lvzwebdesktop.fn.visualeditorSetHeight = function(self,v){
		if ( typeof self.visualcontrol !== 'undefined' ){
			self.visualcontrol.height = v;
			if ( v !== false )
				self.visualcontrol.elements.container.style.height = v+'px';
			else
				self.visualcontrol.elements.container.style.height = '';
		}
		if ( typeof self.setHeight == 'function' ) {
			self.setHeight(v);
		}
		else {
			if ( v !== false ) {
				self.elements.container.style.height = v+'px';
			}
			else {
				self.elements.container.style.height = '';
			}
		}
	};

	/**
	 * Method for setting the vertical position of the object.
	 * Fires the "move" event.
	 * @param {Number|false} v - The desired vertical position in
	 * number of pixels. If this is set to false then the position of
	 * the object will be set to static.
	 * @return {Object} This object.
	 */
	window.lvzwebdesktop.fn.setTop = function ( v ) {

		if ( v !== false ) {
			if ( typeof this.props.minTop != 'undefined'
				 && this.props.minTop !== false
				 && this.props.minTop > v
			   ) v = this.props.minTop;

			if ( typeof this.props.maxTop != 'undefined'
				 && this.props.maxTop !== false
				 && this.props.maxTop < v
			   ) v = this.props.maxTop;	
		}

		var eventData={"target" : this,
					   "oldTop" : this.props.top,
					   "newTop" : v,
					   "oldLeft" : this.props.left,
					   "newLeft" : this.props.left,
					   "currentTopPosition" : this.props.top,
					   "newTopPosition" : v,
					   "currentLeftPosition" : this.props.left,
					   "newLeftPosition" : this.props.left
					  };
		this.fireEvent('move',eventData);
		if ( eventData.defaultPrevented === true ) return this;

		if ( v === false ) {
			this.elements.container.style.position = '';
			this.props.top = false;
			this.props.top = false;
		}
		else {
			this.elements.container.style.position = 'absolute';
			this.elements.container.style.top = v+'px';
			this.props.top = v;
		}

		return this;
	};

	/**
	 * Method for setting the horizontal position of the object.
	 * Fires the "move" event.
	 * @param {Number|false} v - The desired horizontal position in
	 * number of pixels. If this is set to false then the position of
	 * the object will be set to static.
	 * @return {Object} This object.
	 */
	window.lvzwebdesktop.fn.setLeft = function ( v ) {

		if ( v !== false ) {
			if ( typeof this.props.minLeft != 'undefined'
				 && this.props.minLeft !== false
				 && this.props.minLeft > v
			   ) v = this.props.minLeft;

			if ( typeof this.props.maxLeft != 'undefined'
				 && this.props.maxLeft !== false
				 && this.props.maxLeft < v
			   ) v = this.props.maxLeft;	
		}

		var eventData={"target" : this,
					   "oldTop" : this.props.top,
					   "newTop" : this.props.top,
					   "oldLeft" : this.props.left,
					   "newLeft" : v,
					   "currentTopPosition" : this.props.top,
					   "newTopPosition" : this.props.top,
					   "currentLeftPosition" : this.props.left,
					   "newLeftPosition" : v
					  };
		this.fireEvent('move',eventData);
		if ( eventData.defaultPrevented === true ) return this;

		if ( v === false ) {
			this.elements.container.style.position = '';
			this.props.top = false;
			this.props.left = false;
		}
		else {
			this.elements.container.style.position = 'absolute';
			this.elements.container.style.left = v+'px';
			this.props.top = v;
		}

		return this;
	};

	/**
	 * Method for setting the width of the object.
	 * Fires the "resize" event.
	 * @public
	 * @param {Number} v - The desired width in number of pixels.
	 * @return {Object} This object.
	 */
	window.lvzwebdesktop.fn.setWidth = function ( v ) {
		if ( typeof this.props.minWidth != 'undefined'
				 && this.props.minWidth !== false
				 && this.props.minWidth > v
		   ) v = this.props.minWidth;
		if ( typeof this.props.maxWidth != 'undefined'
				 && this.props.maxWidth !== false
				 && this.props.maxWidth < v
			   ) v = this.props.maxWidth;

		var eventData = { "target" : this,
						  "oldWidth" : this.props.width,
						  "oldHeight" : this.props.height,
						  "newWidth" : v,
						  "newHeight" : this.props.height
						};
		this.fireEvent('resize',eventData);
		if ( eventData.defaultPrevented === true ) return this;
		this.props.width = v;
		this.elements.container.style.width = v + 'px';
		return this;
	};

	/**
	 * Method for setting the height of the object.
	 * Fires the "resize" event.
	 * @public
	 * @param {Number} v - The desired height in number of pixels.
	 * @return {Object} This object.
	 */
	window.lvzwebdesktop.fn.setHeight = function ( v ) {
		if ( typeof this.props.minHeight != 'undefined'
				 && this.props.minHeight !== false
				 && this.props.minHeight > v
		   ) v = this.props.minHeight;
		if ( typeof this.props.maxHeight != 'undefined'
				 && this.props.maxHeight !== false
				 && this.props.maxHeight < v
			   ) v = this.props.maxHeight;

		var eventData = { "target" : this,
						  "oldWidth" : this.props.width,
						  "oldHeight" : this.props.height,
						  "newWidth" : this.props.width,
						  "newHeight" : v
						};
		this.fireEvent('resize',eventData);
		if ( eventData.defaultPrevented === true ) return this;
		this.props.height = v;
		this.elements.container.style.height = v + 'px';
		return this;
	};

	/**
	 * Listens for clicks on document to report them on objects.
	 * @param {Event} e - Event object.
	 */
	function CheckClicks ( e ) {
		if ( CheckClicks.element !== null && CheckClicks.element === e.target ) return;

		window.lvzwebdesktop.fn.fireEvent.call(window.lvzwebdesktop, "click", {
			"target" : e.target
		});
	}

	CheckClicks.element = null;

	// Load scripts and css from this script's url
	if ( currentScriptURL.length == 2 ) {
		var currentPath = currentScriptURL[0];
		currentPath = currentPath.match(/^(.*[/])[^/]*$/);
		if ( currentPath === null )currentPath = './';
		else currentPath = currentPath[1];

		currentScriptURL = currentScriptURL[1].split(/[&]/);
		var currentURLArg;
		var items;
		for ( var i = 0; i < currentScriptURL.length; i ++ ) {
			currentURLArg = currentScriptURL[i].split(/=/);
			if ( ['loadjs','loadcss'].indexOf ( currentURLArg[0] ) > -1 ) {
				items = currentURLArg[1].split (/,/);
				for ( var j = 0; j < items.length; j ++ )
					window.lvzwebdesktop[currentURLArg[0]] ( currentPath + items[j] );
			}
		}
	}


	lvzwebdesktop.resizeObserver = new window.ResizeObserver ( function ( e ) {
		var self = e[0].target.dataLVZWDESKTOPOBJ;
		var eobj = {
			"target" : self,
			"oldWidth" : self.props.width,
			"oldHeight" : self.props.height,
			"newWidth" : self.elements.container.offsetWidth,
			"newHeight" : self.elements.container.offsetHeight
		};
		self.fireEvent('resize',eobj);
		if ( eobj.defaultPrevented === true ) {
			self.elements.container.style.height = self.props.height + 'px';
			self.elements.container.style.width = self.props.width + 'px';
		}
		else {
			self.props.height = self.elements.container.offsetHeight + 'px';
			self.props.width = self.elements.container.offsetWidth + 'px';
		}
	} );

	/**
	 * String of current emacs style key combination. (example: C-x )
	 * @public
	 * @type {string}
	 */
	var currentKeyComb = '';

	/**
	 * String of current emacs style multiple key combination (example: C-x 1 )
	 * @public
	 * @type {string}
	 */
	var currentMultipleKeyComb = '';

	/**
	 * Associative array with the currenty pressed keys.
	 * @public
	 * @type {object}
	 */
	var keyspressed = {
		"control" : false,
		"shift" : false,
		"alt" : false,
		"altg" : false,
		"escape" : false,
		"returnkey" : false,
		"super" : false,
		"character" : false,
		"spc" : false,
		"charkey" : false
	};

	/**
	 * Array for storing keycombinations that other objects setting.
	 * @public
	 * @type {array}
	 */
	var keycombs = [];

	/**
	 * Method for adding watch for a keycombination.
	 * @param {string} comb - Emacs like string for key combination.
	 * @param {function} runMethod - Method to run if key combination is captured.
	 */
	lvzwebdesktop.addKeyComb = function ( comb, runMethod, data ) {
		keycombs.push( {
			"combination" : comb,
			"method" : runMethod,
			"data" : data
		} );
	};

	/**
	 * Method for deleting all watches for the given key combination.
	 * @param {string} comb - Emacs like string for key combination.
	 * @return {DesktopObject} This desktop object.
	 */
	lvzwebdesktop.clearKeyComb = function ( comb ) {
		for ( var i = keycombs.length -1; i >= 0 ; i -- )
			if ( keycombs[i].combination == comb )
				keycombs.splice(i,1);
	};

	/**
	 * Array with methods that match exactly the current key combination.
	 * @type {array}
	 */
	var keycombmethodsf = [];

	/**
	 * Array with data for "keycombmethodsf" methods.
	 * @type {array}
	 */
	var keycombmethodss = [];

	/**
	 * Timer for key combinations.
	 * @type {number}
	 */
	var runkeycombtmr = false;

	/**
	 * Function for controling keyup events.
	 *
	 * @param {Event} e - Event data object.
	 */
	function lvzwebdesktopKeyUp ( e ) {
		var mkey = false;

		if ( e.key === "Control" ) {
			keyspressed.control = false;
			mkey = true;
		} else if ( e.key === "Shift" ) {
			keyspressed.shift = false;
			mkey = true;
		} else if ( e.key === "Alt" ) {
			keyspressed.alt = false;
			mkey = true;
		} else if ( e.key === "AltGraph" ) {
			keyspressed.altg = false;
			mkey = true;
		} else if ( e.key === " " )
			keyspressed.spc = false;
		else if ( e.key === "Enter" )
			keyspressed.returnkey = false;
		else if ( e.key === "Escape" )
			keyspressed.escape = false;
		else if ( /^[a-z0-9!@#$%\^&*()`~\[\]{};'\\:"|\-=_+,./<>?]$/.test(e.key) )
			keyspressed.charkey = false;

		if ( mkey ) return ;

		for ( var i = 0; i < keycombs.length; i ++ )
			if ( keycombs[i].combination != currentMultipleKeyComb &&
				 keycombs[i].combination.substr(0,currentMultipleKeyComb.length) == currentMultipleKeyComb
			   ) {
				if ( currentMultipleKeyComb == '' )
					currentMultipleKeyComb = currentKeyComb;
				else
					currentMultipleKeyComb += ' ' + currentKeyComb;
				return ;
			}

		currentKeyComb = buildKeyComb(keyspressed);
		currentMultipleKeyComb = '';
	}

	/**
	 * Function for controling keydown events.
	 *
	 * @param {Event} e - Event data object.
	 */
	function lvzwebdesktopKeyDown ( e ) {
		clearTimeout ( runkeycombtmr );
		var mkey = false;

		if ( e.key === "Control" ) {
			keyspressed.control = true;
			mkey = true;
		} else if ( e.key === "Shift" ) {
			keyspressed.shift = true;
			mkey = true;
		} else if ( e.key === "Alt" ) {
			keyspressed.alt = true;
			mkey = true;
		} else if ( e.key === "AltGraph" ) {
			keyspressed.altg = true;
			mkey = true;
		} else if ( e.key === " " )
			keyspressed.spc = true;
		else if ( e.key === "Enter" )
			keyspressed.returnkey = true;
		else if ( e.key === "Escape" )
			keyspressed.escape = true;
		else if ( /^[a-z0-9!@#$%\^&*()`~\[\]{};'\\:"|\-=_+,./<>?]$/.test(e.key) )
			keyspressed.charkey = e.key;

		if ( mkey ) return ;
		currentKeyComb = buildKeyComb(keyspressed);

		var ccombr = false;
		var possibleKComb = false;

		if ( ! currentMultipleKeyComb || currentMultipleKeyComb == '' ) currentMultipleKeyComb = currentKeyComb;
		else currentMultipleKeyComb += currentKeyComb;

		keycombmethodsf = [];
		keycombmethodss = [];

		for ( var i = 0; i < keycombs.length; i ++ )
			if ( keycombs[i].combination == currentMultipleKeyComb ) {
				keycombmethodsf.push( keycombs[i].method );
				keycombmethodss.push ( { "comb" : currentMultipleKeyComb, "data": keycombs[i].data  } );
				ccombr = true;
			} else if (  keycombs[i].combination.substr(0,currentMultipleKeyComb.length) == currentMultipleKeyComb ) {
				possibleKComb = true;
			}

		if ( ccombr ) {

			if ( ! possibleKComb ) {
				for ( i = 0; i < keycombmethodsf.length; i ++ )
					keycombmethodsf[i] ( currentMultipleKeyComb, keycombmethodss[i].data );
			}
			else {
				runkeycombtmr = setTimeout ( function () {
					clearTimeout ( runkeycombtmr );
					for ( var i = 0; i < keycombmethodsf.length; i ++ ) {
						keycombmethodsf[i] (  keycombmethodss[i].comb, keycombmethodss[i].data );

						currentKeyComb = '';
						currentMultipleKeyComb = '';
					}
					keycombmethodsf = [];
					keycombmethodss = [];

				}, 500 );
			}

		}

		currentKeyComb = '';
		if ( possibleKComb === false  )
			currentMultipleKeyComb = '';
	}

	/**
	 * Function for creating key combination strings from currently pressed keys.
	 * @param {string} keyspressed - The keys object.
	 * @return {string} Emacs style key combination string.
	 */
	function buildKeyComb ( keyspressed ) {
		 let keycomb = '';
		if ( keyspressed.control ) keycomb += '-C';
		if ( keyspressed.alt ||
			 keyspressed.altg
		   ) keycomb += '-M';
		if ( keyspressed.shift ) keycomb += '-S';
		if ( keyspressed.spc ) keycomb += '-SPC';
		else if ( keyspressed.returnkey ) keycomb += '-RET';
		else if ( keyspressed.escape ) keycomb += '-ESC';
		else if ( keyspressed.charkey !== false ) keycomb += '-' + keyspressed.charkey;
		return keycomb.substr(1);
	}

}
