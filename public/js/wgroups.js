{
	document.currentScript.dataLVZWLIB = true;

	/**
	 * Constructor for window group objects. This file must be loaded before "window.js".
	 *
	 * @constructor
	 * @param {HTMLElement} [e] - The constructor's container element.
	 */
	var WindowgroupObject = function ( e ) {

		/**
		 * Windowgroup object id.
		 * @type {number}
		 * @public
		 */
		this.id = WindowgroupObject.list.push(this) - 1;

		/**
		 * Defines the type of the object.
		 * @type {string}
		 * @public
		 */
		this.type = 'windowgroup';

		/**
		 * Associative array for windowgroup properties.
		 * @type {object}
		 * @public
		 */
		this.props = {};

		/**
		 * The id of the container element.
		 * @type {false|string}
		 * @public
		 */
		this.props.elementId = false;

		/**
		 * Sets if should move all windows of the group by moving one of them.
		 * @type {boolean}
		 * @public
		 */
		this.props.moveAllWindowsTogether = true;

		/**
		 * Key combination for adding focused window to group.
		 * @type {false|string}
		 * @public
		 */
		this.props.addwindkey = false;

		/**
		 * Key combination for removing the focused window from group.
		 * @type {false|string}
		 * @public
		 */
		this.props.removewindkey = false;

		/**
		 * Set a shadow for windows of this group. The shadow will be added to each window's shadow.
		 * @type {false|string}
		 * @public
		 */
		this.props.shadow = false;

		/**
		 * Associative array for windowgroup elements class names.
		 * @type {object}
		 * @public
		 */
		this.props.classNames = {};

		/**
		 * CSS class-name for window group container element.
		 * @type {string}
		 * @public
		 */
		this.props.classNames.container = WindowgroupObject.classNames.container;

		/**
		 * Array for storing event functions for windowgroup add member event.
		 * @type {Array}
		 * @public
		 */
		this.onmemberadd = [];

		/**
		 * Array for storing event functions for windowgroup remove member event.
		 * @type {Array}
		 * @public
		 */
		this.onmemberremove = [];

		/**
		 * Desktop object that windowgroup belongs to.
		 * @type {null|DesktopObject}
		 * @public
		 */
		this.desktop = null;

		/**
		 * List of windows belong to this group.
		 * @type {array}
		 * @public
		 */
		this.windows = [];

		/**
		 * List of events of windows members of this group.
		 * @type {array}
		 * @public
		 */
		this.wevents = [];

		/**
		 * Copy events from a member object.
		 * @private
		 * @param {WindowObject} member - Window object, member of this group that will copy events from.
		 * @param {array} eventList - array of strings, names of the events to copy.
		 * @return {object} Associative array with lists of the copied events.
		 */
		function copyEvents ( member, eventList ) {
			var ret = {};
			for ( var i = 0; i < eventList.length; i ++ ) {
				ret [ eventList [ i ] ] = [];
				for ( var j = 0; j < member[ eventList [ i ] ].length; j ++ )
					ret [ eventList [ i ] ].push ( member[ eventList [ i ] ][j] );
			}

			return ret;
		}

		/**
		 * Method for adding events to window group.
		 *
		 * @param {string} e - Event name.
		 * @param {function} f - Function to run on event.
		 * @return {mixed} False on error, current window group object otherwise.
		 */
		this.addEvent = window.lvzwebdesktop.fn.addEvent;

		/**
		 * Method for removing events from window group.
		 *
		 * @param {string} e - The name of the event to remove.
		 * @param {function} [f] - The function to remove. If this param is set then method will remove only the function that matches. Otherwise it will clear all functions from event.
		 * @return {mixed} False on error, current window group object otherwise.
		 */
		this.removeEvent = window.lvzwebdesktop.fn.removeEvent;

		/**
		 * Method for firing events.
		 *
		 * @param {string} e - The name of the event to fire.
		 * @param {object} obj - An object with properties to add to event object.
		 * @return {mixed} False on error, current window group object otherwise.
		 */
		this.fireEvent = window.lvzwebdesktop.fn.fireEvent;

		/**
		 * Method for adding windows to group.
		 * @public
		 * @param {WindowObject} w - Window object to add to this group.
		 * @return {WindowgroupObject|false} Current window group object or false on error.
		 */
		this.addWindow = function ( w ) {
			if ( ! w instanceof lvzwebdesktop.WindowObject ) {
				console.error ( "Argument must be window object." );
				return false;
			}

			// If window already member of this group just exit method
			if ( this.windows.indexOf ( w ) > -1 ) return this;

			// If window is member of another group, remove it
			if ( typeof w.props.group == 'object' &&
				 w.props.group instanceof WindowgroupObject
			   ) w.props.group.removeWindow ( w );

			this.wevents.push ( copyEvents ( w, ['onmove','onclose'] ) );

			if ( typeof this.props.shadow == 'string' ) {
				w.props.oldShadow = w.elements.container.style.boxShadow;
				w.elements.container.style.boxShadow = this.props.shadow;
			}

			w.props.rtop = w.props.top;
			w.props.rleft = w.props.left;

			this.windows.push ( w );
			w
				.addEvent ('move', WindowGroupMove )
				.addEvent ('close', function ( e ) {
					var self = e.target.props.group;
					self.runMemberEvents(self.windows.indexOf(e.target), 'close', e );
					self.removeWindow(e.target);
				});

			w.props.group = this;

			return this.fireEvent('memberadd', {
				"target" : w
			});
		};

		/**
		 * Method for removing windows from group.
		 * @public
		 * @param {WindowObject|Number} w - Window object or member order nubmer to remove.
		 * @return {WindowgroupObject|false} Current window group object or false on error.
		 */
		this.removeWindow = function ( w ) {
			var spos = 0;
			if ( w instanceof lvzwebdesktop.WindowObject ) {
				spos = this.windows.indexOf(w);
				if ( spos == -1 ) {
					console.error ( "Not member of this group.");
					return false;
				}
			}
			else if ( typeof w == 'number' || w instanceof Number ) {
				if ( w < 0 || w >= this.windows.length ) {
					console.error ( "Out of range" );
					return false;
				}
				spos=w;
			}
			else {
				console.error ( "Not a window object" );
				return false;
			}

			var w = this.windows[spos];

			if ( typeof w.props.oldShadow == 'string' ) w.elements.container.style.boxShadow = w.props.oldShadow;

			delete w.props.rtop;
			delete w.props.rleft;

			w.onmove = [];
			for ( var i = 0; i < this.wevents[spos].onmove.length; i ++ )
				w.onmove.push ( this.wevents[spos].onmove[i] );

			w.onclose = [];
			for ( var i = 0; i < this.wevents[spos].onclose.length; i ++ )
				w.onclose.push ( this.wevents[spos].onclose[i] );

			w.props.group = null;

			this.windows.splice(spos,1);
			this.wevents.splice(spos,1);

			return this.fireEvent('memberremove', {
				"target" : w
			});
		};

		/**
		 * Method for running methods of group members.
		 * @param {string} m - Method name to run.
		 * @param {...*} [p] - Method arguments.
		 * @return {WindowgroupObject} Current window group object or false on error.
		 */
		this.runMethod = function ( m, ...p ) {
			if ( this.windows.length == 0 ) return this;
			if ( typeof this.windows[0][m] != 'function' ) {
				console.error ( "Method \""+ m +"\" does not exists" );
				return false;
			}

			for ( var i = 0; i < this.windows.length; i ++ )
				this.windows[i][m].apply ( this.windows[i], p );

			return this;
		};

		/**
		 * Method for setting properties of group members.
		 * @param {string} p - Property name.
		 * @param {any} v - Property value.
		 * @return {WindowgroupObject} Current window group object or false on error.
		 */
		this.setProperty = function ( p, v ) {
			if ( this.windows.length == 0 ) return this;
			if ( typeof this.windows[0][p] == 'function' ||
				 typeof this.windows[0][p] == 'undefined' ) {
				console.error ( "\""+p+"\" Does not exists or not a property." );
				return false;
			}

			for ( var i = 0; i < this.windows.length; i ++ )
				this.windows[i][p] = v;

			return this;
		};

		/**
		 * Method for running events on members.
		 * @param {number} mpos - Order of the member ( can be found using: this.widnows.indexOf (member) )
		 * @param {string} e - Event name.
		 * @param {object} obj - Event data object.
		 * @return {WindowgroupObject|false} Current window group object or false on error.
		 */
		this.runMemberEvents = function ( mpos, e, obj ) {
			if ( mpos < 0 || mpos >= this.wevents.length ) {
				console.error ( "Out of range" );
				return false;
			}
			for ( var i = 0; i < this.wevents[mpos].length; i ++ ) this.wevents[mpos][i].call(this.windows[mpos],e,obj);
			return this;
		};

		/**
		 * Returns the first parent object of given type.
		 *
		 * @param {function|string|undefined} [type] - lvzwebobject constructor or constructor's name. If it's undefined (not set), the method will return the first parent lvzwebdesktop object.
		 * @return {object|null|false} Return's the first parent lvzwebobject that matches "type" parameter. if not found returns null. on error returns false.
		 */
		this.getParentOfType = function ( type ) { return window.lvzwebdesktop(this.elements.container,'').getParentObject( type ); };

		/**
		 * Associative array for storing windowgroup html elements.
		 * @type {object}
		 * @public
		 */
		this.elements = {};

		/**
		 * Windowgroup's container element.
		 * @type {HTMLElement}
		 * @public
		 */
		this.elements.container = null;
		if ( typeof e == 'undefined' || e === false ) this.elements.container = document.createElement('div'); else this.elements.container = e;
		this.elements.container.classList.add ( this.props.classNames.container );
		this.elements.container.dataWindowgroupObject = this;
		this.elements.container.dataLVZWDESKTOPOBJ = this;
		this.props.elementId = !e.id ? false : e.id;

		if ( e instanceof HTMLElement ) {

			var attr;

			attr = this.elements.container.getAttribute('data-move-all-windows-together');
			if ( attr == 'false' ) this.props.moveAllWindowsTogether = parseFloat(attr);

			attr = this.elements.container.getAttribute('shadow');
			if ( attr !== null ) this.props.shadow = attr;


			this.desktop = this.getParentOfType('DesktopObject');

			attr = this.elements.container.getAttribute('add-focused-window-keycomb');
			if ( attr !== null && this.desktop && this.desktop !== null ) {
				this.props.addwindkey = attr;
				// this.desktop.addKeyComb( attr, addFWindK);
				lvzwebdesktop.addKeyComb( attr, addFWindK, this);
			}

			attr = this.elements.container.getAttribute('remove-focused-window-keycomb');
			if ( attr !== null && this.desktop && this.desktop !== null ) {
				if ( removeFWindKSet === false ) {
					removeFWindKSet = attr;
					this.props.removewindkey = attr;
					// this.desktop.addKeyComb( attr, removeFWindK);
					lvzwebdesktop.addKeyComb( attr, removeFWindK, this);
				}
			}

		}

	};

	/**
	 * List of all windowgroup objects for easy access.
	 * @type {array}
	 * @public
	 */
	WindowgroupObject.list = [];

	/**
	 * Default CSS class-names for all window groups.
	 * @type {object}
	 * @public
	 */
	WindowgroupObject.classNames = {};

	/**
	 * Default CSS class-name for window group's container element.
	 * @type {string}
	 * @public
	 */
	WindowgroupObject.classNames.container = 'web-desktop-windowgroup';

	/**
	 * Run when a member of a window group has been moved.
	 * @param {Event} e - Event data.
	 */
	function WindowGroupMove ( e ) {
		var self = e.target.props.group;
		e.target.props.rtop = e.currentPosition.top;
		e.target.props.rleft = e.currentPosition.left;

		var ct = e.currentPosition.top - e.previousPosition.top;
		var cl = e.currentPosition.left - e.previousPosition.left;
		// if ( e.target.props.stickyLeft !== false ) {
		// 	console.log ( e.target.props.stickyLeft );
		// 	var cl = e.currentPosition.left - e.target.props.stickyLeft;
		// } else {
		// 	var cl = e.move.left;
		// }
		if ( self.props.moveAllWindowsTogether === true )
			for ( var i = 0; i < self.windows.length; i ++ )
				if ( self.windows[i] !== e.target ) {
					self.windows[i].removeEvent ('move', WindowGroupMove );
					self.windows[i].props.rtop += ct;
					self.windows[i].props.rleft += cl;
					self.windows[i].props.top = self.windows[i].props.rtop;
					self.windows[i].props.left = self.windows[i].props.rleft;
					self.windows[i].position();
					self.windows[i].elements.container.style.top = self.windows[i].props.top + 'px';
					self.windows[i].elements.container.style.left = self.windows[i].props.left + 'px';
					self.windows[i].addEvent ('move', WindowGroupMove );
					self.runMemberEvents(i, 'move', e );
				}
	}

	/**
	 * Function to run when key combination for adding window to group is pressed.
	 * @param {string} c - Key combination pressed.
	 */
	function addFWindK ( c, d ) {
		self = d.desktop;
		if ( !self || ! self.focusedObject || self.focusedObject === null || self.focusedObject.type != 'window' ) return;

		for ( var i = 0; i < WindowgroupObject.list.length; i ++ )
			if (WindowgroupObject.list[i].props.addwindkey == c ) {
				WindowgroupObject.list[i].addWindow ( self.focusedObject );
				return;
			}
	}

	/**
	 * False if there is no keycomb for removing windows from group. String of key combination otherwise.
	 * @type {false|string}
	 */
	var removeFWindKSet = false;

	/**
	 * Function to run when key combination for removing window from group is pressed.
	 * @param {string} c - Key combination pressed.
	 */
	function removeFWindK ( c, d ) {
		self = d.desktop;
		if ( ! self ||
			 ! self.focusedObject ||
			 self.focusedObject === null ||
			 self.focusedObject.type != 'window' ||
			 ( typeof self.focusedObject.props.group == 'undefined' ||
			   self.focusedObject.props.group === false ||
			   self.focusedObject.props.group === null
			 )
		   ) return;
		self.focusedObject.props.group.removeWindow( self.focusedObject );
	}

	window.lvzwebdesktop.WindowgroupObject = WindowgroupObject;
	window.lvzwebdesktop.constructors.push({
		"selector" : '.'+WindowgroupObject.classNames.container,
		"constructor" : WindowgroupObject
	});

}
