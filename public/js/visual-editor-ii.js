{
	document.currentScript.dataLVZWLIB = true;

	/**
	 * Constructor for creating visual window editor. 
	 * @constructor
	 * @param {HTMLElement} [e] - Element to attach editor to.
	 */
	var VisualeditorObject = function ( e ) {

		lvzwebdesktop.basicObject.call(this,'visualeditor');

		this.addControl = function ( c ) {
			var control;
			var vcontrol;

			if ( c instanceof VisualcontrolObject ) {
				vcontrol = c;
				control = vcontrol.control;
			}
			else if ( w instanceof lvzwebdesktop.WindowObject ) {
				vcontrol = control = w;
			}
			else if ( lvzwebdesktop(c) !== false ) {
				control = c;
				if ( typeof control.visualcontrol == 'undefined' ) {
					vcontrol = new VisualcontrolObject(control);
				}
				else {
					vcontrol = control.visualcontrol;
				}
			}
			else {
				console.warn("Argument must be a visualcontrol, or an lvzwebdesktop control." );
				return this;
			}

			if ( this.controls.indexOf ( vcontrol ) > -1 ) {
				console.log("Control already exists");
			}

			this.controls.push(vcontrol);

			this.controllist.addItem(
				c.constructor.name,
				this.controllist.countItems()
			);
			return this;
		};

		this.removeControl = function ( c ) {
			var control;
			var vcontrol;

			if ( c instanceof VisualcontrolObject ) {
				vcontrol = c;
				control = vcontrol.control;
			}
			else if ( w instanceof lvzwebdesktop.WindowObject ) {
				vcontrol = control = w;
			}
			else if ( lvzwebdesktop(c) !== false ) {
				control = c;
				if ( typeof control.visualcontrol == 'undefined' ) {
					vcontrol = new VisualcontrolObject(control);
				}
				else {
					vcontrol = control.visualcontrol;
				}
			}
			else {
				console.warn("Argument must be a visualcontrol, or an lvzwebdesktop control." );
				return this;
			}

			var pos = this.controls.indexOf(vcontrol);

			if ( pos == -1 ) {
				console.warn("Not in visualeditor");
				return this;
			}

			this.controllist.removeItem( pos );

			return this;
		};

		this.buildToolbar = function ( toolbar ) {
			for ( var i = 0; i < lvzwebdesktop.windowVisualControls.length; i++ ) {
				var controlbtn = new lvzwebdesktop.ButtonObject();
				controlbtn.addEvent('click', visualeditoraddcontrol );
				controlbtn.elements.container.classList.add('wd-toolbar-square-button');
				controlbtn.props.constructor = lvzwebdesktop.windowVisualControls[i].constructor;
				controlbtn.visualeditor = this;
				controlbtn.editorwindow = this.window;

				if ( typeof lvzwebdesktop.windowVisualControls[i].name == 'string' )
					controlbtn.elements.container.title = lvzwebdesktop.windowVisualControls[i].name;

				if ( typeof lvzwebdesktop.windowVisualControls[i].icon == 'string' ) {
					controlbtn.elements.container.style.backgroundImage = "url('"+lvzwebdesktop.windowVisualControls[i].icon+"')";
					controlbtn.elements.container.style.height = window.lvzwebdesktop.windowVisualControls[i].iconh + 'px';
					controlbtn.elements.container.style.width  = window.lvzwebdesktop.windowVisualControls[i].iconw + 'px';
				}

				if ( typeof lvzwebdesktop.windowVisualControls[i].init == 'function' )
					lvzwebdesktop.windowVisualControls[i].init(controlbtn);

				toolbar.addItem(controlbtn);
			}

			return this;
		};

		this.buildPropertiesTable = function () {

			this.properties = (new TableObject())
				.addColumn()
				.addColumn()
			;

			this.properties.elements.container.style.width = this.props.propertiesWidth + 'px';

			var cell;
			for ( var i=0;i<100;i++ ) this.properties.addRow();

			for ( var i=0;i<100;i++ ) {
				cell = this.properties.getCell(i,1);
				cell.props.editable = true;
				cell.addEvent('captionchange',function(e){
					if( typeof e.target.controlprops == 'undefined' ) return;
					if( typeof e.target.controlprops.setValue == 'function' ) {
						if ( typeof e.target.vcontrol == 'undefined' ) return;
						e.target.controlprops.setValue(
							e.target.vcontrol,
							e.target.getValue()
						);
					}
				});

				// cell.elements.container.addEventListener('keydown', visualcontrolKD );
				// cell.elements.listselect.addEventListener('change', visualcontrolCH );
				this.properties.rows[i].style.display = 'none';
			}

			return this;
		};

		this.hideMenubar = function () {
			this.menubar.elements.container.style.display = 'none';
			this.props.showMenu = false;
			return this;
		};

		this.showMenubar = function () {
			this.menubar.elements.container.style.display = '';
			this.props.showMenu = true;
			return this;
		};

		this.hideToolbar = function () {
			this.toolbar.elements.container.style.display = 'none';
			this.props.showToolbar = false;
			return this;
		};

		this.showToolbar = function () {
			this.toolbar.elements.container.style.display = '';
			this.props.showToolbar = true;
			return this;
		};

		this.close = function () {
			this.window.close();
			return this;
		};

		this.controls = [];

		this.libraries = [];

		if ( lvzwebdesktop.constructorsReady === true ) {
			VisualeditorObject.loadLibraries();
		}
		else {
			lvzwebdesktop.fn.addEvent.call(lvzwebdesktop,
										   'constructorsready',
										   VisualeditorObject.loadLibraries
										  );
		}

		this.librariesPath = '';

		/***********************\
		 ** Object Properties **
		\***********************/
		// Set menu properties
		this.props.showMenu = true;
		this.props.menuPosition = 'top-top';

		// Set toolbar properties
		this.props.showToolbar = true;
		this.props.toolbarPosition = 'top';
		this.props.toolbarWidth = '200px';

		// Set statusbar properties
		this.props.showStatusbar = true;
		this.props.statusbarPosition = 'bottom';

		// Set control listbox properties
		this.props.showControlList = true;
		this.props.controlListPosition = 'left';
		this.props.controlListHeight = '20px';

		// Set properties table properties
		this.props.showProperties = true;
		this.props.propertiesPosition = 'left';
		this.props.propertiesWidth = 200;

		/*****************\
		 ** Class names **
		\*****************/
		this.props.classNames.container = 'visual-editor';

		/*********************\
		 ** Editor Elements **
		\*********************/
		this.elements.container = lvzwebdesktop.createBasicElement.call(this,'div',undefined,[
			this.props.classNames.container
		]);

		/*************\
		 ** Objects **
		\*************/
		this.window = lvzwebdesktop(e,false).getParentObject('WindowObject');
		this.window.visualEditor = this;

		this.editedObject = null;

		// Build tabs
		this.tabbar = new lvzwebdesktop.TabbarObject();

		var tmpmi;
		var tmpsm;

		// #### Build menubar ####
		this.menubar = new lvzwebdesktop.MenubarObject();
		this.menubar.visualeditor = this;

		// File menu
		tmpmi = this.menubar.createMenuItem('File');
		this.menubar.addMenuItem(tmpmi);
		// Submenu of File menu
		tmpsm = (new lvzwebdesktop.MenuObject())
			.setTheme('test')
			.addSeparator()
			.addItem('New file',function(e){
			})
			.addItem('Remove file',function(e){
			})
			.addItem('Close file',function(e){
			})
			.addSeparator()
			.addItem('Exit',function(e){ lvzwebdesktop(e.target).visualeditor.close(); })
		;
		tmpsm.visualeditor = this;
		tmpsm.windoweditor = this.window;
		this.menubar.addSubmenu(tmpsm,tmpmi);

		// Edit menu
		tmpmi = this.menubar.createMenuItem('Edit');
		this.menubar.addMenuItem(tmpmi);
		// Submenu of Edit menu
		tmpsm = new lvzwebdesktop.MenuObject();
		tmpsm
			.setTheme('test')
			.addSeparator()
			.addItem('Properties',function(e){
				console.log (e );
			})
		;
		tmpsm.visualeditor = this;
		tmpsm.windoweditor = this.window;
		this.menubar.addSubmenu(tmpsm,tmpmi);

		// View menu
		tmpmi = this.menubar.createMenuItem('View');
		this.menubar.addMenuItem(tmpmi);
		// Submenu of View menu
		tmpsm = (new lvzwebdesktop.MenuObject())
			.setTheme('test')
			.addCheckItem('Show toolbar',
						  this.props.showToolbar,
						  function(e){
							  var self = lvzwebdesktop(e.target);
							  var mi = self.getMenuItem(e.target);
							  var checked = mi.element.querySelector('input[type=checkbox]').checked;
							  var editorWindow = self.menubar.getParentOfType('WindowObject');
							  var editor = editorWindow.visualEditor;
							  editor.props.showToolbar=checked;
							  editor.toolbar.elements.container.style.display = checked ? '' : 'none';
							  editorWindow.resize();
						  })
			.addCheckItem('Show statusbar',
						  this.props.showStatusbar,
						  function(e){
							  var self = lvzwebdesktop(e.target);
							  var mi = self.getMenuItem(e.target);
							  var checked = mi.element.querySelector('input[type=checkbox]').checked;
							  var editorWindow = self.menubar.getParentOfType('WindowObject');
							  var editor = editorWindow.visualEditor;
							  editor.props.showStatusbar=checked;
							  editor.statusbar.elements.container.style.display = checked ? '' : 'none';
							  editorWindow.resize();
						  })
			.addCheckItem('Show properties',
						  this.props.showProperties,
						  function (e){
							  var self = lvzwebdesktop(e.target);
							  var mi = self.getMenuItem(e.target);
							  var checked = mi.element.querySelector('input[type=checkbox]').checked;
							  var editorWindow = self.menubar.getParentOfType('WindowObject');
							  var editor = editorWindow.visualEditor;
							  editor.props.showProperties = checked;
							  editor.elements.propertiescontainer.style.display = checked ? '' : 'none';
							  editor.elements.resizeProperties.style.display = checked ? '' : 'none';
							  editorWindow.resize();
						  })
			.addSeparator()
			.addItem('Get HTML code', function ( e ) {
				console.log ( e );
			})
			.addItem('Get Python3 GTK code', function ( e ) {
				console.log ( e );
			})
		;
		tmpsm.visualeditor = this;
		tmpsm.windoweditor = this.window;
		this.menubar.addSubmenu(tmpsm,tmpmi);

		// Project menu
		tmpmi = this.menubar.createMenuItem('Project');
		this.menubar.addMenuItem(tmpmi);
		// Submenu of Project menu
		tmpsm = (new lvzwebdesktop.MenuObject())
			.setTheme('test')
			.addItem('New Project',function(e){
			})
			.addItem('Open Project',function(e){
			})
			.addItem('Save Project',function(e){
			})
			.disableItem()
			.addItem('Close Project',function(e){
			})
			.addSeparator()
			.addItem('Project Properties',function(e){
			})
		;
		tmpsm.visualeditor = this;
		tmpsm.windoweditor = this.window;
		this.menubar.addSubmenu(tmpsm,tmpmi);

		// Controls menu
		tmpmi = this.menubar.createMenuItem('Controls');
		this.menubar.addMenuItem(tmpmi);
		tmpsm = (new lvzwebdesktop.MenuObject())
			.setTheme('test')
			.addItem('Delete Item',function(e){})
			.disableItem()
		;
		tmpsm.visualteditor = this;
		tmpsm.windoweditor = this.window;
		this.menubar.addSubmenu(tmpsm,tmpmi);

		// Help menu
		tmpmi = this.menubar.createMenuItem('Help');
		tmpmi.style.cssFloat = 'right';
		this.menubar.addMenuItem(tmpmi);
		// Submenu of Help menu
		tmpsm = (new lvzwebdesktop.MenuObject())
			.setTheme('test')
			.addItem('Help about visual editor',function(e){
			})
			.addSeparator()
			.addItem('About',function(e){
			})
		;
		tmpsm.visualeditor = this;
		tmpsm.windoweditor = this.window;
		this.menubar.addSubmenu(tmpsm,tmpmi);

		// Add toolbar
		this.toolbar = new lvzwebdesktop.ToolbarObject();
		this.toolbar.elements.container.classList.add('test');
		this.buildToolbar(this.toolbar);

		// Add statusbar
		this.statusbar = new lvzwebdesktop.StatusbarObject();
		this.statusbar.setTheme('test');

		// Add control listbox
		this.controllist = new lvzwebdesktop.ListboxObject();

		// Add properties table
		this.buildPropertiesTable();

		this.elements.propertiescontainer = lvzwebdesktop.createBasicElement.call(this,'div');
		this.elements.propertiescontainer.style.overflow = 'hidden';
		this.elements.propertiescontainer.style.cssFloat = 'left';

		// element to resize properties table
		this.elements.resizeProperties = lvzwebdesktop.createBasicElement.call(this,'div');
		this.elements.resizeProperties.style.width = '10px';
		this.elements.resizeProperties.style.height = '100%';
		this.elements.resizeProperties.style.backgroundColor = '#aaa';
		this.elements.resizeProperties.style.cssFloat = 'right';
		this.elements.resizeProperties.addEventListener('mousedown',visualeditorpropresizeMD);

		// Add menubar, toolbar and properties table to editor window if exists
		if ( this.window && this.window !== null ) {
			this.window.elements.bodycontainer.style.position = 'relative';


			/*****************\
			 ** Add tab bar **
			\*****************/
			this.window.attachItem(this.tabbar,'center');

			this.tabbar.elements.container.style.width = '100%';
			this.tabbar.elements.container.style.height = '100%';
			this.tabbar.elements.bodycontainer.style.width = '100%';
			this.tabbar.elements.bodycontainer.style.height = '100%';
			this.tabbar
				.addTab()
				.setTabLabel("Window editor")
				.activeTabBody.appendChild(e)
			;

			/***************************\
			 ** Add menubar to window **
			\***************************/
			this.menubar.attachTo(this.window, this.props.menuPosition);
			this.menubar.windoweditor = this.window;

			/***************************\
			 ** Add toolbar to window **
			\***************************/
			if ( this.props.toolbarPosition == 'right' || this.props.toolbarPosition == 'left' ) {
				this.toolbar.elements.container.style.width = this.props.toolbarWidth;
			}
			this.toolbar.attachTo(this.window, this.props.toolbarPosition);
			this.toolbar.windowEditor = this.window;

			// Add statusbar to window
			this.statusbar.attachTo(this.window, this.props.statusbarPosition);
			this.statusbar.windowEditor = this.window;

			// Add control listbox
			this.elements.propertiescontainer.appendChild(this.controllist.elements.container);

			// Add properties table
			this.elements.pcont = lvzwebdesktop.createBasicElement.call(this,'div');
			this.elements.pcont.appendChild(this.properties.elements.container);
			this.elements.pcont.style.height='calc(100% - '+this.props.controlListHeight+')';
			this.elements.pcont.style.overflow = 'scroll';
			this.elements.propertiescontainer.appendChild(this.elements.pcont);

			if ( this.props.propertiesPosition == 'left' ) {
				this.window.attachItem(this.elements.propertiescontainer,this.props.propertiesPosition);

				this.window.attachItem(this.elements.resizeProperties,'left');
			}
			else if ( this.props.propertiesPosition == 'right' ) {
				this.window.attachItem(this.elements.resizeProperties,'right');
				this.window.attachItem(this.elements.propertiescontainer,this.props.propertiesPosition);
			}
		}

		if ( this.props.showMenubar === false ) this.hideMenubar();
		if ( this.props.showToolbar === false ) this.hideToolbar();

	};

	/**
	 * List of all VisualeditorObject instances.
	 * @public
	 */
	VisualeditorObject.list = [];

	VisualeditorObject.loadLibraries = function () {
		var self = VisualeditorObject.loadLibraries.currentScript;
		var scripttags = document.getElementsByTagName('script');
		var linktags = document.getElementsByTagName('link');
		var tmpsrc = '';
		var tmppath = '';
		var ioq;
		var lios;

		// Get loaded libraries
		var libraries = [];
		var libpath = '';
		for (var i=0;i<scripttags.length;i++) {
			tmpsrc = scripttags[i].src;
			ioq= tmpsrc.indexOf('?');
			if ( ioq>-1 ) tmpsrc=tmpsrc.substr(0,ioq);
			lios=tmpsrc.lastIndexOf('/')+1;
			libpath = tmpsrc.substr(0,lios);
			tmpsrc = tmpsrc.substr(lios).split(/\./)[0];
			if ( tmpsrc !== 'lvzwebdesktop' && scripttags[i] !== self )  {
				libraries.push(tmpsrc);
			}
		}

		// Get loaded css files
		var cssFiles=[];
		var cssPath = '';
		for (var i=0;i<linktags.length;i++) if ( linktags[i].type == 'text/css' ) {
			
			tmpsrc = linktags[i].href;
			lios=tmpsrc.lastIndexOf('/')+1;
			tmppath = tmpsrc.substr(0,lios);
			if ( tmpsrc.substr(lios) == 'main.css' ) {
				cssPath = tmppath;
			}
			cssFiles.push(tmpsrc);

		}
		cssFiles = cssFiles.map( x=> x.substr(cssPath.length) );

		// Add libraries to visual editor objects.
		for (var i=0;i<VisualeditorObject.list.length;i++) {
			VisualeditorObject.list[i].libraries = libraries.map((x)=>x);
			VisualeditorObject.list[i].librariesPath = libpath;

			VisualeditorObject.list[i].cssFiles = cssFiles.map((x)=>x);
			VisualeditorObject.list[i].cssPath = cssPath;
		}

	};
	VisualeditorObject.loadLibraries.currentScript = document.currentScript;

	function visualeditoraddcontrol ( e ) {
	}

	/**
	 * Function begin resizing the properties table on mouse move.
	 * @param {Event} e - The event object.
	 */
	function visualeditorpropresizeMD ( e ) {
		var self = lvzwebdesktop(e.target);

		visualeditorpropresizeMD.m.s = self;
		visualeditorpropresizeMD.m.px = e.clientX;
		visualeditorpropresizeMD.m.py = e.clientY;
		visualeditorpropresizeMD.m.pw = self.props.propertiesWidth;

		document.addEventListener('mousemove',visualeditorpropresizeMM);
		document.addEventListener('mouseup',visualeditorpropresizeMU);
	}

	visualeditorpropresizeMD.m={};

	/**
	 * Function for resizing properties table on mouse move.
	 * @param {Event} e - The event object.
	 */
	function visualeditorpropresizeMM ( e ) {
		var self = visualeditorpropresizeMD.m.s;
		self.props.propertiesWidth = visualeditorpropresizeMD.m.pw + (e.clientX - visualeditorpropresizeMD.m.px);
		if ( self.props.propertiesWidth < 0 ) self.props.propertiesWidth = 0;
		self.properties.elements.container.style.width = self.props.propertiesWidth + 'px';
		self.window.resize();
	}

	/**
	 * Function to end resize properties table on mousemove.
	 * @param {Event} e - The event object.
	 */
	function visualeditorpropresizeMU ( e ) {
		document.removeEventListener('mousemove',visualeditorpropresizeMM);
		document.removeEventListener('mouseup',visualeditorpropresizeMU);
	}

	/**
	 * Constructor for creating editor for visual editing windows.
	 * @constructor
	 * @param {HTMLElement} [e] - Element to attach visual editor to.
	 */
	var WindoweditorObject = function ( e ) {

		lvzwebdesktop.basicObject.call(this,'windoweditor');

		this.buildWindow = function () {

			this.editedWindow = new lvzwebdesktop.WindowObject(false,false);

			var vobj;
			var veobj;

			// this.visualEditor.addControl(this);

			for ( var i=0;i<this.items.length;i++) {
				this.editedWindow.elements.bodycontainer.appendChild(this.items[i]);
				vobj = lvzwebdesktop(this.items[i]);
				// veobj = new VisualcontrolObject (vobj!==false?vobj:this.items[i]);
			}

			this.elements.midcentercell.appendChild( this.editedWindow.elements.container);

			this.editedWindow.props.resizable = false;
			this.editedWindow.props.maximizable = false;
			this.editedWindow.props.minimizable = false;
			this.editedWindow.props.movable = false;
			this.editedWindow
				.setWidth( this.props.width )
				.setHeight( this.props.height )
				.buildTheme()
				.decorate()
				.setTitle( this.props.caption )
				.setLayout( this.props.buttonLayout+'' )
				.setTop ( 10 )
				.setLeft ( 10 )
				.addEvent('menu',function(e){e.preventDefault();})
				.addEvent('close',function(e){e.preventDefault();})
			;

			this.editedWindow.windowEditor = this;
			// this.editedWindow.elements.bodycontainer.addEventListener('click',windoweditorclickwindow);

			return this;
		};

		this.editedWindow = null;

		this.control = null;

		this.visualEditor = null;

		/**
		 * CSS classname of the container element.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.container = 'wd-window-editor-container';

		/**
		 * CSS classname of the top row container element.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.toprowcontainer = 'wd-top-row-container';

		/**
		 * CSS classname of the middle row container element.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.midrowcontainer = 'wd-mid-row-container';

		/**
		 * CSS classname of the bottom row container element.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.bottomrowcontainer = 'wd-bottom-row-container';

		/**
		 * CSS classname of the resize boxes.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.resizebox = 'wd-resizebox';

		/**
		 * CSS classname of the cell elements.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.editorcell = 'wd-cell';

		/**
		 * CSS classname of the top left resize box.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.topleftresizebox = 'wd-resize-topleft';

		/**
		 * CSS classname of the top center resize box.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.topcenterresizebox = 'wd-resize-topcenter';

		/**
		 * CSS classname of the top right resize box.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.toprightresizebox = 'wd-resize-topright';

		/**
		 * CSS classname of the middle left resize box.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.middleleftresizebox = 'wd-resize-midleft';

		/**
		 * CSS classname of the middle right resize box.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.middlerightresizebox = 'wd-resize-midright';

		/**
		 * CSS classname of the bottom left resize box.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.bottomleftresizebox = 'wd-resize-bottomleft';

		/**
		 * CSS classname of the bottom center resize box.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.bottomcenterresizebox = 'wd-resize-bottomcenter';

		/**
		 * CSS classname of the bottom right resize box.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.bottomrightresizebox = 'wd-resize-bottomright';

		this.items = [];
		if ( e ) for ( var i = 0; i < e.childNodes.length; i++ )
			this.items.push(e.childNodes[i]);

		/**
		 * Container element of the window editor.
		 * @public
		 * @type {HTMLDivElement}
		 */
		this.elements.container = lvzwebdesktop.createBasicElement.call(this,'div',e,[
			this.props.classNames.container
		]);

		/**
		 * Top row container.
		 * @public
		 * @type {HTMLDivElement}
		 */
		this.elements.toprowcontainer = lvzwebdesktop.createBasicElement.call(this,'div',undefined,[
			this.props.classNames.toprowcontainer
		]);
		this.elements.container.appendChild(this.elements.toprowcontainer);

		/**
		 * Top left cell element.
		 * @public
		 * @type {HTMLDivElement}
		 */
		this.elements.topleftcell = lvzwebdesktop.createBasicElement.call(this,'div',undefined,[
			this.props.classNames.editorcell
		]);
		this.elements.toprowcontainer.appendChild(this.elements.topleftcell);

		/**
		 * Top center cell element.
		 * @public
		 * @type {HTMLDivElement}
		 */
		this.elements.topcentercell = lvzwebdesktop.createBasicElement.call(this,'div',undefined,[
			this.props.classNames.editorcell
		]);
		this.elements.topcentercell.style.textAlign = 'center';
		this.elements.toprowcontainer.appendChild(this.elements.topcentercell);

		/**
		 * Top right cell element.
		 * @public
		 * @type {HTMLDivElement}
		 */
		this.elements.toprightcell = lvzwebdesktop.createBasicElement.call(this,'div',undefined,[
			this.props.classNames.editorcell
		]);
		this.elements.toprowcontainer.appendChild(this.elements.toprightcell);

		/**
		 * Middle row container.
		 * @public
		 * @type {HTMLDivElement}
		 */
		this.elements.midrowcontainer = lvzwebdesktop.createBasicElement.call(this,'div',undefined,[
			this.props.classNames.midrowcontainer
		]);
		this.elements.container.appendChild(this.elements.midrowcontainer);

		/**
		 * Middle left cell element.
		 * @public
		 * @type {HTMLDivElement}
		 */
		this.elements.midleftcell = lvzwebdesktop.createBasicElement.call(this,'div',undefined,[
			this.props.classNames.editorcell
		]);
		this.elements.midrowcontainer.appendChild(this.elements.midleftcell);

		/**
		 * Middle center cell element.
		 * @public
		 * @type {HTMLDivElement}
		 */
		this.elements.midcentercell = lvzwebdesktop.createBasicElement.call(this,'div',undefined,[
			this.props.classNames.editorcell
		]);
		this.elements.midrowcontainer.appendChild(this.elements.midcentercell);

		if ( lvzwebdesktop.constructorsReady === true ) this.buildWindow();

		/**
		 * Middle right cell element.
		 * @public
		 * @type {HTMLDivElement}
		 */
		this.elements.midrightcell = lvzwebdesktop.createBasicElement.call(this,'div',undefined,[
			this.props.classNames.editorcell
		]);
		this.elements.midrowcontainer.appendChild(this.elements.midrightcell);

		/**
		 * Bottom row container.
		 * @public
		 * @type {HTMLDivElement}
		 */
		this.elements.bottomrowcontainer = lvzwebdesktop.createBasicElement.call(this,'div',undefined,[
			this.props.classNames.bottomrowcontainer
		]);
		this.elements.container.appendChild(this.elements.bottomrowcontainer);

		/**
		 * Bottom left cell element.
		 * @public
		 * @type {HTMLDivElement}
		 */
		this.elements.bottomleftcell = lvzwebdesktop.createBasicElement.call(this,'div',undefined,[
			this.props.classNames.editorcell
		]);
		this.elements.bottomrowcontainer.appendChild(this.elements.bottomleftcell);

		/**
		 * Bottom center cell element.
		 * @public
		 * @type {HTMLDivElement}
		 */
		this.elements.bottomcentercell = lvzwebdesktop.createBasicElement.call(this,'div',undefined,[
			this.props.classNames.editorcell
		]);
		this.elements.bottomcentercell.style.textAlign = 'center';
		this.elements.bottomrowcontainer.appendChild(this.elements.bottomcentercell);

		/**
		 * Bottom right cell element.
		 * @public
		 * @type {HTMLDivElement}
		 */
		this.elements.bottomrightcell = lvzwebdesktop.createBasicElement.call(this,'div',undefined,[
			this.props.classNames.editorcell
		]);
		this.elements.bottomrowcontainer.appendChild(this.elements.bottomrightcell);

		/**
		 * Top left resize box element.
		 * @public
		 * @type {HTMLDivElement}
		 */
		this.elements.topleftresizebox = lvzwebdesktop.createBasicElement.call(this,'div',undefined,[
			this.props.classNames.resizebox,
			this.props.classNames.topleftresizebox
		]);
		this.elements.topleftcell.appendChild(this.elements.topleftresizebox);

		/**
		 * Top center resize box element.
		 * @public
		 * @type {HTMLDivElement}
		 */
		this.elements.topcenterresizebox = lvzwebdesktop.createBasicElement.call(this,'div',undefined,[
			this.props.classNames.resizebox,
			this.props.classNames.topcenterresizebox
		]);
		this.elements.topcentercell.appendChild(this.elements.topcenterresizebox);

		/**
		 * Top right resize box element.
		 * @public
		 * @type {HTMLDivElement}
		 */
		this.elements.toprightresizebox = lvzwebdesktop.createBasicElement.call(this,'div',undefined,[
			this.props.classNames.resizebox,
			this.props.classNames.toprightresizebox
		]);
		this.elements.toprightcell.appendChild(this.elements.toprightresizebox);

		/**
		 * Middle left resize box element.
		 * @public
		 * @type {HTMLDivElement}
		 */
		this.elements.middleleftresizebox = lvzwebdesktop.createBasicElement.call(this,'div',undefined,[
			this.props.classNames.resizebox,
			this.props.classNames.middleleftresizebox
		]);
		this.elements.midleftcell.appendChild(this.elements.middleleftresizebox);

		/**
		 * Middle right resize box element.
		 * @public
		 * @type {HTMLDivElement}
		 */
		this.elements.middlerightresizebox = lvzwebdesktop.createBasicElement.call(this,'div',undefined,[
			this.props.classNames.resizebox,
			this.props.classNames.middlerightresizebox
		]);
		this.elements.middlerightresizebox.dataDirection = 'right';
		this.elements.middlerightresizebox.addEventListener('mousedown', windoweditorresizeboxmdown);
		this.elements.midrightcell.appendChild(this.elements.middlerightresizebox);

		/**
		 * Bottom left resize box element.
		 * @public
		 * @type {HTMLDivElement}
		 */
		this.elements.bottomleftresizebox = lvzwebdesktop.createBasicElement.call(this,'div',undefined,[
			this.props.classNames.resizebox,
			this.props.classNames.bottomleftresizebox
		]);
		this.elements.bottomleftcell.appendChild(this.elements.bottomleftresizebox);

		/**
		 * Bottom center resize box element.
		 * @public
		 * @type {HTMLDivElement}
		 */
		this.elements.bottomcenterresizebox = lvzwebdesktop.createBasicElement.call(this,'div',undefined,[
			this.props.classNames.resizebox,
			this.props.classNames.bottomcenterresizebox
		]);
		this.elements.bottomcenterresizebox.dataDirection = 'down';
		this.elements.bottomcenterresizebox.addEventListener('mousedown', windoweditorresizeboxmdown);
		this.elements.bottomcentercell.appendChild(this.elements.bottomcenterresizebox);

		/**
		 * Bottom right resize box element.
		 * @public
		 * @type {HTMLDivElement}
		 */
		this.elements.bottomrightresizebox = lvzwebdesktop.createBasicElement.call(this,'div',undefined,[
			this.props.classNames.resizebox,
			this.props.classNames.bottomrightresizebox
		]);
		this.elements.bottomrightresizebox.dataDirection = 'down right';
		this.elements.bottomrightresizebox.addEventListener('mousedown', windoweditorresizeboxmdown);
		this.elements.bottomrightcell.appendChild(this.elements.bottomrightresizebox);

		this.buildWindow();

	};

	/**
	 * List of all WindoweditorObject instances.
	 * @public
	 * @type {Array}
	 */
	WindoweditorObject.list = [];

	function windoweditorresizeboxmdown ( e ) {
	}

	/**
	 * Constructor for creating control editor objects.
	 * @public
	 * @param {Object} wdobject - A web-desktop object for creating .
	 */
	var VisualcontrolObject = function ( o ) {

		lvzwebdesktop.basicObject(this,'visualcontrol');

		o.fireEvent('visualeditorstart',{
			"target" : o,
			"editorObject" : this
		});

		/**
		 * The lvzwebdesktop object that will be edited.
		 * @public
		 * @type {Object}
		 */
		this.control = o;
		this.control.visualcontrol = this;

		this.visualEditor = null;

		this.windowEditor = null;

		/**
		 * CSS class name of the container element of the visual control.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.container = 'wd-visual-control';

		/**
		 * CSS class name of the vail element of the visual control.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.vail = 'wd-vc-vail';

		/**
		 * CSS class name of the container element of the selected visualcontrol object.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.selected = 'selected';

		/**
		 * Container element of the visual control object.
		 * @public
		 * @type {HTMLDivElement}
		 */
		this.elements.container = lvzwebdesktop.createBasicElement.call(this,'div',undefined,[
			this.props.classNames.container
		]);
		this.elements.container.tabIndex = 1;
		this.elements.container.title = this.control.type;
		this.elements.container.addEventListener('mousedown',visualcontrolMD);

		/**
		 * Vail element of the visual control object.
		 * @public
		 * @type {HTMLDivElement}
		 */
		this.elements.vail = lvzwebdesktop.createBasicElement.call(this,'div',undefined,[
			this.props.classNames.vail
		]);
		this.elements.vail.style.position = 'absolute';

	};

	/**
	 * List with all VisualcontrolObject instances.
	 * @public
	 * @type {Array}
	 */
	VisualcontrolObject.list = [];

	window.lvzwebdesktop.VisualeditorObject = VisualeditorObject;
	window.lvzwebdesktop.constructors.push({
		"selector" : ".visual-editor",
		"constructor" : VisualeditorObject
	});

	window.lvzwebdesktop.WindoweditorObject = WindoweditorObject;
	window.lvzwebdesktop.constructors.push({
		"selector" : ".wd-window-editor-container",
		"constructor" : WindoweditorObject
	});

	window.lvzwebdesktop.VisualcontrolObject = VisualcontrolObject;
	window.lvzwebdesktop.constructors.push({
		"selector" : false,
		"constructor" : VisualcontrolObject
	});

}
