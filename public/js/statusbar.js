{
	document.currentScript.dataLVZWLIB = true;

	/**
	 * Constructor for creating statusbar objects.
	 * @constructor
	 * @param {HTMLElement} [e] - Element to attach statusbar to.
	 */
	var StatusbarObject = function ( e ) {

		lvzwebdesktop.basicObject.call(this, 'statusbar');

		/**
		 * The window object that the statusbar belongs to or null.
		 * @public
		 * @type {WindowObject|null}
		 */
		this.window = null;

		/**
		 * CSS Class name of statubar container element.
		 * @public
		 * @type {String}
		 */
		this.props.classNames.container = 'web-desktop-statusbar';

		/**
		 * Method for attaching this object to another lvzwebdesktop object or an element. Fires "attachto" event.
		 *
		 * @param {HTMLElement|object} item - The lvzwebdesktop or the element to attach this object to.
		 * @param {'top'|'left'|'right'|'bottom'|'center'} [pos] - A string telling the position to attach this object to.
		 * @return {false|StatusbarObject} false on error, otherwise the current statusbar object.
		 */
		this.attachTo = function ( item, pos ) {
			var lvzo = null;

			// If pos not set, set default position to bottom 
			if ( typeof pos == 'undefined' || ! pos ) pos = 'bottom';

			if ( item instanceof HTMLElement ) {
				if ( typeof item.dataLVZWDESKTOPOBJ == 'object' ) {
					lvzo = item.dataLVZWDESKTOPOBJ;
					if ( typeof lvzo.attachItem == 'function' ) {
						lvzo.attachItem(this.elements.container, pos);
					}
					else {
						console.error ( 'Argument must be an lvzwebdesktop object having "attachItem" method.' );
						return false;
					}
				}
				else {
					var eobj = {
						"target" : this,
						"attachElement" : item,
						"item" : item,
						"position" : pos
					};
					this.fireEvent('attachto',eobj);
					if ( eobj.defaultPrevented ) return this;

					item.appendChild ( this.elements.container );
				}
			}
			else {
				var c = lvzwebdesktop.isPartOf(item);
				if ( c === false || typeof item.attachItem != 'function' ) {
					console.error ( ' Argument must be an element or an lvzwebdesktop object. If it is an lvzwebdesktop must having the "attachItem" method.' );
					return false;
				}
				item.attachItem ( this, pos );
			}
			return this;
		};

		/**
		 * Method for attaching the statusbar to a window.
		 *
		 * @param {WindowObject} w - The window object to attach this statusbar.
		 * @return {StatusbarObject} This statusbar object.
		 */
		this.attachToWindow = function ( w ) {

			if ( typeof w.statusbar == 'object'
				 && w.statusbar !== null ) return this;

			w.statusbar = this;
			this.window = w;

			// Add statusbar container after window body.
			w.elements.bodycontainer.appendChild ( this.elements.container );
			w.attachItem ( this, 'bottom' );
		};

		/**
		 * Method for adding items to statusbar.
		 *
		 * @param {HTMLElement|LVZWEBOBJECT} e - The item to add to the statusbar.
		 * @return {StatusbarObject} This statusbar object.
		 */
		this.addItem = function ( e ) {
			if ( typeof e == 'object') {
				if ( e instanceof HTMLElement ) {
					this.elements.container.appendChild ( e ) ;
				}
			}
			return this;
		};

		/**
		 * Container element.
		 * @public
		 * @type {HTMLElement}
		 */
		this.elements.container = null;

		if ( e ) {

			this.elements.container = e;
			this.props.elementId = !e.id ? false : e.id;

			var w = lvzwebdesktop(e,false).getParentObject('WindowObject');
			if ( typeof w == 'object' && w !== null ) {
				w.statusbar = this;
			}

		}
		else {

			this.elements.container = document.createElement('div');

		}

		attr = getComputedStyle(document.body).getPropertyValue('--lvzwebdesktop-default-statusbar-theme').trim();

		if ( attr != '' ) {

			for ( var i = 0 ; i < this.elements.container.classList.length ; i ++ ) {
				if ( getComputedStyle(document.body).getPropertyValue('--lvzwebdesktop-theme-statusbar-'+this.elements.container.classList[i]).trim() == 'true' ) {
					this.props.themeClass = this.elements.container.classList[i];
					break;
				}
			}
			if ( this.props.themeClass === null ) {
				this.props.themeClass = attr;
				this.elements.container.classList.add(attr);
			}
		}
		else {
			console.error ( "There is no theme loaded" );
		}

		this.elements.container.classList.add( this.props.classNames.container );

		this.elements.container.dataStatusbarObject = this;
		this.elements.container.dataLVZWDESKTOPOBJ = this;

	};

	if ( lvzwebdesktop.constructorsReady === false ) {

		lvzwebdesktop.fn.addEvent.call ( lvzwebdesktop, 'constructorsready', function ( e ) {
			var self = null;
			for ( var i =0; i < e.target.StatusbarObject.list.length; i ++ ) {
				self = e.target.StatusbarObject.list[i];
				var w = self.getParentOfType('WindowObject');

				if ( typeof w == 'object' && w instanceof lvzwebdesktop.WindowObject ) {
					if ( self.elements.container.classList.contains('top-orientation'))
						w.attachItem ( self, 'top' );
					else if ( self.elements.container.classList.contains('left-orientation'))
						w.attachItem ( self, 'left' );
					else if ( self.elements.container.classList.contains('right-orientation'))
						w.attachItem ( self, 'right' );
					else
						w.attachItem ( self, 'bottom' );

				}
				w.statusbar = self;
			}
				  
		});
	}

	StatusbarObject.list = [];

	window.lvzwebdesktop.StatusbarObject = StatusbarObject;
	window.lvzwebdesktop.constructors.push({
		"selector" : ".web-desktop-statusbar,web-statusbar",
		"constructor" : StatusbarObject
	});

}
