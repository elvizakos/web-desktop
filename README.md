# web-desktop #

[![License](https://img.shields.io/:license-gpl3-blue.svg)](./LICENSE)

web-desktop is list of libraries written in JavaScript that enables you to build a web desktop system that matches your needs.

Demo at https://elvizakos.gitlab.io/web-desktop/
